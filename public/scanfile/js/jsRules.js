console.info("jsRule.js loaded!");

/*
// #3 JavaScript Explicit Bindng Rule
function foo() {
    console.log(this.bar);
}

console.info("#3 JavaScript Explict it Bindng Rule");
var bar = "bar1";
var obj = { bar: "bar2" };

foo();
foo.call(obj);
*/

/*
// #4 JavaScript Hard Bindng Rule
function foo() {
    console.log(this.bar);
}

console.info("#4 JavaScript Hard it Bindng Rule");
var obj = { bar: "bar" };
var obj2 = { bar: "bar2" };

var orig = foo;
foo = function() { orig.call(obj); };

foo();
foo.call(obj2);
*/

/*
// #4 JavaScript Hard Bindng Rule - Utility
function bind(fn, o) {
    return function() {
        fn.call(o);
    };
}

function foo() {
    console.log(this.bar);
}

console.info("#4 JavaScript Hard it Bindng Rule - Utility");
var obj = { bar: "bar" };
var obj2 = { bar: "bar2" };

foo = bind(foo, obj);

foo();
foo.call(obj2);
*/

/*
// #5 JavaScript New Keyword 
console.info("#5 JavaScript New Keyword");

function foo() {
    this.baz = "baz";
    console.log(this.baz + " " + baz); // prints undefined undefined
}

var bar = "bar";
var baz = new foo();

/**  What is happening here?
 * #1 - A brand new object is being created
 * #2 - the new object is linked to an empty object that is also created *
 * #3 - the new object is market as the "this" object for that function call
 * #4 - it will implisity insert a return is the function does not return anything
 * */

/** How to check for the value of 'this'
 * 1 - Was the function called with 'new'?
 * 2 - Was the function called with 'call' or 'apply' specifyin an explicit 'this'?
 * 3 - Was the function called via a containing / owning object (context)?
 * 4 - DEFAULT: global object (except strict mode)
 */

/*
// Closures 
console.info("Closures");

function foo() {
    var bar = "bar";

    function baz() {
        console.log(bar);
    }
    bam(baz);
}

function bam(baz) {
    baz();
}

foo();
*/

/*
// Other Examples of Closures
console.info("other example of closures 1");

function foo() {
    var bar = "bar";

    return function() {
        console.log(bar);
    }
}

function bam() {
    foo()();
}

bam();
*/

/*
// Other Examples of Closures 2
console.info("other example of closures 2");

function foo() {
    bar = 0;

    setTimeout(function() {
        console.log(bar++);
    }, 100);

    setTimeout(function() {
        console.log(bar++);
    }, 200);

}

foo();
*/

/*
// Closures - Nested Scope
console.info("Closures - Nested Scope");

function foo() {
    var bar = 0;

    setTimeout(function() {
        var baz = 1;
        console.log(bar++);

        setTimeout(function() {
            console.log(bar + baz);
        }, 200);
    }, 100);
}

foo();
*/

/*
// Closures - Loops - it will prints out i:6 six times
console.info("Closures - Loops");
for (var i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log("i:" + i);
    }, i * 1000);
}
*/

/*
// Closures - Loops - it will prints out i:6 six times - fixed with an IIFE
console.info("Closures - Loops - fixed with an IIFE");
for (var i = 1; i <= 5; i++) {
    (function(i) {
        setTimeout(function() {
            console.log("i:" + i);
        }, i * 1000);

    })(i);
}
*/

/*
// Closures - Loops - it will prints out i:6 six times - using let ES6
console.info("Closures - Loops - fixed using \"let\" ES6");
for (let i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log("i:" + i);
    }, i * 1000);

}
*/

/*
// Closures - Closure? - it's not because there is not a function port it out
console.info("Closure ");
var foo = (function() {

    var o = { bar: "bar" };
    return { obj: o };

})();

console.log(foo.obj.bar);
*/

/*
// Closures - The Classic module pattern 
console.info("Closures - The Classic module pattern ");
var foo = (function() {
    var o = { bar: "bar " };

    return {
        bar: function() {
            console.log(o.bar);
        }
    }
})();

foo.bar();
*/

/*
// Closures - The Classic module pattern modified
console.info("Closures - The Classic module pattern modified");

var foo = (function() {
    var publicAPI = {
        bar: function() {
            publicAPI.baz();
        },
        baz: function() {
            console.log("baz");
        }
    }
    return publicAPI;
})();

foo.bar(); // baz
*/

/*
// Closures - Modern module pattern
console.info("Modern module pattern");

define("foo", function() {

    var o = { bar: "bar " };

    return {
        bar: function() {
            console.log(o.bar);
        }
    }
});
*/

/*
// ES6+ - Module pattern
console.log("ES6+ module pattern");
// You create a new file and that is the name of your function caller for exmaple foo.js
var o = { bar: "bar" };
export function bar() {
    return o.bar;
}

// The way you load this file in your project is using one of these two methods.
// #1
import bar from "foo"; // this only loads the function bar from the file foo.js
bar();

//#2 
module foo from "foo"; // this import all the functions inside our module but the call is different / check if stil is valid
foo.bar();
*/