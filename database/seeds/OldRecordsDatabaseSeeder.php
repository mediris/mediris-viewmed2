<?php

use Illuminate\Database\Seeder;
use App\Oldrecordsdatabase;

class OldRecordsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Oldrecordsdatabase::create(['puerto'=>3306,'host'=>'localhost','base'=>'veris2']);
    }
}
