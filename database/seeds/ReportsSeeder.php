<?php

use Illuminate\Database\Seeder;
use App\Report;

class ReportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $report = new Report();
        $report->name = 'Birads';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();
        
        $report = new Report();
        $report->name = 'Listado-Ordenes';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Honorario-Radiologo';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->role_id = '3';
        $report->save();

        $report = new Report();
        $report->name = 'Honorario-Tecnico';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->role_id = '4';
        $report->save();

        $report = new Report();
        $report->name = 'Honorario-Transcriptor';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->role_id = '5';
        $report->save();

        $report = new Report();
        $report->name = 'Listado-Citas';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Ordenes-Por-Aprobar';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Ordenes-Por-Dictar';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Ordenes-Por-Transcribir';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Ordenes-Aprobadas';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Ordenes-Dictadas';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Ordenes-Realizadas';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Ordenes-Transcritas';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Paciente-Por-Genero';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Eficiencia-Ordenes-Terminadas';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();

        $report = new Report();
        $report->name = 'Transcripcion';
        $report->extension = 'jrxml';
        $report->subreport = '0';
        $report->save();
        
    }
}
