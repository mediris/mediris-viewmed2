<?php
use Illuminate\Database\Seeder;
use App\Patient;
use App\Appointment;
use Carbon\Carbon;
use App\ServiceRequest;
use App\Http\Controllers\ServiceRequestsController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Faker\Factory as Faker;
use App\Oldrecordsarchivosescaneados;
use App\Http\Requests\PatientAddRequest;




class DataTestSeeder extends Seeder
{
    private $inst;
    private function Abdominal_Circumference() {
        $faker = Faker::create('es_VE');
        return $faker->randomFloat(2, 0.30, 1);

    }
    private function Weight() {
        $faker = Faker::create('es_VE');
        return $faker->randomFloat(0, 40, 100);

    }
    private function Height() {
        $faker = Faker::create('es_VE');
        return $faker->randomFloat(2, 1, 1.90);

    }

    private function getSMtime() {
        //return time() + (microtime( true ) / 1000000);
        return microtime(true);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */



    public function run()
    {

        $this->call(InstitutionsSeeder::class);
        $this->call(UsersTestSeeder::class);
        $this->call(ChargeSeeder::class);
        //$this->call(ModalityUserSeeder::class); no se ejecuta
        $this->call(ResponsablesSeeder::class);
        $this->call(PatientsSeeder::class);
        $this->call(OldRecordsDatabaseSeeder::class);

        $this->inst = \App\Institution::all()->first();
        Session()->set('institution',$this->inst);

        $this->patients    = Patient::all();
        $this->procedures  = \App\Procedure::remoteFindAll();
        $this->sources     = \App\Source::remoteFindAll();
        $this->platesSizes = \App\PlatesSize::remoteFindAll();
        
        $start = $this->getSMtime();
        
        $this->SeederAppoint();
        
        $end = $this->getSMtime();
        echo "Time elapsed SeederAppoint: " . ($end - $start) . " seconds.\n";
        $start = $this->getSMtime();

        for ($x=1;$x<=150;$x++) {
            $this->SeederPreIn();
        }
        
        $end = $this->getSMtime();
        echo "Time elapsed SeederPreIn: " . ($end - $start) . " seconds.\n";
        $start = $this->getSMtime();

        //$cont = \App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id' => 2]); 
        //dump( count($cont) );
        //while(count( $cont )<120) {
        for ( $x=0; $x<120; $x++ ) {
            //dump( count($cont) );
            $this->SeederTec();
            //$cont = \App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id' => 2]);
        }

        $end = $this->getSMtime();
        echo "Time elapsed SeederTec: " . ($end - $start) . " seconds.\n";
        $start = $this->getSMtime();

        //while(count(\App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id' => 3]))<100){
        for ( $x=0; $x<100; $x++ ) {
            $this->SeederRadiologsetAsToDictate();
        }

        $end = $this->getSMtime();
        echo "Time elapsed SeederRadiologsetAsToDictate: " . ($end - $start) . " seconds.\n";
        $start = $this->getSMtime();

        //while(count(\App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id' => 4]))<50){
        for ( $x=0; $x<50; $x++ ) {
            $this->SeederToTranscriber();
        }

        $end = $this->getSMtime();
        echo "Time elapsed SeederToTranscriber: " . ($end - $start) . " seconds.\n";
        $start = $this->getSMtime();


        // while(count(\App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id' => 5]))<25){
        for ( $x=0; $x<25; $x++ ) {
            $this->SeederToAprobe();
        }

        $end = $this->getSMtime();
        echo "Time elapsed SeederToAprobe: " . ($end - $start) . " seconds.\n";
        $start = $this->getSMtime();

        // while(count(\App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id' => 6]))<10){
        for ( $x=0; $x<10; $x++ ) {
            $this->SeederRadiologsetAsFinished();
        }

        $end = $this->getSMtime();
        echo "Time elapsed SeederRadiologsetAsFinished: " . ($end - $start) . " seconds.\n";
        $start = $this->getSMtime();

        for($x=1;$x<=5;$x++){
            $this->SeederAdendum();
        }

        $end = $this->getSMtime();
        echo "Time elapsed SeederAdendum: " . ($end - $start) . " seconds.\n";
        $start = $this->getSMtime();
    }

    private  function SeederAppoint(){
        $count=0;
        $inst=\App\Institution::all();

        $params=[
            'start_hour'=>'07:00:00','end_hour'=>'18:00:00','block_size'=>'60','avoided_blocks'=>'1'
        ];
        $faker = Faker::create('es_VE');
        $faker->addProvider(new phonesVE($faker));
        //dd($params['rooms']);
        //return  ($expDate->addHour()->toTimeString()===$params['start_hour']) ?  'si':'no';
        foreach ($inst as $item) {
            try{
                $expDate = Carbon::now()->subDays(90);
                $expDate->setTimeFromTimeString($params['start_hour']);
                $salas=\App\Room::remoteFindAll([],$item);
                do {
                    do {
                        foreach ($salas as $sala) {
                            $proId = $sala->procedures[rand(0, count($sala->procedures) - 1)]->id;
                            $startDate = $expDate->toDateTimeString();
                            $endDate = Carbon::createFromDate()->setDate($expDate->year, $expDate->month, $expDate->day)->setTime($expDate->hour + 1, $expDate->minute, $expDate->second)->toDateTimeString();
                            if (!$expDate->isWeekend() and $expDate->toTimeString() != '12:00:00') {
                                $cita = new App\Appointment();
                                $patient = $this->patients->random(1);
                                $citaData = array();
                                $citaData = [
                                    'room_id' => $sala->id,
                                    'procedure_id' => $proId,
                                    'patient_identification_id' => $patient->patient_ID,
                                    'patient_email' => $patient->email,
                                    'patient_first_name' => $patient->first_name,
                                    'patient_last_name' => $patient->last_name,
                                    'patient_telephone_number' => $patient->telephone_number,
                                    'patient_cellphone_number' => $patient->cellphone_number,
                                    //'appointment_date_start' => '2017-10-26 07:00:00',
                                    'appointment_date_start' => $startDate,
                                    'appointment_date_end' => $endDate,
                                    'procedure_duration' => '60',
                                    'appointment_status_id' => ($expDate->toDateString() >=Carbon::now()->toDateString())?1:2,
                                    'string_start_unique' => str_replace('-', '/', $startDate) . '_' . $sala->id . '_' . 1,
                                    'string_end_unique' => str_replace('-', '/', $endDate) . '_' . $sala->id . '_' . 1,
                                    'equipment_id' => '1',
                                    'procedure_contrast_study' => '0',
                                    'patient_allergies_check' => '0',
                                    'patient_allergies' => '',
                                    'patient_weight' => $this->Weight(),
                                    'patient_height' => $this->Height(),
                                    'patient_abdominal_circumference' => $this->Abdominal_Circumference(),
                                    'patient_id' => $patient->id,
                                ];

                                if($count % 2 ==0){
                                    Appointment::remoteCreate($citaData, $item);
                                }

                                $count=$count+1;
                            }
                        }
                        $expDate->addHours(3);
                    } while (!($params['end_hour'] < $expDate->toTimeString() ? true : false));
                    $expDate->addDay();
                } while ($expDate < Carbon::now()->addDays(10));
            }catch (\Exception $e){
            }

        }
    return $count;

    }

    /**
     * Envia a preingreso requested_procedure_status=1
     */
    public function SeederPreIn(){
        $request = new \Illuminate\Http\Request();
        $request->setMethod('post');
        $patient = $this->patients->random();
        $sc = new ServiceRequestsController();
        $request['patient_ID'] = $patient->patient_ID;
        $request['patient_id'] = '';
        $request['patient'] = [
            'id' => $patient->id,
            'photo' => $patient->photo,
            'sex_id' => $patient->sex_id,
            'first_name' => $patient->first_name,
            'last_name' => $patient->last_name,
            'birth_date' => $patient->birth_date,
            'country_id' => $patient->country_id,
            'citizenship' => $patient->citizenship,
            'occupation' => $patient->occupation,
            'address' => $patient->address
        ];
        $request['email'] = $patient->email;
        $request['telephone_number'] = $patient->telephone_number;
        $request['cellphone_number'] = $patient->cellphone_number;
        $request['responsable'] = ['responsable_id' => '', 'name' => ''];
        $request['requestedProcedures'] = [
            'YELebYLYHLPM' => [
                'procedure_id' => $this->procedures->random()->id,
                'comments' => ''
            ]
        ];
        $request['weight'] =$this->Weight();
        $request['height'] = $this->Height();
        $request['patient_type_id'] ='11';
        $request['source_id'] = $this->sources->random()->id;
        $request['referring_id'] =1;
        $request['answer_id'] = '10';
        $results = $sc->add($request);

        /*
        if(is_a($results,Illuminate\Http\RedirectResponse::class)){
            dump($patient->patient_ID . '-> OK');
        }else{
            dump(false);
            break;
        };
        */
    }
    /**
     * Envia a Tecnico requested_procedure_status=2
     */
    public function SeederTec()
    {
        $request = new \Illuminate\Http\Request();
        $request->setMethod('post');

        $RequestedProcedure= \App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id'=>1])->random();

        $preAdm= new \App\Http\Controllers\PreAdmissionController();
        $results =$preAdm->approve($request,$RequestedProcedure->id);

        /*
        if(is_a($results,Illuminate\Http\RedirectResponse::class)){
            dump($requestProcedure->id . '-> OK');
        }else{
            dump(false);
            break;
        };
        */
    }

    /**
     * Envia a Radiologo por Dictar requested_procedure_status=3
     */
    public function SeederRadiologsetAsToDictate(){
        $faker = Faker::create();
        $request = new \Illuminate\Http\Request();
        $request->setMethod('post');
        $requestProcedure=null;
        $reqController= new \App\Http\Controllers\TechnicianController();
        $credentials['id']          = '1';
        $credentials['password']    = 'l0g0sc0rp';
        \Auth::once( $credentials );

        $plates= $this->platesSizes->random();
        $data = \App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id'=>2])->random();
        $data['text']=$faker->text;;
        $request['requestedProcedure'] = [
            'study_number' =>     '',
            'equipment_id' =>     $data->equipment_id,
            'culmination_date' => '',
            'force_images' =>     true,
        ];
        $reqController->edit( $request, $data->id );
    }

    /**
     * Envia a Transcriptor requested_procedure_status=4
     */
    public function SeederToTranscriber()
    {
        //simula la funcion del transcriptor
        $faker = Faker::create();
        $request = new \Illuminate\Http\Request();
        $request->setMethod('post');
        $requestProcedure = null;
        $reqController = new \App\Http\Controllers\RadiologistController();
        $credentials['id'] = '1';
        $credentials['password'] = 'l0g0sc0rp';
        $request['DataTest'] = true;
        \Auth::once($credentials);
        
        $plates = $this->platesSizes->random();
        $data = \App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id' => 3])->random();
        $request['requestedProcedure'] = [
            'number_of_plates' => 1,
            'study_number' => '',
            'plates_size_id' => $plates->size,
            'equipment_id' => $data->equipment_id,
        ];
        $request['text'] = $faker->text;
        $reqController->edit($request, $data->id);
        //$Radiologo = new \App\Http\Controllers\RadiologistController();
        //$Radiologo->addendum($request, $data->id);
    }

    /**
     * Envia a Radiologo por Aprobar requested_procedure_status=5
     */
    public function SeederToAprobe(){
        //simula la funcion del transcriptor
        $faker = Faker::create();
        $request = new \Illuminate\Http\Request();
        $request->setMethod('post');
        $requestProcedure=null;
        $reqController= new \App\Http\Controllers\TranscriberController();
        $credentials['id']          = '1';
        $credentials['password']    = 'l0g0sc0rp';
        \Auth::once( $credentials );
    
        $plates= $this->platesSizes->random();
        $data = \App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id'=>4])->random();
        $request['text']=$faker->text;
        $request['requestedProcedure'
        ]=[
            'number_of_plates'=>1,
            'study_number'=>'',
            'plates_size_id'=>$plates->size,
            'equipment_id'=>$data->equipment_id,
        ];
        $reqController->edit($request,$data->id);
    }

    /**
     * Genera un adendum
     */
    public function SeederAdendum(){

        $faker = Faker::create();
        $request = new \Illuminate\Http\Request();
        $request->setMethod('post');
        $requestProcedure=null;
        $reqController= new \App\Http\Controllers\RadiologistController();
        $credentials['id']          = '1';
        $credentials['password']    = 'l0g0sc0rp';
        \Auth::once( $credentials );
        
        $plates= $this->platesSizes->random();
        $data = \App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id'=>6])->random();
        $request['text']=$faker->text;
        $request['requestedProcedure'
        ]=[
            'number_of_plates'=>1,
            'study_number'=>'',
            'plates_size_id'=>$plates->size,
            'equipment_id'=>$data->equipment_id,
        ];
        $reqController->addendum($request,$data->id);
    }

    /**
     * Envia aprobada requested_procedure_status=6
     */
    public function SeederRadiologsetAsFinished(){
        //Envia a Aprobado
        $faker = Faker::create();
        $request = new \Illuminate\Http\Request();
        $request->setMethod('post');
        $requestProcedure=null;
        $reqController= new \App\Http\Controllers\RadiologistController();
        $credentials['id']          = '1';
        $credentials['password']    = 'l0g0sc0rp';
        $request['DataTest']=true;
        $request['approve']='true';
        \Auth::once( $credentials );
        
        $plates= $this->platesSizes->random();
        $data = \App\RequestedProcedure::remoteFindAll(['requested_procedure_status_id'=>5])->random();
        $request['requestedProcedure'
        ]=[
            'number_of_plates'=>1,
            'study_number'=>'',
            'plates_size_id'=>$plates->size,
            'equipment_id'=>$data->equipment_id,
        ];
        $request['text']=$faker->text;
        $reqController->edit($request,$data->id);
        $Radiologo= new \App\Http\Controllers\RadiologistController();
        $Radiologo->addendum($request,$data->id);
    }
}
