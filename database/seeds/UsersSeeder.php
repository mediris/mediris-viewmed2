<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'last_name' => 'Desarrollo',
            'first_name' => 'Desarrollo',
            'administrative_ID' => 'LOGOSCORP_ADMIN',
            'username' => 'Admin',
            'password' => bcrypt('l0g0sc0rp'),
            'telephone_number' => '2120000000',
            'cellphone_number' => '4120000000',
            'email' => 'desarrollo@logoscorp.com',
            'active' => 1,
            'suffix_id' => 26,
            'prefix_id' => 4,
            'signature' => '',
            'api_token' => '0LMLE6EPYKON6X5eXxgVtWBQ5P3ST6ku9mWM50U3kfs2cauqmP82luHtY9kA',
        ]);

        User::create([
            'last_name' => 'api',
            'first_name' => 'api',
            'administrative_ID' => 'LOGOSCORP_API',
            'username' => 'api',
            'password' => bcrypt('ap1_l0g0sc0rp'),
            'telephone_number' => '2120000000',
            'cellphone_number' => '4120000000',
            'email' => 'api@logoscorp.com',
            'active' => 1,
            'suffix_id' => 26,
            'prefix_id' => 4,
            'signature' => '',
            'api_token' => 'B0l1E6EPYKON6X5eXxgVtWBQ5P3ST6ku9mWM50U3kfs2cauqmP82luHtYJCH',
        ]);
    }
}
