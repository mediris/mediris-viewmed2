<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'last_name' => 'Test',
            'first_name' => 'Meditron',
            'administrative_ID' => 'MEDITRON_ADMIN',
            'username' => 'meditron',
            'password' => bcrypt('032017'),
            'telephone_number' => '2126375432',
            'cellphone_number' => '4126375432',
            'email' => 'meditron@logoscorp.com',
            'active' => 1,
            'suffix_id' => 26,
            'prefix_id' => 4,
            'signature' => '',
            'api_token' => 'zBDKzd78bl3qr6ullV0xEQQ5SH6Qguak5CwJSm4QepNAHWOjq4Rirt7PlBNS',
        ]);

        User::create([
            'last_name' => 'Default',
            'first_name' => 'Radiologo',
            'administrative_ID' => 'LOGOSCORP_RAD',
            'username' => 'rad',
            'password' => bcrypt('rad'),
            'telephone_number' => '2126375432',
            'cellphone_number' => '4126375432',
            'email' => 'rad@logoscorp.com',
            'active' => 1,
            'suffix_id' => 26,
            'prefix_id' => 4,
            'signature' => '',
            'api_token' => 'zBDKzd78bl3qr6ullV0xEQQ5SH6Qguak5CwJSm4QepNAHWOjq4Rirt7PlBNA',
        ]);

        User::create([
            'last_name' => 'Default',
            'first_name' => 'Technician',
            'administrative_ID' => 'LOGOSCORP_TEC',
            'username' => 'tec',
            'password' => bcrypt('tec'),
            'telephone_number' => '2126375432',
            'cellphone_number' => '4126375432',
            'email' => 'tec@logoscorp.com',
            'active' => 1,
            'suffix_id' => 26,
            'prefix_id' => 4,
            'signature' => '',
            'api_token' => 'zBDKzd78bl3qr6ullV0xEQQ5SH6Qguak5CwJSm4QepNAHWOjq4Rirt7PlBNB',
        ]);

        User::create([
            'last_name' => 'Default',
            'first_name' => 'Transcriber',
            'administrative_ID' => 'LOGOSCORP_TRANS',
            'username' => 'trans',
            'password' => bcrypt('trans'),
            'telephone_number' => '2126375432',
            'cellphone_number' => '4126375432',
            'email' => 'trans@logoscorp.com',
            'active' => 1,
            'suffix_id' => 26,
            'prefix_id' => 4,
            'signature' => '',
            'api_token' => 'zBDKzd78bl3qr6ullV0xEQQ5SH6Qguak5CwJSm4QepNAHWOjq4Rirt7PlBNC',
        ]);
    }
}