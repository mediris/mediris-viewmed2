<?php

use Illuminate\Database\Seeder;
use App\Report;

class ReportParamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Report::find(1)->parameters()->attach([1,2,3,4,5,6]);
        
        Report::find(2)->parameters()->attach([1,2,3,4,5,7,10]);
        
        Report::find(3)->parameters()->attach([1,2,3,4,5,6]);
        Report::find(4)->parameters()->attach([1,2,3,4,5,6]);
        Report::find(5)->parameters()->attach([1,2,3,4,5,6]);
        
        Report::find(6)->parameters()->attach([1,2,3,4,8,9]);
        Report::find(7)->parameters()->attach([1,2,3,4,5]);
        Report::find(8)->parameters()->attach([1,2,3,4,5]);
        Report::find(9)->parameters()->attach([1,2,3,4,5]);

        Report::find(10)->parameters()->attach([1,2,3,4,5,6]);
        Report::find(11)->parameters()->attach([1,2,3,4,5,6]);
        Report::find(12)->parameters()->attach([1,2,3,4,5,6]);
        Report::find(13)->parameters()->attach([1,2,3,4,5,6]);

        Report::find(14)->parameters()->attach([1,2,3,4,5]);

        Report::find(15)->parameters()->attach([1,2]);

        Report::find(16)->parameters()->attach([1,2,6]);
    }
}
