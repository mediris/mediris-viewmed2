<?php

use Illuminate\Database\Seeder;
use App\Section;

class SectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($sections = null)
    {
        //'interview'
        //$roots = ['appointments', 'reception', 'preadmission','technician', 'radiologist', 'transcriber', 'results', 'search', 'patients', 'reports', 'administration'];

        //'patientStates'
       //$sections = ['categories', 'subcategories','configurations', 'consumables', 'divisions', 'equipment', 'orders', 'patientTypes', 'alertMessages', 'procedures', 'referrings', 'rooms','sources', 'steps', 'templates', 'units', 'notification_templates', 'suspend_reasons' ];
        /*if(is_null($sections)){
            $sections = ['categories', 'subcategories', 'orders', 'referrings', 'sources', 'templates'];
        }*/

        foreach ($sections as $section) {
            // Add
            Section::Create(array(
                'name'      => $section['value'],
                'parent_id' => $section['parent'],
                'active'    => $section['active'],
                'priority'  => $section['priority'],
            ));
        }

        // OLD WAY
        /*foreach($roots as $root) {
            Section::create([
                'name' => $root,
                'active' => 1
            ]);
        }
        //sections are childs of administration
        foreach($sections as $section) {
            Section::create([
                'name' => $section,
                'parent_id' => count($roots),
            ]);
        }*/

    }

}
