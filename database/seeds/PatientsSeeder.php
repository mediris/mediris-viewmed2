<?php

use Illuminate\Database\Seeder;
use App\Patient;
use Faker\Factory as Faker;


class phonesVE extends \Faker\Provider\Base {
    public function mobilePhoneVE() {
        $formats = [
            '0412#######',
            '0414#######',
            '0424#######',
            '0416#######',
            '0426#######',
        ];

        return static::numerify($this->generator->parse(static::randomElement($formats)));
    }

    public function localPhoneVE() {
        $formats = [
            '0212#######',
        ];

        return static::numerify($this->generator->parse(static::randomElement($formats)));
    }
}


class PatientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_VE');
        $faker->addProvider(new phonesVE($faker));

        for($i = 0; $i < 50; $i++)
        {
            Patient::create([
                'active'    => 1,
                'patient_ID' => rand(10000000, 28000000),
                'active' => 1,
                'last_name' => $faker->lastName(),
                'first_name' => $faker->firstName(),
                'name_suffix' => $faker->suffix(),
                'name_prefix' => $faker->title(),
                'birth_date' => $faker->dateTime(),
                'medical_alerts' => $faker->text(200),
                'allergies' => $faker->text(200),
                'contrast_allergy' => $faker->boolean(50),
                'additional_patient_history' => $faker->text(200),
                'special_needs' => $faker->text(200),
                'occupation' => $faker->text(200),
                'comments' => $faker->text(200),
                'address' => $faker->address(),
                'country_id' => 862,
                'telephone_number' => $faker->localPhoneVE(),
                //'telephone_number_2' => $faker->phoneNumber(),
                'cellphone_number' => $faker->mobilePhoneVE(),
                //'cellphone_number_2' => $faker->phoneNumber(),
                'email' => $faker->email(),
                'death_date' => $faker->dateTime(),
                'photo' => '',
                'citizenship' => 862,
                'sex_id' => 1,
                'blocked_status' => 0
            ]);
        }
    }
}
