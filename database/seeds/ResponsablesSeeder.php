<?php

use Illuminate\Database\Seeder;
use App\Responsable;

class ResponsablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        Responsable::create([
            'responsable_id' => '013697456',
            'name' => 'Responsable Test',
        ]);
    }
}
