<?php

use Illuminate\Database\Seeder;
use App\Suffix;

class SuffixesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Suffix::create([
            'name' => 'B.S.',
            'language' => 'en',
        ]);

        /*
         * Associate Level
         */

        Suffix::create([
            'name' => 'A.A.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'A.A.S.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'A.A.T.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'A.O.T.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'A.S.',
            'language' => 'en',
        ]);

        /*
         * Associate Level
         */

        Suffix::create([
            'name' => 'B.A.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.A.B.A.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.A.Com. ',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.A.E.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.Ag.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.Arch.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.B.A.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.C.E.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.Ch.E.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.D.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.E.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.E.E.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.F.A.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.In.Dsn.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.J.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.L.A.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B.M.Ed.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'B. Pharm.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'A.S.',
            'language' => 'en',
        ]);

        Suffix::create([
            'name' => 'Lcdo.',
            'language' => 'es',
        ]);

        Suffix::create([
            'name' => 'Ing.',
            'language' => 'es',
        ]);
    }
}
