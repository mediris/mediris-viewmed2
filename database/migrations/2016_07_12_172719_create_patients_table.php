<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('active');
            $table->string('patient_ID',11)->unique()->index()->nullable();
            $table->string('last_name', 70)->index();
            $table->string('first_name', 70)->index();
            $table->string('name_suffix', 10);
            $table->string('name_prefix', 10);
            $table->dateTime('birth_date');
            $table->string('medical_alerts', 250);
            $table->string('allergies', 250);
            $table->boolean('contrast_allergy');
            $table->string('additional_patient_history', 250);
            $table->string('special_needs', 250);
            $table->string('occupation', 25)->index();
            $table->string('comments', 250);
            $table->string('address', 250);
            $table->boolean('anonymous')->default(0);
            $table->string('reason', 250)->nullable();
            $table->integer('country_id')->nullable()->index()->unsigned();
            $table->string('telephone_number', 45)->index();
            $table->string('telephone_number_2', 45);
            $table->string('cellphone_number', 45)->index();
            $table->string('cellphone_number_2', 45);
            $table->string('email', 45)->index()->nullable();
            $table->dateTime('death_date')->nullable();
            $table->string('photo', 250);
            $table->string('citizenship', 45)->nullable();
            $table->integer('responsable_id')->index()->unsigned();
            $table->integer('sex_id')->nullable()->index()->unsigned();
            $table->boolean('blocked_status');
            $table->index(['first_name', 'last_name']);
            $table->boolean('from_appointment')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
