<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeyPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {

            $table->foreign('sex_id')->references('id')->on('sexes')->onDelete('restrict');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
            //$table->foreign('responsable_id')->references('id')->on('responsables')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {

            $table->dropForeign('patients_sex_id_foreign');
            $table->dropForeign('patients_country_id_foreign');
            //$table->dropForeign('patients_responsable_id_foreign');

        });
    }
}
