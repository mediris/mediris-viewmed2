<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysParameterReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parameter_report', function(Blueprint $table){
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('restrict');
            $table->foreign('parameter_id')->references('id')->on('parameters')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameter_report', function(Blueprint $table){
            $table->dropForeign(['report_id']);
            $table->dropForeign(['parameter_id']);
        });
    }
}
