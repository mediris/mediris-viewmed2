<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last_name', 25)->index();
            $table->string('first_name', 25)->index();
            $table->integer('suffix_id')->unsigned()->index()->nullable();
            $table->integer('prefix_id')->unsigned()->index();
            $table->string('administrative_ID', 45)->unique()->index();
            $table->boolean('active');
            $table->string('username', 25)->unique()->index();
            $table->string('password', 60);
            $table->string('username_pacs', 25);
            $table->string('password_pacs', 30);
            $table->string('position', 60);
            $table->string('telephone_number', 25)->index();
            $table->string('telephone_number_2', 25);
            $table->string('cellphone_number', 25)->index();
            $table->string('cellphone_number_2', 25);
            $table->string('email')->unique()->index();
            $table->string('signature', 250);
            $table->string('api_token', 60)->unique();
            $table->string('additional_information', 250);
            $table->index(['first_name', 'last_name']);
            $table->rememberToken();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
