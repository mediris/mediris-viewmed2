<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
            $table->foreign('suffix_id')->references('id')->on('suffixes')->onDelete('restrict');
            $table->foreign('prefix_id')->references('id')->on('prefixes')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropForeign('users_suffix_id_foreign');
            $table->dropForeign('users_prefix_id_foreign');
        });
    }
}
