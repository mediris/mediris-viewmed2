<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysActionSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_section', function($table){
            $table->foreign('action_id')->references('id')->on('actions')->onDelete('cascade');
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_section', function(Blueprint $table){
            $table->dropForeign('action_section_action_id_foreign');
            $table->dropForeign('action_section_section_id_foreign');
        });
    }
}
