<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldRecordsdatabasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_records_databases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('host',25);
            $table->integer('puerto');
            $table->string('base');
            $table->string('usuario',25);
            $table->longText('clave');
            $table->bigInteger('id_institucion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_records_databases');
    }
}
