<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Watson\Rememberable\Rememberable;

class Institution extends Model {
    use Rememberable;

    private $logo_path;
    
    public function __construct( array $attributes = [] ) {
        parent::__construct($attributes);

        $this->logo_path = config('constants.paths.institution_logo');
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'active',
        'url',
        'name',
        'address',
        'administrative_ID',
        'group_id',
        'telephone_number',
        'telephone_number_2',
        'institution_id',
        'email',
        //'logo_actives_report',
        'logo',
        'logo_email',
        'report_header',
        'report_footer',
        'url_pacs',
        'url_external_api',
        'base_structure'
    ];

    public function getLogoAttribute($value) {
        if ( $value ) {
            return env('APP_URL', '') . '/storage/' . $value;
        }
        return $value;
    }

    public function getLogoEmailAttribute($value) {
        if ( $value ) {
            return env('APP_URL', '') . '/storage/' . $value;
        }
        return $value;
    }

    public function getReportHeaderAttribute($value) {
        if ( $value ) {
            return env('APP_URL', '') . '/storage/' . $value;
        }
        return $value;
    }

    public function getReportFooterAttribute($value) {
        if ( $value ) {
            return env('APP_URL', '') . '/storage/' . $value;
        }
        return $value;
    }

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Muchos usuarios tienen muchos cargos.
     */
    public function charges() {
        return $this->hasMany(Charge::class);
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Institution pertenece a un Group.
     */
    public function group() {
        return $this->belongsTo(Group::class);
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }

    /**
     * @fecha: 09-02-2017
     * @programador: Mercedes Rodriguez
     * @objetivo: Relación: Un Institution tiene muchos AlertMessage.
     */
    public function alertMessages() {
        return $this->hasMany(AlertMessage::class);
    }

    public function saveLogo( $file, $typeLogo = 'logo' ) {
        if( $file != '' ) {
            //$file is relative to the public directory
            $file = public_path() . $file;
            //move the file to storage
            $filename = Storage::disk('public')->putFile( $this->logo_path, new File($file) );
            
            if ( $filename !== false ) {
                if ($typeLogo == 'email-img') {
                    $this->logo_email = $filename;
                } elseif ( $typeLogo == 'report-header' ) {
                    $this->report_header = $filename;
                } elseif ( $typeLogo == 'report-footer' ) {
                    $this->report_footer = $filename;
                } else {
                    $this->logo = $filename;
                }

                return true;    
            } else {
                return false;
            }            
        }
    }
}
