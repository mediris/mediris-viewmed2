<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class PatientType extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/patienttypes';
        parent::__construct();
    }

    /**
     * @param $url
     * @return Collection
     */
    public function getAllRoots( $url ) {
        $response = $this->client->request('POST', $url . $this->apibase . '/roots', [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);
        $patientTypes = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $patientType = new PatientType();

            foreach ( $element as $key => $value ) {
                $patientType->$key = $value;

            }

            $patientTypes->push($patientType);
        }

        return $patientTypes;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador de la
     *     instancia
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para eliminar una instancia de PatientType dado un identificador. NO ESTÁ EN USO
     */
    public function remove( $url, $id ) {
        $res = $this->client->request('POST', $url . $this->url . '/delete/' . $id, [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);

        return $res;
    }

    public function parentPatientType( $url, $id ) {
        $res = $this->client->request('POST', $url . $this->url . '/parent/' . $id, [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);
        $patientType = new PatientType();
        foreach ( json_decode($res->getBody()) as $key => $value ) {
            $patientType->$key = $value;
        }

        return $patientType;
    }

    public function childPatientTypes( $url, $id ) {
        $response = $this->client->request('POST', $url . $this->url . '/childs/' . $id, [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);

        $patientTypes = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $patientType = new PatientType();

            foreach ( $element as $key => $value ) {
                $patientType->$key = $value;
            }

            $patientTypes->push($patientType);
        }

        return $patientTypes;
    }

    /**
     * Hierarchical Array to sections in patienttypes views
     * @return array
     */
    public function getChildPatientTypes( $id = null, $url ) {
        $childs = [];

        foreach ( $this->childPatientTypes($url, $this->id) as $child ) {

            if ( $child->id == $id ) {
                $text = '<div class="ui-radio"><label class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-radio-off">' . $child->description . '</label><input type="radio" name="parent_id" value="' . $child->id . '" checked></div>';
            }
            else {
                $text = '<div class="ui-radio"><label class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-radio-off">' . $child->description . '</label><input type="radio" name="parent_id" value="' . $child->id . '"></div>';
            }

            if ( sizeof($child->childPatientTypes($url, $child->id)) > 0 ) {
                array_push($childs, [ 'text' => $text, 'items' => $child->getChildPatientTypes($id, $url) ]);
            }
            else {
                array_push($childs, [ 'text' => $text ]);
            }

        }

        return $childs;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $api_url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador del
     *     elemento a editar
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el valor de la columna active dado un identificador.
     */
    public function active( $url, $id ) {
        $res = $this->client->request('POST', $url . $this->apibase . '/active/' . $id, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);

        return $res;
    }

}
