<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model {

    /**
     * @fecha: 15-03-2017
     * @programador: Diego Oliveros
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'description',
        'type',
        'location',
    ];

    /**
     * @fecha: 15-03-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un Parametro esta en varios reportes.
     */
    public function reports() {
        return $this->belongsToMany(Report::class);
    }
}
