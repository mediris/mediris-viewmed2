<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class Result extends Model {
    public function __construct() {
        $this->client = new Client();
        $this->url = 'api/v1/results';
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
    }

    public function indexRemoteData( $url, $data ) {

        $response = $this->client->request('POST', $url . $this->url, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id, 'data' => $data ] ]);

        if ( isset( json_decode($response->getBody())->data ) ) {
            return json_decode($response->getBody())->data;
        }

        return json_decode($response->getBody());
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador del
     *     elemento a buscar
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener una instancia de RequestedProcedure dado un identificador.
     */
    public function get( $url, $id ) {
        $response = $this->client->request('POST', $url . $this->url . '/show/' . $id, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);

        $requestedProcedure = new RequestedProcedure();
        foreach ( json_decode($response->getBody()) as $key => $value ) {
            $requestedProcedure->$key = $value;
        }

        return $requestedProcedure;
    }
}
