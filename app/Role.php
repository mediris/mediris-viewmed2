<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Role extends Model {
    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Muchos usuarios tienen muchos cargos.
     */
    public function charges() {
        return $this->hasMany(Charge::class);
    }

    /**
     * @fecha: 28-03-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un Rol pertenece a varios Reportes.
     */
    public function reports() {
        return $this->hasMany(Report::class);
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Users tienen muchos Users.
     */
    public function users() {
        return $this->belongsToMany(User::class);
    }

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un cargo tiene muchas Secciones.
     */
    public function roleForSection() {
        return $this->belongsToMany(Section::class, 'role_action_section', 'role_id', 'action_section_id');
    }

    public function roleSection() {
        return $this->belongsToMany(Section::class, 'role_action_section', 'role_id', 'action_section_id');
    }

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un cargo tiene muchas Secciones.
     */
    public function hasAction( $action, $section ) {

        if (isset($this->checkAction($action, $section)[0]))
            return "checked";

    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }

    private function checkAction( $action, $section ) {
        return DB::table('role_action_section')
            ->select('role_action_section.action_section_id')
            ->join(DB::raw('(SELECT id FROM action_section WHERE section_id = ' . $section . ' AND action_id = ' . $action . ') AS permission'), function ( $join ) {
                $join->on('permission.id', '=', 'role_action_section.action_section_id');
            })
            ->where('role_id', $this->id)
            ->limit(1)
            ->get();
    }
}
