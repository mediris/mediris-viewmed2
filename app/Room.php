<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;
use Log;

class Room extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/rooms';
        parent::__construct();
    }

    
    /**
     * @param $url
     * @param $table
     * @param $column
     * @param $value
     * @param null $id
     * @return mixed
     * @description function valid de uniquenes
     */

    public function unique( $url, $table, $column, $value, $id = null ) {
        try {
            $data['api_token'] = \Auth::user()->api_token;
            $data['table'] = $table;
            $data['column'] = $column;
            $data['value'] = $value;
            if ( $id != null ) {
                $data['id'] = $id;
            }

            $res = $this->client->request('POST', $url . 'api/v1/unique', [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => $data ]);

            if ( $res->getStatusCode() == 200 ) {
                return json_decode($res->getBody());
            }
        } catch ( \Exception $e ) {
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: rooms. Action: add or edit');
            echo $e->getCode();
            echo '<br>';
            echo $e->getMessage();
        }
    }
}