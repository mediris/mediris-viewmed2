<?php

namespace App;

use App\ServiceRequestDocument;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use GuzzleHttp\Client;
use Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Session;

class ServiceRequest extends RemoteModel {

    public function __construct() {
        $this->apibase = 'api/v1/reception';
        $this->externalApilogPath = '/logs/reception/apicallserrors.log';
        parent::__construct();
    }

    /**
     * @fecha: 23-08-2017
     * @parametros:
     * @programador: Hendember Heras
     * @objetivo: Creates a serviceRequest. After it has been created, then we create the "study instance"
                for each requestedProcedure and write the worklist if necessary
     */
    public static function remoteCreate( array $data = [], $institution=null ) {
        $model = new static();
        //verify if all the uploaded documents are valid
        if ( $model->validUploadDocuments( $data['requestDocuments'] ) ) {
            //set status as created
            $data['request_status_id'] = 1;
            $model = parent::remoteCreate( $data );
            if ( $model ) {
                //upload the serviceRequest documents if any
                $model->uploadDocuments( $data['requestDocuments'] );

                //save the scanned documents if any
                if ( isset( $data['scanfile'] ) && $data['scanfile'] ) {
                    $model->uploadScanDocuments( $data['scanfile'] );
                }

                //Create de Study Instance and write the worklist if necessary
                foreach( $model->requested_procedures as $rp ) {
                    //we need to remote load the rp to also get the procedure and the patient
                    $rp = RequestedProcedure::remoteFind( $rp->id );
                    //create study instance
                    $rp->setStudyInstance( session()->get('institution')->base_structure );
                    
                    if( $rp->procedure->worklist ) {
                        $data['patient_birth_date'] = $rp->patient->birth_date;
                        $data['patient_sex']        = $rp->patient->sex->name;

                        $data['base']        = session()->get('institution')->base_structure;

                        $rp->writeWorklist( $data );
                    }
                }

                if(session()->all()['institution']->url_external_api){

                    $url = session()->all()['institution']->url_external_api . "/solicitud/" . session()->all()['institution']->administrative_id . '-' . $model->id;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    $result = curl_exec($ch);

                    curl_close($ch);
                }

                //make a request to the external api
                if ( Session::get('institution') ) {
                    try {
                        $monolog = new Logger('reception');
                        $monolog->pushHandler( new StreamHandler(storage_path() . $model->externalApilogPath) );

                        $url = Session::get('institution')->url_external_api;
                        if ( $url ) {
                            $httpClient = new Client();
                            $response = $httpClient->request(
                                'GET',
                                $url,
                                ['http_errors' => false]
                            );
                            
                            if( $response->getStatusCode() != 200 ) {
                                $monolog->alert(
                                    "\nOrder: " . $model->id .
                                    "\nHttp error code: " . $response->getStatusCode() .
                                    "\nHttp error message: " . $response->getReasonPhrase() .
                                    "\nResponse Body: " . (string)$response->getBody()
                                );
                            }
                        }
                    } catch( \Exception $e ) {
                        $monolog->alert(
                            "\nOrder: " . $model->id .
                            "\nError code: " . $e->getCode() .
                            "\nError message: " . $e->getMessage()
                        );
                    }
                }
            }
        }
        return $model;
    }

    /**
     * @fecha: 31-01-2017
     * @programador: Alton Bell Smythe
     * @objetivo: Updated a serviceRequest.
     */
    public static function remoteUpdate( $id, $attributes, $institution = null ) {
        $model = new static();
        //verify if all the uploaded documents are valid
        if ( ( !isset( $attributes['requestDocuments'] ) ) || 
             ( isset( $attributes['requestDocuments'] ) && $model->validUploadDocuments( $attributes['requestDocuments'] ) ) ) {
            $model = parent::remoteUpdate( $id, $attributes, $institution);
            if ( $model ) {
                //upload and/or delete the service request documents
                if( isset( $attributes['requestDocuments'] ) ) {
                    $model->uploadDocuments( $attributes['requestDocuments'] );
                }
            } 
        }
        return $model;
    }

    /**
     * @fecha: 23-08-2017
     * @parametros:
     * @programador: Hendember Heras
     * @objetivo: Verify if all the uploaded files are valid ()
     */
    public function validUploadDocuments( $docs ) {
        if ( $docs ) {
            foreach ( $docs as $doc ) {
                if ( $doc instanceOf UploadedFile ) {
                    if ( !$doc->isValid() ) {
                        $this->error = $doc->getError();
                        $this->message = $doc->getErrorMessage();
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @fecha: 23-08-2017
     * @parametros: Array of UploadedFile instances
     * @programador: Hendember Heras
     * @objetivo: Creates a register for each file in the API DB and save the file to the storage locally
     */
    public function uploadDocuments( $docs) {
        if ( $docs ) {
            $cont = 1;
            foreach ( $docs as $doc ) {
                if ( $doc instanceOf UploadedFile ) {
                    $data['type'] = $doc->getClientOriginalExtension();
                    $data['name'] = pathinfo($doc->getClientOriginalName(), PATHINFO_FILENAME) . '_' . date("Ymd") . '_' . date("His") . '_' . $cont++ . '.' . $doc->getClientOriginalExtension();
                    $data['description'] = 'request document';

                    $srd = ServiceRequestDocument::remoteUpdate( $this->id, $data );

                    if ( $srd ) {
                        $srd -> moveToStorage( $doc );    
                    }
                }
            }
        }
    }

    /**
    * @fecha: 16-12-2016
    * @parametros: array with the images in base64 format
    * @programador: Hendember Heras
    * @objetivo: Creates a register for each scanned document in the API DB and save the file to the storage locally
    */
    public function uploadScanDocuments($files) {
        $cont = 1;
        foreach ($files as $key => $imgbase64) {
            $data['type'] = 'png';
            $data['name'] = 'scanfile_'.$this->id.'_'.date("Ymd").'_'.date("His").'_'.$cont++.'.'.$data['type'];
            $data['description'] = 'scan document';
            
            $srd = ServiceRequestDocument::remoteUpdate( $this->id, $data );

            if ( $srd ) {
                $srd -> saveScanToStorage( $imgbase64 );    
            }
        }
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador de la
     *     instancia
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para eliminar una instancia de ServiceRequest dado un identificador. NO ESTÁ EN USO
     */
    public function remove( $url, $id ) {

        $res = $this->client->request('POST',
            $url . $this->url . '/delete/' . $id, [
                'headers' => $this->headers,
                'form_params' => [
                    'api_token' => \Auth::user()->api_token,
                    'user_id' => \Auth::user()->id,
                ],
            ]);

        return $res;
    }

    public static function lock( $id, $option = NULL ) {

        $option = isset( $option ) ? '/' . $option : NULL;

        // $res = $this->client->request('POST', $api_url . $this->apibase . '/lock/' . $id . $option,
        //     [
        //     'headers' => $this->headers,
        //     'form_params' => [
        //             'api_token' => \Auth::user()->api_token,
        //             'user_id' => \Auth::user()->id,
        //             'user_first_name' => \Auth::user()->first_name,
        //             'user_last_name' => \Auth::user()->last_name,
        //         ],
        //     ]
        // );

        // return $res;

        $data['user_first_name'] = \Auth::user()->first_name;
        $data['user_last_name'] = \Auth::user()->last_name;

        $model = new static();
        $response = $model->postRequest( '/lock/'.$id.$option, $data );
        $model->logEditApi( $response );

        return $response;
    }

    public function total( $url ) {

        $res = $this->client->request('POST', $url . $this->apibase . '/total',
            [
                'headers' => $this->headers,
                'form_params' => [ 'api_token' => \Auth::user()->api_token ],
            ]);

        return json_decode($res->getBody());
    }

    public function approve( $institution = null ) {

        $model = new static();
        $action = '/approve/'.$this->id;
        $response = $model->postRequest( $action, $data = null, $institution );

        return $response;

    }
}
