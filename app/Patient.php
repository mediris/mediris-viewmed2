<?php

namespace App;

use App\Notifications\BiradReminder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Log;
use App\Helpers\Utils;

class Patient extends Model {

    use Notifiable;

    private $avatar_path;

    /**
    * @fecha: 16-12-2016
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Campos que no pueden ser llenados a través de eloquent (los que no salgan aquí sí podrán ser
    *     llenados).
    */
    protected $guarded = [ 'id', 'api_token', 'user_id' ];

    protected $fillable = [
        'id',
        'active',
        'patient_ID',
        'last_name',
        'first_name',
        'name_suffix',
        'name_prefix',
        'birth_date',
        'medical_alerts',
        'allergies',
        'contrast_allergy',
        'additional_patient_history',
        'special_needs',
        'occupation',
        'comments',
        'address',
        'country_id',
        'telephone_number',
        'telephone_number_2',
        'cellphone_number',
        'cellphone_number_2',
        'email',
        'death_date',
        'photo',
        'citizenship',
        'responsable_id',
        'sex_id',
        'blocked_status',
        'from_appointment'
    ];


    public function __construct( array $attributes = [] ) {
        parent::__construct($attributes);

        $this->avatar_path = config('constants.paths.patient_avatar');
    }

    /**
    * @fecha: 16-12-2016
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Relación: Un Patient pertenece a un Sex.
    */
    public function sex() {
        return $this->belongsTo(Sex::class);
    }

    /**
    * @fecha: 16-12-2016
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Relación: Un Patient pertenece a un Responsable.
    */
    public function responsable() {
        return $this->belongsTo(Responsable::class);
    }

    /**
    * @fecha: 16-12-2016
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Relación: Un Patient pertenece a un Country.
    */
    public function country() {
        return $this->belongsTo(Country::class);
    }

    /**
    * @fecha: 16-12-2016
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Relación: Un Patient tiene muchos PatientDocuments.
    */
    public function patientDocuments() {
        return $this->hasMany(PatientDocuments::class);
    }

    /**
    * @fecha: 16-12-2016
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
    */
    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }

    public static function updateOrCreate( $attributes = [] ) {
        //format birth date to mysql
        if ( isset( $attributes['birth_date'] ) ) {
            $attributes['birth_date'] = date("Y-m-d H:i:s", strtotime( $attributes['birth_date'] ));    
        }

        //format death date to mysql
        if( isset($attributes['death_date']) && !empty($attributes['death_date']) ) {
            $attributes["death_date"] = date("Y-m-d H:i:s", strtotime($attributes["death_date"]) );
        }else{
            $attributes["death_date"] = null;
        }

        //find or set the patient responsable
        if( isset($attributes['responsable']['responsable_id']) && !empty($attributes['responsable']['responsable_id']) &&
            isset($attributes['responsable']['name']) && !empty($attributes['responsable']['name']) ) {

            $responsable = Responsable::where('responsable_id', $attributes['responsable']['responsable_id'])->get()->first();
            if( empty( $responsable ) ){
                $responsable = new Responsable( $attributes['responsable'] );
                $responsable->save();
            }

            $attributes['responsable_id'] = $responsable->id;
        }

        //the photo may be empty, but we don't want to delete the previous photo if it already exists
        $avatar = '';
        if ( isset($attributes['photo']) ) {
            $avatar = $attributes['photo'];
            unset($attributes['photo']);
        }

        //Find the patient (or fill the data in and empty object)
        self::unguard(false);
        if ( empty( $attributes['id'] ) ) {
            //find if the email has been used already
            //$patientFound = Patient::where(['email' => $attributes['email']])->get();
            $patientFound = [];
            if ( count($patientFound)>0 ) {
                throw new \Exception( trans("labels.email-used") );
            } else {
                //new patient instance
                $model = new self( $attributes );
                
                //flag this patient as new, because it's being created
                $isNew = true;
            }
        } else {
            $model = self::find($attributes['id']);
            $model->fill($attributes);
            
            if ( $model->getOriginal('active') == 0 && $model->getOriginal('from_appointment') ) {
                //the patient exists, but it was created for an appointment, so we treat it as new
                $isNew = true;
            } else {
                //the patient already exists
                $isNew = false;
            }
            
        }

        //upload the avatar to the patient
        $model->setAvatar( $avatar );
        //verificar error
        $model->save();
        //save

        if ( $isNew ) {
            //save the id into additional patient history
            $model->additional_patient_history = $model->id;
            $model->save();

            //try to match this patient to a patient from the legacy database
            $model->matchToLegacyPatient();
        }

        //delete the patient documents
        if( isset( $attributes['patientDocumentsToDelete'] ) ) {
            $model->deletePatientDocuments( $attributes['patientDocumentsToDelete'] );
        }

        //upload and/or delete the patient documents
        if( isset( $attributes['patientDocuments'] ) ) {
            $model->uploadPatientDocuments( $attributes['patientDocuments'] );
        }
        
        $model->isNew = $isNew;
        return $model;
    }

    public function matchToLegacyPatient() {
        $legacyPatient = Oldrecordspaciente::where(['cedula' => $this->patient_ID])->first();

        if ( $legacyPatient ) {
            $legacyPatient->id_patient_mediris = $this->id;
            $legacyPatient->save();
        }
    }

    /**
    * @fecha: 22-08-2017
    * @parametros: $file = uploadedFile
    * @programador: Hendember Heras
    * @objetivo: Set the file as the avatar for the patient, saving it in the storage.
    */
    public function setAvatar( $file ) {
        if( $file != '' ) {
            //$file is relative to the public directory
            $file = public_path() . $file;
            
            //move the file to storage
            $filename = Storage::disk('public')->putFile( $this->avatar_path, new File( $file) );

            if ( $filename !== false ) {
                $this->photo = $filename;

                return true;    
            } else {
                return false;
            }            
        }
    }

    /**
    * @fecha: 16-12-2016
    * @parametros: $patient_documents = Todos los archivos asociados el paciente que se envian en el formulario
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Función para subir y guardar los documentos.
    */
    public function uploadScanDocuments($files)
    {
        if($files){

            foreach ($files as $key => $imgbase64) {
                $data = explode(',',$imgbase64);
                $filename = 'scanfile_'.$key.date("Y_m_d").'_'.date("His").'.png';
                $path = 'files/patients/';
                $fichero =  $path.$filename;
                $png = file_put_contents($fichero,base64_decode($data[1]));
                $patientDocument = new PatientDocuments();
                $patientDocument->type = 'scan';
                $patientDocument->name = $filename;
                $patientDocument->filename =$fichero ;
                $patientDocument->description = 'No description available';
                $patientDocument->location = $path;
                $patientDocument->patient_id = $this->id;

                try {

                    $patientDocument->save();

                } catch ( \Exception $e ) {

                    Log::useFiles(storage_path() . '/logs/reception/reception.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: patient/patient_document. Action: edit');
                    echo $e->getCode();
                    echo '<br>';
                    echo $e->getMessage();

                }

            }


        }
    }

    /**
     * @fecha: 16-12-2016
     * @parametros: $documentsToDelete = Todos los archivos asociados el paciente que se envian en el formulario
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para eliminar las instancias y físicamente los documentos.
     */
    public function deletePatientDocuments( $documentsToDelete ) {
        foreach ( $documentsToDelete as $key => $document ) {

            $filetoDelete = PatientDocuments::where('filename', $document)->where('patient_id', $this->id)->get()->first();

            if ( $filetoDelete ) {
                $fileToDeletePath = $filetoDelete->location . '/' . $filetoDelete->filename;

                if ( Storage::delete($fileToDeletePath) ) {
                    $filetoDelete->delete();
                }
            }
        }
    }

    /**
     * @fecha: 16-12-2016
     * @parametros: $patient_documents = Todos los archivos asociados el paciente que se envian en el formulario.
                    (pueden ser los nuevos archivos o los previamente existentes)
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función crear las instancias y físicamente mover los documentos.
     */
    public function uploadPatientDocuments( $patient_documents ) {
        foreach ( $patient_documents as $document ) {
            if ( $document instanceOf UploadedFile ) {

                $patientDocument = new PatientDocuments();

                $patientDocument->type = $document->getClientOriginalExtension();
                $patientDocument->name = $document->getClientOriginalName();
                $patientDocument->filename = 'doc_' . $this->id . '_' . str_random(10) . '.' . $document->getClientOriginalExtension();
                $patientDocument->description = 'No description available';
                $patientDocument->patient_id = $this->id;

                $patientDocument->moveToStorage( $document );
                $patientDocument->save();
            }
        
        }

    }

    /**
    * @fecha: 16-12-2016
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Función para agregar la edad del paciente en años y meses.
    */
    public function lock( $option = null ) {

        switch ( $option ) {

            case 1:

            $this->blocked_status = 1;

            break;

            default:

            $this->blocked_status = $this->blocked_status == 1 ? 0 : 1;

            break;

        }

        $this->save();

        return $this;
    }

    /**
    * @fecha: 16-12-2016
    * @programador: Juan Bigorra / Pascual Madrid
    * @objetivo: Función para agregar la edad del paciente en años y meses.
    */
    public function getAgeAndMonth() {
            
        $birthday = new \DateTime(date("Y-m-d", strtotime($this->birth_date)));
        $diff = $birthday->diff(new \DateTime());

        return [ 'years' => $diff->y, 'months' => $diff->m ];

    }

    public function getStrAgeAndMonth() {
        $birthdate = $this->getAgeAndMonth();

        return $birthdate["years"] . " " . trans("labels.years") . " " . $birthdate["months"] . " " .
            ($birthdate["months"] == 1 ? trans("labels.month") : trans("labels.months"));
    }

    public function getAge() {
        $birthday = new \DateTime(date("Y-m-d", strtotime($this->birth_date)));
        $diff = $birthday->diff(new \DateTime());

        return $diff->y;
    }

    /**
     * @fecha: 21-08-2017
     * @programador: Hendember Heras
     * @objetivo: Returns the birthDate without the time (yyyy-mm-dd)
     */
    public function getBirthDate() {

        $result=substr($this->birth_date, 0, 10);;
        if($result=='0000-00-00'){
            $result='';
        }
        return $result;
    }

    public static function deleteFileScan($id)
    {

        try {
            DB::table('patient_documents')->where('id','=',$id)->delete();
            return 0;
        } catch (Exception $e) {
            return $e;
        }



    }

    /**
     * @fecha: 21-08-2017
     * @programador: Hendember Heras
     * @objetivo: functions required for the notifications, returns the route for the notification, in this case, the email or the phone number.
     */
    public function routeNotificationForMail() {
        return $this->email;
    }
    public function routeNotificationForMassivaMovil() {
        return Utils::formatCellphoneForMassivaMovil( $this->cellphone_number );
    }

    /**
     * @fecha: 21-08-2017
     * @programador: Alton Bell Smythe
     * @objetivo: function for updating the name and last name of the user in the service request.
     */
    public function updateServiceRequests() {
        // Get Data
        $ServiceRequest = ServiceRequest::remoteFindAll([
            'patient_id' => $this->id
        ]);
        // Validat Change Name
        foreach ($ServiceRequest as $key => $value) {
            if(
                $value->patient_first_name != $this->first_name || 
                $value->patient_last_name != $this->last_name ||
                $value->patient_identification_id != $this->patient_ID ||
                $value->patient_cellphone != $this->cellphone_number ||
                $value->patient_sex_id != $this->sex_id
            ) {
                // New Data
                $data = array(
                    'patient_first_name'        => $this->first_name,
                    'patient_last_name'         => $this->last_name,
                    'patient_identification_id' => $this->patient_ID,
                    'patient_cellphone'         => $this->cellphone_number,
                    'patient_sex_id'            => $this->sex_id,
                );
                $attributes = array_merge($value->attributes, $data);
                // Update Remote
                ServiceRequest::remoteUpdate($value->id, $attributes);
            }
        }
    }
}
