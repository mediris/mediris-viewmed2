<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\MassivaMovil;

class BiradReminderForMigration extends Notification implements ShouldQueue
{
    use Queueable;

    protected $institution;
    protected $templateEmail;
    protected $templateSMS;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct() {
        //Search system's configuration to verify if we need to send the notification 
        $this->institution = \App\Institution::first();
        $conf = \App\Configuration::remoteFind( 1, [], $this->institution );

        $this->templateEmail = null;
        $this->templateSMS = null;
        
        if ( $conf ) {
            if ( $conf->mammography_email && isset($conf->mammography_email_template->id) ) {
                $this->templateEmail = $conf->mammography_email_template;
                
            }

            if ( $conf->mammography_sms && isset($conf->mammography_sms_template->id) ) {
                $this->templateSMS = $conf->mammography_sms_template;
            }
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via( $notifiable ) {
        $channels = [];

        if ( $this->templateEmail )
            $channels[] = 'mail';
        if ( $this->templateSMS )
            $channels[] = MassivaMovil::class;

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        $template = $this->templateEmail->template;
        $template = html_entity_decode( $template );

        $data['logo'] = $this->institution->logo_email;
        
        $mm = (new MailMessage)
                    ->subject( $this->templateEmail->description )
                    ->greeting("&nbsp;")
                    ->line( $template );
        $mm->viewData = array_merge( $mm->viewData, $data );

        return $mm;
    }

    public function toMassivaMovil($notifiable) {
        $template = $this->templateSMS->template;
        $template = html_entity_decode( $template );

        return $template;
    }
}