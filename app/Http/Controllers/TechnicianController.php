<?php

namespace App\Http\Controllers;

use Activity;
use Auth;
use DB;
use Log;
use Session;
use App\Equipment;
use App\Institution;
use App\Patient;
use App\PlatesSize;
use App\Referring;
use App\RequestedProcedure;
use App\ServiceRequest;
use App\Source;
use App\Technician;
use App\User;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class TechnicianController extends Controller {

	public function __construct(){
		$this->logPath = '/logs/technician/technician.log';
		
		
		$this->serviceRequest = new ServiceRequest();
		$this->user = new User();
	}

	/**
	 * @fecha 28-11-2016
	 * @programador Juan Bigorra
	 * @objetivo Renderiza la vista index de la sección Technician.
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function index( Request $request, $print = NULL ){

		try{
			if( $request->isMethod('POST') ){

				if( $print == 1 ){

					$fileName = $request->all()['filename'];
					$contentType = $request->all()['contentType'];
					$base64 = $request->all()['base64'];

					$data = base64_decode($base64);

					header('Content-Type:' . $contentType);
					header('Content-Length:' . strlen($data));
					header('Content-Disposition: attachment; filename=' . $fileName);

					echo $data;

				}else{
					$data = Technician::remoteIndexData( $request->all() );

					echo json_encode($data, JSON_UNESCAPED_UNICODE);
					exit();
				}
			}

            $suspensionReasons = \App\SuspendReason::remoteFindAll(['active' => 1, 'admin_reason' => 0]);
            // dd($suspensionReasons);
			return view('technician.index', compact('suspensionReasons'));

		} catch( \Exception $e ) {
			return $this->logError( $e, "technician", "index");
		}

	}

	public function edit( Request $request, $id ){
		if( $request->isMethod('POST') ){

			$this->validate($request, [
				'requestedProcedure.number_of_plates' => 'integer|min:0',
				'requestedProcedure.study_number' => 'string',
				'requestedProcedure.plates_size_id' => 'string',
				'requestedProcedure.equipment_id'	=> 'required'
			]);

			try {
				$data = $request->all();


				if( isset( $data['patient'] ) ){
					$patient = Patient::find($data['patient']['id']);

					if(!isset($data['patient']['contrast_allergy'])){
						$data['patient']['contrast_allergy'] = '0';
					}
					$data['patient']['patient_ID'] = $patient->patient_ID;
					$patient->update($data['patient']);
				}

				if( isset( $data['requestedProcedure'] ) ){
                    if ( isset( $data['requestedProcedure']['force_images'] ) || $this->validateImg( $patient->patient_ID, $data['requestedProcedure']['id'] ) ) {
                        $res = RequestedProcedure::setAsDictate($id, $data['requestedProcedure']);
                        if( isset($res->error) ){
                            return $this->parseFormErrors($data['requestedProcedure'], $res);
                        }else{
                            session()->flash('message', trans('alerts.success-add'));
                            session()->flash('class', 'alert alert-success');

                            $sql = "DELETE FROM worklist.orden WHERE an = '$id'";
							$dat = DB::delete($sql);

                            // ServiceRequest::lock( $res->service_request->id, 0 );
                            if( isset($data['requestedProcedure']['print_label']) ){
                                return redirect('technician/printer/' . $id . '/' . $data['patient']['id'] . '');
                            }else{
                                return redirect()->route('technician');
                            }
                        }
                    } else {
                        $request->session()->flash('message', trans('labels.order-img'));
                        $request->session()->flash('class', 'alert alert-danger');
                    }
				}


			} catch( \Exception $e ) {
				return $this->logError( $e, "technician", "edit");
			}
		}
		
		try{
			$requestedProcedure 				= RequestedProcedure::remoteFind($id);
			$requestedProcedure->source 		= Source::remoteFind( $requestedProcedure->service_request->source_id );
            // if(!is_null($requestedProcedure->service_request->referring_id)) {
            //     $requestedProcedure->referring = Referring::remoteFind($requestedProcedure->service_request->referring_id);
            // }else{
            //     $requestedProcedure->referring =  null;
            // }
            $requestedProcedure->plates_sizes 	= PlatesSize::remoteFindAll();
            $requestedProcedure->user 			= $this->user->get($requestedProcedure->service_request->user_id);

			$equipments							= Equipment::remoteFindByProcedure( $requestedProcedure->procedure_id );

            ///dd($equipments);
			
			// $requestedProcedure->patient->lock(1);
			// $res = ServiceRequest::lock($requestedProcedure->service_request->id, 1);
			$requestedProcedure->patient->age 	= $requestedProcedure->patient->getAgeAndMonth();

            foreach($equipments as $equipment)  {
                $equipment->name = $equipment->name . " (" . $equipment->cont . ")";
            }
			
			return view('technician.edit', 	[
												'requestedProcedure' 	=> $requestedProcedure,
												'equipments'			=> $equipments,
												'allowImpersonate' 		=> true
											]);

		} catch( \Exception $e ) {
			return $this->logError( $e, "technician", "edit");
		}
	}


	/**
	 * @fecha: 28-03-2017
	 * @programador: Mercedes Rodriguez
	 * @objetivo: Suspender una orden.
	 */
	public function suspend( Request $request ) {

		try{
			$data['suspension_reason_id'] 	= $request->suspensionReason;
			$data['suspension_reason_origin'] 	= $request->typeID;
			$res = RequestedProcedure::setAsSuspended( $request->orderId, $data );
			$res->requested_procedure_status->description = trans('labels.' . $res->requested_procedure_status->description);

			$sql = "DELETE FROM worklist.orden WHERE an = '$request->orderId'";
			$dat = DB::delete($sql);

			return json_encode($res);
		} catch( \Exception $e ) {
			(isset($request->typeID) && $request->typeID=="preadmission") ? $type='preadmission' : 'technician';
			return $this->logError( $e, $type, "suspend");
		}

	}

	public function lock( Request $request, Patient $patient, $service_request_id = NULL ){

		try{
			if( $request->isMethod('POST') ){
				//$oldValues = [ 'patient' => [], 'serviceRequest' => [] ];
				$newValues = [ 'patient' => [], 'serviceRequest' => [] ];

				if( isset( $patient ) ) {
					//array_push($oldValues['patient'], $patient->getOriginal());
					
					$updatedPatient = $patient->lock();
					array_push($newValues['patient'], $updatedPatient);

					if ( isset( $service_request_id ) ) {
						$response = ServiceRequest::lock($service_request_id);

						if ( isset( $response->error ) ) {
							return $this->parseFormErrors( [], $res );
						} else {
							//array_push($oldValues['serviceRequest'], (array)$response->oldValue);
							array_push($newValues['serviceRequest'], (array)$response->newValue);	
						} 
					}
				}

				return json_encode([ 'code' => 200, $newValues ]);
			}
		} catch( \Exception $e ) {
			return $this->logError( $e, "technician", "lock");
		}
	}

	public function printer( Request $request, $requested_procedure_id = NULL, Patient $patient ){

		try{

			if( isset( $requested_procedure_id ) ) {
				$requestedProcedure = Technician::remoteFind($requested_procedure_id);
				$referring 			= $requestedProcedure->service_request->referring_id ? Referring::remoteFind($requestedProcedure->service_request->referring_id) : null;
				$patient_ageinfo = $patient->getAgeAndMonth();

				// Format 
				if(isset($requestedProcedure->technician_end_date)){
					$requestedProcedure->technician_end_date = \Utils::formatDate($requestedProcedure->technician_end_date, \Utils::getLanguageEncode(), "%d-%m-%Y %R");
				}

				$response = [ 'requestedProcedure' => $requestedProcedure, 'patient' => $patient, 'patient_ageinfo'=> $patient_ageinfo, 'referring' => $referring ];

				return view('technician.print', $response);
			}

		}catch( \Exception $e ){

			Log::useFiles(storage_path() . '/logs/technician/technician.log');
			Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Technician. Action: printer');

			return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);

		}
	}

	public function rewrite( Request $request, $requestedProcedureId, $equipmentId = null ) {

		if( $request->isMethod('post') ) {
			try {
				if ( ! is_null( $equipmentId ) ) {
					$requestedProcedure = RequestedProcedure::updateEquipment( $requestedProcedureId, $equipmentId );
				} else {
					$requestedProcedure = RequestedProcedure::remoteFind( $requestedProcedureId );
				}

				$data['patient_birth_date'] = $requestedProcedure->patient->birth_date;
				$data['patient_sex'] = $requestedProcedure->patient->sex->name;

				$data['base']        = session()->get('institution')->base_structure;

				$response = $requestedProcedure->writeWorklist( $data );
				
				/**
				 * Log activity
				 */
				Activity::log(trans('tracking.write-api', [
					'section' => 'technician',
					'id' => $response->id,
					'id-institution' => $request->session()->get('institution')->id,
				]));

				return json_encode($response);
			} catch( \Exception $e ) {
				return $this->logError( $e, "technician", "rewrite");
			}

		}
	}

	// en esta seccion se debe realizar la validacion de la existencia de las imagen asociada a la orden
    public function validateImg($PATIENT_ID, $nroOrden ) {
    	$institution = Session::get('institution');
        $url_pacs = $institution->url_pacs;

        if ( empty( $url_pacs ) ) {
        	throw new \Exception( trans('messages.no-pac-url') );
        } else {
            $url_pacs = $url_pacs . "/opalweb/IntegrationProcessor.aspx?CMD=STUDYLIST_XML&PATIENT_ID=#PATIENT_ID";
            $url_pacs = str_replace("#PATIENT_ID", $PATIENT_ID, $url_pacs);
        }
        
	    $jsonStudy = $this->XMLtoJSON($url_pacs);
		$elementoEstudio = '';
		$estatus = false; 

		if ($jsonStudy){
			// Convertir cadena JSON a arreglo	
			$arrayStudy = json_decode($jsonStudy, true);
			
			//there is only one study, put it in an array
			if ( array_key_exists('STUDY_ID_DICOM', $arrayStudy['study']) ) {
				$tmpArr = $arrayStudy['study'];
				$arrayStudy['study'] = array();
				$arrayStudy['study'][] = $tmpArr;
			}
			
			//try to find the image in each study
			foreach($arrayStudy['study'] as $estudio) {
				if ( $estudio['PATIENT_ID_DICOM'] == $PATIENT_ID) {
					if ( (strcmp($nroOrden,$estudio['ACCESSION_NUMBER']) == 0 ) && ( $estudio['Image_Count'] > 0 ) ){
						$estatus = true;
						break;
					}
				}
			}
		}

		return $estatus;
    }

    public function XMLtoJSON($path) {
    	$xml = @file_get_contents($path);

		if( $xml ) {
			$xml = str_replace(array("\n", "\r", "\t"), '', $xml); 	// elimina nuevas líneas, devoluciones y pestañas
			$xml = trim(str_replace('"', "'", $xml));				// reemplaza las comillas dobles con comillas simples, para asegurar que la función XML simple pueda analizar el XML
			$xml = simplexml_load_string($xml);
			$xml = stripslashes(json_encode($xml));
  			return $xml;    										// devuelve una cadena con el objeto JSON
		} else {
			return false;	
		}
	}
}