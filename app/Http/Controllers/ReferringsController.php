<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\Referring;
use App\User;


class ReferringsController extends Controller {
    public function __construct() {
        $this->logPath = '/logs/admin/admin.log';
    }

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Referrings.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ) {
        try {
            $referrings = Referring::remoteFindAll();

            return view('referrings.index', compact('referrings'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "referrings", "index" );
        }
    }

    public function add( Request $request ) {
        if( $request->isMethod('post') ) {
            $requestObj = new \App\Http\Requests\ReferringRequest();
            $this->validate( $request, $requestObj->rules() );

            try {
                $data = $request->all();
                $data['active'] = 1;
                
                $res = Referring::remoteCreate($data);

                if( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                }

                $request->session()->flash('message', trans('alerts.success-add'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->back();
            } catch( \Exception $e ) {
                return $this->logError( $e, "referrings", "add" );
            }
        }

        try {
            $users = User::orderBy('last_name', 'asc')->get();

            return view('referrings.add', compact('users'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "referrings", "add" );
        }

    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ) {
            $requestObj = new \App\Http\Requests\ReferringRequest(['id' => $id]);
            $this->validate( $request, $requestObj->rules() );

            try {
                $data = $request->all();
                
                $res = Referring::remoteUpdate( $id, $data );

                if( isset( $res->error ) ){
                    return $this->parseFormErrors( $data, $res );
                }

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('referrings');
            } catch( \Exception $e ) {
                return $this->logError( $e, "referrings", "edit" );
            }
        }

        try {
            $users = User::orderBy('last_name', 'asc')->get();
            $referring = Referring::remoteFind($id);

            return view('referrings.edit', [ 'referring' => $referring, 'users' => $users ]);
        } catch( \Exception $e ) {
            return $this->logError( $e, "referrings", "edit" );
        }
    }

    public function delete( Request $request, $id ){
        try{
            $res = $this->referring->remove($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete-api', [ 'section' => 'referrings', 'id' => $id, 'id-institution' => $request->session()->get('institution')->id ]));

                $request->session()->flash('message', trans('alerts.success-delete'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('referrings');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: referrings. Action: delete');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function active( Request $request, $id ){
        try {
            $res = Referring::remoteToggleActive($id);

            if( isset( $res->error ) ){
                return $this->parseFormErrors( $data, $res );
            }

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');
       
            return redirect()->route('referrings');
        } catch( \Exception $e ) {
            return $this->logError( $e, "referrings", "active" );
        }
    }
}
