<?php

namespace App\Http\Controllers;

use Activity;
use App\Appointment;
use App\Modality2;
use App\Patient;
use App\Procedure;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Log;

class AppointmentsController extends Controller {
    public function __construct() {
        $this->logPath = '/logs/appointments/appointments.log';
    }

    /**
     * @fecha 25-11-2016
     * @programador Pascual Madrid / Juan Bigorra
     * @objetivo Obtener todas las instancias de appointments para llenar el scheduler.
     * @param Request $request
     * @return array
     */
    public function datasource( Request $request ){
        try{
            return \App\Appointment::remoteFindByAction('/datasource',['start_date'=>$request['start_date'],'end_date'=>$request['end_date']]);
        } catch( \Exception $e ) {
            return $this->logError( $e, "appointments", "datasource");
        }
    }

    /**
     * @fecha 25-11-2016
     * @programador Pascual Madrid / Juan Bigorra
     * @objetivo Renderiza la vista Index de la sección Appointments.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ) {
        try {
            $appointments=[];
            //$appointments   = Appointment::remoteFindByAction('/datasource',['start_date'=>date('Y/m/d '. '00:00:00'), 'end_date'=>date('Y/m/d '. '00:00:00')]);
            $rooms          = Room::remoteFindAll(['active' => 1]);
            $procedures     = Procedure::remoteFindAll(['active' => 1]);
            
            return view( 'appointments.index', compact('appointments', 'rooms', 'procedures' ) );
        } catch( \Exception $e ){
            return $this->logError( $e, "appointments", "index");
        }
    }

    /**
     * @fecha 12-01-2017
     * @programador Pascual Madrid / Juan Bigorra
     * @objetivo Crear una nueva instancia de appointment (Esta función se llama via ajax desde el scheduler.js).
     * @param Request $request
     * @return mixed
     */
    public function add( Request $request ) {
        if( $request->isMethod('post') ){
            if( $request->ajax() ){
                $this->validate($request, [
                    'applicant'=> 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/',
                    'room_id' => 'required',
                    'procedure_id' => 'required',
                    'patient_identification_id' => 'required',
                    'patient_email' => 'required|email',
                    'patient_first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required',
                    'patient_last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required',
                    //'patient_telephone_number' => 'required|phone:US,VE,FIXED_LINE',
                    'patient_telephone_number' => 'phone:US,VE,FIXED_LINE',
                    //'patient_cellphone_number' => 'required|phone:US,VE,MOBILE',
                    'patient_cellphone_number' => 'phone:US,VE,MOBILE',
                    'appointment_date_start' => 'required',
                    'appointment_date_end' => 'required',
                    'procedure_duration' => 'required',
                    'appointment_status_id' => 'required',
                    'string_start_unique' => 'required',
                    'string_end_unique' => 'required',
                    'equipment_id' => 'required',
                    'procedure_contrast_study' => 'required|in:1,0',
                    'patient_allergies_check' => 'required|in:1,0',
                    'patient_allergies' => 'required_if:patient_allergies_check,1',
                    'patient_weight' => 'required_if:required_wh,true',
                    'patient_height' => 'required_if:required_wh,true',
                    'patient_abdominal_circumference' => 'required_if:required_wh,true',
                ]);

                try {
                    $data = $request->all();
                    
                    $patient = Patient::where('patient_ID', $request->input('patient_identification_id'))->get()->first();

                    if ( $patient == null ) {
                        //create patient
                        $dataPatient["patient_ID"]       = $data["patient_identification_id"];
                        $dataPatient["first_name"]       = $data["patient_first_name"];
                        $dataPatient["last_name"]        = $data["patient_last_name"];
                        $dataPatient["telephone_number"] = $data["patient_telephone_number"];
                        $dataPatient["cellphone_number"] = $data["patient_cellphone_number"];
                        $dataPatient["email"]            = $data["patient_email"];
                        $dataPatient["active"]           = 0;
                        $dataPatient["from_appointment"] = 1;

                        $patient = Patient::updateOrCreate( $dataPatient );

                        //add the newly created patient_identification_id to the recent patients ids
                        $recents_patient_id = session('recents_patient_id', []);
                        array_unshift( $recents_patient_id, $request->patient_identification_id );
                        session()->put('recents_patient_id', $recents_patient_id);
                    }

                    $data['patient_id'] = $patient->id;

                    $res = Appointment::remoteCreate( $data );

                    if ( isset( $res->error ) ) {
                        return response()->json( $res );
                    } else {
                        return response()->json(['code' => '201', 'message' => 'Created', 'id' => $res->id]);
                    }
                } catch( \Exception $e ) {
                    return response()->json([
                        'error' => $e->getCode(),
                        'message' => $e->getMessage() 
                    ]);

                    return $this->logError( $e, "appointments", "add");
                }
            }
        }

        try{
            $procedures = $this->procedure->getAll($request->session()->get('institution')->url);
            $rooms = Room::remoteFindAll(['active' => 1]);
            $appointments = \App\Appointment::remoteFindAll();

            return view('appointments.add', [ 'procedures' => $procedures,
                                              'rooms' => $rooms,
                                              'appointments' => $appointments ]
                        );
        } catch( \Exception $e ) {
            return $this->logError( $e, "appointments", "add");
        }
    }

    /**
     * @fecha 12-01-2017
     * @programador Pascual Madrid / Juan Bigorra
     * @objetivo Editar una instancia de appointment (Esta función se llama via ajax desde el scheduler.js).
     * @param Request $request
     * @return mixed
     */
    public function edit( Request $request, $id ){
        if( $request->isMethod('post') ){

            if( $request->ajax() ){
                $this->validate($request, [ 'room_id' => 'required', 
                    'procedure_id' => 'required',
                    'patient_identification_id' => 'required',
                    'patient_email' => 'required|email',
                    'patient_first_name' => 'regex:/^[A-Za-z\s]+$/|required',
                    'patient_last_name' => 'regex:/^[A-Za-z\s]+$/|required',
                    //'patient_telephone_number' => 'required|phone:US,VE,FIXED_LINE',
                    'patient_telephone_number' => 'phone:US,VE,FIXED_LINE',
                    //'patient_cellphone_number' => 'required|phone:US,VE,MOBILE',
                    'patient_cellphone_number' => 'phone:US,VE,MOBILE',
                    'appointment_date_start' => 'required',
                    'appointment_date_end' => 'required',
                    'procedure_duration' => 'required',
                    'appointment_status_id' => 'required',
                    'string_start_unique' => 'required',
                    'string_end_unique' => 'required',
                    'equipment_id' => 'required',
                    'procedure_contrast_study' => 'required|in:1,0',
                ]);

                try{
                    $patient = Patient::where('patient_ID', $request->input('patient_identification_id'))->get()->first();

                    $data = $request->all();

                    if( $patient != null ){
                        $data['patient_id'] = $patient->id;
                    } else {
                        $data['patient_id'] = 0;
                    }

                    $res = Appointment::remoteUpdate( $id, $data );

                    if ( isset( $res->error ) ) {
                        return response()->json( $res );
                    } else {
                        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $res['original'], 'newValue' => $res]);
                    }
                }catch( \Exception $e ){
                    return $this->logError( $e, "appointments", "edit");
                }
            }
        }

        try {
            $procedures     = $this->procedure->getAll($request->session()->get('institution')->url);
            $rooms          = Room::remoteFindAll(['active' => 1]);
            $appointment    = Appointment::remoteFind($id);
            $appointments   = Appointment::remoteFindAll();

            return view('appointments.edit', [ 'appointments' => $appointments,
                                               'appointment' => $appointment,
                                               'procedures' => $procedures,
                                               'rooms' => $rooms ]
                        );
        } catch( \Exception $e ) {
            return $this->logError( $e, "appointments", "edit");
        }
    }

    public function delete( Request $request, $id ){
        if( $request->isMethod('post') ) {
            try{
                $appointment = Appointment::remoteFind($id);

                $res = $appointment->setAsDeleted($request->all());

                if ( isset( $res->error ) ) {
                    return response()->json( $res );
                } else {
                    return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $res['original'], 'newValue' => $res]);
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "appointments", "cancel");
            }
        }
    }

    public function reject( Request $request, $id ){
        if( $request->isMethod('post') ) {
            try {
                $appointment = Appointment::remoteFind($id);

                $res = $appointment->setAsRejected($request->all());

                if ( isset( $res->error ) ) {
                    return response()->json( $res );
                } else {
                    return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $res['original'], 'newValue' => $res]);
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "appointments", "cancel");
            }
        }
    }

    public function search( Request $request ){
        if( $request->isMethod('post') ){
            try{
                $data = [];
                $data['search_criteria'] = $request->input('search_criteria');
                
                $appointments = Appointment::remoteFindByAction( '/search', $data);
            
                return view('appointments.search', compact('appointments'));
            } catch( \Exception $e ) {
                return $this->logError( $e, "appointments", "search");
            }
        }
    }

    public function show( Request $request, $id ){
        try{
            $appointment = Appointment::remoteFind($id);
            
            return view('appointments.show', compact('appointment'));
        }catch( \Exception $e ){
            return $this->logError( $e, "appointments", "show");
        }
    }
}
