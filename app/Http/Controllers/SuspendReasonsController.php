<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\SuspendReason;
use JsValidator;

class SuspendReasonsController extends Controller
{
    public function __construct() {
        $this->logPath = '/logs/admin/admin.log';
        $this->suspendreason = new SuspendReason();
    }

    public function rules( $unique = null ) {
        return [
            'description' => 'required|max:70|unique_test:suspend_reasons' . (($unique)?','.$unique:'')
        ];
    }

    /**
     * @fecha 22-05-2017
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección SuspendReason.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ) {
        try {
            $suspendreasons = SuspendReason::remoteFindAll();

            return view('suspendreasons.index', compact('suspendreasons'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "suspendreasons", "index" );
        }
    }

    public function add( Request $request ) {
        if( $request->isMethod('post') ) {
            $this->validate( $request, $this->rules() );

            try {
                $data = $request->all();
                if (!isset($data['admin_reason'])) {
                    $data['admin_reason']= 0;
                }
                $data['active']= 1;
                $res = SuspendReason::remoteCreate($data);

                if( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                }

                $request->session()->flash('message', trans('alerts.success-add'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('suspend_reasons');
            } catch( \Exception $e ) {
                return $this->logError( $e, "suspend reasons", "add");
            }
        } else {
            $validator = JsValidator::make( $this->rules(), array(), array(), "#SuspendReasonAddForm" );
            return view('suspendreasons.add', compact('validator'));
        }
    }

    public function edit( Request $request, $id ) {

        if( $request->isMethod('post') ) {


            $this->validate( $request, $this->rules( $id ) );

            try {
                $data = $request->all();

                if (!isset($data['admin_reason'])) {
                    $data['admin_reason']= 0;
                }

                $res =SuspendReason::remoteUpdate($id, $data );

                if( isset( $res->error ) ){
                    return $this->parseFormErrors( $data, $res );
                }

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('suspend_reasons');
            } catch( \Exception $e ) {
                return $this->logError( $e, "suspend reasons", "edit");
            }
        } else {
            $suspendreason = SuspendReason::remoteFind( $id );
            $validator = JsValidator::make( $this->rules(), array(), array(), "#SuspendReasonEditForm" );
            return view('suspendreasons.edit', compact('suspendreason', 'validator'));
        }
    }
}
