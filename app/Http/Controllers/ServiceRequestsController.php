<?php

namespace App\Http\Controllers;

use Activity;
use App\Answer;
use App\Appointment;
use App\Country;
use App\Institution;
use App\Modality2;
use App\Patient;
use App\Oldrecordspaciente;
use App\PatientDocuments;
use App\PatientState;
use App\PatientType;
use App\Prefix;
use App\PregnancyStatus;
use App\Procedure;
use App\Radiologist;
use App\Referring;
use App\RequestStatus;
use App\Responsable;
use App\ServiceRequest;
use App\RequestedProcedure;
use App\Sex;
use App\SmokingStatus;
use App\Source;
use App\Suffix;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use Log;

class ServiceRequestsController extends Controller {

    public function __construct() {
        $this->logPath = '/logs/reception/reception.log';

        $this->serviceRequest = new ServiceRequest();
        $this->requestStatus = new RequestStatus();
        $this->pregnancyStatus = new PregnancyStatus();
        $this->smokingStatus = new SmokingStatus();
        $this->patientState = new PatientState();
    }

    /**
    * @fecha 28-11-2016
    * @programador Pascual Madrid / Juan bigorra
    * @objetivo Renderiza la vista index de la sección Reception.
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
    */
    public function index( Request $request, $print = NULL ){

        try{
            if( $request->isMethod('POST') ){

                if( $print == 1 ){

                    $fileName = $request->all()['filename'];
                    $contentType = $request->all()['contentType'];
                    $base64 = $request->all()['base64'];

                    $data = base64_decode($base64);

                    header('Content-Type:' . $contentType);
                    header('Content-Length:' . strlen($data));
                    header('Content-Disposition: attachment; filename=' . $fileName);

                    echo $data;

                } else {
                    $data = Appointment::remoteIndexData( $request->all() );

                    echo json_encode($data, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }

            return view('serviceRequests.index');

        } catch( \Exception $e ) {
            return $this->logError( $e, "Reception", "index");
        }
    }

    /**
     * @param Request $request
     * @param Patient|NULL $patient
     * @param null $appointment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request, Patient $patient = NULL, $appointment = NULL ) {

        if( $request->isMethod('post') ) {
            if( empty( $request->all()['patient']['id'] ) ) {
                //$uniqueEmail = 'required_if:anonymous,0|unique:patients|email';
                $uniqueEmail = 'required_if:anonymous,0|email';
                $uniquePatientId = 'required_if:anonymous,0|unique:patients|max:45';
            } else {
                //$uniqueEmail = 'required_if:anonymous,0|unique:patients,email,' . $request->all()['patient']['id'] . '|email';
                $uniqueEmail = 'required_if:anonymous,0|email';
                $uniquePatientId = 'required_if:anonymous,0|unique:patients,patient_ID,' . $request->all()['patient']['id'] . '|max:45';
            }
            $this->validate($request, [
                'patient_ID' => $uniquePatientId,
                'patient.first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required_if:anonymous,0|max:50',
                'patient.last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required_if:anonymous,0|max:50',
                'patient.birth_date' => 'required_if:anonymous,0|date',
                'patient.address' => 'required_if:anonymous,0|max:250',
                'patient.country_id' => 'required_if:anonymous,0',
                //'telephone_number_masked' => 'required_if:anonymous,0|min:7|phone:US,VE,FIXED_LINE',
                //'cellphone_number_masked' => 'required_if:anonymous,0|min:7|phone:US,VE,MOBILE',
                'email' => $uniqueEmail,
                'patient.citizenship' => 'required|max:25',
                'patient.sex_id' => 'required_if:anonymous,0',
                'patient_type_id' => 'required',
                'source_id' => 'required',
                'answer_id' => 'required',
                //'weight' => 'required_if:anonymous,0|regex:/^\d*(\.\d{1}\d?)?$/',
                //'height' => 'required_if:anonymous,0|regex:/^\d*(\.\d{1}\d?)?$/',
                'requestedProcedures' => 'array|required',
            ]);
            //find the appointment if any
            if( $request->appointment_id ){
                $appointment = Appointment::remoteFind( $request->appointment_id );
            }
            //set PATIENT values
            $dataPatient                        = $request->all()['patient'];
            $dataPatient['email']               = $request->all()['email'];
            $dataPatient['patient_ID']          = $request->patient_ID;
            $dataPatient['telephone_number']    = $request->all()['telephone_number'];
            $dataPatient['cellphone_number']    = $request->all()['cellphone_number'];
            $dataPatient['responsable']         = $request->all()['responsable'];
            if( $appointment ){
                $dataPatient['allergies']       = $appointment->patient_allergies ;    
            }

            if( isset($request->all()['anonymous']) && $request->all()['anonymous'] == 1 ) {
                $dataPatient['first_name'] = ucfirst(trans('labels.anonymous'));
                $dataPatient['last_name'] = ucfirst(trans('labels.anonymous'));
                $dataPatient['birth_date'] = date('Y-m-d H:i:s');
                $dataPatient['country_id'] = null;
                $dataPatient['citizenship'] =null;
                $dataPatient['email'] = null;
                $dataPatient['patient_ID'] = null;
                $dataPatient['anonymous'] = 1;
            }

            $dataPatient['patientDocumentsToDelete'] = $request->patientDocumentsToDelete;
            $dataPatient['patientDocuments']    = $request->patientDocuments;
            $dataPatient['active']              = 1;
            //save the patient data
            try{
                $patient = Patient::updateOrCreate( $dataPatient );
                if ( isset( $patient->error ) ) {
                    return $this->parseFormErrors( $dataPatient, $patient );
                }    
            } catch( \Exception $e ) {
                return $this->logError( $e, "Reception", "add");
            }

            try {
                //set the values for the serviceRequest
                $data = $request->all();
                
                $data['patient_id'] = $patient->id;
                $data['patient_last_name'] = $patient->last_name;
                $data['patient_first_name'] = $patient->first_name;
                $data['patient_identification_id'] = $patient->patient_ID;
                $data['patient_sex_id'] = $patient->sex_id;
                $data['patient_cellphone'] = $patient->cellphone_number;
                if( $appointment ){
                    $data['procedure_contrast_study'] = $appointment->procedure_contrast_study;
                    $data['abdominal_circumference'] = $appointment->patient_abdominal_circumference;
                } else {
                    $data['procedure_contrast_study'] = 0;
                    $data['abdominal_circumference'] = 0;    
                }
                
                $data['requestDocuments'] = $request->requestDocuments;
                //create the serviceRequest
                $serviceRequest = ServiceRequest::remoteCreate( $data );
                if ( isset( $serviceRequest->error ) ) {
                    return $this->parseFormErrors( $data, $serviceRequest );
                } else {
                    if( $appointment ) {
                        $appointment->setAsAdmitted();
                    }

                    session()->flash('message', trans('alerts.success-add'));
                    session()->flash('class', 'alert alert-success');
                
                    if ( $patient->isNew ) {
                        //return to the route where we can match this patient with a patient of the legacy data
                        return redirect()->route('patients.match', ['patient' => $patient->id, 'redirectTo' => 'reception']);
                    } else {
                        //return to appointment list
                        return redirect()->route('reception');
                    }
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "Reception", "add");
            }
        }
        $response = [
            'requestStatuses'   => $this->requestStatus->getAll($request->session()->get('institution')->url),
            'patientTypes'      => PatientType::remoteFindAll( [ 'active' => 1 ] ),
            'sources'           => Source::remoteFindAll( [ 'active' => 1 ] ),
            'referrings'        => Referring::remoteFindAll( [ 'active' => 1 ] ),
            'answers'           => Answer::remoteFindAll( [ 'active' => 1 ] ),
            'pregnancyStatuses' => $this->pregnancyStatus->getAll($request->session()->get('institution')->url),
            'procedures'        => Procedure::remoteFindByAction('/equipments' ),
            'sexes'             => Sex::orderBy('name', 'asc')->get(),
            'responsables'      => Responsable::orderBy('name', 'asc')->get(),
            'countries'         => Country::getCountryByLocale(),
            'modalities'        => Modality2::remoteFindAll( [ 'active' => 1 ] ),
            'appointment'       => $appointment,
            'patient'           => $patient
        ];

        $referrings=[];
        foreach ($response['referrings'] as $reff){
            //$reff->full_name=trans($reff->first_name . ' ' . $reff->last_name);
            $reff->full_name=trans($reff->last_name . ' ' . $reff->first_name);
            array_push($referrings,$reff);
        }
        $response['referrings']=$referrings;


        $patientTypes=[];
        foreach ($response['patientTypes'] as $ptype){
            if ( $ptype->parent_patient_type == null ){
                array_push($patientTypes,$ptype);
            }
            foreach($ptype->child_patient_types as $patientSubType){
                $patientSubType->description='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$patientSubType->description;
                array_push($patientTypes,$patientSubType);
            }
        }
        $response['patientTypes']=$patientTypes;

        $sexos=[];
        foreach ($response['sexes'] as $sexo){
            $sexo->name=trans('labels.'.$sexo->name);
            array_push($sexos,$sexo);

        }
        $response['sexes']=$sexos;


        if ( $appointment != 0 ) {
            //search the appointment and assign it in the response
            $appointment = Appointment::remoteFind($appointment);
            $response['appointment'] = $appointment;
        }

        if( $patient->exists ) {
            //search the last service request for this patient
            $serviceRequests = ServiceRequest::remoteFindAll( [ 'patient_id' => $patient->id ] );
            if( count($serviceRequests) > 0 ) {
                $response['serviceRequest'] = $serviceRequests->first();
            }
        }
        return view('serviceRequests.add', $response);
    }

    /**
    * @fecha 24-08-2017
    * @programador Hendember Heras
    * @objetivo Returnd the file contents identified by the $document_id 
    * @param $document_id
    */
    public function document( $document_id ) {
        $document = \App\ServiceRequestDocument::remoteFind($document_id);
        
        $path = $document->location  . '/' . $document->name;

        if (!Storage::exists($path)) {
            abort(404);
        }

        $absolutepath = Storage::getDriver()->getAdapter()->applyPathPrefix( $path );
        $type = Storage::mimeType($path);
        
        $headers = [
            'Content-Type' => $type,
            'Content-Disposition' => 'inline;filename='.$document->name
        ];

        return response()
            ->file($absolutepath, $headers);
    }

    public function saveFilesReception($files,$type)
    {
        $filesave = [];

        foreach ($files as $key => $imgbase64) {
            $data = explode(',',$imgbase64);
            $filename = $type.$key.date("Y_m_d").'_'.date("His").'.png';
            $path = "files/reception/";
            $fichero =  $path.$filename;
            $png = file_put_contents($fichero,base64_decode($data[1]));
            $filesave [$key]['filename']= $filename;
            $filesave [$key]['path']= $path;
            $filesave [$key]['filepath']= $fichero;
        }

        return $filesave;
    }

    public function edit( Request $request, $id ){


        if( $request->isMethod('post') ){

            $this->validate($request, [
                'patient_ID' => 'required|max:9',
                'patient.first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'patient.last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'patient.birth_date' => 'required|date',
                'patient.address' => 'required|max:250',
                'patient.country_id' => 'required',
                //'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
                //'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
                'email' => 'required|email',
                'patient.citizenship' => 'required|max:25',
                'patient.sex_id' => 'required',
                'patient_type_id' => 'required',
                'source_id' => 'required',
                'referring_id' => 'required',
                'answer_id' => 'required',
                //'weight' => 'required|numeric',
                //'height' => 'required|numeric',
                'patient_state_id' => 'required',
                'requestedProcedures.*.procedure_id' => 'required',
            ]);

            $dataPatient = $request->all()['patient'];
            $dataPatient['email'] = $request->all()['email'];
            $dataPatient['patient_ID'] = $request->all()['patient_ID'];
            $dataPatient['telephone_number'] = $request->all()['telephone_number'];
            $dataPatient['cellphone_number'] = $request->all()['cellphone_number'];

            if( $request->all()['responsable']['name'] != '' && $request->all()['responsable']['responsable_id'] != '' ){
                $dataResponsable = $request->all()['responsable'];

                $responsable = Responsable::where('responsable_id', $dataResponsable['responsable_id'])->get()->first();

                if( empty( $responsable ) ){
                    $responsable = new Responsable($dataResponsable);
                    $responsable->save();
                }

                $dataPatient['responsable_id'] = $responsable->id;

                unset( $request->all()['responsable'] );
            }

            $patient = Patient::find($dataPatient['id']);

            if( isset( $request->all()['patientDocuments'] ) ){

                unset( $dataPatient['patientDocuments'] );
                $patient_documents = $request->all()['patientDocuments'];
            }

            if( isset( $request->all()['patientDocumentsToDelete'] ) ){

                $documentsToDelete = $request->all()['patientDocumentsToDelete'];
                unset( $dataPatient['patientDocumentsToDelete'] );
            }

            $mb = new MessageBag();

            $filename = $dataPatient['photo'];

            if( $filename != $patient->photo ){

                $default_photo = '/images/patients/default_avatar.jpg';
                $new_photo = '/images/patients/avatars/avatar_' . $patient->patient_ID . '_' . str_random(10) . '.' . 'jpeg';
                $new_filename = ( $patient->photo == $default_photo ? $new_photo : $patient->photo );
                $savePath = public_path() . $new_filename;
                $oldPath = public_path() . $filename;

                if( !rename($oldPath, $savePath) ){
                    $mb->add('Imagen', 'La nueva imagen no pudo ser movida a la carpeta de avatars');

                    return redirect()->back()->withErrors($mb)->withInputs();
                }

                $dataPatient['photo'] = $new_filename;
            }

            try{

                /**
                * Date formatted to MySQL DateTime 
                */
                
                $formated_birth_date = strtotime($dataPatient["birth_date"]);
                
                $dataPatient["birth_date"] = date("Y-m-d H:i:s", $formated_birth_date);
                

                $patient->update($dataPatient);
                ( isset( $patient_documents ) ? $patient->uploadPatientDocuments($patient_documents) : '' );

                if( isset( $documentsToDelete ) ){

                    $patientDocuments = new PatientDocuments();
                    $patientDocuments->deletePatientDocuments($documentsToDelete);

                }

            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/reception/reception.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: add');
                echo $e->getCode();
                echo '<br>';
                echo $e->getMessage();
                exit();
            }

            $data = $request->all();
            $data['api_token'] = \Auth::user()->api_token;
            $data['user_id'] = \Auth::user()->id;
            $data['request_status_id'] = 1;
            unset( $data['patient'] );
            unset( $data['patient_ID'] );
            unset( $data['email'] );
            unset( $data['telephone_number'] );
            unset( $data['cellphone_number'] );

            if( isset( $data['requestDocuments'] ) ){
                $requestDocuments = $data['requestDocuments'];
                $data['requestDocuments'] = [];

                foreach( $requestDocuments as $requestDocument ){
                    if( gettype($requestDocument) == 'array' ){
                        array_push($data['requestDocuments'], $requestDocument);
                    }else{
                        $name = 'requestDocument_' . $patient->id . '_' . str_random(10) . '.' . $requestDocument->extension();
                        $location = public_path() . '/images/serviceRequests/documents';
                        array_push($data['requestDocuments'], [
                            'type' => $requestDocument->extension(),
                            'name' => $name,
                            'description' => 'request document',
                            'location' => $location,
                        ]);
                        $requestDocument->move(public_path() . '/images/serviceRequests/documents', $name);
                    }
                }
            }

            if( isset( $data['requestDocumentsToDelete'] ) ){
                $documentsToDelete = $data['requestDocumentsToDelete'];
                unset( $data['requestDocumentsToDelete'] );
            }

            try{

                $data['patient_id'] = $patient->id;
                $data['patient_last_name'] = $patient->last_name;
                $data['patient_first_name'] = $patient->first_name;
                $data['patient_identification_id'] = $patient->patient_ID;

                $res = $this->serviceRequest->edit($request->session()->get('institution')->url, $data, $id);

                if( $res->getStatusCode() == 200 ){

                    if( isset( json_decode($res->getBody())->error ) ){
                        $mb = new MessageBag();
                        foreach( $data as $key => $value ){
                            if( strpos(strtoupper(json_decode($res->getBody())->message), strtoupper($key)) !== FALSE ){
                                $mb->add($key, trans('errors.' . json_decode($res->getBody())->error, [ 'attribute' => trans('errors.attributes.' . $key) ]));
                            }
                        }

                        return redirect()->back()->withErrors($mb)->withinput();
                    }

                    if( isset( $documentsToDelete ) ){
                        foreach( $documentsToDelete as $documentToDelete ){
                            if( file_exists(public_path() . '/images/serviceRequests/documents/' . $documentToDelete) ){
                                unlink(public_path() . '/images/serviceRequests/documents/' . $documentToDelete);
                            }
                        }
                    }

                    /**
                    * Log activity
                    */
                    Activity::log(trans('tracking.edit-api', [
                        'section' => 'reception',
                        'id' => json_decode($res->getBody())->oldValue->id,
                        'id-institution' => $request->session()->get('institution')->id,
                        'oldValue' => json_encode(json_decode($res->getBody())->oldValue),
                        'newValue' => json_encode(json_decode($res->getBody())->newValue),
                    ]));

                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');
                }

                return redirect()->route('reception');

            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/reception/reception.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: edit');
                echo $e->getCode();
                echo '<br>';
                echo $e->getMessage();
                exit();
            }
        }

        try{
            $sexes              = Sex::orderBy('name', 'asc')->get();
            $countries          = Country::getCountryByLocale();
            $responsables       = Responsable::orderBy('name', 'asc')->get();
            $modalities         = Modality::remoteFindAll( [ 'active' => 1 ] );
            $requestStatuses    = $this->requestStatus->getAll($request->session()->get('institution')->url);
            $patientTypes       = PatientType::remoteFindAll( [ 'active' => 1 ] );
            $sources            = Source::remoteFindAll( [ 'active' => 1 ] );
            $referrings         = Referring::remoteFindAll( [ 'active' => 1 ] );
            $answers            = Answer::remoteFindAll(['active' => 1]);
            $procedures         = Procedure::remoteFindAll(['active' => 1]);
            $serviceRequest     = $this->serviceRequest->get($request->session()->get('institution')->url, $id);

            return view('serviceRequests.edit', [
                'serviceRequest' => $serviceRequest,
                'requestStatuses' => $requestStatuses,
                'patientTypes' => $patientTypes,
                'sources' => $sources,
                'referrings' => $referrings,
                'answers' => $answers,
                'procedures' => $procedures,
                'modalities' => $modalities,
                'responsables' => $responsables,
                'countries' => $countries,
                'sexes' => $sexes,
            ]);

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/reception/reception.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: edit');
            echo $e->getCode();
            echo '<br>';
            echo $e->getMessage();
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = $this->serviceRequest->active($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit-api', [
                    'section' => 'reception',
                    'id' => json_decode($res->getBody())->oldValue->id,
                    'id-institution' => $request->session()->get('institution')->id,
                    'oldValue' => json_encode(json_decode($res->getBody())->oldValue),
                    'newValue' => json_encode(json_decode($res->getBody())->newValue),
                ]));

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('reception');
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: active');
            echo $e->getCode();
            echo '<br>';
            echo $e->getMessage();

            return redirect()->route('errors', [ $e->getCode() ]);
        }
    }

    public function history( Request $request, Patient $patient ){
        try {

            $institutions = Institution::all();
            $serviceRequests = [];
            $total = 0;
            
            foreach( $institutions as $institution ) {
                $serviceRequestsByInstitution = ServiceRequest::remoteFindAll( [ 'patient_id' => $patient->id ], $institution );
                foreach( $serviceRequestsByInstitution as $sr ) {
                    
                    foreach( $sr->requested_procedures as $requestedProcedure ) {
                        $procedure = RequestedProcedure::remoteFind( $requestedProcedure->id );
                        $requestedProcedure->addendums = $procedure->addendums;
                    }
                }

                $total += $serviceRequestsByInstitution->count();
                    
                $serviceRequests[] = [
                    'institution_id' => $institution->id,
                    'institution_name' => $institution->name,
                    'serviceRequests' => $serviceRequestsByInstitution
                ];
            }

            //old data
            $oldPatients = \App\Oldrecordspaciente::where('id_patient_mediris', $patient->id)->get();
            $oldPatientsIds = $oldPatients->map(function ($item, $key) {
                return $item->id;
            })->toArray();

            //dump($oldPatientsIds);

            $oldServiceRequests = \App\Oldrecordssolicitud::whereIn('id_paciente', $oldPatientsIds)->get();
            $oldServiceRequests->load('ordenes', 'institucion');
            foreach ( $oldServiceRequests as $oldServiceRequest ) {
                $oldServiceRequest->ordenes->load('procedimiento', 'estatus');
            }
            
            return view('serviceRequests.history', [ 'serviceRequests' => $serviceRequests,
                                                     'total' => $total,
                                                     'oldServiceRequests' => $oldServiceRequests ]);
        } catch( \Exception $e ) {
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: history');
            echo $e->getCode();
            echo '<br>';
            echo $e->getMessage();

            //return redirect()->route('errors', [ $e->getCode() ]);
        }
    }

    public function legacyhistory( Request $request, Oldrecordspaciente $patient ){
        try {

            $serviceRequests = [];
            $total = 0;

            $oldServiceRequests = \App\Oldrecordssolicitud::where('id_paciente', $patient->id)->get();
            $oldServiceRequests->load('ordenes', 'institucion');
            foreach ( $oldServiceRequests as $oldServiceRequest ) {
                $oldServiceRequest->ordenes->load('procedimiento', 'estatus');
            }
            
            return view('serviceRequests.history', [ 'serviceRequests' => $serviceRequests,
                                                     'total' => $total,
                                                     'oldServiceRequests' => $oldServiceRequests ]);
        } catch( \Exception $e ) {
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: history');
            echo $e->getCode();
            echo '<br>';
            echo $e->getMessage();

            //return redirect()->route('errors', [ $e->getCode() ]);
        }
    }
}
