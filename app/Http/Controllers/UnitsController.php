<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\Unit;

class UnitsController extends Controller {
    public function __construct(){
        $this->url = 'api/v1/units';
        $this->unit = new Unit();
    }


    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Units.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try{
            $units = $this->unit->getAll($request->session()->get('institution')->url);

            return view('units.index', compact('units'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: units. Action: index');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function add( Request $request ){
        if( $request->isMethod('post') ){
            $this->validate($request, [
                'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            ]);
            try{
                $data = $request->all();
                $data['api_token'] = \Auth::user()->api_token;
                $data['user_id'] = \Auth::user()->id;

                $res = $this->unit->add($request->session()->get('institution')->url, $data);

                if( $res->getStatusCode() == 200 ){
                    if( isset( json_decode($res->getBody())->error ) ){
                        $mb = new MessageBag();
                        foreach( $data as $key => $value ){

                            if( strpos(strtoupper(json_decode($res->getBody())->message), strtoupper($key)) !== false ){
                                $mb->add($key, trans('errors.' . json_decode($res->getBody())->error, [ 'attribute' => trans('errors.attributes.' . $key) ]));
                            }
                        }

                        return redirect()->back()->withErrors($mb)->withinput();
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create-api', [ 'section' => 'units', 'id' => json_decode($res->getBody())->id, 'id-institution' => $request->session()->get('institution')->id ]));

                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');
                }

                return redirect()->route('units');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: units. Action: add');

                return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            }
        }

        try{
            return view('units.add');
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: units. Action: add');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function edit( Request $request, $id ){
        if( $request->isMethod('post') ){
            $this->validate($request, [
                'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',

            ]);
            try{
                $data = $request->all();
                $data['api_token'] = \Auth::user()->api_token;
                $data['user_id'] = \Auth::user()->id;

                $res = $this->unit->edit($request->session()->get('institution')->url, $data, $id);

                if( $res->getStatusCode() == 200 ){

                    if( isset( json_decode($res->getBody())->error ) ){
                        $mb = new MessageBag();
                        foreach( $data as $key => $value ){

                            if( strpos(strtoupper(json_decode($res->getBody())->message), strtoupper($key)) !== false ){
                                $mb->add($key, trans('errors.' . json_decode($res->getBody())->error, [ 'attribute' => trans('errors.attributes.' . $key) ]));
                            }
                        }

                        return redirect()->back()->withErrors($mb)->withinput();
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit-api', [ 'section' => 'units', 'id' => json_decode($res->getBody())->oldValue->id, 'id-institution' => $request->session()->get('institution')->id, 'oldValue' => json_encode(json_decode($res->getBody())->oldValue), 'newValue' => json_encode(json_decode($res->getBody())->newValue) ]));

                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');
                }

                return redirect()->route('units');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: consumables. Action: edit');

                return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            }
        }

        try{
            $unit = $this->unit->get($request->session()->get('institution')->url, $id);

            return view('units.edit', compact('unit'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: consumables. Action: edit');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = $this->unit->active($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit-api', [ 'section' => 'units', 'id' => json_decode($res->getBody())->oldValue->id, 'id-institution' => $request->session()->get('institution')->id, 'oldValue' => json_encode(json_decode($res->getBody())->oldValue), 'newValue' => json_encode(json_decode($res->getBody())->newValue) ]));

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('units');
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: units. Action: active');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }
}
