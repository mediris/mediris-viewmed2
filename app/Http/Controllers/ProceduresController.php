<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\Procedure;
use App\Template;
use App\Step;
use App\Modality2;
use App\Equipment;
use App\Consumable;
use App\Room;


class ProceduresController extends Controller {
    public function __construct(){
        $this->logPath = '/logs/admin/admin.log';
        
        $this->template = new Template();
        $this->consumable = new Consumable();
        $this->room = new Room();
    }

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Procedures.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try{
            $procedures = Procedure::remoteFindAll();

            return view('procedures.index', compact('procedures'));
        }catch( \Exception $e ) {
            return $this->logError( $e, "procedures", "index");
        }
    }

    public function add( Request $request ){

        if( $request->isMethod('post') ){

            $this->validate($request, [
                'description' => 'required|max:50',
                'radiologist_fee' => 'required|numeric|min:0',
                'technician_fee' => 'required|numeric|min:0',
                'transcriptor_fee' => 'required|numeric|min:0',
                'administrative_ID' => 'required|max:45|unique_test:procedures',
                'steps.*.description' => 'required|max:25',
                'steps.*.indications' => 'required',
                'steps.*.administrative_ID' => 'required|max:45',
                'steps.*.order' => 'required|numeric|min:1',
            ]);
            try{
                $data = $request->all();

                $res = Procedure::remoteCreate( $data );
                
                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');
            
                    return redirect()->route('procedures');
                }
            }catch( \Exception $e ) {
                return $this->logError( $e, "procedures", "add");
            }
        }
        try{
            $steps = Step::remoteFindAll(['active' => 1]);
            $templates = $this->template->getAll($request->session()->get('institution')->url);
            $modalities = Modality2::remoteFindAll(['active' => 1]);

            return view('procedures.add', compact( 'steps', 'templates', 'modalities' ) );
        }catch( \Exception $e ) {
            return $this->logError( $e, "procedures", "add");
        }
    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){

            $this->validate($request, [
                'description' => 'required|max:50',
                'radiologist_fee' => 'required|numeric|min:0',
                'technician_fee' => 'required|numeric|min:0',
                'transcriptor_fee' => 'required|numeric|min:0',
                'administrative_ID' => 'required|max:45|unique_test:procedures,' . $id,
                'steps.*.description' => 'required|max:25',
                'steps.*.indications' => 'required',
                'steps.*.administrative_ID' => 'required|max:45',
                'steps.*.order' => 'required|numeric|min:1',
            ]);
            try{
                $data = $request->all();
                
                $res = Procedure::remoteUpdate( $id, $data );

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');                

                    return redirect()->route('procedures');
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "procedures", "edit");
            }
        }

        try{
            $procedure  = Procedure::remoteFind($id);
            $steps      = Step::remoteFindAll(['active' => 1]);
            $templates  = $this->template->getAll($request->session()->get('institution')->url);
            $modalities = Modality2::remoteFindAll(['active' => 1]);
            $equipment  = Equipment::remoteFindAll(['active' => 1]);
            $consumables = $this->consumable->getAll($request->session()->get('institution')->url);

            return view('procedures.edit', compact('procedure', 'steps', 'templates', 'modalities', 'equipment', 'consumables'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "procedures", "edit");
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = Procedure::remoteToggleActive( $id );

            if ( isset( $res->error ) ) {
                return $this->parseFormErrors( $data, $res );
            } else {
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('procedures');
            }
        }catch( \Exception $e ){
            return $this->logError( $e, "procedures", "active");
        }
    }

     public function form( Request $request, $position, $procedure, $duration = null ){

        try{
            $procedure = Procedure::remoteFind($procedure);
            if( $duration != null ){
                return view('procedures.form', [ 'position' => $position, 'procedure' => $procedure, 'duration' => $duration ]);
            }

            return view('procedures.form', [ 'position' => $position, 'procedure' => $procedure ]);
        }catch( \Exception $e ){
            return $this->logError( $e, "procedures", "form");
        }
    }

    /*
    public function delete( Request $request, $id ){
        try{
            $res = $this->procedure->remove($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                Activity::log(trans('tracking.delete-api', [ 'section' => 'procedures', 'id' => $id, 'id-institution' => $request->session()->get('institution')->id ]));

                $request->session()->flash('message', trans('alerts.success-delete'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('procedures');

        }catch( \Exception $e ){
            return $this->logError( $e, "procedures", "delete");
        }
    }

    public function modalities( Request $request, $id ){
        try{
            $procedure = $this->procedure->get($request->session()->get('institution')->url, $id);

            return $procedure->getModalities($request->session()->get('institution')->url, $id);
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: procedures. Action: modalities');

            return response()->json([ 'error' => $e->getCode(), 'message' => $e->getMessage() ]);
        }
    }

   */
}
