<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\NotificationTemplate;
use App\Configuration;
use App\BiRad;
use Activity;
use Log;


class ConfigurationsController extends Controller {
    public function __construct(){
        $this->logPath = '/logs/admin/admin.log';
    }

     public function rules( $unique = null ) {
        return [
            'appointment_sms_notification' => 'required|numeric|min:1',
            //'birthday_expression' => 'required|max:45',
            'birad_1' => 'required|numeric|min:1',
            'birad_2' => 'required|numeric|min:1',
            'birad_3' => 'required|numeric|min:1',
            'birad_4' => 'required|numeric|min:1',
            'birad_5' => 'required|numeric|min:1',
            'birad_6' => 'required|numeric|min:1',
            'birad_7' => 'required|numeric|min:1',
            'birad_8' => 'required|numeric|min:1',
            'birad_9' => 'required|numeric|min:1',
            'mammography_expression' => 'required|max:45',
            'mammography_reminder' => 'required|numeric|min:1',
            'appointment_sms_expression' => 'required|max:45',
            'appointment_sms_template_id' => 'required_with:appointment_sms', 
            'appointment_email_template_id' => 'required_with:appointment_email',
            'results_sms_template_id' => 'required_with:results_sms',
            'results_email_auto_template_id' => 'required_with:results_email_auto',
            'mammography_email_template_id' => 'required_with:mammography_email',
            'mammography_sms_template_id' => 'required_with:mammography_sms',
            'active_poll' => 'boolean',
            'url_poll' => 'url',
        ];
    }

    /**
     * @fecha 25-11-2016
     * @programador Juan Bigorra
     * @objetivo Renderiza la vista Edit de la sección Configurations.
     * @param Request $request
     * @param $id ID de la institución.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit( Request $request, $id ) {
        if( $request->isMethod('post') ) {
            $this->validate( $request, $this->rules( $id ) );
            try {
                $data = $request->all();
                if(!isset($data['active_poll'])){
                    $data['active_poll'] =0;
                }
                elseif($data['url_poll']  ==""){
                    $request->session()->flash('message', trans('messages.error-url-poll'));
                    $request->session()->flash('class', 'alert alert-danger');
    
                    return redirect()->route('configurations.edit', [ $id ]); 
                }
                $res = Configuration::remoteUpdate( 1, $data );

                if( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                }
    
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('configurations.edit', [ $id ]);
            } catch( \Exception $e ) {
                return $this->logError( $e, "configuration", "edit");
            }
        }

        try {
            $configuration = Configuration::remoteFind(1);
            $notificationTemplates = NotificationTemplate::remoteFindActive();
            $birads = BiRad::remoteFindAll();
            
            return view('configurations.edit', compact('configuration','notificationTemplates','birads'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "configuration", "edit");
        }
    }

    /**
     * @fecha 30-11-2017
     * @programador Alton Bell Smythe
     * @objetivo Pseudo Routing for web.php with params and work like static
     */
    public function configurations() {
        return redirect()->route('configurations.edit', [1]);
    }
}