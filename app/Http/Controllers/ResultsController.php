<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Result;
use App\ServiceRequest;
use App\Deliver;
use App\RequestedProcedure;
use Log;
use Activity;
use Illuminate\Support\MessageBag;

class ResultsController extends Controller {
    public function __construct(){
        $this->result = new Result();
        $this->serviceRequest = new ServiceRequest();
        $this->deliver = new Deliver();
    }

    public function index( Request $request, $print = null ){
        try{
            if( $request->isMethod('post') ){
                if( $print == 1 ){

                    $fileName = $request->all()['filename'];
                    $contentType = $request->all()['contentType'];
                    $base64 = $request->all()['base64'];

                    $data = base64_decode($base64);

                    header('Content-Type:' . $contentType);
                    header('Content-Length:' . strlen($data));
                    header('Content-Disposition: attachment; filename=' . $fileName);

                    echo $data;

                }else{
                    $data = $this->result->indexRemoteData($request->session()->get('institution')->url, $request->all());
                    echo json_encode($data, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }

            return view('results.index');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/results/results.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: results. Action: index');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'receptor_id' => 'required',
                'receptor_name' => 'required'
            ]);
            try{
                $data = $request->all();
                $data['user_id'] = \Auth::user()->id;
                $data['responsable_id'] = \Auth::user()->id;
                $data['responsable_name'] = \Auth::user()->first_name . ' ' . \Auth::user()->last_name;
                $data['api_token'] = \Auth::user()->api_token;
                $data['requested_procedure_id'] = $id;
                $data['date'] = date('Y-m-d H:i:s');

                $res = $this->deliver->add($request->session()->get('institution')->url, $data);
                
                if( $res->getStatusCode() == 200 ){
                    if( isset( json_decode($res->getBody())->error ) ){
                        $mb = new MessageBag();
                        foreach( $data as $key => $value ){
                            if( strpos(strtoupper(json_decode($res->getBody())->message), strtoupper($key)) !== false ){
                                $mb->add($key, trans('errors.' . json_decode($res->getBody())->error, [ 'attribute' => trans('errors.attributes.' . $key) ]));
                            }
                        }

                        return redirect()->back()->withErrors($mb)->withinput();
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create-api', [ 'section' => 'results', 'id' => json_decode($res->getBody())->id, 'id-institution' => $request->session()->get('institution')->id ]));

                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');
                }

                return redirect()->back();
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/results/results.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: results. Action: edit');

                return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            }
        }

        try{
            $requestedProcedure = RequestedProcedure::remoteFind( $id );
            return view('results.edit', compact('requestedProcedure'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/results/results.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: results. Action: edit');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }
}
