<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AlertMessage;
use App\AlertMessageType;
use App\AlertMessagesUser;
use App\Institution;

use App\Http\Requests;
use Activity;
use Log;
use Session;
use Carbon\Carbon;
use DB;

class AlertMessagesController extends Controller {

    /**
     * @fecha 02-02-2017
     * @programador Mercedes Rodriguez
     * @objetivo Renderiza la vista index de la sección Mensajes de alerta.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try{
            $alertmessages = AlertMessage::all();
            $alertmessages->load('alertMessageType');

            return view('alertMessages.index', compact('alertmessages'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: alert messages. Action: index');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    /**
     * @fecha 02-02-2017
     * @programador Mercedes Rodriguez
     * @objetivo Renderiza la vista add de la sección Mensajes de alerta.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function add( Request $request ){
        try{
            if( $request->isMethod('post') ){

                //Format de fechas para que puedan ser guardadas en DB
                $request["start_date"] = date('Y-m-d H:i:s', strtotime($request->start_date));
                $request["end_date"] = date('Y-m-d H:i:s', strtotime($request->end_date));

                $alertmessage = new AlertMessage($request->all());
                $alertmessage->save();

                //Log Activity
                Activity::log(trans('tracking.create', [ 'section' => 'alertmessages', 'id' => $alertmessage->id ]));

                $request->session()->flash('message', trans('alerts.success-add'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('alertMessages');
            }

            $institutions = Institution::all('id', 'name'); //Todas las instituciones para el select
            $alertmessagetypes = AlertMessageType::all('id', 'name');//Todos los tipos de mensajes de alerta para el select

            return view('alertMessages.add', compact('institutions', 'alertmessagetypes'));

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: alertmessages. Action: add');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    /**
     * @fecha 07-02-2017
     * @programador Mercedes Rodriguez
     * @objetivo Renderiza la vista edit de la sección Mensajes de alerta.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit( Request $request, AlertMessage $alertmessage ){
        try{
            if( $request->isMethod('post') ){

                //Format de fechas para que puedan ser guardadas en DB
                $request["start_date"] = date('Y-m-d H:i:s', strtotime($request->start_date));
                $request["end_date"] = date('Y-m-d H:i:s', strtotime($request->end_date));

                $alertmessage->update($request->all());

                //Log activity
                Activity::log(trans('tracking.create', [ 'section' => 'alertmessages', 'id' => $alertmessage->id ]));

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('alertMessages');
            }

            $institutions = Institution::all('id', 'name'); //Todas las instituciones para el select
            $alertmessagetypes = AlertMessageType::all('id', 'name');//Todos los tipos de mensajes de alerta para el select

            return view('alertMessages.edit', compact('alertmessage', 'institutions', 'alertmessagetypes'));

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: alertmessages. Action: edit');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function show( Request $request ){
        try{
            if( $request->isMethod('post') ) $alertmessages = $this->getCurrentMessagesUser();

            return json_encode($alertmessages);
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: alertmessages. Action: show');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function rewrite( Request $request ){
        try{
            if( $request->isMethod('post') ) $alertmessages = $this->getNoReadMessages();

            return json_encode($alertmessages);
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: alertmessages. Action: rewrite');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    /**
     * @fecha: 09-03-2017
     * @programador: Mercedes Rodriguez
     * @objetivo: Obtener los mensajes de alerta que van a ser pintados
     */
    public function getCurrentMessagesUser(){
        try{

            $today = Carbon::now()->toDateTimeString();
            $currentId = Session::get('institution')->id;
            $institutionsIds = [ 0, $currentId ];
            $userId = \Auth::user()->id;

            $noReadMessages = 0;

            $seenMessages = AlertMessagesUser::whereIn('institution_id', $institutionsIds)->where('user_id', $userId)->pluck('alert_message_id')->toArray();


            $alertMessages = AlertMessage::getCurrentMessages();
            if(!empty($alertMessages)){
                foreach( $alertMessages as &$message ){
                    if( !in_array($message->id, $seenMessages) ){
                        $message->new = 1;
                        $noReadMessages += 1;
                    }else{
                        $message->new = 0;
                    }
                }
            }

            return [ 'messages' => $alertMessages, 'noReadMessages' => $noReadMessages ];
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: alertmessages. Action: getCurrentMessagesUser');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    /**
     * @fecha: 16-03-2017
     * @programador: Mercedes Rodriguez
     * @objetivo: Marcar mensajes como leidos
     */
    public function lock( Request $request ){
        try{
            if( $request->isMethod('post') ){
                $currentId = Session::get('institution')->id;
                $userId = \Auth::user()->id;
                $idMessages = $request->all();

                foreach( $idMessages as $key => $idMessage ){
                    $readMessage = AlertMessagesUser::create([ 'institution_id' => $currentId, 'alert_message_id' => $idMessage, 'user_id' => $userId ]);
                }
            }

            return 1;

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: alertmessages. Action: lock');

            return 0;
        }


    }

}
