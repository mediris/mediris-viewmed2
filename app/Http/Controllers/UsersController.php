<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\User;
use App\Oldrecordsusuario;
use App\Suffix;
use App\Prefix;
use App\Modality2;
use App\Role;
use App\Institution;
use App\Charge;
use Activity;
use Log;
use Auth;
use DB;

class UsersController extends Controller {

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Users.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(){

        try{

            $users = User::all();

            return view('users.index', compact('users'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: users. Action: index');

            return view('errors.index', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    public function show( User $user ){

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show',
            [ 'section' => 'user', 'id' => $user->id ]));

        $user->load('prefix');
        $user->load('suffix');

        return $user;
    }

    public function add( Request $request ){

        if( $request->isMethod('post') ){

            $this->validate($request, [
                'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'administrative_ID' => 'required|unique:users|max:45',
                'username' => 'required|max:25|unique:users',
                'password' => 'required|min:6|confirmed',
                'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
                'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
                'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
                'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
                'email' => 'required|email|max:255|unique:users',
                'prefix_id' => 'required',
                'suffix_id' => 'required',
            ]);
            try{
                $data = $request->all();

                unset( $data['password_confirmation'] );
                
                $data['api_token'] = str_random(64);
                $data['password'] = bcrypt($data['password']);
                $user = new User($data);
                $user->saveSignature( $request->signature );

                if( $user->save() ) {
                    $institutions = Institution::all();

                    for( $i = 0; $i < count($data['roles']); $i++ )
                        if( $data['roles'][$i] != 0 )
                            Charge::create([ 'user_id' => $user->id, 'role_id' => $data['roles'][$i], 'institution_id' => $institutions[$i]->id ]);

                    $institutions = $request->session()
                        ->get('institutions');

                    foreach( $institutions as $institution ){
                        $user->createToken($institution->url);
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create',
                        [ 'section' => 'user', 'id' => $user->id ]));

                    $request->session()
                        ->flash('message', trans('alerts.success-add'));
                    $request->session()
                        ->flash('class', 'alert alert-success');
                }else{

                    /**
                     * Log activity
                     */
                     dd('fail');
                    Activity::log(trans('tracking.attempt',
                        [ 'section' => 'user', 'action' => 'create' ]));

                    $request->session()
                        ->flash('message', trans('alerts.error-add'));
                    $request->session()
                        ->flash('class', 'alert alert-danger');
                }

                return redirect()->route('users');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: users. Action: add');

                return view('errors.index', [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString(),
                ]);
            }
        }

        try{
            $roles = Role::orderBy('name', 'asc')->get();
            $prefixes = Prefix::where('language', \App::getLocale())
                ->orderBy('name', 'asc')
                ->get();
            $suffixes = Suffix::where('language', \App::getLocale())
                ->orderBy('name', 'asc')
                ->get();

            $institutions = Institution::all();

            return view('users.add', [
                'roles' => $roles,
                'prefixes' => $prefixes,
                'suffixes' => $suffixes,
                'institutions' => $institutions,
            ]);

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: users. Action: add');

            return view('errors.index', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    public function edit( Request $request, User $user ){
        if( $request->isMethod('post') ){
            $this->validate($request, [
                'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'administrative_ID' => 'required|unique:users,administrative_ID,' . $user->id . '|max:45',
                'username' => 'required|max:25|unique:users,username,' . $user->id,
                'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
                'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
                'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
                'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
                'email' => 'required|email|max:255|unique:users,email,' . $user->id,
                'prefix_id' => 'required',
                'suffix_id' => 'required',
            ]);
            try{
                //dd($request->all());
                $original = new User();
                foreach( $user->getOriginal() as $key => $value ){
                    $original->$key = $value;
                }

                $original->load('prefix');
                $original->load('suffix');
                
                $updated_user = $request->all();
                $roles = $request->input('roles');
                unset( $updated_user['roles'] );
                $user->active = 0;

                try {

                    $user->saveSignature( $updated_user['signature'] );
                    unset($updated_user['signature']);

                    if( $user->update($updated_user) ){

                        $user->load('prefix');
                        $user->load('suffix');
                        
                        $institutions = Institution::all();

                        for( $i = 0; $i < count($roles); $i++ ){
                            $charge = Charge::where([ [ 'user_id', $user->id ], [ 'institution_id', $institutions[$i]->id ] ])->get();

                            if( count($charge) > 0 ){
                                if( $roles[$i] == 0 )
                                    Charge::where([ [ 'user_id', $user->id ], [ 'institution_id', $institutions[$i]->id ] ])->delete();
                                elseif( $charge[0]->role_id != $roles[$i] )
                                    Charge::where([ [ 'user_id', $user->id ], [ 'institution_id', $institutions[$i]->id ] ])->update([ 'role_id' => $roles[$i] ]);
                            }else{
                                if( $roles[$i] != 0 )
                                    Charge::create([ 'user_id' => $user->id, 'role_id' => $roles[$i], 'institution_id' => $institutions[$i]->id ]);
                            }
                        }
                        /**
                         * Log activity
                         */

                        Activity::log(trans('tracking.edit', [
                            'section' => 'user',
                            'id' => $user->id,
                            'oldValue' => $original,
                            'newValue' => $user,
                            'action' => 'edit',
                        ]));

                        $request->session()
                            ->flash('message', trans('alerts.success-edit'));
                        $request->session()
                            ->flash('class', 'alert alert-success');
                    }else{

                        /**
                         * Log activity
                         */

                        Activity::log(trans('tracking.attempt-edit', [
                            'id' => $user->id,
                            'section' => 'user',
                            'action' => 'edit',
                        ]));


                        $request->session()
                            ->flash('message', trans('alerts.error-edit'));
                        $request->session()
                            ->flash('class', 'alert alert-danger');
                    }
                }catch( \Exception $e ){
                    Log::useFiles(storage_path() . '/logs/admin/admin.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: users. Action: edit');

                    return view('errors.index', [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'trace' => $e->getTraceAsString(),
                    ]);
                }


                return redirect()->route('users');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: users. Action: edit');

                return view('errors.index', [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString(),
                ]);
            }
        }

        try{
            $user->load('prefix');
            $user->load('suffix');
            $roles = Role::orderBy('name', 'asc')->get();
            $prefixes = Prefix::where('language', \App::getLocale())
                ->orderBy('name', 'asc')
                ->get();
            $suffixes = Suffix::where('language', \App::getLocale())
                ->orderBy('name', 'asc')
                ->get();

            $institutions = Institution::all();
        
            $charges = DB::table('institutions')
                ->select('charges.role_id', 'institutions.id', 'institutions.name')
                ->leftJoin(DB::raw('(SELECT * FROM charges WHERE user_id = ' . $user->id . ') AS charges'), function ( $join ){
                    $join->on('charges.institution_id', '=', 'institutions.id');
                })
                ->orderBy('institutions.id', 'ASC')
                ->get();

            return view('users.edit', [
                'user' => $user,
                'roles' => $roles,
                'prefixes' => $prefixes,
                'suffixes' => $suffixes,
                'institutions' => $institutions,
                'charges' => $charges,
                'allowKillSessions' => true,
            ]);
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: users. Action: edit');

            return view('errors.index', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    public function change_password( Request $request, User $user ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
            ]);
            try{
                    $user->password = bcrypt($request->all()['password']);
                    $user->update();

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.change-password', [
                        'section' => 'user',
                        'id' => $user->id,
                        'action' => 'change_password',
                    ]));

                    $request->session()
                        ->flash('message',
                            trans('alerts.success-edit-password'));
                    $request->session()->flash('class', 'alert alert-success');
                    return redirect()->route('home');
                }catch( \Exception $e ){
                    Log::useFiles(storage_path() . '/logs/admin/admin.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: users. Action: change password');

                return view('errors.index', [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString(),
                ]);
            }
        }

    }


    public function active( Request $request, User $user ){

        try{
            $user->active();
            $original = new User();

            foreach( $user->getOriginal() as $key => $value ){
                $original->$key = $value;
            }

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', [
                'section' => 'users',
                'id' => $user->id,
                'oldValue' => $original,
                'newValue' => $user,
                'action' => 'active',
            ]));

            $request->session()
                ->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

            return redirect()->route('users');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: users. Action: active');

            return view('errors.index', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);
        }

    }

    public function info( User $user ){
        return $user->first_name . " " . $user->last_name;
    }

    /**
     * @fecha 06-08-2017
     * @programador Hendember Heras
     * @objetivo Returns the signature image extracted from the storage
     */
    public function signature( User $user ) {
        if (!Storage::exists($user->signature)) {
            abort(404);
        }

        $absolutepath = Storage::getDriver()->getAdapter()->applyPathPrefix( $user->signature );
        $type = Storage::mimeType($user->signature);
        
        $headers = [
            'Content-Type' => $type
        ];

        return response()
            ->file($absolutepath, $headers);
    }

    /**
     * @fecha 30-03-2018
     * @programador Hendember Heras
     * @objetivo Returns the signature image extracted from the storage
     */
    public function legacySignature( Oldrecordsusuario $user ) {
        if (!Storage::exists($user->firma)) {
            abort(404);
        }

        $absolutepath = Storage::getDriver()->getAdapter()->applyPathPrefix( $user->firma );
        $type = Storage::mimeType($user->firma);
        
        $headers = [
            'Content-Type' => $type
        ];

        return response()
            ->file($absolutepath, $headers);
    } 
}
