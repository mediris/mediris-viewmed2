<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modality2;
use Activity;
use Log;
use App\Http\Requests;
use App\Http\Requests\ModalityAddRequest;

class ModalitiesController extends Controller {
    public function __construct(){
        $this->logPath = '/logs/admin/admin.log';
    }

    /**
     * @fecha 25-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Modalities.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try{

            $modalities = Modality2::remoteFindAll();

            return view('modalities.index', compact('modalities'));
        }catch( \Exception $e ) {
            return $this->logError( $e, "modalities", "index");
        }

    }

    public function add( Request $request ){

        if( $request->isMethod('post') ){

            $this->validate($request, [
                'name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:10',
                'parent_id' => 'required',
            ]);
            try{
                $data = $request->all();

                // Se agrego el campo requires_additional_fields para que se incluyan en cita la presentación de los campos "Peso, Estatura, Circunferencia". Ricmary Bron. 11/2017.
                if (array_key_exists('requires_additional_fields', $data )) {
                    $data ['requires_additional_fields'] = 1;
                }

                $res = Modality2::remoteCreate($data);

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');
            
                    return redirect()->route('modalities');
                }
            }catch( \Exception $e ){
                return $this->logError( $e, "modalities", "add");
            }
        }
        try{
            $modalities = Modality2::remoteFindAll();

            return view('modalities.add', compact('modalities'));
        }catch( \Exception $e ){
            return $this->logError( $e, "modalities", "add");
        }
    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){

            $this->validate($request, [
                'name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:10',
                'parent_id' => 'required',
            ]);
            try{
                $data = $request->all();
                
                // Se agrego el campo requires_additional_fields para que se incluyan en cita la presentación de los campos "Peso, Estatura, Circunferencia". Ricmary Bron. 11/2017.
                if (array_key_exists('requires_additional_fields', $data )) {
                    $data ['requires_additional_fields'] = 1;
                }
                else
                { 
                    $data ['requires_additional_fields'] = 0;
                }

                $res = Modality2::remoteUpdate( $id, $data );
                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');

                    return redirect()->route('modalities');
                }
            }catch( \Exception $e ){
                return $this->logError( $e, "modalities", "edit");
            }
        }
        try {
            $modality = Modality2::remoteFind($id);
            $modalities = Modality2::remoteFindAll();
            return view('modalities.edit', [ 'modality' => $modality, 'modalities' => $modalities ]);

        } catch( \Exception $e ) {
            return $this->logError( $e, "modalities", "edit");
        }

    }

    public function active( Request $request, $id ){
        try{
            $res = Modality2::remoteToggleActive( $id );

            if ( isset( $res->error ) ) {
                return $this->parseFormErrors( $data, $res );
            } else {
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('modalities');
            }
        }catch( \Exception $e ){
            return $this->logError( $e, "modalities", "active");
        }
    }
}
