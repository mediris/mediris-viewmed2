<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\Step;
use App\Equipment;
use App\Consumable;


class StepsController extends Controller {
    public function __construct(){
        $this->logPath = '/logs/admin/admin.log';

        $this->consumable = new Consumable();
    }

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Steps.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */

    public function index( Request $request ){
        try{
            $steps = Step::remoteFindAll();

            return view('steps.index', compact('steps'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "steps", "index");
        }
    }

    public function add( Request $request ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'indications' => 'required',
                'administrative_ID' => 'required|max:45|unique_test:steps',
            ]);

            try {
                $data = $request->all();
                $res = Step::remoteCreate($data);

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');
            
                    return redirect()->route('steps');
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "steps", "add");
            }
        }

        try{
            $equipment   = Equipment::remoteFindAll(['active' => 1]);
            $consumables = $this->consumable->getAll($request->session()->get('institution')->url);

            return view('steps.add', [ 'equipment' => $equipment, 'consumables' => $consumables ]);
        } catch( \Exception $e ) {
            return $this->logError( $e, "steps", "add");
        }
    }

    public function form( Request $request, $position, $random, $step = null ){
        try{
            $equipment   = Equipment::remoteFindAll(['active' => 1]);
            $consumables = $this->consumable->getAll($request->session()->get('institution')->url);

            if( $step == null ){
                return view('steps.form', [ 'position' => $position, 'equipment' => $equipment, 'consumables' => $consumables, 'random' => $random ]);
            }else{
                $step = Step::remoteFind($step);

                return view('steps.form_selected', [ 'position' => $position, 'equipment' => $equipment, 'consumables' => $consumables, 'step' => $step, 'random' => $random ]);
            }
        }catch( \Exception $e ){
            return $this->logError( $e, "steps", "add");
        }
    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'indications' => 'required',
                'administrative_ID' => 'required|max:45|unique_test:steps,' . $id,
            ]);
            try{
                $data = $request->all();
                
                $res = Step::remoteUpdate($id, $data);

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');
            
                    return redirect()->route('steps');
                }
            }catch( \Exception $e ){
                return $this->logError( $e, "steps", "edit");
            }
        }

        try{
            $step = Step::remoteFind($id);
            $equipment   = Equipment::remoteFindAll(['active' => 1]);
            $consumables = $this->consumable->getAll($request->session()->get('institution')->url);

            return view('steps.edit', [ 'step' => $step, 'equipment' => $equipment, 'consumables' => $consumables ]);
        }catch( \Exception $e ){
            return $this->logError( $e, "steps", "edit");
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = Step::remoteToggleActive( $id );

            if ( isset( $res->error ) ) {
                return $this->parseFormErrors( $data, $res );
            } else {
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('steps');
            }
        }catch( \Exception $e ){
            return $this->logError( $e, "steps", "active");
        }
    }
}
