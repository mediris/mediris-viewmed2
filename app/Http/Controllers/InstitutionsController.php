<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Institution;
use App\Group;
use Activity;
use Log;
use Form;
use HTML;
use File;

class InstitutionsController extends Controller {

    /**
     * @fecha 25-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Institutions.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){

        try{
            $institutions = Institution::all();
            $institutions->load('group');
            foreach ($institutions as $institution) {
                $institution->group_name = $institution->group->name;
            }

            return view('institutions.index', compact('institutions'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: institutions. Action: index');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function show( Institution $institution ){

        /**
         * Log activity
         */

        $institution->load('group');
        Activity::log(trans('tracking.show', [ 'section' => 'institution', 'id' => $institution->id ]));

        return $institution;
    }

    public function add( Request $request ){


        if( $request->isMethod('post') ){

            $this->validate($request, [
                'name' => 'required|unique:institutions|max:50',
                'address' => 'required|max:250',
                'administrative_ID' => 'required|unique:institutions|max:45',
                'group_id' => 'required',
                //'url' => 'required|unique:institutions|max:250|url',
                'url' => 'required|max:250|url',
                //'email' => 'required|email|max:255|unique:institutions|regex:/^[\w._-]+@[a-zA-Z]+.[a-z]+.[a-z]*$/',
                'email' => 'required|email|max:255|regex:/^[\w._-]+@[a-zA-Z]+.[a-z]+.[a-z]*$/',
                'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
                'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
                'institution_id' => 'required|unique:institutions|max:45',
                'url_pacs' => 'required|max:250|url',
                'url_external_api' => 'max:250|url',
                //'logo_actives_report'=> 'boolean',
                'base_structure' => 'required|max:30',
            ]);
            try{
                $data = $request->all();
                // if(!isset($data['logo_actives_report'])){
                //     $data['logo_actives_report'] = 0;
                // }

                if( substr($data['url'], -1) != '/' ){
                    $data['url'] = $data['url'] . '/';
                }

                $data['active'] = 1;
                $institution = new Institution($data);

                $institution->saveLogo( $data['logo'], 'logo' );
                unset($data['logo']);

                $institution->saveLogo( $data['email-img'], 'email-img' );
                unset($data['email-img']);

                $institution->saveLogo( $data['report-header'], 'report-header' );
                unset($data['report-header']);

                $institution->saveLogo( $data['report-footer'], 'report-footer' );
                unset($data['report-footer']);

                $institution->save();

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.create', [ 'section' => 'institution', 'id' => $institution->id ]));

                $request->session()->flash('message', trans('alerts.success-add'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('institutions');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: institutions. Action: add');

                return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            }
        }

        try{
            $groups = Group::orderBy('name', 'asc')->get();

            return view('institutions.add', compact('groups'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: institutions. Action: add');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function edit( Request $request, Institution $institution ){


        if( $request->isMethod('post') ){
            $this->validate($request, [
                'name' => 'required|unique:institutions,name,' . $institution->id . '|max:50',
                'address' => 'required|max:250',
                'administrative_ID' => 'required|unique:institutions,administrative_ID,' . $institution->id . '|max:45',
                'group_id' => 'required',
                //'url' => 'required|unique:institutions,url,' . $institution->id . '|max:250|url',
                'url' => 'required|max:250|url',
                //'email' => 'required|email|max:255|unique:institutions,email,' . $institution->id,
                'email' => 'required|email|max:255',
                'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
                'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
                'institution_id' => 'required|unique:institutions,institution_id,' . $institution->id . '|max:45',
                'url_pacs' => 'required|max:250|url',
                'url_external_api' => 'max:250|url',
                //'logo_actives_report'=> 'boolean',
                'base_structure' => 'required|max:30',
            ]);

            try{

                $original = new Institution();
                foreach( $institution->getOriginal() as $key => $value ){
                    $original->$key = $value;
                }
                $data = $request->all();
                
                if( substr($data['url'], -1) != '/' ){
                    $data['url'] = $data['url'] . '/';
                }

                $institution->saveLogo( $data['logo'], 'logo' );
                unset($data['logo']);

                $institution->saveLogo( $data['email-img'], 'email-img' );
                unset($data['email-img']);

                $institution->saveLogo( $data['report-header'], 'report-header' );
                unset($data['report-header']);

                $institution->saveLogo( $data['report-footer'], 'report-footer' );
                unset($data['report-footer']);
                
                // if(!isset($data['logo_actives_report'])){
                //     $data['logo_actives_report'] = 0;
                // }
                $institution->update($data);

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit', [ 'section' => 'institution', 'id' => $institution->id, 'oldValue' => $original, 'newValue' => $institution ]));

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                $institutions = \Auth::user()->getAllinstitutions();
                $request->session()->forget('institutions');
                $request->session()->put('institutions', $institutions);
                
                
                return redirect()->route('institutions');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: institutions. Action: edit');

                return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            }
        }

        try{
            $groups = Group::orderBy('name', 'asc')->get();

            return view('institutions.edit', [ 'institution' => $institution, 'groups' => $groups ]);
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: institutions. Action: edit');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }

    }

    public function delete( Institution $institution, Request $request ){

        try{
            $institution->delete();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.delete', [ 'id' => $institution->id, 'section' => 'institution' ]));

            $request->session()->flash('message', trans('alerts.success-delete'));
            $request->session()->flash('class', 'alert alert-success');


            return redirect()->route('institutions');
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: institutions. Action: delete');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function select( Institution $institution = null, Request $request ){

        try{
            if( $institution->id == null ){
                if( $request->session()->has('institution') ){
                    return redirect('/');
                }else{
                    if( $request->session()->has('institutions') ){
                        $institutions = $request->session()->get('institutions');
                    }else{
                        $institutions = \Auth::user()->getAllinstitutions();
                        $request->session()->put('institutions', $institutions);
                    }

                    return view('institutions.select', compact('institutions'));
                }
            }else{

                if( !$request->session()->has('roles') ){
                    $rol = \Auth::user()->getRoleByInstitution($institution->id);

                    $permission = \Auth::user()->getPermissions($rol[0]->id);
                    $menu       = \Auth::user()->getMenu($rol[0]->id);

                    $request->session()->put('roles', $rol);
                    $request->session()->put('permissions', $permission);
                    $request->session()->put('menu', $menu);
                }

                $request->session()->forget('institution');
                $request->session()->put('institution', $institution);

                $rolId = $rol[0]['id'];

                if($rolId == '5' || $rolId == '13'){
                    return redirect()->route("radiologist.OrdenesDictar");
                }else{
                    return redirect()->route("/");
                }

            }
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: institutions. Action: select');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function active( Request $request, Institution $institution ){

        try{
            $institution->active();
            $original = new Institution();
            foreach( $institution->getOriginal() as $key => $value ){
                $original->$key = $value;
            }

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', [ 'section' => 'institutions', 'id' => $institution->id, 'oldValue' => $original, 'newValue' => $institution, 'action' => 'active' ]));

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

            return redirect()->route('institutions');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: institutions. Action: active');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }

    }
}
