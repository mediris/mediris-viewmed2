<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PreAdmission;
use App\Patient;
use App\ServiceRequest;

class PreAdmissionControllerAPI extends Controller {
    
    public function __construct() {
        $this->middleware('auth:api');
        $this->logPath = '/logs/preadmission/preadmission.log';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        try{
            $institutions = \App\Institution::all();

            $ret = [];
            $index = 0;
            $cont = 1;
            
            $data = ["page" => 0];
            foreach ( $institutions as $institution ) {
                $res = PreAdmission::indexRemoteData( $data, $institution );


                for ($i=0; $i<count($res->data); $i++) {
                    $element = $res->data[$i];
                    
                    if ( !isset( $ret[$index]['cabecera']['NUMERO_INGRESO'] ) ||
                          $ret[$index]['cabecera']['NUMERO_INGRESO'] != $element->serviceRequestID || 
                          $ret[$index]['cabecera']['LOCALIDAD'] != $institution->id ) {

                        $patient = Patient::find( $element->patientID );
                        $patient->load('country', 'sex');
                        $nacionalidad = ($patient->country->iso_3166_2 == 'VE')?'V':'E';
                        
                        $ret[]['cabecera'] =
                        [
                            //'LOCALIDAD'         => $institution->id,
                            'LOCALIDAD'         => $institution->administrative_id,
                            'NUMERO_INGRESO'    => $element->serviceRequestID,
                            'TIPO_IDENTIF'      => 'CI',                    //Data con longitud de 2 dígitos, Ejemplo: 'CI'
                            'NRO_DOCUM_IDENT'   => $element->patientIdentificationID,
                            'NOMBRE_PACIENTE'   => $element->patientName,
                            'NACIONALIDAD'      => $nacionalidad,           //Longitud de 1 Digito, ejemplo: 'V'
                            'FECHA_NACIMIENT'   => $patient->birth_date,
                            'DIRECCION'         => $patient->address,
                            'TELEFONOS'         => $patient->cellphone_number,
                            'SEXO'              => $patient->sex->administrative_ID,//debe ser 'M' o 'F'
                            'CODIGO_REMITENTE'  => $element->referringID,
                            'NOMBRE_REMITENTE'  => $element->referringName,
                            'TIPO_PACIENTE'     => $element->patientTypeAID,//Longitud de 2 Dígitos
                            'STATUS'            => $element->orderStatusID,
                        ];

                        $index = count($ret)-1;    
                    }


                    $ret[$index]['detalle'][] =
                    [
                        //'LOCALIDAD'         => $institution->id,        //el código de la institución
                        'LOCALIDAD'         => $institution->administrative_id,        //el código de la institución
                        'NUMERO_ORDEN'      => $element->orderID,       //nro de orden
                        'TIPO_REGISTRO'     => 'E',                     //tipo de registro (E=estudio, I=insumo)
                        'COD_INSUM_ESTUD'   => $element->procedureAID,  //Este trata del código administrativo del procedimiento
                        'CANTIDAD'          => 1,                       //viene dada de la cantidad que indique la orden, en general 
                                                                        //debería ser 1, pero se pide porque el ris
                                                                        //actual espera solicitar insumos utilizados
                        'UNIDAD_MEDIDA'     => 1,                       //Mediris no utiliza unidad de medida, usar 1 como código
                        'CODIGO_MEDICO'     => NULL,                    //id radiologo que realiza informe, si no se tiene enviar NULL
                        'NOMBRE_MEDICO'     => NULL,            
                        'CODIGO_TECNICO'    => NULL,                    //id técnico que realiza estudio, si no se tiene enviar NULL
                        'NOMBRE_TECNICO'    => NULL,            
                        'NUMERO_ACCION'     => $cont++,                 //un consecutivo, y que desde mediris se puede indicar 
                                                                        //cuantas ordenes se han enviado
                        'STATUS_DETALLE'    => $element->orderStatusID, //estado en que está la orden
                    ];
                }
            }

            return response()->json( $ret );
        } catch( \Exception $e ) {
            return $this->logError( $e, "preAdmission API", "index");
        }
    }

    public function approve(Request $request) {
        $id = $request->all();
        try{
            //$institution = \App\Institution::find( $id['LOCALIDAD'] );
            $institution = \App\Institution::where('administrative_ID',$id['LOCALIDAD'])->first();

            $res = ServiceRequest::remoteFind($id['NUMERO_INGRESO'], $array = [], $institution );
            $response = $res->approve( $institution );

            
            return response()->json([ 'success'=> 'true', 'error' => '']);

        } catch( \Exception $e ) {

            if($e->getCode() == 404){
                return response()->json([ 'success'=> 'false', 'error' => 'no serviceRequest found']);
            }else{
                return response()->json([ 'success'=> 'false', 'error' => $e->getMessage()]);
            }

        }
    }
}