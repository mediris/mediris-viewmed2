<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RoomRequest;
use App\Http\Requests\RoomEditRequest;
use Log;
use Activity;
use App\Room;
use App\Division;
use App\Procedure;
use App\Modality2;

class RoomsController extends Controller {
    public $blockSizes = [
        ['5', '5 min'],
        ['10', '10 min'],
        ['15', '15 min'],
        ['30', '30 min'],
        ['60', '1 hr'],
    ];

    public function __construct() {
        $this->logPath = '/logs/admin/admin.log';
        
        $this->division = new Division();
    }

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Rooms.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try{
            $rooms = Room::remoteFindAll();

            return view('rooms.index', compact('rooms'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "rooms", "index");
        }
    }


    public function add( Request $request ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'name'              => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:50',
                'description'       => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:250',
                'administrative_ID' => 'required|unique_test:rooms|max:45',
                'division_id'       => 'required',
                'start_hour'        => 'required',
                'end_hour'          => 'required',
                'block_size'        => 'required',
                'quota'             => 'required|numeric|min:1',
                'equipment_number'  => 'required|numeric|min:1',
                'procedures'        => 'required'
            ]);

            try {
                $data = $request->all();
                
                $res = Room::remoteCreate($data);

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');                

                    return redirect()->route('rooms');
                }

                return redirect()->route('rooms');
            } catch( \Exception $e ) {
                return $this->logError( $e, "rooms", "add");
            }
        }

        try{
            $divisions = $this->division->getAll($request->session()->get('institution')->url);
            $modalities = Modality2::remoteFindAll(['active' => 1]);
            $procedures = Procedure::remoteFindAll(['active' => 1]);
            $blockSizes = $this->blockSizes;

            return view('rooms.add', compact( 'divisions', 'modalities', 'procedures', 'blockSizes' ) );
        }catch( \Exception $e ){
            return $this->logError( $e, "rooms", "add");
        }
    }


    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'name'              => 'required|max:50',
                'description'       => 'required|max:250',
                'administrative_ID' => 'required|unique_test:rooms,' . $id . '|max:45',
                'division_id'       => 'required',
                'start_hour'        => 'required|date_format:"h:i A"',
                'end_hour'          => 'required|date_format:"h:i A"',
                'block_size'        => 'required',
                'quota'             => 'required|numeric|min:1',
                'equipment_number'  => 'required|numeric|min:1',
                'procedures'        => 'required'
            ]);
            try{
                $data = $request->all();

                $res = Room::remoteUpdate($id, $data);

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');                

                    return redirect()->route('rooms');
                }

                return redirect()->route('rooms');
            } catch( \Exception $e ) {
                return $this->logError( $e, "rooms", "edit");
            }
        }

        try{
            $room       = Room::remoteFind($id);
            $divisions  = $this->division->getAll($request->session()->get('institution')->url);
            $modalities = Modality2::remoteFindAll(['active' => 1]);
            $procedures = Procedure::remoteFindAll(['active' => 1]);
            $blockSizes = $this->blockSizes;

            return view('rooms.edit', compact('room', 'divisions', 'modalities', 'procedures', 'blockSizes') );
        } catch( \Exception $e ) {
            return $this->logError( $e, "rooms", "edit");
        }
    }


    public function active( Request $request, $id ){
        try{
            $res = Room::remoteToggleActive( $id );

            if ( isset( $res->error ) ) {
                return $this->parseFormErrors( $data, $res );
            } else {
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('rooms');
            }
        }catch( \Exception $e ){
            return $this->logError( $e, "rooms", "active");
        }
    }
}
