<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\ServiceRequest;
use App\RequestedProcedure;
use App\AlertMessage;
use Log;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
        $this->serviceRequest = new ServiceRequest();
        $this->requestedProcedure = new RequestedProcedure();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request ){
        try{
            $totalServiceRequests = $this->serviceRequest->total($request->session()->get('institution')->url);
            $totalOrdersToAdmit = $this->requestedProcedure->total( 1 );
            $totalOrdersToDo = $this->requestedProcedure->total( 2 );
            $totalOrdersToDictate = $this->requestedProcedure->total( 3 );
            $totalOrdersToTranscribe = $this->requestedProcedure->total( 4 );
            $totalOrdersToApprove = $this->requestedProcedure->total( 5 );
            $totalOrdersFinished = $this->requestedProcedure->total( 6 );
            $totalOrdersSuspended = $this->requestedProcedure->total( 7 );

            $totalOrdersToAdmitByMonth = $this->requestedProcedure->totalByMonth( 1 );
            $totalOrdersToDoByMonth = $this->requestedProcedure->totalByMonth( 2 );
            $totalOrdersToDictateByMonth = $this->requestedProcedure->totalByMonth( 3 );
            $totalOrdersToTranscribeByMonth = $this->requestedProcedure->totalByMonth( 4 );
            $totalOrdersToApproveByMonth = $this->requestedProcedure->totalByMonth( 5 );
            $totalOrdersFinishedByMonth = $this->requestedProcedure->totalByMonth( 6 );
            $totalOrdersSuspendedByMonth = $this->requestedProcedure->totalByMonth( 7 );

            $totalOrdersDoItByUser = $this->requestedProcedure->totalByUser( 3, \Auth::user()->id );
            $totalOrdersDictatedByUser = $this->requestedProcedure->totalByUser( 4, \Auth::user()->id );
            $totalOrdersTranscribedByUser = $this->requestedProcedure->totalByUser( 5, \Auth::user()->id );
            $totalOrdersApprovedByUser = $this->requestedProcedure->totalByUser( 6, \Auth::user()->id );

            /* Mensajes alerta */

            $alertmessages = AlertMessage::getCurrentMessages();

            return view('welcome', [ 'totalServiceRequests' => $totalServiceRequests,
                'totalOrdersToAdmit' => $totalOrdersToAdmit,
                'totalOrdersToDo' => $totalOrdersToDo,
                'totalOrdersToDictate' => $totalOrdersToDictate,
                'totalOrdersToTranscribe' => $totalOrdersToTranscribe,
                'totalOrdersToApprove' => $totalOrdersToApprove,
                'totalOrdersFinished' => $totalOrdersFinished,
                'totalOrdersSuspended' => $totalOrdersSuspended,

                'totalOrdersToAdmitByMonth' => $totalOrdersToAdmitByMonth,
                'totalOrdersToDoByMonth' => $totalOrdersToDoByMonth,
                'totalOrdersToDictateByMonth' => $totalOrdersToDictateByMonth,
                'totalOrdersToTranscribeByMonth' => $totalOrdersToTranscribeByMonth,
                'totalOrdersToApproveByMonth' => $totalOrdersToApproveByMonth,
                'totalOrdersFinishedByMonth' => $totalOrdersFinishedByMonth,
                'totalOrdersSuspendedByMonth' => $totalOrdersSuspendedByMonth,

                'totalOrdersDoItByUser' => $totalOrdersDoItByUser,
                'totalOrdersDictatedByUser' => $totalOrdersDictatedByUser,
                'totalOrdersTranscribedByUser' => $totalOrdersTranscribedByUser,
                'totalOrdersApprovedByUser' => $totalOrdersApprovedByUser,

                'alertmessages' => $alertmessages
            ]);
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: home. Action: index');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }
}
