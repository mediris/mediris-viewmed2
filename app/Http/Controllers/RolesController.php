<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Action;
use App\Section;
use App\Institution;
use App\Http\Requests;
use Activity;
use Log;
use App\Charge;
use DB;


class RolesController extends Controller {

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Roles.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(){

        try{

            $roles = Role::all();

            return view('roles.index', compact('roles'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: roles. Action: index');

            return view('results.edit', compact('requestedProcedure'));
        }
    }

    public function show( Role $role ){

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show',
            [ 'section' => 'role', 'id' => $role->id ]));

        $role->load('actions');
        $role->actions->load('section');

        return $role;

    }

    public function add( Request $request ){

        if( $request->isMethod('post') ){

            $this->validate($request, [
                'name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|unique:roles|max:50',
            ]);
            try{
                $role = new Role($request->all());
                $role->save();

                $actions = $request->input('actions');

                if( isset( $request->all()['actions'] ) ){
                    foreach( $actions as $action ){
                        if( $action == 'all' ){

                            $sections = count(Section::all());
                            $actions = count(Action::all());

                            $total = $actions * $sections;

                            for( $i = 0; $i < $total; $i++ )
                                $role->roleForSection()->attach([ 'action_section_id' => $i + 1 ]);

                            break;
                        }elseif( strpos($action, 'section') !== FALSE ){
                            $split = explode('_', $action);
                            $section_id = $split[1];

                            $sections = Section::find($section_id)->actions()->get();

                            foreach( $sections as $section )
                                $role->roleForSection()->attach([ 'action_section_id' => $section->pivot->id ]);

                        }else{
                            $split = explode('_', $action);
                            $action_id = $split[1];
                            $section_id = $split[2];

                            $acts = Action::find($action_id)->sectionByAction($section_id)->get();

                            foreach( $acts as $act )
                                $role->roleForSection()->attach([ 'action_section_id' => $act->pivot->id ]);
                        }

                    }
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.create',
                    [ 'section' => 'role', 'id' => $role->id ]));

                $request->session()->flash('message', trans('alerts.success-add'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('roles');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: roles. Action: add');

                return view('errors.index', [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString(),
                ]);
            }
        }

        try{
            $sections = Section::all();
            $sections->load('actions');

            return view('roles.add', [
                'sections' => $sections,
            ]);

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: roles. Action: add');

            return view('errors.index', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    public function edit( Request $request, Role $role ){
        if( $request->isMethod('post') ) {
            // se eliminan todos los roles anteriores de la tabla role_action_section
            DB::table('role_action_section')->where('role_id', '=', $role->id)->delete();

            $this->validate($request, [
                'name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|unique:roles,name,' . $role->id . '|max:50',
            ]);
            try{
                $role->update(['name'=>$request['name']]);
                $actions = $request->input('actions');

                if( isset( $request->all()['actions'] ) ){

                    $isAll = false;
                    $section_id = null;

                    foreach( $actions as $action ) {

                        if( $action == 'all' ) {
                            $isAll = true;
                            $sections = count(Section::all());
                            $actions = count(Action::all());

                            $total = $actions * $sections;

                            for( $i = 0; $i < $total; $i++ ){
                                // $result = $role->roleForSection()->where('action_section_id', $i + 1)->get();
                                $result = DB::table('action_section')->find($i + 1);
                                if( $result != null ) {
                                    $role->roleForSection()->attach([ 'action_section_id' => $i + 1 ]);
                                }
                            }
                        // si es una section
                        } elseif ( (strpos($action, 'section') !== FALSE) and !$isAll ) {
                            $split = explode('_', $action);
                            $section_id = $split[1];
                            // se obtienen las acciones de la seccion especificada en $sention_id
                            $sections = Section::find($section_id)->actions()->get();
                            foreach( $sections as $section ) {
                                // $result = $role->roleForSection()->where('action_section_id', $section->pivot->id)->get();
                                $result = DB::table('action_section')->find($section->pivot->id);
                                if( $result != null ) {
                                    $role->roleForSection()->attach([ 'action_section_id' => $section->pivot->id ]);
                                }
                            }
                        // si es una action
                        } elseif (!$isAll) {
                            $split = explode('_', $action);
                            $action_id = $split[1];
                            $section_id2 = $split[2];

                            if ($section_id != $section_id2) {
                                $acts = Action::find($action_id)->sectionByAction($section_id2)->get();
                                foreach( $acts as $act ){
                                    // $result = $role->roleForSection()->where('action_section_id', $act->pivot->id)->get();
                                    $result = DB::table('action_section')->find($act->pivot->id);
                                    if( $result != null ) {
                                        $role->roleForSection()->attach([ 'action_section_id' => $act->pivot->id ]);
                                    }
                                }
                            }
                        }
                    }

                }
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('roles');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: roles. Action: edit');

                return view('errors.index', [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString(),
                ]);
            }
        }

        try {
            $sections = Section::all();
            $sections->load('actions');

            return view('roles.edit', [
                'role' => $role,
                'sections' => $sections,
                'allowKillSessions' => true
            ]);

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: roles. Action: edit');

            return view('errors.index', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    public function delete( Role $role, Request $request ){

        try{
            if( $role->delete() ){

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete',
                    [ 'id' => $role->id, 'section' => 'role' ]));

                $request->session()
                    ->flash('message', trans('alerts.success-delete'));
                $request->session()->flash('class', 'alert alert-success');
            }else{

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', [
                    'id' => $role->id,
                    'section' => 'role',
                    'action' => 'delete',
                ]));

                $request->session()
                    ->flash('message', trans('alerts.error-delete',
                        [ 'name' => trans('messages.role') ]));
                $request->session()->flash('class', 'alert alert-danger');
            }

            return redirect()->route('roles');
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: roles. Action: edit');
            echo 'Error code: ' . $e->getCode();
            echo '<br>';
            echo 'Error message: ' . $e->getMessage();

            $mb = new MessageBag();
            $mb->add($e->getCode(), $e->errorInfo[2]);

            return redirect()->back()->withErrors($mb);
        }

    }

    public function active( Request $request, Role $role ){

        try{
            $role->active();
            $original = new Role();
            foreach( $role->getOriginal() as $key => $value ){
                $original->$key = $value;
            }

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', [
                'section' => 'roles',
                'id' => $role->id,
                'oldValue' => $original,
                'newValue' => $role,
                'action' => 'active',
            ]));

            $request->session()
                ->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

            return redirect()->route('roles');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: roles. Action: active');

            return view('errors.index', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);
        }

    }
}
