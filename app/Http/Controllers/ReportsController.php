<?php

namespace App\Http\Controllers;

require_once( "http://localhost:8080/JavaBridge/java/Java.inc" );
//require_once( "http://localhost:8080/JavaBridge/java/Java.inc" );
//require_once (__DIR__ ."/../../temporalBorrar.php" );

use App\Referring;
use App\Role;
use App\Sex;
use Illuminate\Contracts\Redis\Database;
use Illuminate\Foundation\Console\IlluminateCaster;
use Java;
use JavaClass;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\MessageBag;
use Log;
use Activity;
use Auth;
use App\RequestStatus;
use App\PatientType;
use App\Procedure;
//use App\Modality;
use App\User;
use App\Equipment;
use App\Room;
use App\Patient;
use App\RequestedProcedureStatus;
use App\AppointmentStatus;
use Illuminate\Support\Facades\File;
use App\Report;
use App\Modality2;
use Illuminate\Support\Facades\Storage;

class ReportsController extends Controller {

    public function __construct(){

        $this->url = 'api/v1/reports';
        $this->requestStatus = new RequestStatus();
        $this->patientType = new PatientType();
        $this->patient = new Patient();
        $this->rooms = new Room();
        $this->equipments = new Equipment();
        $this->reports = new Report();
        $this->requestedProcedureStatus = new RequestedProcedureStatus();
        $this->appointmentStatus = new AppointmentStatus();
        $this->table = "";
        $this->convertUser = 'convertUserToString';
        $this->convertGender = 'convertGenderToString';
        $this->referring = new Referring();
        //$this->modalities = new Modality();
        $this->modalities = new Modality2();
        $this->procedure = new Procedure();

    }

    /**
     * @fecha 12-12-2016, 21-02-2017
     * @programador Juan Bigorra, Diego Oliveros
     * @objxetivo Renderiza la vista index de la sección Reports.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(){

        //$this->pruebaAccesos();

        try{
            // se obtienen todos los reportes contenido en el modelo Report de la tabla Reports de la BD
            $reports = Report::all();

            // carga todos los parent (sub-reports?) asociados con el reporte
            $reports->load([ 'parent' ]);
            return view('reports.index', compact('reports'));
        }catch( \Exception $e ){

            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reports. Action: index');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    /**
     * @fecha 21-02-2017
     * @programador Diego Oliveros / modificado por: Joan Charaima
     * @objetivo Renderiza la vista show(Generar el Reporte) en la seccion de Reportes.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    //public function show( Request $request, Report $report ){;
    public function show( Request $request, Report $report ){
        // carga todos los parametros asociados con el $report desde el objeto parameters
        // se deben incluir los parametros deseados para cada reporte en la tabla "parameter_report"
        $report->load('parameters');

        // obtiene los parametros del reporte y los convierte en un array - cambio JCH
        $parameters = $report->parameters->toArray();

        if( $request->isMethod('post') ){

            //dd( \App::getLocale() );
            $params = new Java("java.util.HashMap");
            //$parameters = $report->parameters->toArray();

            /********************* VALIDACION DE CAMPOS DE LA VISTA *********************/
            // se crea un array para validar los campos
            $validation = array();

            // crean las reglas de validacion y se almancenan en la variable $validation
            foreach( $parameters as $key => $value ) {

                if( $value['type'] == 'class' )
                    $validation += [ $value['description'] => 'required|array|min:1' ];
                elseif ( $value['type'] == 'date' )
                    if ( $value['description'] == 'startDate' )
                        $validation += [ $value['description'] => 'required|date' ];
                    elseif  ( $value['description'] == 'endDate' )
                        $validation += [ $value['description'] => 'required|date' ];
                        //$validation += [ $value['description'] => 'required|date|after:startDate' ];
            }

            // el metodo validate se encuentra declarado en el trait ValidatesRequests de la clase Controller (de la cual hereda esta clase)
            $this->validate($request, $validation);

            /********************* FIN DE LA VALIDACION DE CAMPOS DE LA VISTA *********************/

            try{

                /* en $data se almacena todo lo recuperado desde el request, en el cual se guarda un arreglo que entre otras cosas contiene unos arreglos con cada unos de los parametros necesarios para armar el reporte */
                $data = $request->all();

                $path = storage_path() . '/app/reports';

                //contiene la ruta y  nombre del archivo jrxml
                $file = $path . '/' . $report->name . '.' . $report->extension;

                if( !File::exists($path) )
                    File::makeDirectory($path, 0755, true, true);

                /* Se realiza la consulta al API sobre el reporte */
                /* De ser necesario, desde este metodo se realiza el llamado para la creacion de la tabla secondTemporal */
                $response = $this->responseQuery($request, $data, $report);
                // ESCRIBIR PARA PROBAR EL PRIMER RESPONSE
                //dd($response);

                // si no es vacio el resultado
                /** Si no hay resultado entonces se muestra un mensaje de alerta */
                if( !empty( $response['results'] ) ) {
                    //if (true) {
                    /* se invoca a la creacion de la tabla temporal */

                    $this->temporaryTable($response);
                    /* HASTA AQUI SE MANTIENE LA INFORMACION DEL REPORTE EN MEMORIA, YA QUE DICHA INFORMACION ES ALMACENADA EN LA TABLA TEMPORAL */

                    // AQUI CAPAS PUEDAS QUITAR EL MENSAJE

                }
                else{   // se imprime que el reporte esta vacio

                    $request->session()->flash('message', trans('alerts.no-data'));
                    $request->session()->flash('class', 'alert alert-danger');
                    return redirect()->route('reports');
                }

                /**
                 * @jasperxml Permite cargar el re
                porte con la extension JRXML
                 * @jasperDesign Tiene el diseno o estructura del reporte cargado
                 * @compileManager Es la clase para poder compilar el JRXML
                 * @reportCompile Tiene el reporte .JRXML compilado a .Jasper
                 * @fillManager Es la clase para el llenado del reporte jasper
                 * @params Es un HashMap de Java que permite colocar el nombre=>valor de los parametros que recibe el reporte
                 * @class Es la definicion de la clase que luego carga el driver para MySQL
                 * @driverManager Tiene la clase para cargar el driver MySQL con los parametros del host, username y password
                 * @conn Tiene la conexion previamente creada para MySQL
                 * @jasperPrint Tiene el Reporte con los datos "pintados" ya que se pasa por parametro el reporte compilado, los parametros del reporte y la conexion del MySQL
                 */

                $jasperxml = new Java("net.sf.jasperreports.engine.xml.JRXmlLoader");
                $jasperDesign = $jasperxml->load($file);
                $compileManager = new JavaClass("net.sf.jasperreports.engine.JasperCompileManager");
                $reportCompile = $compileManager->compileReport($jasperDesign);
                $fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");

                /********************* SE CREAN LOS PARAMETROS PARA EL ENCABEZADO DEL REPORTE *********************/
                // IMPORTANTE: En esta seccion se arman los parametros necesarios para generar el reporte

                $date = getdate();
                $date = $date['mday'] . ' ' . trans('months.'.($date['month'])) . ' ' . $date['year'];

                $params->put("DATE", $date);
                $params->put('TOTAL', $response['total']);
                // Titulo sin acento y en mayuscula
                //$params->put('TITLE', str_replace('-', ' ', strtoupper($report->name)));
                $params->put('TITLE', str_replace('-', ' ', trans('labels.'.$report->name)));
                // Titulo con acento y en mayuscula (el acento se ve mal cuando la letra es mayuscula)
                //$params->put('TITLE', str_replace('-', ' ', strtoupper(trans('labels.'.$report->name))));
                $params->put("INSTITUCION", $request->session()->get('institution')->name);

                // se retorna la ruta Path Actual
                $logo = $request->session()->get('institution')->getOriginal('logo');

                // si $logo es diferente a ""
                if ($logo != "") {
                    // la funcion Storage::disk('public') retorna el directorio publico del proyecto
                    $logo = Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix($logo);
                    // si el archivo del logo existe
                    if( file_exists($logo) )
                        $params->put('LOGO', $logo);
                }

                // se realiza el llamado para realizar la asignacion de los parametros INGLES - ESPANOL
                //$this->setLang($params);
                /* en caso de que el logo de la institucion no tenga valor en la BD o que el mismo no sea una ruta valida, se generar el reporte sin ningun tipo de logo */

                // SE DECLARO MAS ARRIBA AL COMIENZO DEL METODO SHOW, EVALUAR SI FUNCIONA BIEN ASI
                //$parameters = $report->parameters->toArray();

                foreach( $parameters as $key => $value ){
                    if( $value['type'] != 'class' ){
                        //dump(strtoupper($value['description'])." - ".$data[$value['description']]);
                        $params->put(strtoupper($value['description']), $data[$value['description']]);
                    }elseif( $value['type'] == 'class' ){
                        if( $value['description'] == "modalities" ){
                            //dump($data[$value['description']]);
                            //dump($this->getModalities($data[$value['description']]));
                            $result = $this->getModalities($data[$value['description']]);
                            if( $result != 'all' ) {
                                //dump(strtoupper($value['description'])." - ".$result);
                                $params->put(strtoupper($value['description']), $result);
                            }
                            else {
                                //dump(strtoupper($value['description'])." - ".ucfirst(trans('labels.alls')));
                                $params->put(strtoupper($value['description']), ucfirst(trans('labels.alls')));
                            }
                        }elseif( $value['description'] == "users" ){
                            $result = $this->getUsers($data[$value['description']]);
                            if( $result != 'all' ) {
                                //dump(strtoupper($value['description'])." - ".$result);
                                $params->put(strtoupper($value['description']), $result);
                            }
                            else {
                                //dump(strtoupper($value['description'])." - ".ucfirst(trans('labels.alls')));
                                $params->put(strtoupper($value['description']), ucfirst(trans('labels.alls')));
                            }
                        }else{
                            /* si type = class and description no es modalities ni users, es decir si es appointmentStatus,
                            procedures, rooms o referring; con el fin de ubicar remotamente en la BD de api-meditron */

                            $result = $this->getRemoteData($data[$value['description']], $value['description'], $request);

                            if( $result != 'all' ) {
                                //dump(strtoupper($value['description'])." - ".$result);
                                $params->put(strtoupper($value['description']), $result);
                            }
                            else {
                                //dump(strtoupper($value['description'])." - ".ucfirst(trans('labels.alls')));
                                $params->put(strtoupper($value['description']), ucfirst(trans('labels.alls')));
                            }
                        }
                    }
                }

                /********************* FIN CREACION DE PARAMETROS PARA EL ENCABEZADO DEL REPORTE *********************/

                $class = new JavaClass("java.lang.Class");
                $class->forName("com.mysql.jdbc.Driver");
                $driverManager = new JavaClass("java.sql.DriverManager");

                $host = config('database.connections.mysql.host');
                $dbname = config('database.connections.mysql.database');
                $username = config('database.connections.mysql.username');
                $password = config('database.connections.mysql.password');

                $conn = $driverManager->getConnection("jdbc:mysql://" . $host . ":3306/" . $dbname, $username, $password);
                // BORRAR - SOLO DEBE SER UTLIZADO PARA PRUEBAS UNITARIAS
                //$params->clear();

                $jasperPrint = $fillManager->fillReport($reportCompile, $params, $conn);

                switch( $data['button'] ){
                    case trans('labels.pdf'):
                        $this->toPDF($jasperPrint, $request, $report->name);
                        break;
                    case trans('labels.csv'):
                        $this->toCSV($jasperPrint, $request, $report->name);
                        //$this->toExcel($jasperPrint, $request, $report->name);
                        break;
                }

                // Se eliminan las tablas existentes
                //DB::unprepared(DB::raw("DROP TABLE IF EXISTS " . $this->table));
                //DB::unprepared(DB::raw("DROP TABLE IF EXISTS secondtemporal"));

                //return redirect()->route('reports');
                return route('reports');

            }catch( JavaException $e ){

                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reports. Action: show');

                return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            }

        }   // FIN IF POST

        try{

            $fields = array();
            // obtiene los parametros del reporte y los convierte en un array
            //$parameters = $report->parameters->toArray();
            // se arman los parametros que se mostraran como opciones de filtrado al usuario
            foreach( $parameters as $key => $value ) {

                if( $value['type'] == 'class' ){
                    if( $value['description'] == "users" ) {
                        $fields += [ $value['description'] => $this->getUser('first_name', $report->role_id, $request->session()->get('institution')->id) ];
                    }
                    else {
                        $remoteInstance = new $this->{$value['description']}();
                        if ( $remoteInstance instanceof \App\RemoteModel ) {
                            //$fields += [ $value['description'] => $this->modalities->remoteFindAll() ];
                            //$fields += [ $value['description'] => $remoteInstance->remoteFindAll() ];
                            $fields += [ $value['description'] => $remoteInstance->remoteFindAll([ 'active' => 1 ]) ];
                        } else {
                            $fields += [ $value['description'] => $this->{$value['description']}->getAll($request->session()->get('institution')->url) ];
                        }
                    }
                }else
                    $fields += [ $value['description'] => $value['type'] ];
            }

            // se asignan a la "Fecha Fin" la fecha actual y a la "Fecha Inicio" la fecha del primer dia del mes
            $fields += [ 'id' => $report->id,
                'endToday' => $this->getToday(),
                'startToday' => $this->getFirstDayDate(),
            ];

            /* se envia la peticion a la vista reports/show, enviandole toda la informacion en el arreglo $field, el cual contiene los parametros que se deben cargar de forma incial en la vista en donde el usuario seleccionara los distintos items para la elaboracion del reporte */
            return view('reports.show', compact('fields'));

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reports. Action: show');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }

    }   // FIN DE SHOW

    private function getFirstDayDate(){
        return date("Y-m-d", strtotime("first day of this month"));
    }

    private function getToday(){
        return date("Y-m-d", strtotime("today"));
    }

    private function responseQuery( $request, $data, $report ){
        // la funcion explode Devuelve un array de string, siendo cada uno un substring del parámetro string formado por la división realizada por los delimitadores (em este caso el caracter "-") indicados en el parámetro delimiter.
        $reportName = explode("-", $report->name);
        $size = count($reportName) - 1;

        if ( $size > 0 ) {
            // Reportes de Honorario
            if ( $reportName[$size] == 'Radiologo' || $reportName[$size] == 'Tecnico' || $reportName[$size] == 'Transcriptor' ) {
                $data += [ 'type' => $reportName[$size] ];
                $response = $this->generateSubTable($request, $data, $report->name, 'countOrder' . $reportName[0], $this->convertUser, $this->convertUser);
            }elseif ( $reportName[$size] == 'Citas' ) {
                // realiza el llamado para generar la tabla secondTemporal
                $response = $this->generateSubTable($request, $data, $report->name, 'countOrderCitas');
            } elseif ( $reportName[$size] == 'Aprobadas' || $reportName[$size] == 'Realizadas' || $reportName[$size] == 'Dictadas' || $reportName[$size] == 'Transcritas' ){
                $data += [ 'type' => $reportName[$size] ];
                //$response = $this->generateSubTable($request, $data, $report->name, 'countOrders', $this->convertUser);
                $response = $this->generateSubTable($request, $data, $report->name, 'countOrders', $this->convertUser, $this->convertUser);
            }elseif( $reportName[$size] == 'Dictar' || $reportName[$size] == 'Aprobar' || $reportName[$size] == 'Transcribir' ){
                $data += [ 'type' => $reportName[$size] ];
                $response = $this->generateSubTable($request, $data, $report->name, 'countOrdersFor');
            }elseif( $reportName[$size] == 'Genero' ){
                $response = $this->reports->get($request->session()->get('institution')->url, $data, $report->name);
                $response = $this->convertGenderToString($response);
            }elseif( $reportName[$size] == 'Terminadas' ){
                $response = $this->generateSubTable($request, $data, $report->name, 'countEfficient', $this->convertUser, $this->convertUser);
            }elseif( $reportName[$size] == 'Ordenes' ){
                $response = $this->reports->get($request->session()->get('institution')->url, $data, $report->name);
                //dd($response);
            }
        }elseif ($reportName[$size] == 'Transcripcion') {
            $response = $this->reports->get($request->session()->get('institution')->url, $data, $report->name);
            $i = 0;
            // se obtienen los nombres de cada uno de los usuarios realacionados con el userId
            foreach($response['results'] as $results) {
                $response['results'][$i]['userId'] = $this->getUsers([$results['userId']]);
                // la funcion explode Devuelve un array de string, siendo cada uno un substring del parámetro string formado por la división realizada por los delimitadores (en este caso el caracter "-") indicados en el parámetro delimiter.
                $transcriptionDate = explode(" ", $results['transcriptionDate']);
                // se realiza la traduccion de la porcion del mes y se le vuelve a concatenar el anio que trajo desde la consulta a apimeditron
                $response['results'][$i]['transcriptionDate'] = trans('months.'.$transcriptionDate[0]) . " " . $transcriptionDate[1];
                $i++;
            }
        } else
            $response = $this->reports->get($request->session()->get('institution')->url, $data, $report->name);

        return $response;
    }

    private function generateSubTable( $request, $data, $reportName, $functionName, $convertData = null, $convertDataAux = null ) {
        // se realiza el llamado al metodo get de la clase Report, este metodo permite realizar la llamada remota al query del reporte que se construye a partir del codigo de apimeditron

        $response = $this->reports->get($request->session()->get('institution')->url, $data, $reportName);

        if( !empty( $response['results'] ) ) {
            $responseAux = $this->reports->get($request->session()->get('institution')->url, $data, $functionName);
            if( isset( $convertData ) )
                $response = $this->$convertData($response);
            if( isset( $convertDataAux ) )
                $responseAux = $this->$convertDataAux($responseAux);
            $this->subTemporaryTable($responseAux);
        }
        // ESCRIBIR PARA PROBAR EL SEGUNDO RESPONSE
        //dump($response);
        //dd($responseAux);

        return $response;
    }

    // permite ubicar los usuarios para los filtros requeridos para la generacion de los reportes (en los que aplique )
    private function getUser( $order = 'first_name', $role = null, $institution = null ){
        if( isset( $role ) and isset($institution) ) {
            /* DO: se agrego que se incluyera a los usuarios con role_id = 1 (root) en la lista de usuarios que se muestra en la vista */
            $user = User::join(DB::raw("(SELECT DISTINCT user_id FROM charges WHERE (role_id = " . $role . " AND institution_id = " . $institution . ") or role_id = 1) AS rol"),
            function ( $join ){
                $join->on('rol.user_id', '=', 'users.id');
            })
            ->where('active','=', 1)
            ->get();
            return $user;
        } else
            return User::where('active', 1)->orderBy($order, 'asc')->get();
    }

    private function getModalities( $data ){

        $modality = Modality2::remoteFindAll([ 'active' => 1 ]);

        $names = "";

        if( count($modality->toArray()) != count($data) )
            foreach( $data as $key => $value )
                $names .= $modality->keyBy('id')->get($value)->name . ', ';
        else
            return "all";

        $names = trim($names, ', ');

        return $names;
    }

    private function getUsers( $data ){
        $users = $this->getUser();
        $names = "";

        if( count($users->toArray()) != count($data) )
            foreach( $data as $key => $value )
                $names .= $users->keyBy('id')->get($value)->first_name . ' ' . $users->keyBy('id')->get($value)->last_name . ', ';
        else
            return "all";

        $names = trim($names, ', ');

        return $names;
    }

    private function getRemoteData( $data, $class, $request ){
        // TO DO - CAMBIAR CUANDO SE HOMOLOGEN TODAS LAS CLASES DE MODEL A REMOTEMODEL
        // se valida el tipo de clase (Model o RemoteModel)
        $remoteInstance = $this->$class;
        if ( $remoteInstance instanceof \App\RemoteModel ) {
            $api = $remoteInstance->remoteFindAll();
        } else {
            $api = $this->$class->getAll($request->session()->get('institution')->url);
        }
        //getRemoteData($data[$value['description']], $value['description'], $request));
        //$api = $this->$class->getAll($request->session()->get('institution')->url);
        $names = "";

        if( count($api->toArray()) != count($data) )
            foreach( $data as $key => $value ){
                /* la funcion strcmp permite comparar dos cadenas de caracteres de forma binaria, si el valor retornado
                es cero(0) las cadenas son iguales, en caso contrario no lo son */
                if (strcmp($class, "requestedProcedureStatus") == 0)
                    $names .= trans('labels.' . $api->keyBy('id')->get($value)->description) . ', ';
                elseif (strcmp($class, "referring") == 0)
                    $names .= $api->keyBy('id')->get($value)->first_name . ' ' .  $api->keyBy('id')->get($value)->last_name . ', ';
                else
                    $names .= $api->keyBy('id')->get($value)->description . ', ';
            }
        else
            return "all";
        $names = trim($names, ', ');
        return $names;
    }

    private function temporaryTable( $response ){

        $this->table = "temporal";
        $columns = $response['colums'];
        $types = $response['types'];

        DB::unprepared(DB::raw("DROP TABLE IF EXISTS " . $this->table));
        DB::insert(DB::raw("CREATE TABLE " . $this->table . " (id INT AUTO_INCREMENT PRIMARY KEY," . implode(",", $types) . ")"));

        if( !empty( $response['results'] ) ){
            $sql = 'INSERT INTO ' . $this->table . ' (' . implode(',', $columns) . ') VALUES';
            $values = "";
            foreach( $response['results'] as $row ) {
                if( isset( $row['grants'] ) ){
                    // se recupera el usuario a traves de su id de usuario (grants)
                    $user = (User::find($row['grants']));
                    $row['grants'] = $user->last_name." ".$user->last_name;
                }
                $values .= ' ("' . implode('","', $row) . '"),';
            }

            $sql .= $values;
            $sql = trim($sql, ',');
        }else{
            $sql = 'INSERT INTO ' . $this->table . ' (' . implode(',', $columns) . ') VALUES';
            $values = "";
            foreach( $columns as $row )
                $values .= 'null,';

            $values = trim($values, ',');
            $insert = '(' . $values . ')';
            $sql .= $insert;
        }

        DB::insert(DB::raw($sql));
    }

    private function subTemporaryTable( $response ){

        $this->table = "secondtemporal";
        $columns = $response['colums'];
        $types = $response['types'];

        DB::unprepared(DB::raw("DROP TABLE IF EXISTS " . $this->table));
        DB::insert(DB::raw("CREATE TABLE " . $this->table . " (id INT AUTO_INCREMENT PRIMARY KEY," . implode(",", $types) . ")"));

        if( !empty( $response['results'] ) ){
            $sql = 'INSERT INTO ' . $this->table . ' (' . implode(',', $columns) . ') VALUES';
            $values = "";

            $total = $this->getTotalPer($response['results'], "counts");
            // si existe el 'amounts' es porque el reporte a generar es de "Honorarios". Se genera el segundo total
            if( isset( $response['results'][0]['amounts'] ) )
                $totalAux = $this->getTotalPer($response['results'], "amounts");

            foreach( $response['results'] as $row ){
                if( isset( $row['status'] ) )
                    $row['status'] = trans('labels.' . $row['status']);

                $per = $this->calcPer($row['counts'], $total);
                // si existe el 'amounts' es porque el reporte a generar es de "Honorarios". Se calcula el segundo porcentaje
                //if (isset($response['results'][0]['amounts']))
                if( isset( $row['amounts'] ) )
                    // condicion para evitar la division entre cero (0)
                    if( $totalAux != 0 )
                        $per .= '","' . $this->calcPer($row['amounts'], $totalAux);
                    else
                        $per .= '","0';

                $values .= ' ("' . implode('","', $row) . '","' . $per . '"),';
            }
            $sql .= $values;
            $sql = trim($sql, ',');
        }else{
            $sql = 'INSERT INTO ' . $this->table . ' (' . implode(',', $columns) . ') VALUES';
            $values = "";
            foreach( $columns as $row )
                $values .= 'null,';

            $values = trim($values, ',');
            $insert = '(' . $values . ')';
            $sql .= $insert;
        }

        if( isset( $row['amounts'] ) ){
            //$sql .= ', ("Total","'. $total . '", "' . $totalAux . '", "100", "100")';
            $aux = ( $totalAux == 0 ) ? '"0")' : '"100")';
            $sql .= ', ("Total","' . $total . '", "' . $totalAux . '", "100",' . $aux;
        } elseif (isset($row['counts']))
            $sql .= ', ("Total","'. $total . '", "100")';
            //$sql .= ', ("Total","'. $total . ($totalAux=0)? '", "0")':'", "100")';

        DB::insert(DB::raw($sql));
    }

    /* Funcion que permite obtener a partir del id del usuario su first_name y last_name */
    private function convertUserToString( $response ){
        for( $i = 0; $i < count($response['results']); $i++ )   {
            //dump($response['results'][$i]['userId']);
            $user = User::find($response['results'][$i]['userId']);
            if (!is_null($user))
                $response['results'][$i]['userId'] = $user->first_name . " " . $user->last_name;
        }
        return $response;
    }

    private function convertGenderToString( $response ){
        for( $i = 0; $i < count($response['results']); $i++ ){
            $gender = Sex::find($response['results'][$i]['gender']);
            $response['results'][$i]['gender'] = trans('labels.'.$gender->name);
        }
        return $response;
    }

    //PDF
    private function toPDF( $jasperPrint, Request $request, $name ){

        try{
            // contiene la ruta y el nombre del archivo donde se crea temporalmente el reporte
            //$outputPath = realpath(".") . "\\" . trans('labels.' . $name) . "-" . date("dmY") . ".pdf";
            $outputPath = storage_path() . "/" . trans('labels.' . $name) . "-" . date("dmY") . ".pdf";

            // se crea el objeto $export a partir de una clase java
            $exporter = new java("net.sf.jasperreports.engine.export.JRPdfExporter");
            // se asignan los parametros $jasperPrint y $outputPath al objeto
            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);

            // se arman los header para forzar la descarga del archivo adjunto
            header("Content-Type: application/pdf");
            header("Content-Disposition: attachment; filename=" . trans('labels.' . $name) . "-" . date("dmY") . ".pdf");
            // se exporta el reporte
            $exporter->exportReport();
            // se realiza la lectura del archivo
            readfile($outputPath);
            // se cierra el archivo
            unlink($outputPath);

        }catch( JavaException $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reports. Action: show');
            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }

    }

    // CSV
    private function toCSV( $jasperPrint, Request $request, $name ){

        try{
            // contiene la ruta y el nombre del archivo donde se crea temporalmente el reporte
            //$outputPath = realpath(".") . "\\" . trans('labels.' . $name) . "-" . date("dmY") . ".csv";
            $outputPath = storage_path() . "/" . trans('labels.' . $name) . "-" . date("dmY") . ".csv";
            // se crea el objeto $export a partir de una clase java
            $exporter = new java("net.sf.jasperreports.engine.export.JRCsvExporter");
            // se asignan los parametros $jasperPrint y $outputPath al objeto
            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRCsvExporterParameter")->FIELD_DELIMITER, ";");
            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRCsvExporterParameter")->RECORD_DELIMITER, "\n");
            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRCsvExporterParameter")->CHARACTER_ENCODING, "UTF-8");

            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);

            // se arman los header para forzar la descarga del archivo adjunto
            header("Content-type: application/csv");
            header("Content-Disposition: attachment; filename=" . trans('labels.' . $name) . "-" . date("dmY") . ".csv");
            // se exporta el reporte
            $exporter->exportReport();
            // se realiza la lectura del archivo
            readfile($outputPath);
            // se cierra el archivo
            unlink($outputPath);

        }catch( JavaException $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reports. Action: show');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }

    }

    // Excel
    private function toExcel( $jasperPrint, Request $request, $name ){

        try{
            // contiene la ruta y el nombre del archivo donde se crea temporalmente el reporte
            //$outputPath = realpath(".") . "\\" . trans('labels.' . $name) . "-" . date("dmY") . ".xls";
            $outputPath = storage_path() . "/" . trans('labels.' . $name) . "-" . date("dmY") . ".xls";
            // se crea el objeto $export a partir de una clase java
            $exporter = new java("net.sf.jasperreports.engine.export.JRXlsExporter");

            // se arman los header para forzar la descarga del archivo adjunto
            header("Content-type: application/vnd.ms-excel");
            //header("Content-type: application/xls");F
            header("Content-Disposition: attachment; filename=" . trans('labels.' . $name) . "-" . date("dmY") . ".xls");

            // se asingan los parametros propios del formato Excel
            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_ONE_PAGE_PER_SHEET, FALSE);
            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_WHITE_PAGE_BACKGROUND, FALSE);
            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, TRUE);
            // se asignan los parametros $jasperPrint y $outputPath al objeto
            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->JASPER_PRINT, $jasperPrint);
            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
            // se exporta el reporte
            $exporter->exportReport();

            readfile($outputPath);
            // se cierra el archivo
            unlink($outputPath);

        }catch( JavaException $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reports. Action: show');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    /**
     * @fecha 18-07-2017
     * @programador Joan Charaima
     * @objetivo Obtener el total de los registros (100%) necesarios para el calculo del porcentaje
     * @param list
     * @return integet
     */
    public function getTotalPer($list, $param) {
        $count = 0;
        foreach( $list as $val )
            $count += $val[$param];
        return $count;
    }

    /**
     * @fecha 18-07-2017
     * @programador Joan Charaima
     * @objetivo Calcular el porcentaje de un valor en especifico
     * @param list
     * @return float
     */
    public function calcPer($val, $total) {
        return round( (($val * 100) / $total), 2 );
    }

    /*public function setLang($params) {
        if (( \App::getLocale() ) == 'es') {
            $params->put("LBL_PROCEDURE", "PROCEDIMIENTO");
            $params->put("LBL_USER", "USUARIO");
        }
        else {
            $params->put("LBL_PROCEDURE", "PROCEDURE");
            $params->put("LBL_USER", "USER");
        }
    }*/

    public function pruebaAccesos() {
        $user = \Auth::user();
        dump($user);

        dump($user->hasAccess(1, 1, 'reports', 'index'));

        dd('culmino');
    }

}   // Fin de la Clase
