<?php

namespace App\Http\Controllers;

use Activity;
use App\Country;
use App\Http\Requests\PatientAddRequest;
use App\Patient;
use App\PatientDocuments;
use App\Prefix;
use App\Responsable;
use App\ServiceRequest;
use App\Sex;
use App\Suffix;
use DataSourceResult;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Storage;
use Log;
use File;


class PatientsController extends Controller {

    public function __construct() {
        $this->logPath = '/logs/patients/patients.log';

        //Constructor for managing kendo grid remote data binding
        $host = config('database.connections.mysql.host');
        $dbname = config('database.connections.mysql.database');
        $conn = config('database.default');
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');

        $this->DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);
    }

    public function all( Request $request ){
        try{

            return Patient::all();

        } catch( \Exception $e ) {
            return $this->logError( $e, "patients", "all");
        }
    }

    /**
    * @fecha 25-11-2016
    * @programador Pascual Madrid / Juan Bigorra
    * @objetivo Renderiza la vista index de la sección Patients.
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
    */
    public function index( Request $request, $print = null ){

        try{
            if( $request->isMethod('POST') ){

                if( $print == 1 ){

                    $fileName = $request->all()['filename'];
                    $contentType = $request->all()['contentType'];
                    $base64 = $request->all()['base64'];

                    $data = base64_decode($base64);

                    header('Content-Type:' . $contentType);
                    header('Content-Length:' . strlen($data));
                    header('Content-Disposition: attachment; filename=' . $fileName);

                    echo $data;

                }else{

                    $fields = [
                        'id',
                        'active',
                        'patientID',
                        'lastName',
                        'firstName',
                        'email',
                        'phoneNumber',
                        'cellPhoneNumber',
                        'photo',
                    ];

                    $data = (object)$request->all();

                    if( isset( $data->filter ) ){
                        $data->filter = (object)$data->filter;
                        foreach( $data->filter->filters as $key => $filter ){
                            $data->filter->filters[$key] = (object)$filter;
                        }
                    }

                    if( isset( $data->sort ) ){
                        foreach( $data->sort as $key => $sortArray ){
                            $data->sort[$key] = (object)$sortArray;
                        }
                    }

                    $result = $this->DbHandler->read('patientsIndexView', $fields, $data);

                    echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }

            return view('patients.index');

        } catch( \Exception $e ) {
            return $this->logError( $e, "patients", "index");
        }
    }

    public function show( Request $request, Patient $patient ){

        try{
            /**
            * Log activity
            */

            Activity::log(trans('tracking.show', [ 'section' => 'patients', 'id' => $patient->id ]));

            //$patient->load('patientDocuments');

            return view('patients.show', compact('patient'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "patients", "show");
        }
    }

    public function add( Request $request ){


        if( $request->isMethod('post') ){
            $this->validate($request, [
                'patient_ID' => 'required|unique:patients|max:45',
                'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'birth_date' => 'required|date',
                //'address' => 'required|max:250',
                'country_id' => 'required',
                //'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
                //'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
                'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
                'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
                //'email' => 'required|unique:patients|email',
                'email' => 'required|email',
                'citizenship' => 'required|max:25',
                'sex_id' => 'required',
            ]);
            try {
                $patient = Patient::updateOrCreate( $request->all() );
                
                Activity::log(trans('tracking.create', [ 'section' => 'patients', 'id' => $patient->id ]));

                $request->session()->flash('message', trans('alerts.success-add'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('patients');
            }catch( \Exception $e ){
                return $this->logError( $e, "patients", "add");
            }
        }

        try{
            $sexes = Sex::orderBy('name', 'asc')->get();
            $responsables = Responsable::orderBy('name', 'asc')->get();
            $prefixes = Prefix::where('language', \App::getLocale())->orderBy('name', 'asc')->get();
            $suffixes = Suffix::where('language', \App::getLocale())->orderBy('name', 'asc')->get();
            $countries = Country::getCountryByLocale();

            return view('patients.add', [ 'sexes' => $sexes, 'responsables' => $responsables, 'suffixes' => $suffixes, 'prefixes' => $prefixes, 'countries' => $countries ]);
        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "add");
        }
    }

    public function edit( Request $request, Patient $patient ){

        if( $request->isMethod('post') ){

            $this->validate($request, [
                'patient_ID' => 'required|unique:patients,patient_ID,' . $patient->id . '|max:45',
                'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'birth_date' => 'required|date',
                //'address' => 'required|max:250',
                'country_id' => 'required',
                //'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
                //'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
                'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
                'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
                //'email' => 'required|unique:patients,email,' . $patient->id . '|email',
                'email' => 'required|email',
                'citizenship' => 'required|max:25',
                'sex_id' => 'required',
            ]);
            try {                
                $original = new Patient();
                foreach( $patient->getOriginal() as $key => $value ){
                    $original->$key = $value;
                }

                $new_patient = $request->all();
                $new_patient['id'] = $patient->id;

                $patient = Patient::updateOrCreate( $new_patient );

                $patient->updateServiceRequests();

                Activity::log(trans('tracking.edit', [ 'section' => 'patients', 'id' => $patient->id, 'oldValue' => $original, 'newValue' => $patient ]));

                $request->session()->flash('message', trans('alerts.success-edit', [ 'name' => trans('messages.patient') ]));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('patients');
            }catch( \Exception $e ){
                return $this->logError( $e, "patients", "edit");
            }
        }

        try{
            $sexes = Sex::orderBy('name', 'asc')->get();
            $responsables = Responsable::orderBy('name', 'asc')->get();
            $prefixes = Prefix::where('language', \App::getLocale())->orderBy('name', 'asc')->get();
            $suffixes = Suffix::where('language', \App::getLocale())->orderBy('name', 'asc')->get();
            $countries = Country::getCountryByLocale();
            $patient->load('patientDocuments');
            $patient->birth_date = substr($patient->birth_date, 0, 10);
            $patient->death_date = substr($patient->death_date, 0, 10);
            $files_scan = $patient->patientDocuments->where('type','=', 'scan');
            $files_upload = $patient->patientDocuments->where('type','!=','scan');

            return view('patients.edit', [
                'patient' => $patient,
                'sexes' => $sexes,
                'responsables' => $responsables,
                'suffixes' => $suffixes,
                'prefixes' => $prefixes,
                'countries' => $countries,
                'files_scan'=>$files_scan,
                'files_upload' => $files_upload
            ]);

        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "edit");
        }
    }

    public function match( Request $request, Patient $patient ){

        if( $request->isMethod('post') ){

            try {
                //delete the association of this patient with any patient from the old data
                $legacyPatients = $legacyPatient = \App\Oldrecordspaciente::where('id_patient_mediris', $patient->id)->get();
                foreach ($legacyPatients as $legacyPatient) {
                    $legacyPatient->id_patient_mediris = null;
                    $legacyPatient->save();
                }

                //if at least one patient was chosen, then associate this patient with the old data patients 
                if (isset($request->old_patients)) {
                    foreach ($request->old_patients as $oldpatient_id) {
                        $legacyPatient = \App\Oldrecordspaciente::find($oldpatient_id);
                        if ( $legacyPatient ) {
                            $legacyPatient->id_patient_mediris = $patient->id;
                            $legacyPatient->save();
                        }
                    }
                }

                $request->session()->flash('message', trans('alerts.success-edit', [ 'name' => trans('messages.patient') ]));
                $request->session()->flash('class', 'alert alert-success');

                if ( session()->has( 'backUrl' ) ) {
                    $backUrl = session()->get( 'backUrl' );
                    session()->forget('backUrl');
                    return redirect()->route( $backUrl );
                } else {
                    return redirect()->route('patients');
                }
            }catch( \Exception $e ){
                return $this->logError( $e, "patients", "match");
            }
        }

        try {
            $oldPatients = \App\Oldrecordspaciente::where([
                    ['nombres', 'like', '%'.$patient->first_name.'%'],
                    ['apellidos', 'like', '%'.$patient->last_name.'%'],
                ])
                ->orWhere('cedula', 'like', '%'.$patient->patient_ID.'%')
                ->get();

            if ( !empty($request->redirectTo) ) {
                session()->put( 'backUrl', $request->redirectTo );
            }

            return view('patients.match', [
                'patient' => $patient,
                'patients' => $oldPatients
            ]);

        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "match");
        }
    }

    public function delete( Request $request, Patient $patient ){

        try{
            $patient->delete();

            /**
            * Log activity
            */

            Activity::log(trans('tracking.delete', [ 'id' => $patient->id, 'section' => 'patient' ]));

            $request->session()->flash('message', trans('alerts.success-delete'));
            $request->session()->flash('class', 'alert alert-success');

            return redirect()->route('patients');

        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "delete");
        }
    }

    public function active( Request $request, Patient $patient ){

        try{
            $patient->active();
            $original = new Patient();
           
            foreach( $patient->getOriginal() as $key => $value ){
                $original->$key = $value;
                
            }
            /**
            * Log activity
            */

            Activity::log(trans('tracking.edit', [ 'section' => 'patients', 'id' => $patient->id, 'oldValue' => $original, 'newValue' => $patient, 'action' => 'active' ]));

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

            return redirect()->route('patients');

        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "active");
        }

    }

    public function search( Request $request ){


        if( $request->isMethod('post') ){

            $this->validate($request, [
                'email' => 'email',
            ]);
            try{
                $query = \DB::table('patientsIndexView');

                if( $request->patient_ID_search )
                $query = $query->where('patientID', '=', $request->patient_ID_search);

                if( $request->email_search )
                $query = $query->where('email', '=', $request->email_search);

                if( $request->first_name_search )
                $query = $query->where('firstName', 'LIKE', '%'.$request->first_name_search.'%');

                if( $request->last_name_search )
                $query = $query->where('lastName', 'LIKE', '%'.$request->last_name_search.'%');

                $patients = $query->get();
                
                return view('patients.search-result', [ 'patients' => $patients, 'appointment_id' => $request->appointment_id ]);

            }catch( \Exception $e ){
                return $this->logError( $e, "patients", "search");
            }
        }

        try{

            return view('patients.search-result', [ 'patients' => 'The method use is not a POST' ]);

        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "search");
        }
    }

    public function history( Request $request, Patient $patient ){
        try{
            $patient->load([ 'country' ]);

            //return $serviceRequests;
            return view('patients.history', [ 'patient' => $patient ]);
        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "history");
        }
    }

    public function info( Request $request ){
        try{
            if( $request->isMethod('post') )
            return Patient::where('patient_ID', $request->input('patient_identification_id'))->get()->first();

        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "info");
        }
    }

    public function deleteFileScan($file_id)
    {
        $deleteimage = Patient::deleteFileScan($file_id);
        return $deleteimage;

    }

    public function document( PatientDocuments $document, Request $request ) {
        $path = $document->location  . '/' . $document->filename;

        if (!Storage::exists($path)) {
            abort(404);
        }

        $absolutepath = Storage::getDriver()->getAdapter()->applyPathPrefix( $path );
        $type = Storage::mimeType($path);
        
        $headers = [
            'Content-Type' => $type,
            'Content-Disposition' => 'inline;filename='.$document->name
        ];

        return response()
            ->file($absolutepath, $headers);
    }
}
