<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\Order;
use App\Procedure;

class OrdersController extends Controller {
    public function __construct(){
        $this->url = 'api/v1/orders';
        $this->order = new Order();
    }

    /**
     * @fecha 25-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Orders.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try{
            $orders = $this->order->getAll($request->session()->get('institution')->url);

            return view('orders.index', compact('orders'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: orders. Action: index');

            return redirect()->route('modalities');
        }
    }

    public function add( Request $request ){
        
        if( $request->isMethod('post') ){

            $this->validate($request, [
                'name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'administrative_ID' => 'required|max:45|unique_test:orders',
            ]);

            try {
                $data = $request->all();
                $data['api_token'] = \Auth::user()->api_token;
                $data['user_id'] = \Auth::user()->id;

                $res = $this->order->add($request->session()->get('institution')->url, $data);

                if( $res->getStatusCode() == 200 ){
                    if( isset( json_decode($res->getBody())->error ) ){
                        $mb = new MessageBag();
                        foreach( $data as $key => $value ){
                            if( strpos(strtoupper(json_decode($res->getBody())->message), strtoupper($key)) !== false ){
                                $mb->add($key, trans('errors.' . json_decode($res->getBody())->error, [ 'attribute' => trans('errors.attributes.' . $key) ]));
                            }
                        }

                        return redirect()->back()->withErrors($mb)->withinput();
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create-api', [ 'section' => 'orders', 'id' => json_decode($res->getBody())->id, 'id-institution' => $request->session()->get('institution')->id ]));

                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');
                }

                return redirect()->route('orders');
            } catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: orders. Action: add');

                return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            }
        }

        try {
            $procedures = Procedure::remoteFindAll();

            return view('orders.add', [ 'procedures' => $procedures ]);
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: orders. Action: add');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }

    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){

            $this->validate($request, [
                'name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'administrative_ID' => 'required|max:45|unique_test:orders,' . $id,
            ]);
            try{
                $data = $request->all();
                $data['api_token'] = \Auth::user()->api_token;
                $data['user_id'] = \Auth::user()->id;

                $res = $this->order->edit($request->session()->get('institution')->url, $data, $id);

                if( $res->getStatusCode() == 200 ){

                    if( isset( json_decode($res->getBody())->error ) ){
                        $mb = new MessageBag();
                        foreach( $data as $key => $value ){

                            if( strpos(strtoupper(json_decode($res->getBody())->message), strtoupper($key)) !== false ){
                                $mb->add($key, trans('errors.' . json_decode($res->getBody())->error, [ 'attribute' => trans('errors.attributes.' . $key) ]));
                            }
                        }

                        return redirect()->back()->withErrors($mb)->withinput();
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit-api', [ 'section' => 'orders', 'id' => json_decode($res->getBody())->oldValue->id, 'id-institution' => $request->session()->get('institution')->id, 'oldValue' => json_encode(json_decode($res->getBody())->oldValue), 'newValue' => json_encode(json_decode($res->getBody())->newValue) ]));

                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');
                }

                return redirect()->route('orders');
            }catch( \Exception $e ){
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: orders. Action: edit');

                return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            }
        }

        try{
            $procedures = Procedure::remoteFindAll();
            $order = $this->order->get($request->session()->get('institution')->url, $id);

            return view('orders.edit', [ 'order' => $order, 'procedures' => $procedures ]);

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: orders. Action: edit');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function delete( Request $request, $id ){
        try{
            $res = $this->order->remove($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete-api', [ 'section' => 'orders', 'id' => $id, 'id-institution' => $request->session()->get('institution')->id ]));

                $request->session()->flash('message', trans('alerts.success-delete'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('orders');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: orders. Action: delete');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = $this->order->active($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit-api', [ 'section' => 'orders', 'id' => json_decode($res->getBody())->oldValue->id, 'id-institution' => $request->session()->get('institution')->id, 'oldValue' => json_encode(json_decode($res->getBody())->oldValue), 'newValue' => json_encode(json_decode($res->getBody())->newValue) ]));

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('orders');
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: orders. Action: active');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }
}
