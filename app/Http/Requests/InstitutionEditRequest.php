<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitutionEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'name' => 'required|unique:institutions,name,' . $this->id . '|max:50',
            'address' => 'required|max:250',
            'administrative_ID' => 'required|unique:institutions,administrative_ID,' . $this->id . '|max:45',
            'group_id' => 'required',
            'url' => 'required|unique:institutions,url,' . $this->id . '|max:250|url',
            'email' => 'required|email|max:255|unique:institutions,email,' . $this->id,
            'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
            'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
            'institution_id' => 'required|unique:institutions,institution_id,' . $this->id . '|max:45',
            'url_pacs' => 'required|max:250|url',
            'url_external_api' => 'max:250|url',
            //'logo_actives_report'=> 'boolean',
            'base_structure' => 'required|max:30',
        ];
    }
}
