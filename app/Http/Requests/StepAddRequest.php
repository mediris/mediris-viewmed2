<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StepAddRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'indications' => 'required',
            'modality_id' => 'required',
            'administrative_ID' => 'required|max:45|unique_test:steps',
        ];
    }
}
