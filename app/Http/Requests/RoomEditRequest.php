<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Room;

class RoomEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){

        return [
            'name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:50',
            'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:250',
            'administrative_ID' => 'required|unique_test:rooms,' . $this->id . '|max:45',
            'division_id' => 'required',
            'start_hour' => 'required',
            'end_hour' => 'required',
            'block_size' => 'required',
            'quota' => 'required|numeric|min:1',
            'equipment_number' => 'required|numeric|min:1',
        ];


    }
}
