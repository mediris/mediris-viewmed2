<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class AlertMessageAddRequest extends Request {

    public function authorize(){
        return true;
    }


    public function rules(){
        return [
            'message' => 'required',
            'institution_id' => 'required',
            'alert_message_type_id' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|after:start_date|date'
        ];
    }

}
