<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConsumableAddRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'description' => 'regex:/^[A-Za-z\s]+$/|required|max:25|unique_test:consumables',
            'unit_id' => 'required',
        ];
    }
}
