<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PatientEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'patient_ID' => 'required|unique:patients,patient_ID,' . $this->id . '|max:11',
            'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            //'name_prefix' => 'required',
            'birth_date' => 'required|date',
            //'occupation' => 'required|max:25',
            //'address' => 'required|max:250',
            'country_id' => 'required',
            //'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
            //'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
            'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
            //'email' => 'required|unique:patients,email,' . $this->id . '|email',
            'email' => 'required|email',
            'citizenship' => 'required|max:25',
            'sex_id' => 'required',
        ];
    }
}
