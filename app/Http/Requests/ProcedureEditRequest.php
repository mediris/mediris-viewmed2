<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProcedureEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'description' => 'required|max:50',
            'radiologist_fee' => 'required|numeric|min:0',
            'technician_fee' => 'required|numeric|min:0',
            'transcriptor_fee' => 'required|numeric|min:0',
            'administrative_ID' => 'required|max:45|unique_test:procedures,' . $this->id,
            'steps.*.description' => 'required|max:25',
            'steps.*.indications' => 'required',
            'steps.*.modality_id' => 'required',
            'steps.*.administrative_ID' => 'required|max:45',
            'steps.*.order' => 'required|numeric|min:1',
        ];
    }
}
