<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'administrative_ID' => 'required|unique:users,administrative_ID,' . $this->id . '|max:45',
            'username' => 'required|max:25|unique:users,username,' . $this->id,
            'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
            'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
            'email' => 'required|email|max:255|unique:users,email,' . $this->id,
            'prefix_id' => 'required',
            'suffix_id' => 'required',
        ];
    }
}
