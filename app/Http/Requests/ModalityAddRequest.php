<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use GuzzleHttp\Client;
use App\Modality;

class ModalityAddRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:10',
            'parent_id' => 'required',
        ];
    }
}
