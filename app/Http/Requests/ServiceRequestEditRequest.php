<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceRequestEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'patient_ID' => 'required|max:9',
            'patient.first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'patient.last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'patient.name_prefix' => 'required',
            'patient.birth_date' => 'required|date',
            'patient.occupation' => 'required|max:25',
            'patient.address' => 'required|max:250',
            'patient.country_id' => 'required',
            //'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
            //'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
            'email' => 'required|email',
            'patient.citizenship' => 'required|max:25',
            'patient.sex_id' => 'required',
            'patient_type_id' => 'required',
            'source_id' => 'required',
            'answer_id' => 'required',
            //'weight' => 'required|numeric|min:1',
            //'height' => 'required|numeric|min:1',
            'pregnancy_status_id' => 'required',
            'smoking_status_id' => 'required',
            'referring_id' => 'required',
            'issue_date' => 'required',
            'patient_state_id' => 'required',
            'requestedProcedures.*.procedure_id' => 'required',
        ];
    }
}
