<?php

namespace App\Http\Middleware;

use App\ServiceRequest;
use Closure;
use App\RequestedProcedure;
use Log;

class RequestedProceduresMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next ){

        $section = explode('/', $request->route()->getPrefix())[1];
        $action = explode('@', $request->route()->getActionName())[1];
        $id = $request->route()->getParameter('id');

        switch( $section ){
            case 'preadmission':
                try{
                    $requestedProcedure = RequestedProcedure::remoteFind($id);
                }catch( \Exception $e ){
                    Log::useFiles(storage_path() . '/logs/technician/technician.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: ' . $section . '. Action: ' . $action);
                    echo $e->getFile() . '<br>' . $e->getLine() . '<br>' . $e->getCode() . '<br>' . $e->getMessage();

                    return redirect()->route('errors', [ $e->getCode() ]);
                }

                if( $requestedProcedure->requested_procedure_status_id == 1 ){
                    return $next($request);
                }else{
                    //return redirect()->route('errors', [ 403 ]);
                    abort(403);
                }

                break;

            case 'technician':
                try{
                    if( $action == 'lock' ){
                        $serviceRequest = ServiceRequest::remoteFind($id);

                        $requestedProcedures = array_filter((array)$serviceRequest->requested_procedures, function ( $requested_procedure ){
                            return $requested_procedure->requested_procedure_status_id == 2;
                        });

                        $keys = array_keys($requestedProcedures);

                        $requestedProcedure = $requestedProcedures[$keys[0]];

                    } elseif( $action == 'edit' ) {
                        $requestedProcedure = RequestedProcedure::remoteFind($id);
                    }

                }catch( \Exception $e ){
                    Log::useFiles(storage_path() . '/logs/technician/technician.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: ' . $section . '. Action: ' . $action);
                    echo $e->getFile() . '<br>' . $e->getLine() . '<br>' . $e->getCode() . '<br>' . $e->getMessage();

                    return redirect()->route('errors', [ $e->getCode() ]);
                }

                if( $requestedProcedure->requested_procedure_status_id == 2 ){

                    if( $requestedProcedure->blocked_status == 0 ){

                        return $next($request);

                    }else{

                        if( $requestedProcedure->blocking_user_id == $request->user()->id || $request->user()->impersonating ){

                            return $next($request);

                        }else{

                            if( $request->ajax() ){

                                return response()->json([ 'code' => '5000', 'message' => trans('errors.5000'), 'blocking_user_name' => $requestedProcedure->blocking_user_name ]);

                            }else{

                                $request->session()->flash('message', trans('errors.5000') . $requestedProcedure->blocking_user_name);
                                $request->session()->flash('class', 'alert alert-warning');

                                return redirect()->back();
                            }

                        }
                    }
                }else{

                    $request->session()->flash('message', 'Orden Bloqueada');
                    $request->session()->flash('class', 'alert alert-warning');

                    return redirect()->back();
                }

                break;

            case 'radiologist':
                try{

                    $requestedProcedure = RequestedProcedure::remoteFind($id);

                }catch( \Exception $e ){
                    Log::useFiles(storage_path() . '/logs/technician/technician.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: ' . $section . '. Action: ' . $action);
                    echo $e->getFile() . '<br>' . $e->getLine() . '<br>' . $e->getCode() . '<br>' . $e->getMessage();

                    return redirect()->route('errors', [ $e->getCode() ]);
                }

                if( $action == 'addendum' ){
                    if( $requestedProcedure->requested_procedure_status_id == 6 ){
                        return $next($request);
                    }else{
                        //return redirect()->route('errors', [ 403 ]);
                        abort(403);
                    }
                }else{
                    if( $action == 'approve' ){
                        if( $requestedProcedure->requested_procedure_status_id == 5 ){
                            return $next($request);
                        }else{
                            //return redirect()->route('errors', [ 403 ]);
                            abort(403);
                        }
                    }else{
                        if( $requestedProcedure->requested_procedure_status_id == 3 ){
                            if( $requestedProcedure->blocked_status == 0 ){
                                return $next($request);
                            }else{
                                if( $requestedProcedure->blocking_user_id == $request->user()->id ){
                                    return $next($request);
                                }else{
                                    //return redirect()->route('errors', [ 403 ]);
                                    //abort(403);
                                    $request->session()->flash('message', trans('errors.5000') . $requestedProcedure->blocking_user_name);
                                    $request->session()->flash('class', 'alert alert-warning');

                                    return redirect()->back();
                                }
                            }
                        }else{
                            //return redirect()->route('errors', [ 403 ]);
                            abort(403);
                        }
                    }
                }

                break;

            case 'transcription':
                try{

                    $requestedProcedure = RequestedProcedure::remoteFind($id);

                }catch( \Exception $e ){
                    Log::useFiles(storage_path() . '/logs/technician/technician.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: ' . $section . '. Action: ' . $action);
                    echo $e->getFile() . '<br>' . $e->getLine() . '<br>' . $e->getCode() . '<br>' . $e->getMessage();

                    return redirect()->route('errors', [ $e->getCode() ]);
                }

                if( $requestedProcedure->requested_procedure_status_id == 4 ){
                    if( $requestedProcedure->blocked_status == 0 ){
                        return $next($request);
                    }else{
                        if( $requestedProcedure->blocking_user_id == $request->user()->id ){
                            return $next($request);
                        }else{
                            //return redirect()->route('errors', [ 403 ]);
                            abort(403);
                        }
                    }
                }else{
                    //return redirect()->route('errors', [ 403 ]);
                    abort(403);
                }
                break;

            default:
                //return redirect()->route('errors', [ 403 ]);
                abort(403);
        }
    }
}
