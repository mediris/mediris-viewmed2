<?php

namespace App\Http\Middleware;

use Closure;
use App\RequestedProcedure;
use App\ServiceRequest;
use Log;
use Activity;

class BlockMiddleware {

    public function __construct(){

        $this->requestedProcedure = new RequestedProcedure();
        $this->serviceRequest = new ServiceRequest();

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next ){

        try{

            $section = explode('/', $request->route()->getPrefix())[1];
            $user_id = $request->user()->id;

            if( $section == 'technician' ){

                $service_request_id = $request->route()->getParameter('serviceRequest');
                $service_request = $this->serviceRequest->get($request->session()->get('institution')->url, $service_request_id);

                if( count($service_request->requested_procedures) > 0 ){

                    if( ( $service_request->requested_procedures[0]->blocked_status == 0 && $service_request->requested_procedures[0]->blocking_user_id == 0 )
                        || ( $service_request->requested_procedures[0]->blocking_user_id == $user_id )
                    ){

                        return $next($request);

                    }else{
                        if( $request->ajax() ){
                            return response()->json([ 'code' => '5000', 'message' => trans('errors.5000'), 'serviceRequest' => $service_request ]);
                        }
                    }
                }
            }
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/technician/technician.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Technician. Action: lock');
            echo $e->getFile() . '<br>' . $e->getLine() . '<br>' . $e->getCode() . '<br>' . $e->getMessage();

            return redirect()->route('errors', [ $e->getCode() ]);
        }
    }
}
