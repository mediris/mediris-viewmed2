<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PermissionsMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next ){

        $role_id = $request->session()->get('roles')[0]->id;
        $institution = $request->session()->get('institution')->id;
        $action = explode('@', $request->route()->getActionName())[1];
        $section = explode('/', $request->route()->getPrefix())[1];

        if( Auth::user()->hasAccess($institution, $role_id, $section, $action) )
            return $next($request);
        else
            if( $request->ajax() )
                return response('Unauthorized.', 401);
            else
                //return redirect()->route('errors', [ 403 ]);
                abort(403);
    }
}
