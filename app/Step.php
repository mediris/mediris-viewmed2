<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class Step extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/steps';
        parent::__construct();
    }

    /*public function remove( $url, $id ) {
        $res = $this->client->request('POST', $url . $this->url . '/delete/' . $id, [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);

        return $res;
    }*/

    public function hasEquipment( $id ) {
        foreach ( $this->equipment as $equipment ) {
            if ( $equipment->id == $id ) {
                return true;
            }
        }

        return false;
    }

    public function hasConsumable( $id ) {
        foreach ( $this->consumables as $consumable ) {
            if ( $consumable->id == $id ) {
                return true;
            }
        }

        return false;
    }
}
