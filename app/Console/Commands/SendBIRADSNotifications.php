<?php

namespace App\Console\Commands;

use App\Notifications\BiradReminderForMigration;
use Illuminate\Console\Command;
use Carbon\Carbon;

class SendBIRADSNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:biradsNotifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //SEND BIRADS NOTIFICATIONS TO CURRENT MEDIRIS PATIENTS
        $requestedProcedures = \App\RequestedProcedure::remoteFindBirad();
        
        foreach( $requestedProcedures as $rp ) {
            for ( $i=0; $i<count($rp); $i++ ) {
                $today = Carbon::today();
                $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $rp[ $i ]->culmination_date );
                $frecuency = $rp[ $i ]->bi_rad->frecuency;
                $nextDate = $endDate->addMonths( $frecuency ); 

                if ( $frecuency > 0 ) {
                    if ( $today->gt($nextDate) ) {
                        $patient = \App\Patient::find( $rp[$i]->service_request->patient_id );
                        $patient->notify( new BiradReminder( $rp[$i] ) );

                        $id = $rp[$i]->id;
                        $data['bi_rad_notified'] = true;
                        \App\RequestedProcedure::remoteUpdate( $id, $data, $rp[$i]->institution );
                        echo "Notificación enviada";                 
                        echo "\n\n";
                    } else {
                        echo "Aún no ha llegado el plazo de frecuencia para el envío:";
                        echo " hoy: " . $today;
                        echo " fecha de envío: " . $nextDate;
                        echo "\n\n";
                    }
                }
            }
        }



        //SEND BIRADS NOTIFICATIONS TO LEGACY SYSTEM PATIENTS (from migrated data)
        //Months to wait for the notification to expire
        $expireMonths = 1;
        //Find the first institution
        $institution = \App\Institution::first();
        //Find all orders that hasn't been procesed, has birad and are done.
        $ordenes = \App\Oldrecordsorden::where('id_estatus', 6)
                                ->where('bi_rad_notificado', false)
                                ->whereNotNull('birad')
                                ->get();
        foreach( $ordenes as $orden ) {
            $today = Carbon::today();
            $birad_id = $orden->birad + 1; //se debe aumentar uno para igualar con los id's en las tablas de mediris
            $birad = \App\BiRad::remoteFind($birad_id, [], $institution);
            $frecuency = $birad->frecuency;
            $dueDate = Carbon::createFromFormat('Y-m-d H:i:s', $orden->fecharealizacion )->addMonths( $frecuency ); 
            
            if ( $frecuency > 0 ) {
                if ( $today->gt($dueDate) ) {
                    $dueDateExpired = Carbon::createFromFormat('Y-m-d H:i:s', $orden->fecharealizacion )->addMonths( $frecuency ); 
                    $dueDateExpired->addMonths($expireMonths);
                    if ( $today->lt($dueDateExpired) ) {
                        $orden->load('solicitud');
                        $orden->solicitud->load('paciente');
                        $paciente = $orden->solicitud->paciente;
                        
                        $paciente->notify( new BiradReminderForMigration( $orden ) );

                        $orden->bi_rad_notificado = true;
                        $orden->save();

                        echo "\nNotificación enviada";                 
                        echo "\n\n";
                    } else {
                        $orden->bi_rad_notificado = true;
                        $orden->save();

                        echo "\nLa fecha de envío ya expiró, no se enviará la notificación (" . $orden->id . ")";
                        echo "\nhoy: " . $today;
                        echo "\nfecha de envío: " . $dueDate;
                        echo "\n\n";
                    }
                } else {
                    echo "\nAún no ha llegado el plazo de frecuencia para el envío:";
                    echo "\nhoy: " . $today;
                    echo "\nfecha de envío: " . $dueDate;
                    echo "\n\n";
                }
            } else {
                echo "\nNo se envían las notificaciones porque el valor de frecuencia es 0 para el BiRad: " . $birad->description;
                echo "\n\n";
            }
        }
    }
}
