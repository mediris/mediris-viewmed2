<?php

namespace App\Console\Commands;

use App\Appointment;
use App\Institution;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SetAppointmentStatusNoShow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setAppointmentStatus:noShow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Set the appointment status as no show, when the current date it's older than the appointment date";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $institutions = Institution::all();
        
        foreach ( $institutions as $institution ) {
            $appointments = Appointment::remoteFindByAction('/showcreated', [], $institution);
            
            foreach ($appointments as $appointment) {
                $now = Carbon::now();
                $dt = Carbon::createFromFormat('Y-m-d H:i:s', $appointment->appointment_date_start);
                
                if ( $now->gt($dt) ) {
                    $appointment->setAsNoShow();
                }

            }
        }       
    }
}
