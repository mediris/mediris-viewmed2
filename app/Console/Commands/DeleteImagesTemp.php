<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DeleteImagesTemp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:delete-images-temp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para eliminar las imagenes guardadas temporalmente en public/images/temp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $images = glob(public_path().'/images/temp/*');
        foreach ($images as $key => $image) {
            if(is_file($image)){
                unlink($image);
            }
        }

        echo "Proceso culminado";
    }
}
