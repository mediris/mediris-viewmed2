<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {
    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Group tiene muchos Institutions.
     */
    public function institutions() {
        return $this->hasMany(Institution::class);
    }
}
