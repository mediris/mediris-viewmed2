<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sex extends Model {
    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Sex tiene muchos Patients.
     */
    public function patients() {
        return $this->hasMany(Patient::class);
    }
}
