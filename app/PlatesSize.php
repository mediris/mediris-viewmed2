<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class PlatesSize extends RemoteModel {

    public function __construct() {
        $this->apibase = 'api/v1/platessizes';
        parent::__construct();
    }
    /*
    public function __construct() {
        $this->client = new Client();
        $this->url = 'api/v1/platessizes';
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
    }
*/
    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener una colección de PlatesSizes desde el api.
     */
    /*
    public function getAll( $url ) {
        $response = $this->client->request('POST', $url . $this->url, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);
        $plates_sizes = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $plate_size = new PlatesSize();

            foreach ( $element as $key => $value ) {
                $plate_size->$key = $value;
            }

            $plates_sizes->push($plate_size);
        }

        return $plates_sizes;
    }
    */





}
