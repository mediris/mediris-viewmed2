<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oldrecordsorden extends Model
{
    //
    protected $table='old_records_ordenes';

    public function procedimiento() {
        return $this->belongsTo(Oldrecordsprocedimiento::class, 'id_procedimiento', 'id');
    }

    public function estatus() {
        return $this->belongsTo(Oldrecordsestatus::class, 'id_estatus', 'id');
    }

    public function solicitud() {
    	return $this->belongsTo(Oldrecordssolicitud::class, 'id_solicitud', 'id');
    }

    public function usuario() {
    	return $this->belongsTo(Oldrecordsusuario::class, 'id_usr', 'id');
    }

    public function formatReportData() {
    	$this->load('solicitud', 'procedimiento', 'usuario');
    	$solicitud = $this->solicitud;

        $solicitud->load('paciente');
        $paciente = $solicitud->paciente;

        $reportData = [];

        $reportData['id'] = $this->id;

        if ( $this->fecharealizacion == "0000-00-00 00:00:00" ) {
            $endDate = new \DateTime();
        } else {
            $endDate = new \DateTime( $this->fecha );
        }
        setlocale(LC_ALL, \App::getLocale());
        $reportData["date"] = strftime( "%B %d, %Y", $endDate->getTimestamp() );

        $reportData["type"] = '';

        $reportData["name"] = $paciente->nombres . " " . $paciente->apellidos;

        $reportData["ci"] = $paciente->cedula;

        if ( isset( $paciente ) ) {
            $reportData["birthdate"] = date("Y-m-d", strtotime($paciente->fecha_nacimiento));
            if ( $paciente->sexo == 'M' ) {
            	$reportData["sex"] = 'male';
            } elseif ( $paciente->sexo == 'F' ) {
            	$reportData["sex"] = 'female';
            } else {
            	$reportData["sex"] = $paciente->sexo;
            }
            $reportData["sex"] = trans('labels.'.$reportData["sex"]);
        } else {
            $reportData["birthdate"] = "";
            $reportData["sex"] = "";
        }

        $reportData["referring"] = trans('labels.unspecified');

        $reportData['study'] = $this->procedimiento->descripcion;

        $reportData['text'] = html_entity_decode($this->texto);
        $reportData['text'] = preg_replace("/<\?xml[^>]+\>/i", "", $reportData['text']);
		$reportData['text'] = str_replace("<o:p>", '', $reportData['text']);
		$reportData['text'] = str_replace("</o:p>", '', $reportData['text']);

		$reportData['text'] = preg_replace("/mso-[\w-]+:[\s\S]'?[\w.\-\s]+'?[;]?/i", "", $reportData['text']);//mso-pagination: none;
		$reportData['text'] = preg_replace("/tab-stops:[\s\S][\w.-]+[;]?/i", "", $reportData['text']);        //tab-stops: 127.6pt;
		$reportData['text'] = preg_replace("/<\w+:\w+[\s\w\.:=\"áéíóúÁÉÍÓÚ]*>/i", "", $reportData['text']);   //<st1:metricconverter w:st="on" ProductID="2.1 mm">
		$reportData['text'] = preg_replace("/<\/\w+:\w+>/i", "", $reportData['text']);                        //</st1:metricconverter>

        $reportData['text'] = preg_replace("/<FONT[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/FONT>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<style[\s]*=[^>]+/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<SPAN style[\s]*=[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<SPAN lang[\s]*=[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/SPAN>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<BR>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/B>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/style[\s]*=[^>]+/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<B[\s\S]>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<span style[\s]*=[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<span>/i", "", $reportData['text']);       
        $reportData['text'] = preg_replace("/<span lang[\s]*=[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/span>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/SPAN>/i", "", $reportData['text']);

        $this->load('usuario');
        if ( $this->usuario->firma ) {
            $reportData['signature'] = route('users.legacy.signature', ['user' => $this->usuario->id]);    
        } else {
            $reportData['signature'] = '';
        }

        $reportData["signatureRadiologist"] = strtoupper($this->usuario->nombre . " " . $this->usuario->apellido);
        $reportData["signatureRadiologistCharge"] = '';        

        $reportData["signatureTechnician"] = "";

        $techDate = new \DateTime( $this->fecharealizacion );
        setlocale(LC_ALL, \App::getLocale());
        $reportData["signatureDate"] = strftime( "%B %d, %Y", $techDate->getTimestamp() );

        //logo
        $institution = session()->all()['institution'];
        //$reportData["logoActives"] = $institution->logo_actives_report; 
        $reportData["header"] = $institution->report_header;
        $reportData["footer"] = $institution->report_footer;
        
        return $reportData;
    }
}
