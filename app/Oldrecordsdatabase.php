<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oldrecordsdatabase extends Model
{
    public static function url_logs(){
       return storage_path() . '/olddata/logs/';
    }
    //
    protected $table='old_records_databases';
}
