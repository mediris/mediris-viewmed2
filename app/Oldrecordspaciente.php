<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Helpers\Utils;

class Oldrecordspaciente extends Model {
	use Notifiable;
    //
    protected $table='old_records_pacientes';

    /**
     * @fecha: 29-11-2017
     * @programador: Hendember Heras
     * @objetivo: functions required for the notifications, returns the route for the notification, in this case, the email or the phone number.
     */
    public function routeNotificationForMail() {
        return $this->email;
    }
    public function routeNotificationForMassivaMovil() {
        return Utils::formatCellphoneForMassivaMovil( $this->telefonoMovil );
    }
}
