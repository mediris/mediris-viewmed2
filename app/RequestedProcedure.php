<?php

namespace App;

use App\Notifications\RequestedProcedureDone;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use URL;

class RequestedProcedure extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/requestedprocedures';
        parent::__construct();

        $this->audio_path = config('constants.paths.requestedprocedure_audio');
    }

    public static function remoteFind( $id, $data =[], $url = null ) {
        $model = parent::remoteFind( $id, $data, $url );

        if ( $model ) {
            $model->patient             =   $model->loadPatient( $model->service_request->patient_id );
            $model->user                =   $model->loadUser( $model->technician_id );
            $model->technician_user     =   $model->loadUser( $model->technician_user_id );
            $model->radiologist_user    =   $model->loadUser( $model->radiologist_user_id );
            $model->transcriptor_user   =   $model->loadUser( $model->transcriptor_user_id );
            $model->service_request->user = $model->loadUser( $model->service_request->user_id );
            $model->approve_user   =   $model->loadUser( $model->approve_user_id );
        }

        return $model;
    }

    public static function remoteUpdate( $id, $data, $url = null ) {
        $model = parent::remoteUpdate( $id, $data, $url );

        if ( $model ) {
            $model->patient             =   $model->loadPatient( $model->service_request->patient_id );
            $model->user                =   $model->loadUser( $model->technician_id );
            $model->technician_user     =   $model->loadUser( $model->technician_user_id );
            $model->radiologist_user    =   $model->loadUser( $model->radiologist_user_id );
            $model->transcriptor_user   =   $model->loadUser( $model->transcriptor_user_id );
            $model->service_request->user = $model->loadUser( $model->service_request->user_id );
        }

        return $model;
    }

    /**
     * @fecha: 16-06-2017
     * @parametros:
     * @programador: Hendember Heras
     * @objetivo: Busca todas los requestedProcedures filtradas por este endpoint específico para cada institución
                    En este caso: que tenga birad, que esté finalizado y el más reciente de cada paciente
     */
    public static function remoteFindBirad() {
        if ( \App::runningInConsole() ) {
            $institutions = \App\Institution::all();
            $ret = [];

            foreach ( $institutions as $institution ) {
                $ret[] = parent::remoteFindByAction('/showbirad', [], $institution );
            }

            return $ret;
        }
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $api_url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador del
     *     elemento a editar
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el valor de la columna active dado un identificador.
     */
    public function active( $url, $id, $reason, $status = NULL ) {

        $res = $this->client->request('POST',
            $url . $this->url . '/active/' . $id . ( isset( $status ) ? '/' . $status : '' ),
            [
                'headers' => $this->headers,
                'form_params' => [
                    'api_token' => \Auth::user()->api_token,
                    'user_id' => \Auth::user()->id,
                    'suspensionReason' => $reason
                ],
            ]);

        return $res;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $id = Identificador del paciente
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener la información de un paciente dado
     */
    public function loadPatient( $id ) {

        $patient = Patient::find($id);
        if ( $patient ) {
            $patient->load('patientDocuments');
        }

        return $patient;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $id = Identificador del usuario
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener la información de un usuario dado
     */
    public function loadUser( $id ) {

        $user = User::find($id);

        return $user;
    }

    public static function setAsToDo( $id, $data = [] ) {
        $data['requested_procedure_status_id'] = 2;

        $model = self::remoteUpdate( $id, $data );

        return $model;
    }

    public static function setAsDictateFromRevert( $id, $data ) {
        $data['requested_procedure_status_id'] = 3;
        $data['blocking_user_id'] = \Auth::user()->id;
        $data['blocked_status'] = 1;

        $model = self::remoteUpdate( $id, $data );

        return $model;
    }

    /**
     * @fecha: 15-08-2017
     * @parametros: $id = identificador del requestedProcedure
                    $data = arreglo de datos a modificar
     * @programador: Hendember Heras
     * @objetivo: Cambia el status del RequestedProcedure a "to-dictate" (esto es cuando el técnico termina la orden)
     *      Se verifica que si el equipo cambia, entonces se debe generar de nuevo el StudyInstance y reescribir el worklist
     */
    public static function setAsDictate( $id, $data ) {
        $data['requested_procedure_status_id'] = 3; //to-dictate
        $data['blocked_status'] = 0;
        $data['blocking_user_id'] = 0;
        $data['blocking_user_name'] = '';
        $data['technician_user_id'] = \Auth::user()->id;
        $data['technician_user_name'] = \Auth::user()->first_name . ' ' . \Auth::user()->last_name;
        if( $data['culmination_date']=='' ){
            $data['culmination_date'] = date("Y-m-d H:i:s");
        }
        $data['technician_end_date'] = date("Y-m-d H:i:s");


        $model = self::remoteUpdate( $id, $data );

        return $model;
    }

    public static function setAsTranscribe( $id, $data ) {
        $data['requested_procedure_status_id'] = 4; //to-transcribe

        $model = self::remoteUpdate( $id, $data );

        //save the audio file if any
        if ( isset( $data['audio_data'] ) && $data['audio_data'] ) {
            $model->uploadDictationAudio( $data['audio_data'] );
        }

        return $model;
    }

    public static function setAsApprove( $id, $data ) {
        $data['requested_procedure_status_id'] = 5; //to-approve        
        $data['transcriptor_user_id'] = \Auth::user()->id;
        $data['transcriptor_user_name'] = \Auth::user()->first_name . ' ' . \Auth::user()->last_name;
        $data['transcription_date'] = date("Y-m-d H:i:s");
        $model = self::remoteUpdate( $id, $data );

        return $model;
    }

    public static function setAsFinished( $id, $data ) {
        $data['requested_procedure_status_id'] = 6; //finished
        $data['approve_user_id'] = \Auth::user()->id; //Add the user_id who approve
        $data['approve_user_name'] = \Auth::user()->first_name . ' ' . \Auth::user()->last_name; //Add the name who dictate
        $data['culmination_date'] = date("Y-m-d H:i:s");
        $data['approval_date'] = date("Y-m-d H:i:s");

        $model = self::remoteUpdate( $id, $data );

        //enviar la notificación al paciente de que la orden ya fue finalizada
        $model->patient->notify( new RequestedProcedureDone( $model ) );
        
        //enviar la notificación al referente de que la orden ya fue finalizada
        if ($model->service_request->referring) {
            $model->service_request->referring->institution = $model->institution;
            $referer = new Referring( (array) $model->service_request->referring );
            $referer->notify( new RequestedProcedureDone( $model ) );
        }

        return $model;
    }

    public static function setAsSuspended( $id, $data ) {
        $data['requested_procedure_status_id'] = 7;
        $data['suspension_date']        = date("Y-m-d H:i:s");
        $data['suspension_user_id']     = \Auth::user()->id;
        $data['suspension_user_name']   = \Auth::user()->first_name . ' ' . \Auth::user()->last_name;

        $model = self::remoteUpdate( $id, $data );
        //echo '<pre>'; print_r($model) ; echo '</pre>';die();
        return $model;
    }

    /**
     * @fecha: 15-08-2017
     * @parametros: $base = string que es la base del studyInstance
     * @programador: Hendember Heras
     * @objetivo: Llamada al endpoint que se encarga de crear el StudyInstance
     */
    public function setStudyInstance( $base ) {
        $data['base'] = $base;
        $response = $this->postRequest( '/setsi/' . $this->id, $data );

        return $response;
    }

    public static function updateEquipment( $id, $equipment_id ) {

        $data["equipment_id"] = $equipment_id;

        $model = self::remoteUpdate( $id, $data );
        
        //verify if the equipment has changed, then we need recreate de study instance id
        if ( $model->attributes['equipment_id'] != $model->original['equipment_id'] ) {
            $model->setStudyInstance( session('institution')->base_structure );
        }

        return $model;
    }

    function uploadDictationAudio( $base64Data ) {
        $data = explode(',',$base64Data);
                    //data:audio/webm;base64
        $mimeType = explode( ';', $data[0] );
        $mimeType = explode( '/', $mimeType[0] );
        $extension = $mimeType[1];

        $name = 'audiofile_'.$this->id.'_'.date("Ymd").'_'.date("His").'.'.$extension;

        $filename = $this->audio_path.'/'.$name;
        
        $res = Storage::put($filename , base64_decode($data[1]) );
        
        if ( $res !== false ) {
            $attributes['dictation_file'] = $filename;
            $this->remoteUpdate($this->id, $attributes);
        }    
    }

    function getDictationFile() {
        $ret = "";

        if ( $this->dictation_file ) {
            $contents = Storage::get( $this->dictation_file );
            $mimetype = Storage::mimeType( $this->dictation_file );

            return 'data:'.$mimetype.';base64,'.base64_encode($contents);
        }

        return $ret;
    }

    /**
     * @fecha: 15-08-2017
     * @parametros: $data = arreglo que contiene la fecha de nacimiento y el sexo del paciente
     * @programador: Hendember Heras
     * @objetivo: Llamada al endpoint que se encarga de escribir el worklist
     */
    public function writeWorklist( $data ) {
        $response = $this->postRequest( '/writeworklist/' . $this->id, $data );

        return $response;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $data = Los datos a ser
     *     editados, $requested_procedure_status_id = Identificador del estado del RequestedProcedure
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener el total de los RequestedProcedures dado un estado
     */
    public function total( $requested_procedure_status_id ) {
        $response = $this->postRequest( '/total/'. $requested_procedure_status_id );

        return $response;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $data = Los datos a ser
     *     editados, $requested_procedure_status_id = Identificador del estado del RequestedProcedure
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener el total de los RequestedProcedures del mes actual dado un estado
     */
    public function totalByMonth( $requested_procedure_status_id ) {
        $response = $this->postRequest( '/total-by-month/'. $requested_procedure_status_id );

        return $response;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $data = Los datos a ser
     *     editados, $requested_procedure_status_id = Identificador del estado del RequestedProcedure, $user_id =
     *     Identificador del Usuario
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener el total de los RequestedProcedures por usuario dado un estado
     */
    public function totalByUser( $requested_procedure_status_id, $user_id ) {

        $response = $this->postRequest( '/total-by-user/' . $requested_procedure_status_id . '/' . $user_id );

        return $response;
    }

    /**
     * @fecha: 02-02-2017
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador de la
     *     instancia a ser editada, $option = opción para definir el tipo de bloqueo (toggle o no-toggle)
     * @programador: Juan Bigorra
     * @objetivo: Función para bloquear y desbloquear las órdenes en función del parámetro $option
     */
    public function lock( $role, $option = NULL ) {

        $data = [
            'user_first_name' => \Auth::user()->first_name,
            'user_last_name' => \Auth::user()->last_name,
        ];

        $option = isset( $option ) ? '/1' : NULL;

        $res = $this->postRequest('/lock/' . $this->id . '/' . $role . $option, $data);

        return $res;
    }


    

    public function formatReportData( $isAddendum = false ) {
        $reportData = [];

        $reportData['id'] = $this->id;
        
        //The report date is the date when the study was performed
        if ( $this->technician_end_date == "0000-00-00 00:00:00" ) {
            $endDate = new \DateTime();
        } else {
            $endDate = new \DateTime( $this->technician_end_date );
        }

        setlocale(LC_ALL, \Utils::getLanguageEncode());
        $reportData["date"] = ucwords(strftime( "%B %d, %Y", $endDate->getTimestamp() ));

        $reportData["type"] = $this->service_request->patient_type->description;

        $reportData["name"] = $this->service_request->patient_first_name . " " . $this->service_request->patient_last_name;

        $reportData["ci"] = $this->service_request->patient_identification_id;

        if ( isset( $this->patient->id ) ) {
            $reportData["birthdate"] = date("Y-m-d", strtotime($this->patient->birth_date));
            $reportData["birthdate"] .= " (" . $this->patient->getStrAgeAndMonth() . ")";

            $sex = Sex::find( $this->patient->sex_id );
            $reportData["sex"] = trans('labels.'.$sex->name);
        } else {
            $reportData["birthdate"] = "";
            $reportData["sex"] = "";
        }

        if ( isset( $this->service_request->referring->id ) ) {
            $reportData["referring"] = $this->service_request->referring->first_name;
            $reportData["referring"] .= " " . $this->service_request->referring->last_name ;
        } else {
            $reportData["referring"] = trans('labels.unspecified');
        }

        $reportData['study'] = $this->procedure->description;

        if ( $isAddendum ) {
            $reportData['text'] = html_entity_decode($this->addendums[0]->text);
        } else {
            $reportData['text'] = html_entity_decode($this->text);
        }
        
        $reportData['text'] = preg_replace("/background-color[\s]*:[\s]*#[\w]+;?/i", "", $reportData['text']); //background-color:#ffffff;
        $reportData['text'] = preg_replace("/box-sizing[\s]*:[\s]*[\w-]+[;]?/i", "", $reportData['text']);     //box-sizing : border-box;
        $reportData['text'] = preg_replace("/text-autospace[\s]*:[\s]*[\w]+[;]?/i", "", $reportData['text']);  //text-autospace : none;
        $reportData['text'] = preg_replace("/<img([\w\W]+?)\/>/i", "", $reportData['text']);                   //<img src='...'/>;

        $reportData['text'] = preg_replace("/<FONT[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/FONT>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<style[\s]*=[^>]+/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<SPAN style[\s]*=[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<SPAN lang[\s]*=[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/SPAN>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<BR>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/B>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/style[\s]*=[^>]+/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<B[\s\S]>/i", "", $reportData['text']);       
        $reportData['text'] = preg_replace("/<span style[\s]*=[^>]+>/i", "", $reportData['text']);       
        $reportData['text'] = preg_replace("/<span>/i", "", $reportData['text']);       
        $reportData['text'] = preg_replace("/<span lang[\s]*=[^>]+>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/span>/i", "", $reportData['text']);
        $reportData['text'] = preg_replace("/<\/SPAN>/i", "", $reportData['text']);
        
        //$radiologist = User::find( $this->radiologist_user_id );
        $radiologist = User::find( $this->approve_user_id );
        if ( is_object($radiologist) && $radiologist->signature ) {
            $reportData['signature'] = route('users.signature', ['user' => $radiologist->id]);    
        } else {
            $reportData['signature'] = '';
        }

        $radiologist->load(["prefix"]);
        $reportData["signatureRadiologist"] = strtoupper($radiologist->prefix->name . " " . $radiologist->first_name . " " . $radiologist->last_name);

        $reportData["signatureRadiologistCharge"] = strtoupper($radiologist->position);

        if ( $this->technician_user_id ) {
            $technician = User::find( $this->technician_user_id );
            $reportData["signatureTechnician"] = strtoupper( $technician->last_name . ", " . $technician->first_name );
        } else {
            $reportData["signatureTechnician"] = "";
        }

        //the date for the technician signature must be the dictation date
        $signatureDate = new \DateTime( $this->culmination_date );
        setlocale(LC_ALL, \App::getLocale());
        $reportData["signatureDate"] = ucwords(strftime( "%B %d, %Y", $signatureDate->getTimestamp() ));
        // dump($data);
        // dump($radiologist);
        // dd($this->patient);

        //logo
        $institution = session()->all()['institution'];
        //$reportData["logoActives"] = $institution->logo_actives_report; 
        $reportData["header"] = $institution->report_header;
        $reportData["footer"] = $institution->report_footer;

        return $reportData;
    }
}
