<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {
    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Country tiene muchos Patients.
     */
    public function patients() {
        return $this->hasMany(Patient::class);
    }
    public static function getCountryByLocale(){
    	if(\App::getLocale() === 'es'){
            return Country::select('id','citizenship_es as citizenship', 'name_es as name')->orderBy('name', 'ASC')->get();
        }else{
        	return Country::select('id','citizenship', 'name')->orderBy('name', 'ASC')->get();
        }
    }
}