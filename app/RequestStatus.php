<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class RequestStatus extends Model {
    public function __construct() {
        $this->client = new Client();
        $this->url = 'api/v1/requeststatuses';
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener una colección de RequestStatuses desde el api.
     */
    public function getAll( $url ) {
        $response = $this->client->request('POST', $url . $this->url, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);
        $requestStatuses = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $requestStatus = new RequestStatus();
            foreach ( $element as $key => $value ) {
                $requestStatus->$key = $value;
            }
            $requestStatuses->push($requestStatus);
        }

        return $requestStatuses;
    }
}
