<?php

namespace App;

class CustomKendoGrid {

	protected $read;
	protected $transport;
	protected $model;
	protected $schema;
	protected $dataSource;
	protected $grid;
	protected $stringOperators;
	protected $numberOperators;
	protected $dateOperators;
	protected $operators;
	protected $excel;
	protected $margin;
	protected $pdf;
	protected $filterable;
	protected $pageable;
	protected $columnMenu;

	/**
	 * Constructor Custom Kendo Grid
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $options 		object
     * @param $read 		boolean
     * @param $transport 	boolean
     */
	function __construct($options, $read = true, $transport = true) {

		// Generate Transport
		$this->transport 	= new \Kendo\Data\DataSourceTransport();
		// Generate Read
        $this->read 		= new \Kendo\Data\DataSourceTransportRead();

        // Specify the url of the PHP page which will act as the remote service
        if(isset($options)) {
        	// URL
        	if (!isset($options->url)) {
        		$options->url = '';
        	}
        	// Type Connection
        	if(!isset($options->contentType)) {
        		$options->contentType = 'application/json';
        	}
        	// Type
        	if(!isset($options->type)) {
        		$options->type = 'POST';
        	}
        }

        if ($read) {
	        $this->read->url($options->url)
	                ->contentType($options->contentType)
	                ->type($options->type);
	    }

	    if ($read && $transport) {
	        // Configure the transport to send the data source parameters as JSON.
	        // This is required by the DataSourceResult helper.
	        $this->transport->read($this->read)
			                ->parameterMap('function(data) {
								                return kendo.stringify(data);
								            }');
		}

		// Model
        $this->model = new \Kendo\Data\DataSourceSchemaModel();

        // Data Source
       	$this->dataSource = new \Kendo\Data\DataSource();

       	// String Operators
       	$this->stringOperators = new \Kendo\UI\GridFilterableOperatorsString();
	    $this->stringOperators->eq(trans('labels.eq'))
	                    ->neq(trans('labels.neq'))
	                    ->isempty(trans('labels.isempty'))
	                    ->isnotempty(trans('labels.isnotempty'))
	                    ->contains(trans('labels.contains'))
	                    ->isnull(trans('labels.isnull'))
	                    ->isnotnull(trans('labels.isnotnull'));

	    // Number Operators
	    $this->numberOperators = new \Kendo\UI\GridFilterableOperatorsNumber();
	    $this->numberOperators->eq(trans('labels.eq'))
	                    ->neq(trans('labels.neq'))
	                    ->gt(trans('labels.gt'))
	                    ->gte(trans('labels.gte'))
	                    ->lt(trans('labels.lt'))
	                    ->lte(trans('labels.lte'))
	                    ->isnull(trans('labels.isnull'))
	                    ->isnotnull(trans('labels.isnotnull'));

	    // Date Operators
	    $this->dateOperators = new \Kendo\UI\GridFilterableOperatorsDate();
	    $this->dateOperators->eq(trans('labels.eq'))
	                    ->neq(trans('labels.neq'))
	                    ->gt(trans('labels.gt'))
	                    ->gte(trans('labels.gte'))
	                    ->lt(trans('labels.lt'))
	                    ->lte(trans('labels.lte'))
	                    ->isnull(trans('labels.isnull'))
	                    ->isnotnull(trans('labels.isnotnull'));

	    $this->operators = new \Kendo\UI\GridFilterableOperators();
	    $this->operators->string($this->stringOperators);
	    $this->operators->number($this->numberOperators);
	    $this->operators->date($this->dateOperators);

	}

	/**
	 * Set a String Operator
	 * Requires $this->operators
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param $operator 	object // Operator
     */
	public function setStringOperators($operator) {

		$this->operators->string($operator);

	}

	/**
	 * Set a Number Operator
	 * Requires $this->operators
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param $operator 	object // Operator
     */
	public function setNumberOperators($operator) {

		$this->operators->number($operator);

	}

	/**
	 * Set a Date Operator
	 * Requires $this->operators
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param $operator 	object // Operator
     */
	public function setDateOperators($operator) {

		$this->operators->date($operator);

	}

	/**
	 * Add Fields to the Model
	 * Requires $this->model
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $fields 		array
     */
	public function addFields($fields) {

		foreach ($fields as $field) {
			// Add each Field to the Model
			$this->model->addField($field);
		}

	}

	/**
	 * Create a Schema
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $others 		boolean
     * @param $parse 		boolean
     * @param $model 		object
     */
	public function createSchema($others = true, $parse = true, $model = null) {

		if(!isset($model)) {
			$model = $this->model;
		}

		$this->schema = new \Kendo\Data\DataSourceSchema();
        $this->schema->model($model);

        if ($others) {
			$this->schema->data('data')
			             ->errors('errors')
			             ->aggregates('aggregates')
			             ->total('total');
		}

		if ($parse) {
	        $this->schema->parse(new \Kendo\JavaScriptFunction(
	            'function(response) {
	                return JSON.parse(response);
	            }'
	        ));
	    }

	}

	/**
	 * Create a Data Source
	 * Requires $this->dataSource
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $page 			integer
     * @param $pageSize			integer
     * @param $filtering		array
     * @param $serverPaging 	boolean
     * @param $serverSorting	boolean
     * @param $serverFiltering	boolean
     */
	public function createDataSource($page = 1, $pageSize = 10, $filtering = null, $serverPaging = true, $serverSorting = true, $serverFiltering = true) {

		$this->dataSource->transport($this->transport)
                   ->page($page)
                   ->pageSize($pageSize)
                   ->schema($this->schema)
                   ->serverPaging($serverPaging)
                   ->serverSorting($serverSorting)
                   ->serverFiltering($serverFiltering);

        if(isset($filtering)) {

        	foreach ($filtering as $filter) {
        		$this->dataSource->addFilterItem($filter);
        	}

        }

	}

	/**
	 * Set a Data Source (Custom)
	 * Requires $this->dataSource
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $data 		array
     * @param $pageSize 	integer
     */
	public function setDataSource($data, $pageSize = 20) {

		$this->dataSource->data($data)
						->pageSize($pageSize)
						->schema($this->schema);

	}

	/**
	 * Create a Grid
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $view 		string
     * @param $type 		string
     */
	public function createGrid($view = null, $type = null) {

		if (!isset($type)) {
			$type = 'grid';
		}

		$this->grid = new \Kendo\UI\Grid($type);

		if (isset($view)) {
        	$this->grid->attr('view-attr', $view);
        }

	}

	/**
	 * Set a Filter
     * Requires $this->operators
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $view 		string
     * @param $type 		string
     */
	public function setFilter() {

		// Filter
	    $this->filterable = new \Kendo\UI\GridFilterable();
	    $this->filterable->extra(false)
	               		 ->operators($this->operators);

	}

	/**
	 * Set a Pager
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     */
	public function setPager() {

	    // Pagebles
	    $this->pageable = new \Kendo\UI\GridPageable();
	    $this->pageable->refresh(false)
	            	   ->buttonCount(7)
	            	   ->previousNext(true)
	            	   ->numeric(false)
	            	   ->input(true)
	            	   ->pageSize(10)
	            	   ->pageSizes(array(10, 20, 50, 'all'))
	            	   ->messages(array(
			                'allPages'  => trans('labels.allPages'),
			                'empty'     => trans('labels.empty')
			            ));

	}

	/**
	 * Set a Column Menu
     * Requires $this->operators
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     */
	public function setColumnMenu() {

	    // Column Menu
	    $this->columnMenu = new \Kendo\UI\GridColumnMenu();
	    $this->columnMenu->columns(false)
	               		 ->filterable(true)
	               		 ->sortable(false);

	}

	/**
	 * Set a Excel File for downloads
     * Requires $this->operators
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $title 		string
     * @param $function		call back (function)
     * @param $extension	string
     * @param $extra 		string
     */
	public function setExcel($title, $function, $extension = '.xlsx', $extra = null) {

		$this->excel = new \Kendo\UI\GridExcel();

		if (!isset($extra)) {
	        $this->excel->fileName(trans($title) . $extension);
		} else {
			$this->excel->fileName(trans($title) . $extra . $extension);
		}

	    $this->excel->filterable(true);

        $this->grid->excelExport($function);

	}

	/**
	 * Set a PDF File for downloads
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $title 		string
     * @param $url 			string
     * @param $function		call back (function)
     * @param $extra 		string
     * @param $page 		string
     * @param $margin 		object
     * @param $landscape	boolean
     * @param $scale 		float (0 - 1)
     */
	public function setPDF($title, $url, $function, $extra = null, $template = null, $page = 'letter', $margin = null, $landscape = true, $scale = 0.8) {

		if(!isset($margin)) {
			$margin = (object) array(
				'top'	 => '2cm',
				'left'	 => '1cm',
				'right'	 => '1cm',
				'bottom' => '1cm'
			);
		}

		if(!isset($template)) {
			$template = "page-template";
		}

		$this->margin = new \Kendo\UI\GridPdfMargin();
        $this->margin->top($margin->top)
               		 ->left($margin->left)
               		 ->right($margin->right)
               		 ->bottom($margin->bottom);

        $this->pdf = new \Kendo\UI\GridPdf();
        $this->pdf->allPages(true)
                  ->avoidLinks(true)
                  ->paperSize($page)
                  ->margin($this->margin)
                  ->landscape($landscape)
                  ->repeatHeaders(true)
                  ->templateId($template)
                  ->scale($scale)
                  ->proxyURL($url);

        if (!isset($extra)) {
        	$this->pdf->fileName(trans($title) . '.pdf');
        } else {
        	$this->pdf->fileName(trans($title) . $extra . '.pdf');
        }

        $this->grid->pdfExport($function);

	}

	/**
	 * Add Columns to the Grid
     * Requires $this->grid
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $columns 		array
     */
	public function addColumns($columns) {

		foreach ($columns as $column) {
			$this->grid->addColumn($column);
		}
		
	}

	/**
	 * Defines Data Bound
     * Requires $this->grid
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $databound	string
     */
	public function dataBound($databound = 'onDataBoundRemote') {

		$this->grid->dataBound($databound);
		
	}

	/**
	 * Defines Auto Bind
     * Requires $this->grid
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $autobind		boolean
     */
	public function autoBind($autobind = true) {

		$this->grid->autobind($autobind);
		
	}

	/**
	 * Generate the Grid Configuration
     * Requires $this->grid
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @param $excel 		boolean
     * @param $pdf 			boolean
     * @param $class 		string
     * @param $height 		integer
     * @param $scrollable 	boolean
     * @param $sortable 	boolean
     * @param $resizable	boolean
     * @param $columnMenu 	boolean
     */
	public function generate($excel = false, $pdf = false, $class = null, $height = null, $scrollable = true, $sortable = true, $resizable = true, $columnMenu = false) {

		$this->grid->dataSource($this->dataSource)  
				   ->scrollable(true)     
				   ->sortable(true)
				   ->resizable(true)
				   ->filterable($this->filterable)
				   ->columnMenu(false)
				   ->pageable($this->pageable)
				   ->dataBound('onDataBoundRemote');

		if($excel) {

			$this->grid->addToolbarItem(new \Kendo\UI\GridToolbarItem('excel'))
						->excel($this->excel);

		}

		if($pdf) {

			$this->grid->addToolbarItem(new \Kendo\UI\GridToolbarItem('pdf'))
						->pdf($this->pdf);
			
		}

		if(isset($class)) {

			$this->grid->attr('class', $class);

		}

		if(isset($height)) {

			$this->grid->height($height);

		}

	}

	/**
	 * Render the Kendo Grid
     * Requires $this->grid
	 * 
     * @version 27-11-2017
     * @author Alton Bell Smythe
     *
     * @return $this->grid->render();  	Object / String Kendo Grid
     */
	public function render() {

		return $this->grid->render();

	}

}