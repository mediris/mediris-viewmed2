<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class RequestedProcedureStatus extends Model {

    public function __construct() {

        $this->client = new Client();
        $this->url = 'api/v1/requestedprocedurestatus';
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];

    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos
     * @programador: Juan Bigorra
     * @objetivo: Función para obtener una colección de RequestedProcedureStatus desde el api.
     */
    public function getAll( $url ) {

        $response = $this->client->request('POST', $url . $this->url, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);
        $requestedProcedureStatuses = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $requestedProcedureStatus = new RequestedProcedureStatus();
            foreach ( $element as $key => $value ) {
                $requestedProcedureStatus->$key = $value;
            }
            $requestedProcedureStatuses->push($requestedProcedureStatus);
        }

        return $requestedProcedureStatuses;

    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos
     * @programador: Juan Bigorra
     * @objetivo: Función para obtener una colección de RequestedProcedureStatus desde el api.
     */
    public function filterByOrderStatus( $statuses, $orderStatus ) {

        $total = count($statuses);
        $diff = $total - $orderStatus;
        $toRemove = [];

        for ( $i = $orderStatus + 1; $i = $diff; $i++ ) {
            array_push($toRemove, $i);
        }

        $requestedProcedureStatuses = $statuses->except($toRemove);

        return $requestedProcedureStatuses;

    }


}
