<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Oldrecordsarchivosescaneados extends Model
{

    protected $table='old_records_archivos_escaneados';
    public function __construct( array $attributes = array() ) {

            parent::__construct($attributes);

            $this->location = Storage::getFacadeApplication()->basePath() . "/storage/app/olddata/requests/documents/";

        if ( !file_exists($this->location) ) {
            if ( !mkdir( $this->location, 0775, true ) ) {
                throw new \Exception("Can't create the directory: " + $this->location );
            }
        }

        }


}
