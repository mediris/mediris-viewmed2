<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;


class Action extends Model {

    use Rememberable;

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Action pertenece a un Section.
     */
    public function sections() {
        return $this->belongsToMany(Section::class)->withPivot('id');
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Action pertenece a un Section.
     */
    public function sectionByAction( $id ) {
        return $this->belongsToMany(Section::class)->where('section_id', $id)->withPivot('id');
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
