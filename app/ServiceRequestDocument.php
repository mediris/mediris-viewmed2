<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ServiceRequestDocument extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/servicerequestdocument';
        parent::__construct();

        $this->location = config('constants.paths.servicerequest_document');
    }

    /**
     * @fecha: 23-08-2017
     * @parametros:
     * @programador: Hendember Heras
     * @objetivo: insert the document in databas (not the file but the info), we overwrite the
                remoteUpdate to insert the location field before submitting
     */
    public static function remoteUpdate( $id, $data, $url = null ) {
        $model = new static();

        $data['location'] = $model->location;

        $model = parent::remoteUpdate( $id, $data, $url );

        return $model;
    }

    /**
     * @fecha: 22-08-2017
     * @programador: Hendember Heras
     * @objetivo: Move the uploaded file to the storage.
     */
    function moveToStorage( $uFile ) {
        $res = Storage::putFileAs( $this->location, $uFile, $this->name );
        
        if ( $res === false ) {
            return false;    
        } else {
            return true;
        } 
    }

    function saveScanToStorage( $base64Img ) {
        $data = explode(',',$base64Img);
        
        $res = Storage::put( $this->location.'/'.$this->name, base64_decode($data[1]) );
        
        if ( $res === false ) {
            return false;    
        } else {
            return true;
        }
    }
}