<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertMessagesUser extends Model {
    protected $fillable = [
        'institution_id',
        'alert_message_id',
        'user_id'
    ];
}
