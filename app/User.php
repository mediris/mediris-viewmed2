<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Watson\Rememberable\Rememberable;
use GuzzleHttp\Client;
use DB;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Notifications\NotificationPasswordReset;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject {
    use Rememberable;
    use Notifiable;

    private $signature_path;

    public function __construct( array $attributes = [] ) {
        parent::__construct($attributes);

        $this->signature_path = config('constants.paths.user_signature');
    }
    
    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que no pueden ser llenados a través de eloquent (los que sí salgan aquí no podrán ser
     *     llenados).
     */
    protected $fillable = [
        'last_name', 'first_name', 'suffix_id', 'prefix_id', 'administrative_ID', 'active', 'username', 'password', 'username_pacs', 'password_pacs', 'position', 'telephone_number', 'telephone_number_2', 'cellphone_number', 'cellphone_number_2', 'email', 'signature', 'api_token', 'additional_information', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Muchos usuarios tienen muchos cargos.
     */
    public function charges() {
        return $this->hasMany(Charge::class);
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un User tiene un Suffix.
     */
    public function suffix() {
        return $this->belongsTo(Suffix::class);
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un User tiene un Prefix.
     */
    public function prefix() {
        return $this->belongsTo(Prefix::class);
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Verificar si el usuario actual está activo.
     */
    public function isActive() {
        if ( $this->active ) {
            return "checked";
        }
    }

    /**
     * @fecha: 16-12-2016
     * @parametros: $id = Identificador del Role
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Verificar si el usuario actual tiene el rol suministrado
     */
    public function hasRole( $id ) {

        if ( \Session::has('roles') ) {
            $roles = \Session::get('roles');
            foreach ( $roles as $role )
                if ( $role->id == $id )
                    return true;

        }
        else {
            $find = Charge::where([ [ 'role_id', $id ], [ 'user_id', \Auth::user()->id ] ])->limit(1)->get();
            if (count($find) > 0)
                return true;
        }

        return false;
    }

    /**
     * @fecha: 11-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Obtener todas las instituciones a las que el usuario tiene acceso.
     */
    public function getAllinstitutions() {

        return DB::table('charges')
            ->select(DB::raw('institutions.*'))
            ->join('institutions', 'charges.institution_id', '=', 'institutions.id')
            ->where('user_id', '=', \Auth::user()->id)
            ->get();
    }

    /**
     * @fecha: 11-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Obtener todas las instituciones a las que el usuario tiene acceso.
     */
    public function getRoleByInstitution( $institution ) {
        return Charge::select('role_id AS id')->where([ [ 'user_id', \Auth::user()->id ], [ 'institution_id', $institution ] ])->get();
    }

    /**
     * @fecha: 11-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Obtener todas las instituciones a las que el usuario tiene acceso.
     */
    public function getPermissions( $rol ) {
        $results = DB::table('role_action_section')
            ->select(DB::raw('actions.name AS `action`, sections.name AS `section`'))
            ->join('action_section', 'action_section.id', '=', 'action_section_id')
            ->join('actions', 'actions.id', '=', 'action_id')
            ->join('sections', 'sections.id', '=', 'section_id')
            ->where('role_id', '=', $rol)
            ->get();

        /**
         * Agrupar el arbol de roles por Seccion
         */
        $json = array();
        foreach ( $results as $result ) {
            $json[ $result->section ][ $result->action ] = true;
        }

        return $json;
    }

    /**
     * @fecha: 17-11-2017
     * @programador: Alton Bell Smythe
     * @objetivo: Obtener todas los accesos del menu para el usuario.
     */
    public function getMenu( $rol ) {
        $results = DB::table('role_action_section AS ras')
            ->select('s.active AS active',
                     's.id AS id',
                     's.name AS section',
                     's.priority AS priority',
                     's1.id AS parent_id',
                     's1.name AS parent',
                     'a.id AS action_id',
                     'a.name AS action',
                     'a.display AS display')
            ->join('action_section AS asec', 'ras.action_section_id', '=', 'asec.id')
            ->join('sections AS s', 'asec.section_id', '=', 's.id')
            ->join('actions AS a', 'asec.action_id', '=', 'a.id')
            ->leftJoin('sections AS s1', 's.parent_id', '=', 's1.id')
            ->where([
                ['ras.role_id', '=', $rol],
                ['s.priority', '!=', -1],        // Unactive
                ['a.display', '=', 1],
            ])
            ->groupBy('s.active')
            ->groupBy('s.id')
            ->groupBy('s.name')
            ->groupBy('s.parent_id')
            ->groupBy('a.id')
            ->groupBy('a.name')
            ->groupBy('a.display')
            ->orderBy('s.priority', 'desc')
            ->orderBy('s.name', 'asc')
            ->get();

        $tmp    = [];
        $others = [];

        foreach ($results as $key => $value) {
            if(!isset($value->parent_id) && is_null($value->parent_id)){
                $not = false;
                foreach ($tmp as $subkey => $subvalue) {
                    if($value->id == $subvalue->id){
                        $not = true;
                    }
                }
                if(!$not){
                    $tmp[]      = $value;
                } else {
                    $others[]   = $value;
                }
            }
        }
        foreach ($results as $key => $value) {
            if(isset($value->parent_id) && $value->parent_id > 0){
                $id = null;
                foreach ($tmp as $sub_key => $sub_value) {
                    if($value->parent_id == $sub_value->id){
                        $id = $sub_key;
                    }
                }
                if(isset($id)) {
                    $tmp[$id]->sub[] = $value;
                }
            }
        }
        foreach ($others as $key => $value) {
            foreach ($tmp as $subkey => $subvalue) {
                if($value->id == $subvalue->id){
                    $tmp[$subkey]->sub[] = $value;
                }
            }
        }
        $final = $tmp;
        foreach ($tmp as $key => $value) {
            if(isset($value->sub)){
                foreach ($value->sub as $sub_key => $sub_value) {
                    foreach ($results as $result) {
                        if(isset($sub_value->parent_id)){
                            if($result->parent_id == $sub_value->id){
                                // 3 Nivel
                                $final[$key]->sub[$sub_key]->sub[] = $result;
                            }
                        }
                    }
                }
            }
        }

        // Sort
        foreach ($final as $key => $value) {
            if(isset($value->sub)){
                do
                {
                    $swapped = false;
                    for( $i = 0, $c = count( $value->sub ) - 1; $i < $c; $i++ )
                    {
                        if( ucfirst(trans('labels.' . $value->sub[$i]->section)) > ucfirst(trans('labels.' . $value->sub[$i + 1]->section)) )
                        {
                            list( $value->sub[$i + 1], $value->sub[$i] ) = array( $value->sub[$i], $value->sub[$i + 1] );
                            $swapped = true;
                        }
                    }
                }
                while( $swapped );

            }
        }

        return $final;
    }

    /**
     * @fecha: 16-12-2016
     * @parametros: $institution_id = Identificador de la institución, $section_name = Nombre de la sección,
     *     $action_name = Nombre de la acción
     * @programador: Juan Bigorra / Pascual Madrid / JCH
     * @objetivo: Verificar si el usuario actual tiene acceso a la ruta suministrada.
     */
    public function hasAccess( $institution_id, $role_id, $section_name, $action_name ) {
        // JCH: QUITAR PARA QUE FUNCIONE LA VALIDACION DE ACCESOS A LA URL
        //return true;

        if ( $action_name == 'search' || $action_name == 'remote' || $action_name == 'uploadImage' || $action_name == "cropImage" || $action_name == "discharge" || $action_name == "history" )
            return true;
        if ( \Session::has('permissions') )
            if ( isset( \Session::get('permissions')[$section_name][$action_name] ) )
                return true;
        else{
            $result = $this->hasPermission($institution_id, $role_id, $section_name, $action_name);
            //if ( null !== $this->hasPermission($institution_id, $role_id, $section_name, $action_name) ){
            if( isset($result->action_section_id) )
                return true;
        }
        return false;
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para limpiar el cache de los modelos Section, Action & Institution.
     */
    public function clearCache() {
        Section::flushCache();
        Action::flushCache();
        Institution::flushCache();
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }

    public function get( $id ) {
        return User::find($id);
    }

    public function createToken( $url ) {
        $client = new Client();
        $short_url = 'api/v1/users';
        $headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];

        $res = $client->request('POST', $url . $short_url . '/add', [ 'headers' => $headers, 'form_params' => [ 'administrative_ID' => $this->administrative_ID, 'api_token' => \Auth::user()->api_token, 'api_token_2' => $this->api_token, 'user_id' => \Auth::user()->id ] ]);

        return $res;
    }

    private function hasPermission( $institution_id, $role_id, $section_name, $action_name ) {
        return DB::table('action_section')
            ->select('role_action_section.action_section_id')
            ->join(DB::raw('(SELECT id FROM sections WHERE name = "' . $section_name . '" ) AS section'), function ( $join ) {
                $join->on('section.id', '=', 'action_section.section_id');
            })
            ->join(DB::raw('(SELECT id FROM actions WHERE name = "' . $action_name . '" ) AS actions'), function ( $join ) {
                $join->on('actions.id', '=', 'action_section.action_id');
            })
            ->join('role_action_section', 'role_action_section.action_section_id', '=', 'action_section.id')
            ->join('roles', 'roles.id', '=', 'role_action_section.role_id')
            ->join(DB::raw('( SELECT * FROM charges WHERE institution_id = ' . $institution_id . ' AND user_id = ' . \Auth::user()->id . ' ) AS charge'), function ( $join ) {
                $join->on('charge.role_id', '=', 'roles.id');
            })
            ->get();
    }

    public function saveSignature( $file ) {
        if( $file != '' ) {
            //$file is relative to the public directory
            $file = public_path() . $file;
            
            //move the file to storage
            $filename = Storage::putFile( $this->signature_path, new File($file) );
            
            if ( $filename !== false ) {
                $this->signature = $filename;

                return true;    
            } else {
                return false;
            }            
        }
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new NotificationPasswordReset($token));
    }
}
