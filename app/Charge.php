<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model {
    protected $fillable = [
        'user_id', 'role_id', 'institution_id',
    ];

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un cargo tiene muchos usuarios.
     */
    public function users() {
        return $this->belongsToMany(User::class, 'charges', 'user_id', 'id');
    }

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un cargo tiene muchos roles.
     */
    public function roles() {
        return $this->belongsToMany(Role::class, 'charges', 'role_id', 'id');
    }

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un cargo esta en muchas Instituciones.
     */
    public function institutions() {
        return $this->belongsToMany(Institution::class, 'charges', 'institution_id', 'id');
    }

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un cargo tiene muchas Secciones.
     */
    public function chargeForSection() {
        return $this->belongsToMany(Section::class, 'charge_action_section', 'charge_id', 'action_section_id');
    }

}
