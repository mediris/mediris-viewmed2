<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldRecordsMigrateOrders extends Model
{
    public function __construct( array $attributes = array() ) {
        parent::__construct($attributes);
        $this->location ='/app/olddata/imports';
        //$this->location = config('constants.paths.patient_document');
    }
    function moveToStorage( $uFile ) {
        $res = Storage::putFileAs( $this->location, $uFile, 'ordenes.csv' );
        if ( $res === false ) {
            return false;
        } else {
            return true;
        }
    }
}
