<?php

namespace App;

class BiRad extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/birads';
        parent::__construct();
    }
}