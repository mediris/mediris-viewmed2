<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use PDF;

class FinalReport extends Model {
    protected $path;
    protected $filename;
    protected $stream;

    public function __construct( array $attributes = array() ) {
    }
    
    /**
     * Creates a PDF document for the final report
     * @param $data
     * @param $draft
     */
    static function createReport( $reportData, $draft = false, $returnStream = false ) {
        $reportData['draft'] = ($draft)?'draft '.\App::getLocale():'';

        $pdf = PDF::loadView('requestedProcedures.pdf', $reportData );
        
        //we need to provide an http context to the dompdf object so it can resolve
        //the image url that are visible only to the authenticated user
        $dompdf = $pdf->getDomPDF();
        $aHTTP['http']['header'] = "Cookie: " . $_SERVER['HTTP_COOKIE'] . "\r\n";
        $aHTTP['ssl']['verify_peer'] = false;
        $aHTTP['ssl']['verify_peer_name'] = false;
        $context = stream_context_create($aHTTP); 
        $dompdf->setHttpContext( $context );
        
        $model = new static();
        $model->pathFilename = $model->getPathFilenameReport( $reportData['id'] );
        $model->stream = $pdf->stream();

        return $model;
    }

    static function getReport( $id ) {
        $model = new static();
        $pathFilename = $model->getPathFilenameReport( $id ); 
        
        if ( Storage::exists( $pathFilename ) ) {
            return Storage::get( $pathFilename );
        } else {  
            return false;
        }
    }

    private function getPathFilenameReport( $id ) {
        $path = config('constants.paths.order');
        $filename = trans('printer.final-order') . $id . '.pdf';

        return $path . '/' . $filename;
    }



    /**
     * Creates a PDF document for an addendum report
     * @param $data
     * @param $draft
     */
    static function createAddendum( $reportData, $draft = false ) {
        $reportData['draft'] = ($draft)?'draft '.\App::getLocale() :'';
        $pdf = PDF::loadView('requestedProcedures.pdf', $reportData );

        //we need to provide an http context to the dompdf object so it can resolve
        //the image url that are visible only to the authenticated user
        $dompdf = $pdf->getDomPDF();
        $aHTTP['http']['header'] = "Cookie: " . $_SERVER['HTTP_COOKIE'] . "\r\n";
        $aHTTP['ssl']['verify_peer'] = false;
        $aHTTP['ssl']['verify_peer_name'] = false;
        $context = stream_context_create($aHTTP); 
        $dompdf->setHttpContext( $context );

        $model = new static();
        $model->pathFilename = $model->getPathFilenameAddendum( $reportData['id'] );
        $model->stream = $pdf->stream();

        return $model;
    }

    static function getAddendum( $id ) {
        $model = new static();
        $pathFilename = $model->getPathFilenameAddendum( $id ); 
        
        if ( Storage::exists( $pathFilename ) ) {
            return Storage::get( $pathFilename );
        } else {  
            return false;
        }   
    }

    private function getPathFilenameAddendum( $id ) {
        $path = config('constants.paths.addendum');
        $filename = trans('printer.addendum-order') . $id . '.pdf';

        return $path . '/' . $filename;
    }

    public function toStorage() {
        return Storage::put( $this->pathFilename, $this->stream );
    }

    public function getStream() {
        return $this->stream;
    }
}
