<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during .
    |
    */

    'welcome' => 'Welcome',
    'landing' => "Your Application's Landing Page.",
    'delete' => "Are you sure to delete this element?",

    'no-pac-url' => "The pacs url hasn't been set",
    'no-pac-username' => "The pacs username or password hasn't been set",


    /**
     * Sections
     */

    'action' => 'action',
    'actions' => "actions",
    'sections' => "sections",
    'section' => 'section',
    'subsections' => "subsections",
    'subsection' => 'subsection',
    'role' => 'role',
    'roles' => 'roles',
    'user' => 'user',
    'users' => 'users',
    'institution' => 'institution',
    'institutions' => 'institutions',
    'division' => 'division',
    'divisions' => 'divisions',
    'room' => 'room',
    'rooms' => 'rooms',
    'modality' => 'modality',
    'modalities' => 'modalities',
    'equipment' => 'equipment',
    'referring' => 'referring',
    'referrings' => 'referrings',
    'configuration' => 'configuration',
    'configurations' => 'configurations',
    'patient-types' => 'patient types',
    'patient-type' => 'patient type',
    'ubication' => 'ubication',
    'ubications' => 'ubications',
    'source' => 'source',
    'sources' => 'sources',
    'procedure' => 'procedure',
    'procedures' => 'procedures',
    'template' => 'template',
    'templates' => 'templates',
    'consumable' => 'consumable',
    'consumables' => 'consumables',
    'printer' => 'printer',
    'printers' => 'printers',
    'step' => 'step',
    'steps' => 'steps',
    'order' => 'order',
    'orders' => 'orders',
    'patient' => 'patient',
    'patients' => 'patients',
    'sex' => 'sex',
    'sexes' => 'sexes',
    'responsable' => 'responsable',
    'responsables' => 'responsables',
    'patient-id' => 'patient id',
    'other-institutions' => 'other institutions',
    'filter' => 'filter',
    'reset'=> 'reset',


    'password' => 'password',
    'lbl-password-confirmation' => 'Password Confirmation',
    'spn-reset-password' => 'Reset Password',
    'remember' => 'remember',
    'forgot-password' => 'Forgot your password?',
    'login' => 'login',
    'btn-send' => 'Send',
    'lbl-email' => 'Email',
    'email' => 'Email',

    'success-add' => 'The :name was added',
    'error-add' => 'The :name cannot be added',
    'success-edit' => 'The :name was edited',
    'error-edit' => 'The :name cannot be edited',
    'success-delete' => 'The :name was deleted',
    'error-delete' => 'The :name cannot be deleted',

    'show' => 'show',
    'add' => 'add',
    'edit' => 'edit',
    'purge' => 'delete',

    /*
     * Search
     * */

    'search' => 'Search',
    'bas-search' => 'Basic Search',
    'adv-search' => 'Advanced Search',
    'period' => 'Period',
    'doctor-assigned' => 'Doctor Assigned',
    'all' => 'All',
    'anyone' => 'Anyone',
    'dictated-by' => 'Dictated by',

    /*
     * Nav Menu
     * */

    'main' => 'Main',
    'appoiments' => 'Appoiments',
    'reception' => 'Reception',
    'pre-entry' => 'Pre-entry',
    'interview' => 'Interview',
    'technical' => 'Technical',
    'radiologist' => 'Radiologist',
    'issue-orders' => 'Issue orders',
    'approve-orders' => 'Approve orders',
    'addendum' => 'Addendum',
    'orders-issued' => 'Orders issued',
    'transcription' => 'Transcription',
    'results' => 'Results',
    'search-menu' => 'Search',
    'patient' => 'Patient',
    'reports' => 'Reports',
    'administration' => 'Administration',

    /*
     * Grid titles
     * */

    'id' => 'Id',
    'name' => 'Name',
    'group' => 'Group',
    'printers' => 'Impresoras',
    'description' => 'Description',
    'units' => 'Units',
    'title' => 'Title',
    'equipments' => 'Equipments',

    /*
     * Forms labels
     * */

    'add-action' => 'Add Action',
    'edit-action' => 'Edit Action',
    'lbl-action-name' => 'Action Name',
    'lbl-section' => 'Section',
    'btn-save' => 'Save',
    'lbl-division-name' => 'Division Name',
    'add-division' => 'Add Division',
    'edit-division' => 'Edit Division',
    'add-consumable' => 'Add Consumable',
    'edit-consumable' => 'Edit Consumable',
    'add-institution' => 'Add Institution',
    'edit-institution' => 'Edit Institution',
    'administrative_id' => 'Administrative ID',
    'url' => 'URL',
    'address' => 'Address',
    'add-modality' => 'Add Modality',
    'edit-modality' => 'Edit Modality',
    'parent-modality' => 'Parent Modality',
    'add-room' => 'Add Room',
    'edit-room' => 'Edit Room',
    'first-name' => 'First Name',
    'last-name' => 'Last Name',
    'user-name' => 'User Name',
    'password-confirmation' => 'Password Confirmation',
    'telephone-number' => 'Telephone Number',
    'telephone-number-2' => 'Telephone Number 2',
    'cellphone-number' => 'Cellphone Number',
    'cellphone-number-2' => 'Cellphone Number 2',
    'signature' => 'Signature',
    'additional-information' => 'Additional Information',
    'suffix-id' => 'Suffix',
    'prefix-id' => 'Prefix',
    'add-user' => 'Add User',
    'edit-user' => 'Edit User',
    'add-section' => 'Add Section',
    'edit-section' => 'Edit Section',
    'lbl-section-name' => 'Section Name',
    'add-role' => 'Add Role',
    'edit-role' => 'Edit Role',
    'lbl-role-name' => 'Role Name',
    'add-equipment' => 'Add Equipment',
    'edit-equipment' => 'Edit Equipment',
    'active' => 'is Active?',
    'add-patient-type' => 'Add Patient Type',
    'edit-patient-type' => 'Edit Patient-type',
    'priority' => 'Priority',
    'icon' => 'Icon',
    'admin-aprobbation' => 'Admin Aprobbation?',
    'sms-send' => 'SMS Send?',
    'email-patient' => 'Email Patient?',
    'email-refeer?'=> 'Email refeer?',
    'add-source' => 'Add Source',
    'edit-source' => 'Edit Source',
    'add-template' => 'Add Template',
    'edit-template' => 'Edit Template',
    'no-history'=>'This patient has no history',
    'death-date' => "The death date can't be less or equal to the birth date",

     /*
     * Messages
     * */

    'approve-orders'=> 'There is/are :number orders to approve.',
    'password-reset-line1'=>'You are receiving this email because we received a password reset request for your account.',
    'password-reset-noaction'=>'If you did not request a password reset, no further action is required.',
    'password-reset-button'=>'Reset Password',
    'password-reset-link1'=>'If you’re having trouble clicking the ',
    'password-reset-link2'=>' button, copy and paste the URL below into your web browser: ',
    'password-reset-dontresponse'=>'(Please do not answer this email, this is an unmonitored account)',
    'password-reset-subject'=>'Notification Password Reset',
        /***
     * 
     * polls
     * 
     */
    'error-polls-inactive' => 'The survey is inactive for this Institution',
    'error-polls-not-coincidence' => 'there is no match with those search parameters',
    'error-url-poll' => 'The url of the surveys can not be empty.'
];
