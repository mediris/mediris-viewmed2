<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'url'                  => 'The :attribute format is invalid.',
    "phone" => "The :attribute field contains an invalid number.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => array(
            'rule-name'  => 'custom-message',
        ),
        'procedures' => array(
            'required' => 'Must add at least one procedure', 
        ),
        'start_hour' => array(
            'date_format' => 'start hour field must be in the format 06:30 AM',
        ),
        'end_hour' => array(
            'date_format' => 'end hour field must be in the format 05:30 PM',
        ),
        'patient_allergies' => array(
            'required_if' => 'Allergie field is required',
        ),
        'patient_weight' => array(
            'required_if' => 'Weight is required'
        ),
        'patient_height' => array(
            'required_if' => 'Height is required'
        ),
        'patient_abdominal_circumference' => array(
            'required_if' => 'Abdominal girth it\'s required'
        ),
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'section_id' => 'section',
        'ae_title' => 'title',
        'room_id' => 'room',
        'modality_id' => 'modality',
        'administrative_id' => 'administrative Id',
        'group_id' => 'group',
        'first_name' => 'name',
        'patient_ID' => 'patient Id',
        'sex_id' => 'gender',
        'name_prefix' => 'prefix',
        'division_id' => 'division',
        'parent_id' => 'parent',
        'telephone_number_2' => 'secondary telephone number',
        'cellphone_number_2' => 'Second mobile number',
        'last_name' => 'lastname',
        'referring_id' => 'referring',
        'steps' => [
            '*' => [
                'description' => 'step description',
                'administrative_ID' => 'step administrative Id',
                'modality_id' => 'step modality',
                'indications' => 'step indications',
                'order' => 'step order',
            ],
        ],
        'procedure_id' => 'procedure',
        'prefix_id' => 'prefix',
        'country_id' => 'country of residence',
        'unit_id' => 'units',
        'requestedProcedures' => 'procedures',
        'birthday_expression' => 'CRON expression birthday',
        'mammography_expression' => 'CRON expression mammography',
        'appointment_sms_expression' => 'CRON expression appointments',
        'appointment_sms_notification' => 'appointment sms notification',
        'patient_type_id' => 'patient type',
        'source_id' => 'source',
        'answer_id' => 'how did you find us?',
        'pregnancy_status_id' => 'pregnancy status',
        'smoking_status_id' => 'smoking status',
        'patient_state' => 'patient state',
        'issue_date' => 'issue date',
        'patient_identification_id' => 'patient Id',
        'patient' => [
            'first_name' => 'name',
            'sex_id' => 'gender',
            'last_name' => 'lastname',
            'birth_date' => 'birthdate',
            'name_prefix' => 'prefix',
            'country_id' => 'country',
            'citizenship' => 'citizenship',
            'occupation' => 'occupation',
            'address' => 'address',
        ],
        'patient_email' => 'email',
        'patient_telephone_number' => 'telephone number',
        'patient_cellphone_number' => 'cellphone number',

        'radiologist_fee' => "radiologist's fee",
        'technician_fee' => "technician's fee",
        'transcriptor_fee' => "transcriptor's fee",

        'alert_message_type_id' => 'alert message type',
        'start_date' => 'start',
        'end_date' => 'end',
        'message' => 'message',

        'serviceRequest' => [
            'weight' => 'weight',
            'height' => 'height',
            'smoking_status_id' => 'Smoking',
        ],

        'requestedProcedure' => [
            'number_of_plates' => 'Plates #',
            'equipment_id'     => 'Equipment'
        ],
        'endDate' => 'End Date',
        'startDate' => 'Start Date',
        'modality' => 'Modality',
        'procedure' => 'Procedure',
        'patientType' => 'Patient Type',
        'requestedProcedureStatus' => 'Status',
        'suspensionReason' => 'Suspension reason',
        'template' => 'Template',
        'base_structure' => 'base structure',
        'modalities' => 'Modalities',
        'appointmentStatus' => 'Appointment Status',
        'users' => 'Users',
        'rooms' => 'Rooms',
        
        //Alphabetical order please 
        'patient_allergies_check' => 'Allergies',
        'patient_first_name' => 'First name',
        'patient_last_name' => 'Last name',
        'procedure_contrast_study' => 'Contrast study',
        'referring' => 'Referring',
    ],
];
