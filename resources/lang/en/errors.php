<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alerts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */

    '23000' => 'Duplicate entry for :attribute',
    '403' => 'The requested operation is forbidden and cannot be completed.',

    'attributes' => [
        'ae_title' => 'title',
        'administrative_ID' => 'administrative Id',
        'steps' => [
            '*' => [
                'description' => 'step description',
                'administrative_ID' => 'step administrative Id',
                'modality_id' => 'step modality',
                'indications' => 'step indications',
                'order' => 'step order',
            ],
        ],
    ],
    
    'refreshing' => 'An error was occurred refreshing the scheduler',
    'no-match' => 'No match found',

    //:::::::::::::::: CUSTOM ERROR CODES FROM 5000 AND UP :::::::::::::::::::::
    '5000' => 'Order blocked by: ',
    '5001' => 'At least one created step is require to continue.',
    '5002' => 'Please insert at least one fill on the patient search form.',
    'bigger-than-now' => 'Error! The date is newer than today',
    'unknown' => 'Unknown Error'

];