<?php

return [

    'cronjob-assistant' => 'CronJobs Assistant',
    'minutes' => 'Minutes',
    'hours' => 'Hours',
    'days' => 'Days',
    'months' => 'Months',
    'dow' => 'Day of the Week',
    'miscellaneous' => 'Miscellaneous',
    'command' => 'Command',
    'cronjob' => 'Cron Expression',


    /*:::::::::::::::: Minutes Select :::::::::::::::::*/

    'cron-1-min' => 'Every minute',
    'cron-2-min' => 'Every 2 minutes',
    'cron-3-min' => 'Every 3 minutes',
    'cron-4-min' => 'Every 4 minutes',
    'cron-5-min' => 'Every 5 minutes',
    'cron-6-min' => 'Every 6 minutes',
    'cron-10-min' => 'Every 10 minutes',
    'cron-15-min' => 'Every 15 minutes',
    'cron-20-min' => 'Every 20 minutes',
    'cron-30-min' => 'Every 30 minutes',
    'cron-45-min' => 'Every 45 minutes',


    /*::::::::::::::::: Hours Select ::::::::::::::::::*/

    'cron-1-hour' => 'Every hour',
    'cron-2-hour' => 'Every 2 hours',
    'cron-4-hour' => 'Every 4 hours',
    'cron-6-hour' => 'Every 6 hours',
    'cron-8-hour' => 'Every 8 hours',
    'cron-12-hour' => 'Every 12 hours',


    /*::::::::::::::::: Hours Select ::::::::::::::::::*/

    'cron-1-day-midnight' => 'Every day at midnight',
    'cron-1-day-noon' => 'Every day at noon',
    'cron-2-day' => 'Every 2 days',
    'cron-3-day' => 'Every 3 days',
    'cron-5-day' => 'Every 5 days',
    'cron-10-day' => 'Every 10 days',
    'cron-15-day' => 'Every 15 days',

    /*::::::::::::::::: Month Select ::::::::::::::::::*/

    'cron-15-of-month-noon' => '15th of every month at noon',
    'cron-15-of-month-midnight' => '15th of every month at midnight',
    'cron-30-of-month-midnight' => '30th of every month at midnight',
    'cron-30-of-month-noon' => '30th of every month at noon',
    'cron-1-of-month-noon' => '1st of every month at noon',
    'cron-1-of-month-midnight' => '1st of every month at midnight',


    /*:::::::::::::::: Day of the week :::::::::::::::::*/

    'monday' => 'Every monday',
    'tuesday' => 'Everys tuesday',
    'wednesday' => 'Every wednesday',
    'thursday' => 'Every thursday',
    'friday' => 'Everyes friday',
    'saturday' => 'Every saturday',
    'sunday' => 'Every sunday',
    'mon-fri-noon' => 'From monday to friday at noon',
    'mon-fri-midnight' => 'From monday to friday at midnight',
    'sat-sun-noon' => 'Every saturday and sunday at noon',
    'sat-sun-midnight' => 'Every saturday and sunday at midnight',


];
