<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alerts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */

    'appointments' => 'citas',
    'reception' => 'recepcion',
    'preadmission' => 'preingreso',
    'interview' => 'entrevista',
    'technician' => 'tecnico',
    'radiologist' => 'radiologo',
    'transcriber' => 'transcripcion',
    'results' => 'resultados',
    'search' => 'busqueda',
    'patients' => 'pacientes',
    'legacypatients' => 'pacientes',
    'reports' => 'reportes',
    'administration' => 'administracion',
    'configuration' => 'configuracion',
    'configurations' => 'configuracion',
    'system' => 'sistema',

];