<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alerts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */

    '23000' => 'Valor para :attribute duplicado',
    '403' => 'La operación solicitada está prohibida y no puede ser completada.',


    'attributes' => [
        'description' => 'descripción',
        'name' => 'nombre',
        'ae_title' => 'título',
        'administrative_ID' => 'Id administrativo',
        'steps' => [
            '*' => [
                'description' => 'descripción del paso',
                'administrative_ID' => 'Id administrativo del paso',
                'modality_id' => 'modalidad del paso',
                'indications' => 'indicaciones del paso',
                'order' => 'orden del paso',
            ],
        ],
    ],

    'refreshing' => 'Ha ocurrido un error refrescando el calendario',
    'no-match' => 'No se encontraron resultados',

    //:::::::::::::::: CUSTOM ERROR CODES FROM 5000 AND UP :::::::::::::::::::::
    '5000' => 'Orden bloqueada por: ',
    '5001' => 'Al menos un paso debe ser creado para continuar.',
    '5002' => 'Debe introducir al menos un campo para la búsqueda.',
    'bigger-than-now' => 'Error! La fecha es mayor a la de hoy',
    'unknown' => 'Error Desconocido'

];