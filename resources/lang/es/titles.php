<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Tracking Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to set the titles of the views.
    |
    */

    'home' => 'inicio',
    'create' => 'crear',
    'new' => 'nuevo',
    'delete' => 'eliminar',
    'last' => 'último',
    'last-sm' => 'últ.',

    'institution' => 'institución',
    'patient' => 'paciente',
    'section' => 'sección',
    'action' => 'acción',
    'role' => 'rol',
    'user' => 'usuario',
    'configuration' => 'configuración',
    'division' => 'división',
    'room' => 'sala',
    'modality' => 'modalidad',
    'equipment' => 'equipo',
    'patient-type' => 'tipo de paciente',
    'printer' => 'impresora',
    'source' => 'procedencia',
    'template' => 'plantilla',
    'notificationtemplate' => 'plantilla de notificación',
    'consumable' => 'consumible',
    'referring' => 'Referente',
    'suspendreasons' => 'Motivos de suspensión',
    'order' => 'orden',
    'procedure' => 'procedimiento',
    'step' => 'paso',

    'select' => 'seleccionar',
    'add' => 'agregar',
    'edit' => 'editar',
    'login' => 'entrar',

    'change' => 'cambiar',
    'password' => 'contraseña',
    'availability' => 'Disponibilidad',

    //PLURAL
    'institutions' => 'instituciones',
    'patients' => 'pacientes',
    'patients_migration' => 'Pacientes Migración',
    'sections' => 'secciones',
    'actions' => 'acciones',
    'roles' => 'roles',
    'users' => 'usuarios',
    'configurations' => 'configuraciones',
    'divisions' => 'divisiones',
    'rooms' => 'salas',
    'modalities' => 'modalidades',
    'alertmessages' => 'mensajes de alerta',
    'patient-types' => 'tipos de pacientes',
    'printers' => 'impresoras',
    'sources' => 'procedencias',
    'templates' => 'plantillas',
    'notificationtemplates' => 'plantillas de notificaciones',
    'consumables' => 'consumibles',
    'referrings' => 'Referentes',
    'orders' => 'Órdenes',
    'procedures' => 'procedimientos',
    'steps' => 'pasos',
    'reset' => 'reestablecer',
    'forbidden' => 'prohibido',
    '401' => 'no autorizado',
    '402' => 'pago requerido',
    '403' => 'prohibido',
    '404' => 'no encontrado',
    '405' => 'método no permitido',
    '500' => 'error interno del servidor',
    '502' => 'pasarela incorrecta',
    '503' => 'servicio no disponible',
    'units' => 'unidades',
    'unit' => 'unidad',
    'appointments' => 'citas',
    'appointment' => 'cita',
    'new-appointment' => 'Nueva cita',
    'edit-appointment' => 'Editar cita',
    'docent-file' => 'Archivo Docente',
    'categories' => 'categorías',
    'category' => 'categoría',
    'subcategories' => 'subcategorías',
    'subcategory' => 'subcategoría',
    'teaching-file' => 'archivo docente',

    //Reception Section
    'reception' => 'recepción',
    'new-request' => 'Nueva Solicitud',
    'edit-request' => 'Editar Solicitud',
    'patient-search' => 'Búsqueda del paciente',
    'patient-info' => "Información del paciente",
    'request-info' => "Información de la solicitud",
    'related-documents' => 'Documentos asociados a la solicitud',
    'number-request' => 'número de solicitud',
    'appointments-list' => "Listado de citas",
    "patient-states" => "estados de paciente",
    "patient-state" => "estado de paciente",

    //Technician Section
    'orders-list' => 'Listado de órdenes',
    'order-details' => 'Detalles de la orden',
    'detail' => 'Detalle',
    'patient-record' => "Ficha del paciente",
    'appointment-record' => "Ficha de la cita",
    'search' => 'búsqueda',
    'search-of-appointments' => 'búsqueda',
    'show' => 'ver',
    'radiologist' => 'radiólogo',
    'approve' => 'aprobar',
    'technician-orders-list' => 'Listado de órdenes (Técnico)',
    'print-label' => 'imprimir etiqueta',
    
    //Radiologist Section
    'transcription' => 'Transcripción',
    'addendum' => 'Addendum',
    'transcriber' => 'transcriptor',
    'orders-to-approve' => 'Órdenes por aprobar',
    'orders-to-dictate' => 'Órdenes por dictar',
    'orders-to-dictate-list' => 'Lista de órdenes por dictar',
    'my-orders-to-dictate-list' => 'Lista de mis órdenes por dictar',
    'orders-to-approve-list' => 'Lista de órdenes por aprobar',
    'my-orders-to-approve-list' => 'Lista de mis órdenes por aprobar',
    'addendums-list' => 'Lista de addendums',
    'dictated-orders-list' => 'Lista de órdenes dictadas',

    //Pre-admission Section
    'preadmission-orders-list' => 'Listado de órdenes',
    'pre-admission' => 'Pre-Ingreso',
    'orders-to-transcribe' => 'Órdenes por transcribir',

    'results' => 'resultados',
    'deliver' => 'entregar',
    'global-statistics' => 'Estadísticas globales',
    'delivery-information' => 'Datos de la entrega',

    //Search Section
    'filters' => 'Filtros',
    'column-change' => 'Cambio de Columnas',
    'search-orders-list'=>'Listado de órdenes',

    // Home Section
    'month-statistics' => 'Estadísticas mensuales',
    'statistics' => 'Estadística',
    'personal-statistics' => 'Estadística personal',
    

    //Reports Section
    'reports' => 'Reportes',
    'generate' => 'Generar',

    // Results Section
    'finished-orders-list' => 'Lista de órdenes finalizadas',

    //Migrations Section
    'migrations-history'=>'Migración de histórico',
    'migrations'=>'Migración',
    'requests'=>'Solicitud',
    'scan-files'=>'Documento escaneado',
    'data-conn'=>'datos de conexión',
    'title-confirm'=>'confirmación'

    

];