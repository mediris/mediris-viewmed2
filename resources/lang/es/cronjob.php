<?php

return [
    'cronjob-assistant' => 'Asistente de CronJobs',
    'minutes' => 'Minutos',
    'hours' => 'Horas',
    'days' => 'Días',
    'months' => 'Meses',
    'dow' => 'Día de la Semana',
    'miscellaneous' => 'Misceláneos',
    'command' => 'Comando',
    'cronjob' => 'Expresión Cron',


    /*:::::::::::::::: Minutes Select :::::::::::::::::*/

    'cron-1-min' => 'Cada minuto',
    'cron-2-min' => 'Cada 2 minutos',
    'cron-3-min' => 'Cada 3 minutos',
    'cron-4-min' => 'Cada 4 minutos',
    'cron-5-min' => 'Cada 5 minutos',
    'cron-6-min' => 'Cada 6 minutos',
    'cron-10-min' => 'Cada 10 minutos',
    'cron-15-min' => 'Cada 15 minutos',
    'cron-20-min' => 'Cada 20 minutos',
    'cron-30-min' => 'Cada 30 minutos',
    'cron-45-min' => 'Cada 45 minutos',


    /*::::::::::::::::: Hours Select ::::::::::::::::::*/

    'cron-1-hour' => 'Cada hora',
    'cron-2-hour' => 'Cada 2 horas',
    'cron-4-hour' => 'Cada 4 horas',
    'cron-6-hour' => 'Cada 6 horas',
    'cron-8-hour' => 'Cada 8 horas',
    'cron-12-hour' => 'Cada 12 horas',


    /*::::::::::::::::: Hours Select ::::::::::::::::::*/

    'cron-1-day-midnight' => 'Todos los días a media noche',
    'cron-1-day-noon' => 'Todos los días a medio día',
    'cron-2-day' => 'Cada 2 días',
    'cron-3-day' => 'Cada 3 días',
    'cron-5-day' => 'Cada 5 días',
    'cron-10-day' => 'Cada 10 días',
    'cron-15-day' => 'Cada 15 días',

    /*::::::::::::::::: Month Select ::::::::::::::::::*/

    'cron-15-of-month-noon' => '15 de cada mes a medio día',
    'cron-15-of-month-midnight' => '15 de cada mes a media noche',
    'cron-30-of-month-midnight' => '30 de cada mes a media noche',
    'cron-30-of-month-noon' => '15 de cada mes a medio día',
    'cron-1-of-month-noon' => '1ero de cada mes a medio día',
    'cron-1-of-month-midnight' => '1ero de cada mes a media noche',


    /*:::::::::::::::: Day of the week :::::::::::::::::*/

    'monday' => 'Todos los lunes',
    'tuesday' => 'Todos los martes',
    'wednesday' => 'Todos los iércoles',
    'thursday' => 'Todos los jueves',
    'friday' => 'Todos los viernes',
    'saturday' => 'Todos los sábado',
    'sunday' => 'Todos los domingo',
    'mon-fri-noon' => 'De lunes a viernes a media noche',
    'mon-fri-midnight' => 'De lunes a viernes a medio día',
    'sat-sun-noon' => 'Sábado y domingo a medio día',
    'sat-sun-midnight' => 'Sábado y domingo a media noche',


];