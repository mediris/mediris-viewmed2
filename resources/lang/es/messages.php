<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during .
    |
    */

    'welcome' => 'Bienvenido',
    'landing' => "La página de inicio de su aplicación.",
    'delete' => "Está seguro de borrar este elemento?",

    'no-pac-url' => "El url pacs no está configurado para esta institución",
    'no-pac-username' => "Su usuario no tiene configurado el nombre de usuario o contraseña del pacs",

    /**
     * Sections
     */

    'actions' => "acciones",
    'action' => 'acción',
    'sections' => "secciones",
    'section' => 'sección',
    'subsections' => "subsecciones",
    'subsection' => 'subsección',
    'role' => 'rol',
    'roles' => 'roles',
    'user' => 'usuario',
    'users' => 'usuarios',
    'institution' => 'institución',
    'institutions' => 'instituciones',
    'division' => 'división',
    'divisions' => 'divisiones',
    'room' => 'habitación',
    'rooms' => 'habitaciones',
    'modality' => 'modalidad',
    'modalities' => 'modalidades',
    'equipment' => 'equipo',
    'referring' => 'referente',
    'referrings' => 'referentes',
    'configuration' => 'configuración',
    'configurations' => 'configuraciones',
    'patient-types' => 'tipos de pacientes',
    'patient-type' => 'tipo de paciente',
    'ubication' => 'ubicación',
    'ubications' => 'ubicaciones',
    'source' => 'procedencia',
    'sources' => 'procedencias',
    'procedure' => 'procedimiento',
    'procedures' => 'procedimientos',
    'template' => 'plantilla',
    'templates' => 'plantillas',
    'consumable' => 'consumible',
    'consumables' => 'consumibles',
    'printer' => 'impresora',
    'printers' => 'impresoras',
    'step' => 'paso',
    'steps' => 'pasos',
    'order' => 'orden',
    'orders' => 'ordenes',
    'patient' => 'paciente',
    'patients' => 'pacientes',
    'sex' => 'sexo',
    'sexes' => 'sexos',
    'responsable' => 'responsable',
    'responsables' => 'responsables',
    'patient-id' => 'Id paciente',
    'other-institutions' => 'otras instituciones',
    'filter' => 'filtrar',
    'reset'=> 'reestablecer',


    'password' => 'contraseña',
    'lbl-password-confirmation' => 'Confirma Contrase&ntilde;a',
    'spn-reset-password' => 'Cambiar Contrase&ntilde;a',
    'remember' => 'recordar',
    'forgot-password' => '¿Olvidó su Contraseña?',
    'login' => 'iniciar sesión',
    'btn-send' => 'Enviar',
    'lbl-email' => 'Correo Electr&oacute;nico',
    'email' => 'Correo Electr&oacute;nico',

    'success-add' => ':name agregad@ con éxito',
    'error-add' => ':name sin agregar',
    'success-edit' => ':name editad@ con éxito',
    'error-edit' => ':name no editad@',
    'success-delete' => ':name eliminad@ con éxito',
    'error-delete' => ':name no eliminad@',

    'show' => 'ver',
    'add' => 'crear',
    'edit' => 'editar',
    'purge' => 'eliminar',


    /*
     * Search
     * */

    'search' => 'Buscar',
    'bas-search' => 'B&uacute;squeda B&aacute;sica',
    'adv-search' => 'B&uacute;squeda Avanzada',
    'period' => 'Per&iacute;odo',
    'doctor-assigned' => 'M&eacute;dico Asignado',
    'all' => 'Todos',
    'anyone' => 'Cualquiera',
    'dictated-by' => 'Dictado por',

    /*
     * Nav Menu
     * */

    'main' => 'Principal',
    'appoiments' => 'Citas',
    'reception' => 'Recepci&oacute;n',
    'pre-entry' => 'Pre-ingreso',
    'interview' => 'Entrevista',
    'technical' => 'T&eacute;cnico',
    'radiologist' => 'Radi&oacute;logo',
    'issue-orders' => '&Oacute;rdenes por dictar',
    'approve-orders' => '&Oacute;rdenes por aprobar',
    'addendum' => 'Addendum',
    'orders-issued' => '&Oacute;rdenes dictadas',
    'transcription' => 'Transcripci&oacute;n',
    'results' => 'Resultados',
    'search-menu' => 'B&uacute;squeda',
    'patient' => 'Paciente',
    'reports' => 'Reportes',
    'administration' => 'Administraci&oacute;n',

    /*
     * Grid titles
     * */

    'id' => 'Id',
    'name' => 'Nombre',
    'group' => 'Grupo',
    'printers' => 'Impresoras',
    'description' => 'Descripci&oacute;n',
    'units' => 'Unidades',
    'title' => 'T&iacute;tulo',
    'equipments' => 'Equipos',

    /*
     * Forms labels
     * */

    'add-action' => 'Agregar Acci&oacute;n',
    'edit-action' => 'Editar Acci&oacute;n',
    'lbl-action-name' => 'Nombre de Acci&oacute;n',
    'lbl-section' => 'Secci&oacute;n',
    'btn-save' => 'Guardar',
    'lbl-division-name' => 'Nombre de Divisi&oacute;n',
    'add-division' => 'Agregar Divisi&oacute;n',
    'edit-division' => 'Editar Divisi&oacute;n',
    'add-consumable'=> 'Agregar Consumible',
    'edit-consumable'=> 'Editar Consumible',
    'add-institution' => 'Agregar Instituci&oacute;n',
    'edit-institution' => 'Editar Instituci&oacute;n',
    'administrative_id' => 'ID Administrativo',
    'url' => 'URL',
    'address' => 'Direcci&oacute;n',
    'add-modality' => 'Agregar Modalidad',
    'edit-modality' => 'Editar Modalidad',
    'parent-modality' => 'Modalidad Matriz',
    'add-room' => 'Agregar Habitaci&oacute;n',
    'edit-room' => 'Editar Habitaci&oacute;n',
    'first-name' => 'Nombre',
    'last-name' => 'Apellido',
    'user-name' => 'Nombre de Usuario',
    'password-confirmation' => 'Confirmaci&oacute;n de Contrase&ntilde;a',
    'telephone-number' => 'N&uacute;mero de Tel&eacute;fono',
    'telephone-number-2' => 'N&uacute;mero de Tel&eacute;fono 2',
    'cellphone-number' => 'N&uacute;mero Celular',
    'cellphone-number-2' => 'N&uacute;mero Celular 2',
    'signature' => 'Firma',
    'additional-information' => 'Informaci&oacute;n Adicional',
    'suffix-id' => 'Suffix',
    'prefix-id' => 'T&iacute;tulo',
    'add-user' => 'Agregar Usuario',
    'edit-user' => 'Editar Usuario',
    'add-section' => 'Agregar Secci&oacute;n',
    'edit-section' => 'Editar Secci&oacute;n',
    'lbl-section-name' => 'Nombre de Secci&oacute;n',
    'add-role' => 'Agregar Rol',
    'edit-role' => 'Editar Rol',
    'lbl-role-name' => 'Nombre del Rol',
    'add-equipment' => 'Agregar Equipo',
    'edit-equipment' => 'Editar Equipo',
    'active' => '&iquest;Est&aacute; Activo?',
    'add-patient-type' => 'Agregar Tipo de Paciente',
    'edit-patient-type' => 'Editar Tipo de Paciente',
    'priority' => 'Prioridad',
    'icon' => 'Icon',
    'admin-aprobbation' => '&iquest;Aprobaci&oacute;n Admin?',
    'sms-send' => '&iquest;Enivar SMS?',
    'email-patient' => '&iquest;Email del Paciente?',
    'email-refeer'=> '&iquest;Email de referencia?',
    'add-source' => 'Agregar Procedencia',
    'edit-source' => 'Editar Procedencia',
    'add-template' => 'Agregar Plantilla',
    'edit-template' => 'Editar Plantilla',
    'no-history'=>'Este paciente no contiene historial',
    'death-date' => 'La fecha de muerte no puede ser menor a la fecha de nacimiento',

    /*
     * Messages
     * */

    'approve-orders'=> 'Hay :number &oacute;rden/es por aprobar.',
    'password-reset-line1'=>'Usted est&aacute; recibiendo este Email porque hemos recibido una solicitud de cambio de Contrase&ntilde;a',
    'password-reset-noaction'=>'Si usted no solicit&oacute; un cambio de contrase&ntilde;a, no requiere ninguna acci&oacute;n.',
    'password-reset-button'=>'Cambiar Contrase&ntilde;a',
    'password-reset-link1'=>'Si usted est&aacute; teniendo problemas haciendo click en el bot&oacute;n ',
    'password-reset-link2'=>' copie y pegue la siguiente URL en su navegador web: ',
    'password-reset-dontresponse'=>'(Por favor no responda este correo, esta es una cuenta no monitoreada)',
    'password-reset-subject'=>'Notificación de cambio de contraseña',
    /***
     * 
     * polls
     * 
     */
    'error-polls-inactive' => 'El envio de encuesta se encuentra inactivo para esta Institución',
    'error-polls-not-coincidence' => 'No hay coincidencia con esos parámetros de búsqueda',
    'error-url-poll' => 'El url de las encuestas no puede estar vacio.'
];
