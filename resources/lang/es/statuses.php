<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Statuses Language Lines
    |--------------------------------------------------------------------------
    |
    | .
    |
    */

    'doctor' => 'por un doctor',
    'clinic' => 'por una clínica',
    'friends' => 'por amigos',
    'patient' => 'por otra paciente',
    'radio' => 'via radio',
    'press' => 'via prensa',
    'magazines' => 'por revistas',
    'tv' => 'via tv',
    'cinema' => 'via cine',
    'internet' => 'internet',
    'social' => 'redes sociales',
    'other' => 'otros',

    'not-pregnant' => 'No embarazada',
    'possibly-pregnant' => 'Posiblemente embarazada',
    'definitely-pregnant' => 'Definitivamente embarazada',
    'unknown' => 'Desconocido',
    'na' => 'N/A',

    'yes' => 'sí',
    'no' => 'no',


];