{{-- 
    By: Ricardo Martos

    @include(

        'includes.procedures-select',[

            'idname'       => 'id/name del select', --requerido

            'id'       => 'id del select', --requerido

            'name'       => 'name del select', --requerido

            'procedures'   => 'variable que contenga los procedimientos', --requerido

            'coordinador'  => 'la autentificacion para ver si es coordinador, para deshabilitar el select' --no requerido

            'match'        => 'el varos para seleccionar el option' -- requerido

        ]

    )
     
--}}

<select class="form-control" 
		name="{{ $name or $idname }}" 
		id="{{ $id or $idname }}"
        data-live-search="true"
        data-hide-disabled="true"
        title="{{ ucfirst(trans('labels.select')) }}" 
        data-none-results-text="{{ucfirst(trans('errors.no-match'))}}" multiple="multiple">
    @foreach($procedures as $procedure)
        {!! $procedure->modality_id  !!}
        <option disabled data-modality-id="{{ $procedure->modality_id }}" 

                {{  isset($match) ? $procedure->id == $match ? 'selected' : '' : '' }}>

        	{{ $procedure->description }}

        </option>

    @endforeach

</select>