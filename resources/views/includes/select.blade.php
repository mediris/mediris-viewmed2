<select class="form-control {{ $class or '' }}" name="{{ $name or $idname }}" id="{{ $id or $idname }}" {{ $attr or '' }}>
    <option value="" disabled selected>{{ ucfirst(trans('labels.select')) }}</option>
    @foreach($data as $element)
    	<?php
    		if ( gettype($element) == 'array' ) {
    			$opValue = $element[$keys[0]];
    			$opLabel = $element[$keys[1]];
    		} else {	
				$opValue = $element->{ $keys[0] };
				$opLabel = $element->{ $keys[1] };
    		}
    		$selected = ( isset($value) && $value == $opValue )?'selected':'';
    	?>
    	<option value="{{ $opValue }}" {{ $selected }}>{{ $opLabel }}</option>
	@endforeach
</select>