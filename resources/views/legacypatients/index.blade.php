@extends('layouts.app')

@section('title', ucfirst(trans('titles.patients')))

@section('content')


@include('partials.actionbar',[
    'title' => ucfirst(trans('titles.patients_migration')),
    'elem_type' => '',
    'elem_name' => '',
    'form_id' => '',
    'fancybox' => '',
    'routeBack' => '',
    'clearFilters' => true
])

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="title">
                <h1>{{ ucfirst(trans('messages.patients')) }}</h1>
            </div>

            <?php

                // Grid
                $options = (object) array(
                    'url' => 'legacypatients'
                );
                $kendo    = new \App\CustomKendoGrid($options);

    

                // Create Schema
                $kendo->createSchema(true, true);

                // Create Data Source
                $kendo->createDataSource();
                
                // Create Grid
                $kendo->createGrid('patients');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
                $Id->field('id')
                        ->filterable(false)
                        ->title(trans('labels.id'));
                $PatientId = new \Kendo\UI\GridColumn();
                $PatientId->field('cedula')
                        ->title(ucfirst(trans('labels.patient-id')));
                $FirstName = new \Kendo\UI\GridColumn();
                $FirstName->field('nombres')
                        ->title(ucfirst(trans('labels.patient.first-name')));
                $LastName = new \Kendo\UI\GridColumn();
                $LastName->field('apellidos')
                        ->title(ucfirst(trans('labels.patient.last-name')));
                $Email = new \Kendo\UI\GridColumn();
                $Email->field('email')
                        ->encoded(false)
                        ->title(ucfirst(trans('labels.email')));
                $PhoneNumber = new \Kendo\UI\GridColumn();
                $PhoneNumber->field('telefonoFijo')
                        ->encoded(false)
                        ->title(ucfirst(trans('labels.telephone-number')))
                        ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<a href=tel:\"' + row.telefonoFijo + '\">' + row.telefonoFijo + '</a>';
                            }
                        "));
                $CellPhoneNumber = new \Kendo\UI\GridColumn();
                $CellPhoneNumber->field('telefonoMovil')
                        ->encoded(false)
                        ->title(ucfirst(trans('labels.cellphone-number')))
                        ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<a href=tel:\"' + row.telefonoMovil + '\">' + row.telefonoMovil + '</a>';
                            }
                        "));
                $Actions = new \Kendo\UI\GridColumn();
                $Actions->title(ucfirst(trans('labels.actions')));
 
                $history = "\"<div class='order-action show-patient-detail'><a href='" . url('legacypatients/history') .
                        "/\" + row.id + \"" .
                        "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.patient-history') .
                        "'><i class='fa fa-archive' aria-hidden='true'></i></a></div>\"";

                $ableToBeBlockedButtons = "<div class='actions'>\" + " .  $history . " + \"</div>\"";

                $actionButtons = "\"<div class='action-buttons'>" . $ableToBeBlockedButtons . " + \"</div>\"";

                $Actions->template(new Kendo\JavaScriptFunction("
                        function (row) {
                            //console.log(row);
                            return  " . $actionButtons . ";
                        }"));

                // Excel
                $kendo->setExcel('titles.patients-list', '
                    function(e) {
                        if (!exportFlag) {
                            e.sender.hideColumn(6);
                            e.preventDefault();
                            exportFlag = true;
                            setTimeout(function () {
                              e.sender.saveAsExcel();
                            });
                          } else {
                            e.sender.showColumn(6);
                            exportFlag = false;
                          }
                    }', '.xlsx');

                // PDF
                $kendo->setPDF('titles.patients-listt', 'patients/1', '
                    function (e) {
                        e.sender.hideColumn(6);
                        e.promise.done(function() {
                            e.sender.showColumn(6);
                        });
                    }');

                // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
                    $PatientId,
                    $LastName,
                    $FirstName,
                    $Email,
                    $PhoneNumber,
                    $CellPhoneNumber,
                    $Actions,
                ));

                $kendo->generate(true, true, null, 550);

            ?>

            {!! $kendo->render() !!}

        </div>
    </div>
</div>


<script type="x/kendo-template" id="page-template">
    <div class="page-template">
    <div class="header">
    <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
    
    </div>
    <div class="watermark">{{ Session::get('institution')->name }}</div>
    <div class="footer">
    {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
    </div>
    </div>
</script>

<style type="text/css">
    /* Page Template for the exported PDF */
    .page-template {
        font-family: "Open Sans", "Arial", sans-serif;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
    }
    .page-template .header {
        position: absolute;
        top: 30px;
        left: 30px;
        right: 30px;
        border-bottom: 1px solid #888;
        color: #888;
    }
    .page-template .footer {
        position: absolute;
        bottom: 30px;
        left: 30px;
        right: 30px;
        border-top: 1px solid #888;
        text-align: center;
        color: #888;
    }
    .page-template .watermark {
        font-weight: bold;
        font-size: 400%;
        text-align: center;
        margin-top: 30%;
        color: #aaaaaa;
        opacity: 0.1;
        transform: rotate(-35deg) scale(1.7, 1.5);
    }
</style>

@endsection
