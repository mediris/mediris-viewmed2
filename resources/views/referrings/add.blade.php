
    <div class="container-fluid">

            <form method="post" action="{{ route('referrings.add') }}" id="ReferringAddForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="modal-header">
                            <h4 class="modal-title">{{ ucfirst(trans('titles.add')).' '.trans('titles.referring') }}</h4>
                        </div>
                    </div>

                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div>
                                <label for='first_name'>{{ ucfirst(trans('labels.name')) }} *</label>
                            </div>

                            <div>
                                <input type="text" name="first_name" id="first_name" class="input-field form-control user btn-style" value="{{ old('first_name') }}"/>
                                @if ($errors->has('first_name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div>
                                <label for='last_name'>{{ ucfirst(trans('labels.last-name')) }} *</label>
                            </div>

                            <div>
                                <input type="text" name="last_name" id="last_name" class="input-field form-control user btn-style" value="{{ old('last_name') }}"/>
                                @if ($errors->has('last_name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div>
                                <label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }} *</label>
                            </div>

                            <div>
                                <input type="text" name="administrative_ID" id="units" class="input-field form-control user btn-style" value="{{ old('administrative_ID') }}"/>
                                @if ($errors->has('administrative_ID'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div>
                                <label for='telephone_number'>{{ ucfirst(trans('labels.telephone-number')) }}</label>
                            </div>
                            <div>
                                <input type="tel" class="form-control" name="telephone_number" id="telephone_number" value="{{ old('telephone_number') }}">
                                @if ($errors->has('telephone_number'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('telephone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div>
                                <label for='telephone_number_2'>{{ ucfirst(trans('labels.telephone-number-2')) }}</label>
                            </div>
                            <div>
                                <input type="tel" class="form-control" id="telephone_number_2" name="telephone_number_2" value="{{ old('telephone_number_2') }}">
                                @if ($errors->has('telephone_number_2'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('telephone_number_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div>
                                <label for='cellphone_number'>{{ ucfirst(trans('labels.cellphone-number')) }}</label>
                            </div>
                            <div>
                                <input type="tel" class="form-control" name="cellphone_number" id="cellphone_number" value="{{ old('cellphone_number') }}">
                                @if ($errors->has('cellphone_number'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('cellphone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div>
                                <label for='cellphone_number_2'>{{ ucfirst(trans('labels.cellphone-number-2')) }}</label>
                            </div>
                            <div>
                                <input type="tel" class="form-control" name="cellphone_number_2" id="cellphone_number_2" value="{{ old('cellphone_number_2') }}">
                                @if ($errors->has('cellphone_number_2'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('cellphone_number_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div>
                                <label for='email'>{{ ucfirst(trans('labels.email')) }} *</label>
                            </div>
                            <div>
                                <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div>
                                <label for='address'>{{ ucfirst(trans('labels.address')) }}</label>
                            </div>
                            <div>
                                <textarea class="form-control" name="address" id="address">{{ old('address') }}</textarea>
                                @if ($errors->has('address'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <label class="control-label" for="user_id">{{ ucfirst(trans('labels.user')) }}</label>
                            </div>
                            <div>
                                <select class="form-control" name="user_id" id="user_id">
                                    <option value="0" selected>{{ ucfirst(trans('labels.none')) }}</option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->administrative_ID }} {{ $user->first_name }} {{ $user->last_name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('user_id'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="modal-footer">
                            <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                                <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                            </button>
                        </div>
                    </div>
            </form>
            <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
            {!! JsValidator::formRequest('App\Http\Requests\ReferringRequest', '#ReferringAddForm'); !!}
       
    </div>


    <script>
        //ColorBoxSelectsInit();
        ColorBoxmultiSelectsInit();
        initMaskedInputs();
    </script>