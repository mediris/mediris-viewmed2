@extends('layouts.app')

@section('title',ucfirst(trans('titles.show')).' '.trans('titles.order').' - '.ucfirst(trans('labels.transcription')))

@section('content')

    @include('partials.actionbar',[ 'title' => trans('titles.transcription').' - '.ucfirst(trans('titles.order-details')),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('labels.transcribe-order')),
                                    'form_id' => '#TranscriberEditForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('transcriber'),
                                ])

    <div class="container-fluid transcriber edit">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-file" aria-hidden="true"></i> {{ trans('titles.patient-info') }}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="patient-img ">
                                      @if( isset($requestedProcedure->patient->id) && !empty($requestedProcedure->patient->photo))
                                          <img src="{{ asset( 'storage/'. $requestedProcedure->patient->photo ) }}"
                                          alt="Patient-Image" class="img-responsive" id='avatar'>
                                      @else
                                          <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                          class="img-responsive" id='avatar'>
                                      @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="patient-info">
                                                <p><strong>{{ ucfirst(trans('labels.name')) }}:</strong> {{ $requestedProcedure->patient->first_name . ' ' . $requestedProcedure->patient->last_name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.age')) }}:</strong> {{ $requestedProcedure->patient->age['years'] }} {{ trans('labels.years') }}, {{ $requestedProcedure->patient->age['months'] }} {{ $requestedProcedure->patient->age_months == 1 ? trans('labels.month') : trans('labels.months') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.gender')) }}:</strong> {{ $requestedProcedure->patient->sex_id == 1 ? trans('labels.male') : trans('labels.female') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.height')) }}:</strong> {{ $requestedProcedure->service_request->height
                                            . ' ' . trans('labels.meters') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.weight')) }}:</strong> {{ $requestedProcedure->service_request->weight
                                            . ' ' . trans('labels.kilograms') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.birth-date')) }}:</strong> {{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($requestedProcedure->patient->birth_date)) : date('Y-m-d', strtotime($requestedProcedure->patient->birth_date)) }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.history-id')) }}:</strong> {{ $requestedProcedure->patient->id }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.address')) }}:</strong> {{ $requestedProcedure->patient->address }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.responsable')) }}:</strong> {{ $requestedProcedure->service_request->parent or trans('labels.na') }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.admission-id')) }}:</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.contrast-allergy')) }}:</strong> {{ $requestedProcedure->patient->contrast_allergy ? ucfirst(trans('labels.yes')) : 'No' }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.medical-alerts')) }}:</strong> {{ $requestedProcedure->patient->medical_alerts }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ ucfirst(trans('labels.allergies')) }}: </strong>
                                        {{ $requestedProcedure->patient->allergies }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-file" aria-hidden="true"></i> {{ ucfirst(trans('titles.procedure')) }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ $requestedProcedure->procedure->description }}</strong> </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.request-num') }}:</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.order-num') }}:</strong> {{ $requestedProcedure->id }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.patient-type')) }}:</strong> {{ $requestedProcedure->service_request->patient_type->description }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.source')) }}:</strong> {{ $requestedProcedure->service_request->source->description }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.referring') . ': ' }}</strong>{{ $requestedProcedure->service_request->referring ? ucwords($requestedProcedure->service_request->referring->first_name . ' ' . $requestedProcedure->service_request->referring->last_name) : ucfirst(trans('labels.not-specified'))}}
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.admitted-by') }}:</strong> {{ $requestedProcedure->service_request->user->first_name }} {{ $requestedProcedure->service_request->user->last_name }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.attended-by') }}:</strong> {{ $requestedProcedure->technician_user_name }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.dictated-by') }}:</strong> {{ $requestedProcedure->radiologist_user_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                      <strong>{{ trans('labels.contrast-study') . ': ' }}</strong>
                                        @if($requestedProcedure->procedure_contrast_study == 1)
                                            {{ trans('labels.yes')}}
                                        @else
                                            {{ trans('labels.not')}}
                                        @endif    
                                      </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.patient-abdominal-circumference') . ': ' }}</strong> {{ $requestedProcedure->abdominal_circumference or trans('labels.na') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.observations') }}:</strong> {{ $requestedProcedure->comments }}</p>
                                </div>
                            </div>

                            @if($requestedProcedure->reject_user_name != "")
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>Transcripci&oacute;n rechazada por:</strong> {{ $requestedProcedure->reject_user_name }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>Raz&oacute;n del rechazo:</strong> {{ $requestedProcedure->reject_reason }}</p>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('labels.documents')) }}</h3>
                        </div>

                        <div class="panel-body">
                            <div class="k-widget k-upload k-header">
                                <ul class="k-upload-files k-reset">
                                @foreach($requestedProcedure->patient->patientDocuments as $key => $patientDocument)
                                    <a href="{{ route('patients.document', ['document' => $patientDocument->id ]) }}" target="_blank">
                                        <li class="k-file" id="k-file-{{ $key + 100 }}" attr-id="{{ $patientDocument->id }}">
                                            <span class="k-progress" style="width: 100%;"></span>
                                        <span class="k-file-extension-wrapper">
                                            <span class="k-file-extension">{{ $patientDocument->type }}</span>
                                            <span class="k-file-state"></span>
                                        </span>
                                        <span class="k-file-name-size-wrapper">
                                            <span class="k-file-name" filename="{{ $patientDocument->filename }}">{{ $patientDocument->name }}</span>
                                            <span class="k-file-size">95.56 KB</span>
                                        </span>
                                        </li>
                                    </a>
                                @endforeach

                                @foreach($requestedProcedure->service_request->request_documents as $key => $requestDocument)
                                    <a href="{{ route('reception.document', ['document' => $requestDocument->id ]) }}" target="_blank">
                                        <li class="k-file" id="k-file-{{ $key + 100 }}">
                                            <span class="k-progress" style="width: 100%;"></span>
                                        <span class="k-file-extension-wrapper">
                                            <span class="k-file-extension">{{ $requestDocument->type }}</span>
                                            <span class="k-file-state"></span>
                                        </span>
                                        <span class="k-file-name-size-wrapper">
                                            <span class="k-file-name" title="{{ $requestDocument->name }}">{{ $requestDocument->name }}</span>
                                            <span class="k-file-size">95.56 KB</span>
                                        </span>
                                        </li>
                                    </a>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="panel sombra">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('labels.dictation') }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <p><strong>Dictado por: {{ $requestedProcedure->radiologist_user_name }}</strong></p>
                                <audio id="audio" controls src="{{ $audio }}"></audio>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="panel sombra">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('labels.order-info') }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                <form action="{{ route('transcriber.edit', [$requestedProcedure->id]) }}" method='post' id="TranscriberEditForm">

                                    <input id="approve" type="hidden" name="approve" value="0">

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-xs-12">

                                                    <div class="btn-icons">

                                                        <label for="template_id">{{ ucfirst(trans('labels.template')) }}:</label>

                                                        <select name="template_id" id="template_id">
                                                            <option value="" selected disabled>{{ ucfirst(trans('labels.select')) }}</option>
                                                            @foreach($requestedProcedure->procedure->templates as $template)
                                                                <option value="{{ $template->id }}">{{ $template->description }}</option>
                                                            @endforeach
                                                            <option value="create-temprate"> Crear Plantilla</option>
                                                        </select>

                                                        @foreach($requestedProcedure->procedure->templates as $template)
                                                            <input type="hidden" id="template_{{ $template->id }}" value="{{ $template->template }}">
                                                        @endforeach

                                                        <a href="{{ route('radiologist.edit.draft', [$requestedProcedure->id]) }}" id="btn-save-draft" class="btn btn-form btn-icon" data-toggle="tooltip" title="{{ trans('labels.save-draft') }}">
                                                          <i class="fa fa-floppy-o"></i>
                                                        </a>

                                                        <!--
                                                        <a href="#" class="btn btn-form btn-icon" data-toggle="tooltip" title="{{ trans('labels.print-draft') }}">
                                                            <i class="fa fa-print"></i>
                                                        </a>
                                                        -->

                                                        <a href="{{ route('radiologist.edit.draft', [$requestedProcedure->id]) }}" target="_blank" class="btn btn-form btn-icon" data-toggle="tooltip" title="{{ trans('labels.preview-draft') }}">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row m-t-20">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 big-textarea-plantilla">
                                                    <textarea class="" name="text" id="template">{{ $requestedProcedure->text }}</textarea>
                                                    @if ($errors->has('text'))
                                                        <span class="help-block error-help-block">{{ $errors->first('text') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection