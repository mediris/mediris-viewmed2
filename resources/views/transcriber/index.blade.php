@extends('layouts.app')

@section('title', ucfirst(trans('labels.transcription')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('labels.transcription')),
                                    'elem_type' => '',
                                    'elem_name' => '',
                                    'form_id' => '',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => '',
                                    'clearFilters' => true
                                ])
    <div class="container-fluid transcriber index">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="title">
                    <h1>{{ trans('labels.orders-to-transcribe') }}</h1>
                </div>

                <?php

                // Grid
                $options = (object) array(
                    'url' => 'transcriber'
                );
                $kendo    = new \App\CustomKendoGrid($options);

                // Fields
                $IsUrgentField = new \Kendo\Data\DataSourceSchemaModelField('isUrgent');
                $IsUrgentField->type('string');

                // $RejectUserIdField = new \Kendo\Data\DataSourceSchemaModelField('rejectUserID');
                // $RejectUserIdField->type('string');
                // $RejectUserNameField = new \Kendo\Data\DataSourceSchemaModelField('rejectUserName');
                // $RejectUserNameField->type('string');

                $RadiologistUserIdField = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserID');
                $RadiologistUserIdField->type('string');                
                $RadiologistUserNameField = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserName');
                $RadiologistUserNameField->type('string');

                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
                $PatientTypeIconField->type('string');
                $PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
                $PatientTypeField->type('string');
                $DictationDateField = new \Kendo\Data\DataSourceSchemaModelField('dictationDate');
                $DictationDateField->type('date');
                $DictationHourField = new \Kendo\Data\DataSourceSchemaModelField('dictationDate');
                $DictationHourField->type('date');
                $ServiceRequestIdField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
                $ServiceRequestIdField->type('string');
                $OrderIdField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
                $OrderIdField->type('string');
                $ProcedureDescriptionField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
                $ProcedureDescriptionField->type('string');
                $PatientNameField = new \Kendo\Data\DataSourceSchemaModelField('patientName');
                $PatientNameField->type('string');
                $PatientIdField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                $PatientIdField->type('string');
                
                // Add Fields
                $kendo->addFields(array(
                    $IsUrgentField,

                    //$RejectUserIdField,
                    //$RejectUserNameField,

                    $RadiologistUserIdField,
                    $RadiologistUserNameField,

                    $PatientTypeIconField,
                    $PatientTypeField,
                    $DictationDateField,
                    $DictationHourField,
                    $ServiceRequestIdField,
                    $OrderIdField,
                    $ProcedureDescriptionField,
                    $PatientNameField,
                    $PatientIdField,
                ));

                // Create Schema
                $kendo->createSchema(true, true);

                // Create Data Source
                $kendo->createDataSource();
                
                // Create Grid
                $kendo->createGrid('transcriber');

                // Columns
                $IsUrgent = new \Kendo\UI\GridColumn();
                $IsUrgent->title(ucfirst(trans('labels.urgent')))
                        ->template(new Kendo\JavaScriptFunction("function(row){
                            if(row.isUrgent == 1)
                            {
                                return '<i class=\"fa fa-exclamation-triangle red-danger\" aria-hidden=\"true\"></i>';
                            }
                            else
                            {
                               return '<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>'
                            }
                        }"));

                // $RejectUserName = new \Kendo\UI\GridColumn();
                // $RejectUserName->field('rejectUserName')
                //         ->title(ucfirst(trans('labels.rejected-by')))->width(50);

                $RadiologistUserName = new \Kendo\UI\GridColumn();
                $RadiologistUserName->field('radiologistUserName')
                        ->title(ucfirst(trans('labels.dictated-by')))->width(50);

                $PatientTypeIcon = new \Kendo\UI\GridColumn();
                $PatientTypeIcon->field('patientTypeIcon')
                        ->attributes(['class' => 'font-awesome-td'])
                        ->filterable(false)
                        ->encoded(false)
                        ->title(ucfirst(trans('labels.patient-type-icon')))->width(50);
                $PatientType = new \Kendo\UI\GridColumn();
                $PatientType->field('patientType')
                        ->title(ucfirst(trans('labels.patient-type')))->width(50);
                $DictationDate = new \Kendo\UI\GridColumn();
                $DictationDate->field('dictationDate')
                        ->format('{0: dd/MM/yyyy}')
                        ->title(ucfirst(trans('labels.dictation-date')))->width(50);
                $DictationHour = new \Kendo\UI\GridColumn();
                $DictationHour->field('dictationDate')
                        ->filterable(false)
                        ->format('{0: h:mm tt}')
                        ->title(ucfirst(trans('labels.dictation-hour')))->width(50);
                $RequestNumber = new \Kendo\UI\GridColumn();
                $RequestNumber->field('serviceRequestID')
                        ->title(ucfirst(trans('labels.service-request-id')))->width(50);
                $OrderNumber = new \Kendo\UI\GridColumn();
                $OrderNumber->field('orderID')
                        ->title(ucfirst(trans('labels.order-id')))->width(50);
                $Procedure = new \Kendo\UI\GridColumn();
                $Procedure->field('procedureDescription')
                        ->title(ucfirst(trans('labels.procedure')))->width(50);
                $PatientName = new \Kendo\UI\GridColumn();
                $PatientName->field('patientName')
                        ->title(ucfirst(trans('titles.patient')))->width(50);
                $PatientID = new \Kendo\UI\GridColumn();
                $PatientID->field('patientID')
                        ->title(ucfirst(trans('labels.patient-id')))->width(50);
                $actions = new \Kendo\UI\GridColumn();
                $actions->title(trans('labels.actions'))
                ->width(50);

                $slash = "<span class='slash'>|</span>";

                $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'transcriber', 'edit');

                $editAction = $canEdit ? "\"<a href='" . url('transcriber/edit') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '';

                $actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $editAction . ";

                            }"));

                // Excel
                $kendo->setExcel('titles.orders-to-transcribe', '
                    function(e) {
                        if (!exportFlag) {
                            e.sender.hideColumn(0);
                            e.sender.hideColumn(10);
                            e.preventDefault();
                            exportFlag = true;
                            setTimeout(function () {
                              e.sender.saveAsExcel();
                            });
                          } else {
                            e.sender.showColumn(0);
                            e.sender.showColumn(10);
                            exportFlag = false;
                          }
                    }', '.xlsx', ' ' . date('d-M-Y'));

                // PDF
                $kendo->setPDF('titles.orders-to-transcribe', 'transcriber/1', '
                function (e) {
                    e.sender.hideColumn(0);
                    e.sender.hideColumn(10);
                    e.promise.done(function() {
                        e.sender.showColumn(0);
                        e.sender.showColumn(10);
                    });
                }', ' ' . date('d-M-Y'), 'page-template');

                // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
                    $PatientTypeIcon,
                    $PatientType,
                    $IsUrgent,

                    //$RejectUserName,
                    $RadiologistUserName,

                    $DictationDate,
                    $DictationHour,
                    $RequestNumber,
                    $OrderNumber,
                    $Procedure,
                    $PatientName,
                    $PatientID,
                    $actions,
                ));

                $kendo->generate(true, true, null, 550);

                ?>

                {!! $kendo->render() !!}

            </div>
        </div>
    </div>

    <script type="x/kendo-template" id="page-template">
        <div class="page-template">
            <div class="header">
                <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
                {{ trans('titles.orders-to-transcribe') }}
            </div>
            <div class="watermark">{{ Session::get('institution')->name }}</div>
            <div class="footer">
                {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
            </div>
        </div>
    </script>

    <style type="text/css">
        /* Page Template for the exported PDF */
        .page-template {
            font-family: "Open Sans", "Arial", sans-serif;
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
        }
        .page-template .header {
            position: absolute;
            top: 30px;
            left: 30px;
            right: 30px;
            border-bottom: 1px solid #888;
            color: #888;
        }
        .page-template .footer {
            position: absolute;
            bottom: 30px;
            left: 30px;
            right: 30px;
            border-top: 1px solid #888;
            text-align: center;
            color: #888;
        }
        .page-template .watermark {
            font-weight: bold;
            font-size: 400%;
            text-align: center;
            margin-top: 30%;
            color: #aaaaaa;
            opacity: 0.1;
            transform: rotate(-35deg) scale(1.7, 1.5);
        }
    </style>

@endsection