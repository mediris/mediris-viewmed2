@extends('layouts.app')
@section('title', ucfirst(trans('titles.migrations')))

@section('content')

 @include('partials.actionbar',[ 'title' =>  ucfirst(trans('titles.migrations')),
                                 'elem_type' => 'button',
                                 'elem_name' => '',
                                 'form_id' => '',
                                 'route' => '',
                                 'fancybox' => '',
                                 'routeBack' => ''
                             ])




 <style>


  .div-flex-down {

   display: flex;flex-wrap: wrap;
   vertical-align: bottom;

  }
  .span-button{

   vertical-align: text-bottom;
  }

  #myBar {
      width: 10%;
      height: 30px;
      background-color: #4CAF50;
      text-align: center; /* To center it horizontally (if you want) */
      line-height: 30px; /* To center it vertically */
      color: white;
  }


  .progress {
      display: block;
      text-align: center;
      width: 0;
      height: 3px;
      background: red;
      transition: width .3s;
  }
  .progress.hide {
      opacity: 0;
      transition: opacity 1.3s;
  }



 </style>


 <script>

     $(document).ready(function(){

         $id='{{$vars['id_centro']}}';
         if("{{$vars['id_centro']}}"=='')
         {
             enavform('migrationform',true);
         }
         else
         {
             enavform('migrationform',false);
         }
         if("{{$vars['centro_a_migrar']}}"!=''){
             endischildrens('divin',false);
         }

         endischildrens('div-status',"{{$vars['tablas']['status']}}");
         endischildrens('div-modal',"{{$vars['tablas']['modalidades']}}");
         endischildrens('div-process',"{{$vars['tablas']['procedimientos']}}");
         endischildrens('div-users',"{{$vars['tablas']['usuarios']}}");
         endischildrens('div-patient',"{{$vars['tablas']['pacientes']}}");
         endischildrens('div-request',"{{$vars['tablas']['solicitudes']}}");
         endischildrens('div-orders',"{{$vars['tablas']['ordenes']}}");
         endischildrens('div-docs',"{{$vars['tablas']['escaneados']}}");











         //uploader


     });


     var eventName = function() {
         $.getJSON("/path", function(data) {
             // update the view with your fresh data
             if (data.progress < 100)
                 eventName();
         });
     }

     var tiempo=0;
     var myApp;
     myApp = myApp || (function () {
         var pleaseWaitDiv = $('<div class="modal" id="pleaseWaitDiv" role="dialog" style="display:none"><div class="modal-dialog" data-backdrop="static" data-keyboard="false"><div class="modal-content"><h1>Attendi...</h1><div class="progress progress-striped active"><div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div></div></div></div>');
         return {
             showPleaseWait: function() {
                 pleaseWaitDiv.modal();
             },
             hidePleaseWait: function () {
                 pleaseWaitDiv.modal('hide');
             },
         };
     })()

     function enavform($form,$disabled) {
         $("#"+ $form +" *").prop('disabled',$disabled);
     }

     function endischildrens($parent,$mode) {
         $("#"+ $parent +"").children().prop('disabled',$mode);
     }

     function confirmar($route,$form,$method,$content,$title){
        $("<div class='panel-body ' id='record-content'></div>").kendoAlert({
             content: $content,
             //width: '800px',
             title:$title,
             primary: true,
             actions:  [{
                 text:"{{ucfirst(trans('statuses.yes'))}}",
                 action: function(e){
                     // e.sender is a reference to the dialog widget object
                     // OK action was clicked
                     // Returning false will prevent the closing of the dialog
                     var form =document.getElementById($form);
                     form.action=$route;
                     form.method=$method;
                     form.submit();
                     //myApp.showPleaseWait();
                 }
             },
                 {
                     text: "{{ucfirst(trans('statuses.no'))}}",
                     action: function(e){
                         // e.sender is a reference to the dialog widget object
                         // OK action was clicked
                         // Returning false will prevent the closing of the dialog

                         $("<div class='panel-body ' id='record-content'></div>").kendoAlert({
                             content: "{{ucfirst(trans('alerts.user-cancel-action'))}}",
                             //width: '800px',
                             title:"{{ucfirst(trans('titles.title-confirm'))}}",
                             primary: true}).data("kendoAlert").open();

                     }
                 }]
         }).data("kendoAlert").open();
     }

     function showLog($content,$title){
         var myWindow = $("#window"),
             undo = $("#undo");

         undo.click(function() {
             myWindow.data("kendoWindow").open();
             undo.fadeOut();
         });

         myWindow.kendoWindow({
             width: "70%",
             height:"70%",
             title: "Log " + $title,
             content: "{{route('migrations.showlog',['log'=>'var'])}}" . replace('var',$content),
             close: onClose

         }).data("kendoWindow").center().open();

         function onClose() {
             undo.fadeIn();
         }
     }

     function test_conn($form,$route) {
         var form =document.getElementById($form);
         form.action=$route;
         form.method='post';
         form.submit();



         return true;
     }

     function validateForm(){
         alert("Name must be filled out");
         return false;
         var x = document.form["connectionForm"]["host"].value;
         if (x == "") {
             alert("Name must be filled out");
             return false;
         };


         $("#button1").click(function(){
             document.getElementById('loadarea').src = 'progressbar.php';
         });
         $("#button2").click(function(){
             document.getElementById('loadarea').src = '';
         });

     }

 </script>


 <div class="row">

  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
   @if(Session::has('message'))
    <div class="{{ Session::get('class') }}">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <p>{{ Session::get('message') }}</p>
    </div>
   @endif

   @if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
  </div>
 </div>

    <div class="container-fluid  ">


    <div class=" col-xs-12"  >


       <div class="col-xs-3">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel sombra">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-file"
                                                   aria-hidden="true"></i> {{ trans('titles.orders-list') }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row ">
                            <form name="fileForm" id="fileForm"  action="{{ route('migrations.fileOrders')  }}" method="post">
                            <div class="col-xs-12 ">
                                    <label for='requestDocuments'>{{ ucfirst(trans('labels.file-orders')) }}</label>
                            </div>
                                <div class="col-xs-12">
                                    <input class="col-xs-12 " name="migrateOrders" id="migrateOrders"  type="file"  >
                                    <button type="submit" class=" btn btn-form btn-right " id="btn"
                                            data-style="expand-left" >
                                        <span  class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       </div>



        <div class="col-xs-3">
            <div class="panel x4 x4-less-5 elastic sombra">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ucfirst(trans('titles.data-conn'))}}   </h3>
                </div>
                <div class="panel-body">
                    <form name="conexionform" id="connectionForm">
                        <label  >
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('labels.host'))}}</span>
                        </label>
                        <input style="width: 95%" type="text" name="host" id="host" class="input-field form-control user btn-style"
                               value={{$vars['host']}} >
                        <label  >
                            <span class="ladda-label" style="vertical-align: middle">{{ ucfirst(trans('labels.port')) }}</span>
                        </label>
                        <input style="width: 95%" type="text" name="puerto" id="puerto" class="input-field form-control user btn-style"
                               value={{$vars['puerto']}} >
                        <label  >
                            <span class="ladda-label" style="vertical-align: middle">{{ ucfirst(trans('labels.database')) }}</span>
                        </label>
                        <input style="width: 95%" type="text" name="base" id="base" class="input-field form-control user btn-style"
                               value={{$vars['base']}}>
                        <label  >
                            <span class="ladda-label" style="vertical-align: middle">{{ ucfirst(trans('labels.user')) }}</span>
                        </label>
                        <input style="width: 95%" type="text" name="usuario" id="usuario" class="input-field form-control user btn-style"
                               value={{$vars['usuario']}}>
                        <label  >
                            <span class="ladda-label" style="vertical-align: middle">{{ ucfirst(trans('labels.password')) }}</span>
                        </label>
                        <input style="width: 95%" type="password" name="clave" id="clave" class="input-field form-control user btn-style"
                               value={{($vars['clave']=='') ? '' : decrypt($vars['clave'])}}>
                    </form>
                    <div>
                        <a class=" btn-left">
                            <button class="btn btn-form btn-left" id="btn"
                                    data-style="expand-left"
                                    formmethod="post"
                                    onclick="confirmar('{{ route('migrations.data'),$vars['id_centro']  }}' ,'connectionForm','post','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                                <span  class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                            </button>
                            <button class="btn btn-form btn-left" id="btn"

                                    onclick="confirmar('{{ route('migrations.test'),$vars['id_centro']  }}' ,'connectionForm','post','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                                <span class="ladda-label">{{ ucfirst(trans('labels.test-conn')) }}</span>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>


    <div class="col-xs-5" id="migrationform">

        <div class="panel x4 x4-less-5 elastic sombra">
            <div class="panel-heading">
                <h3 class="panel-title">{{ucfirst(trans('titles.migrations-history')) }} </h3>
            </div>

            <div class="panel-body">
                <div class="row " >
                    <div class="col-xs-4 "  >
                        <label class="col-md text-left" >
                            <span  class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('labels.institution')) }} </span>
                        </label>
                    </div>
                    <div class="col-xs-3 text-center" id="divin">
                        <form id="InstMigrateForm" >
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.inst'),$vars['id_centro']  }}' ,'InstMigrateForm','POST','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>


                    </div>
                    <div class="col-xs-4">
                        <label class="span-button">

                            @if($vars['logs'][ucfirst(trans('labels.institution'))]!='')
                                <a><span onclick="showLog('{{ucfirst(trans('labels.institution')) }}','{{ucfirst(trans('labels.institution')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['institucion']}} / Log: {{ucfirst(trans('labels.institution')) }}</span></a>
                            @else
                                <span onclick="showLog('{{ucfirst(trans('labels.institution')) }}','{{ucfirst(trans('labels.institution')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['institucion']}}</span>
                            @endif

                        </label>
                    </div>
                </div>


                <div class="row m-t-15">
                    <div class="col-xs-4 text-left" >
                        <label style="align-items: flex-start">
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('labels.status')) }}</span>
                        </label>
                    </div>
                    <div class="col-xs-3 text-center" id="div-status">
                        <form id="statusForm">
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.status',$vars['id_centro'])  }}' ,'InstMigrateForm','post','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}','{{ route('migrations.data'),$vars['id_centro']  }}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>
                    </div>
                    <div class="col-xs-4 text-left">
                        <label class="span-button" style="vertical-align: middle">
                            @if($vars['logs'][ucfirst(trans('labels.status'))]!='')
                                <a><span onclick="showLog('{{ucfirst(trans('labels.status')) }}','{{ucfirst(trans('labels.status')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['estatus']}} / Log: {{ucfirst(trans('labels.status')) }}</span></a>
                            @else
                                <span >{{ucfirst(trans('labels.total'))}}: {{$vars['estatus']}}</span>
                            @endif
                        </label>

                    </div>
                </div>

                <div class="row m-t-15">
                    <div class="col-xs-4 text-left" >
                        <label style="align-items: flex-start">
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('labels.modality')) }}</span>
                        </label>
                    </div>
                    <div class="col-xs-3 text-center" id="div-modal">
                        <form  id="OrderAddForm">
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.modal',$vars['id_centro'] ) }}' ,'InstMigrateForm','POST','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>
                    </div>
                    <div class="col-xs-4 text-left">
                        <label style="align-items: flex-start">

                            @if($vars['logs'][ucfirst(trans('labels.modality'))]!='')
                                <a><span onclick="showLog('{{ucfirst(trans('labels.modality')) }}','{{ucfirst(trans('labels.modality')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['modalidades']}} / Log: {{ucfirst(trans('labels.modality')) }}</span></a>
                            @else
                                <span >{{ucfirst(trans('labels.total'))}}: {{$vars['modalidades']}}</span>
                            @endif

                        </label>
                    </div>
                </div>

                <div class="row m-t-15">
                    <div class="col-xs-4 text-left" >
                        <label style="align-items: flex-start">
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('labels.procedure')) }}</span>
                        </label>
                    </div>
                    <div class="col-xs-3 text-center" id="div-process">
                        <form id="OrderAddForm">
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.process',$vars['id_centro'] ) }}' ,'InstMigrateForm','POST','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>

                    </div>
                    <div class="col-xs-4 text-left">
                        <label >
                            @if($vars['logs'][ucfirst(trans('labels.procedure'))]!='')
                                <a><span onclick="showLog('{{ucfirst(trans('labels.procedure')) }}','{{ucfirst(trans('labels.procedure')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['procedimientos']}} / Log: {{ucfirst(trans('labels.procedure')) }}</span></a>
                            @else
                                <span >{{ucfirst(trans('labels.total'))}}: {{$vars['procedimientos']}}</span>
                            @endif
                        </label>
                    </div>
                </div>

                <div class="row m-t-15">
                    <div class="col-xs-4 text-left" >
                        <label style="align-items: flex-start">
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('labels.user')) }}</span>
                        </label>
                    </div>
                    <div class="col-xs-3 text-center" id="div-users">
                        <form  id="OrderAddForm" >
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.users',$vars['id_centro'] )  }}' ,'InstMigrateForm','POST','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>
                    </div>
                    <div class="col-xs-4 text-left">
                        <label >
                            @if($vars['logs'][ucfirst(trans('labels.user'))]!='')
                                <a><span onclick="showLog('{{ucfirst(trans('labels.user')) }}','{{ucfirst(trans('labels.total')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['usuarios']}} / Log: {{ucfirst(trans('labels.user')) }}</span></a>
                            @else
                                <span >{{ucfirst(trans('labels.total'))}}: {{$vars['usuarios']}}</span>
                            @endif

                        </label>
                    </div>
                </div>

                <div class="row m-t-15">
                    <div class="col-xs-4 text-left" >
                        <label style="align-items: flex-start">
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('titles.patient')) }}</span>
                        </label>
                    </div>
                    <div class="col-xs-3 text-center" id="div-patient">
                        <form  id="OrderAddForm" >
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.patient',$vars['id_centro'])  }}' ,'InstMigrateForm','POST','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>

                    </div>
                    <div class="col-md-4 text-left">
                        <label >

                            @if($vars['logs'][ucfirst(trans('titles.patient'))]!='')
                                <a><span onclick="showLog('{{ucfirst(trans('titles.patient')) }}','{{ucfirst(trans('labels.total')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['pacientes']}} / Log: {{ucfirst(trans('titles.patient')) }}</span></a>
                            @else
                                <span >{{ucfirst(trans('labels.total'))}}: {{$vars['pacientes']}}</span>
                            @endif

                        </label>
                    </div>
                </div>

                <div class="row m-t-15">
                    <div class="col-xs-4 text-left" >
                        <label style="align-items: flex-start">
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('titles.requests')) }}</span>
                        </label>
                    </div>
                    <div class="col-xs-3 text-center" id="div-request">
                        <form  id="OrderAddForm">
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.request',$vars['id_centro'])  }}' ,'InstMigrateForm','POST','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>

                    </div>
                    <div class="col-xs-4 text-left">
                        <label >
                            @if($vars['logs'][ucfirst(trans('titles.requests'))]!='')
                                <a><span onclick="showLog('{{ucfirst(trans('titles.requests')) }}','{{ucfirst(trans('labels.total')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['solicitudes']}} / Log: {{ucfirst(trans('titles.requests')) }}</span></a>
                            @else
                                <span >{{ucfirst(trans('labels.total'))}}: {{$vars['solicitudes']}}</span>
                            @endif
                        </label>
                    </div>
                </div>

                <div class="row m-t-15">
                    <div class="col-xs-4 text-left" >
                        <label style="align-items: flex-start">
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('titles.order')) }}</span>
                        </label>
                    </div>
                    <div class="col-xs-3 text-center" id="div-orders">

                        <form id="OrderAddForm">
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.orders',$vars['id_centro'] ) }}' ,'InstMigrateForm','POST','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>

                    </div>
                    <div class="col-xs-3 text-left">
                        <label >
                            @if($vars['logs'][ucfirst(trans('titles.order'))]!='')
                                <a><span onclick="showLog('{{ucfirst(trans('titles.order')) }}','{{ucfirst(trans('labels.total')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['ordenes']}} / Log: {{ucfirst(trans('titles.order')) }}</span></a>
                            @else
                                <span >{{ucfirst(trans('labels.total'))}}: {{$vars['ordenes']}}</span>
                            @endif
                        </label>
                    </div>
                </div>

                <div class="row m-t-15">

                    <div class="col-xs-4 text-left" >
                        <label style="align-items: flex-start">
                            <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('titles.scan-files')) }}</span>
                        </label>
                    </div>
                    <div class="col-md-3 text-center" id="div-docs">
                        <form id="OrderAddForm">
                        </form>
                        <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left"
                                onclick="confirmar('{{ route('migrations.docs',$vars['id_centro'])  }}' ,'InstMigrateForm','POST','{{ucfirst(trans('alerts.confirm-action'))}}','{{ucfirst(trans('titles.title-confirm'))}}')">
                            <span class="ladda-label">{{ucfirst(trans('labels.migrate'))}}</span>
                        </button>

                    </div>
                    <div class="col-md-4 text-left" >
                        <label >

                            @if($vars['escaneados']>0)
                                <a><span onclick="showLog('{{ucfirst(trans('titles.scan-files')) }}','{{ucfirst(trans('titles.scan-files')) }}')">{{ucfirst(trans('labels.total'))}}: {{$vars['escaneados']}} / Log: {{ucfirst(trans('titles.scan-files')) }}</span></a>
                            @else
                                <span class="ladda-label">{{ucfirst(trans('labels.total'))}}: {{$vars['escaneados']}} </span>
                            @endif
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>


     </div>
</div>


     <div id="window"></div>












@endsection