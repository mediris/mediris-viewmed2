<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->

<html lang="en">
<head>
    <title>@yield('title','') | Meditron Ris</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    @include('partials.links')

</head>
<body id="app-layout" data-page="panels">

<?php
require_once '../vendor/telerik/wrappers/php/lib/DataSourceResult.php';
require_once '../vendor/telerik/wrappers/php/lib/Kendo/Autoload.php';
?>




<div id="wrapper" >

    <div id="main-content" class="p-20" >
        @yield('content')
    </div>

</div>

@include('partials.jscripts')

</body>
</html>