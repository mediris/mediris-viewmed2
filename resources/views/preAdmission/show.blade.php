@extends('layouts.app')

@section('title',ucfirst(trans('titles.pre-admission')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.pre-admission')),
	'elem_type' => 'button',
	'elem_name' => '',
	'form_id' => '',
	'route' => '',
	'fancybox' => '',
	'routeBack' => route('preadmission')
])
	
	<div class="container-fluid pre-admission">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="panel sombra">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="dashboard-icon icon-user" aria-hidden="true"></i> {{ trans('titles.patient-info') }}</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
								
								<input readonly type="hidden" name="patient[id]" id="patient_table_id"
										value="{{ isset($patient->id) ? $patient->id : '' }}">
								
								<div class="patient-img ">
                                    @if( isset($patient->id) && !empty($patient->photo))
                                        <img src="{{ asset( 'storage/'. $patient->photo ) }}"
                                        alt="Patient-Image" class="img-responsive" id='avatar'>
                                    @else
                                        <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                        class="img-responsive" id='avatar'>
                                    @endif
                                </div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div>
											<label for='patient_ID'>{{ ucfirst(trans('labels.patient-id')) }}
												*</label>
										</div>
										<div>
											<input readonly type="text" class="form-control" name="patient_ID"
													id="patient_ID" value=
													@if (isset($patient->id))
															"{{ $patient->patient_ID }}"
											@else
												"{{ old('patient_ID') or '' }}"
											@endif
											>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div>
											<label for="sex_id">{{ ucfirst(trans('labels.gender')) }} *</label>
										</div>
										<div>
											@foreach($sexes as $sex)
												@if($patient->sex_id == $sex->id)
													<input readonly type="text" class="form-control" value="{{ucfirst(trans('labels.'.$sex->name))}}">
												@endif
											@endforeach
										</div>
									</div>
								</div>
							
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='first_name'>{{ ucfirst(trans('labels.name')) }} *</label>
								</div>
								<div>
									<input readonly type="text" name="patient[first_name]" id="first_name"
											class="input-field form-control user btn-style" value=
									
											@if (isset($patient->id))
													"{{ $patient->first_name }}"
									@else
										"{{ old('first_name') or '' }}"
									@endif
									>
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='last_name'>{{ ucfirst(trans('labels.last-name')) }} *</label>
								</div>
								<div>
									<input readonly type="text" name="patient[last_name]" id="last_name"
											class="input-field form-control user btn-style" value=
									
											@if (isset($patient->id))
													"{{ $patient->last_name }}"
									@else
										"{{ old('last_name') or '' }}"
									@endif
									>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='birth_date'>{{ ucfirst(trans('labels.birth-date')) }} *</label>
								</div>
								<div>
									<input readonly type="text" name="patient[birth_date]" id="birth_date"
											class="form-control"
											value="{{ isset($patient->id) ? $patient->birth_date : old('birth_date') }}">
								</div>
							</div>
							


							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='email'>{{ ucfirst(trans('labels.email')) }} *</label>
								</div>
								<div>
									<input readonly type="email" class="form-control" name="email" id="email" value=
									
									@if (isset($patient->id))
											"{{ $patient->email }}"
									@else
										"{{ old('email') or '' }}"
									@endif
									readonly>
								
								</div>
							</div>



						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								
								<div>
									<label for='country_of_residence'>{{ ucfirst(trans('labels.country-residence')) }}
										*</label>
								</div>
								<div>
									@foreach($countries as $country)
										@if($patient->country_id == $country->id)
											<input readonly type="text" class="form-control" value="{{$country->name}}">
										@endif
									@endforeach
								</div>
							
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								
								<div>
									<label for='citizenship'>{{ ucfirst(trans('labels.citizenship')) }}
										*</label>
								</div>
								<div>
									@foreach($countries as $country)
										@if($patient->citizenship == $country->id)
											<input readonly type="text" class="form-control" value="{{$country->citizenship}}">
										@endif
									@endforeach
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='telephone_number'>{{ ucfirst(trans('labels.telephone-number')) }}
										*</label>
								</div>
								<div>
									<input readonly type="tel" class="form-control" name="telephone_number"
											id="telephone_number" value=
									
											@if (isset($patient->id))
													"{{ $patient->telephone_number }}"
									@else
										"{{ old('telephone_number') or '' }}"
									@endif
									>
								
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='cellphone_number'>{{ ucfirst(trans('labels.cellphone-number')) }}
										*</label>
								</div>
								<div>
									<input readonly type="tel" class="form-control" name="cellphone_number"
											id="cellphone_number" value=
									
											@if (isset($patient->id))
													"{{ $patient->cellphone_number }}"
									@else
										"{{ old('cellphone_number') or '' }}"
									@endif
									>
								
								</div>
							</div>
						</div>
						
						<div class="row">	
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div>
									<label for='occupation'>{{ ucfirst(trans('labels.occupation')) }}</label>
								</div>
								<div>
									<input readonly type="text" class="form-control" name="patient[occupation]"
											id="occupation"
											value="{{ isset($patient->id) ? $patient->occupation : old('patient[occupation]') }}">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='responsable_id'>{{ ucfirst(trans('labels.responsable-id')) }}</label>
								</div>
								<div>
									<input readonly type="text" class="form-control" name="responsable[responsable_id]"
											id="responsable_id"
											value="{{ isset($patient->responsable->id) ? $patient->responsable->responsable_id : old('responsable[responsable_id]') }}">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='responsable_name'>{{ ucfirst(trans('labels.responsable-name')) }}</label>
								</div>
								<div>
									<input readonly type="text" class="form-control" name="responsable[name]"
											id="responsable_name"
											value="{{ isset($patient->responsable->id) ? $patient->responsable->name : old('responsable[name]') }}">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div>
									<label for='address'>{{ ucfirst(trans('labels.address')) }} *</label>
								</div>
								<div>
									<textarea readonly class="form-control" name="patient[address]"
												id="address">{{ isset($patient->id) ? $patient->address : old('patient[address]') }}</textarea>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="panel sombra">
					<div class="panel-heading">
						<h3 class="panel-title"><i
									class="dashboard-icon icon-edit"></i> {{ trans('titles.request-info') }}
						</h3>
					</div>
					
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='weight'>{{ ucfirst(trans('labels.weight')) }} kg *</label>
								</div>
								<div>
									<input readonly type="number" class="form-control" step='5' name="serviceRequest[weight]"
											id="weight"
											min="0"
											value="{{ $serviceRequest->weight or old('weight') }}"
											placeholder="50">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='height'>{{ ucfirst(trans('labels.height')) }} m *</label>
								</div>
								<div>
									<input readonly type="number" class="form-control" step='0.1' name="serviceRequest[height]"
											id="height"
											min="0"
											value="{{ $serviceRequest->height or old('height') }}"
											placeholder="1.5">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for="patient_type_id">{{ ucfirst(trans('labels.patient-type')) }}
										*</label>
								</div>
								<div>	
									@if(isset($serviceRequest))
										@foreach($patientTypes as $patientType)
											@if($patientType->id == $serviceRequest->patient_type_id)
												<input readonly type="text" class="form-control" value="{{ucfirst($patientType->description)}}">
											@endif
										@endforeach
									@endif
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for="source_id">{{ ucfirst(trans('labels.source')) }} *</label>
								</div>
								<div>
									@if(isset($serviceRequest))
										@foreach($sources as $source)
											@if($serviceRequest->source_id == $source->id)
												<input readonly type="text" class="form-control" value="{{ucfirst($source->description)}}">
											@endif
										@endforeach
									@endif
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div>
									<label for="referring_id">{{ ucfirst(trans('labels.referring')) }}*</label>
								</div>
								<div>	
									@if( !$serviceRequest->referring == null )
										<input readonly type="text" class="form-control" value="{{$serviceRequest->referring->last_name." ".$serviceRequest->referring->first_name}}">
									@else
										<input readonly type="text" class="form-control" value="">
									@endif
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div>
									<label for="answer_id">{{ trans('labels.how-did') }} *</label>
								</div>
								<div>
									@if(isset($serviceRequest))
										@foreach($answers as $answer)
											@if($serviceRequest->answer_id == $answer['id'])
												<input readonly type="text" class="form-control" value="{{$answer['description']}}">
											@endif
										@endforeach
									@endif
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div>
									<label for='issue_date'>{{ ucfirst(trans('labels.issue-date')) }}</label>
								</div>
								<div>
									<input readonly type="text" name="serviceRequest[issue_date]" id="issue_date"
											class="form-control"
											value="{{ date('Y-m-d') }}">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div>
									<label for='comments'>{{ ucfirst(trans('labels.comments')) }}</label>
								</div>
								<div>
									<textarea readonly class="form-control" name="serviceRequest[comments]"
												id="service_request_comments">{{ old('service_request_comments') }}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="panel sombra">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-file-text"
													aria-hidden="true"></i> {{ ucfirst(trans('titles.procedures')) }}
						</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								@include('includes.general-checkbox', [
                                                    'id'=>'urgent',
                                                    'name'=>'requestedProcedure[urgent]',
                                                    'label'=>"labels.urgent",
                                                    'readOnly' => true,
                                                    'condition' => $requestedProcedure->urgent

                                                ])
							
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div>
									<label for="modality_id_reception">{{ ucfirst(trans('labels.modality')) }}</label>
								</div>
								<div>	
									@foreach($modalities as $modality)
										@if($modality->name == 'CR')
											<input readonly type="text" class="form-control" value="{{$modality->name}}">
										@endif
									@endforeach
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div>
									<label for="procedure_id">{{ ucfirst(trans('labels.procedures')) }}</label>
								</div>
								<div>
									@foreach($procedures as $procedure)
										@if($procedure->id == $requestedProcedure->procedure_id)
											<input readonly type="text" class="form-control" value="{{$procedure->description}}">
										@endif
									@endforeach
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div>
									<label for="comments">{{ ucfirst(trans('labels.comments')) }}</label>
								</div>
								<div>
									<textarea readonly class="form-control" name="requestedProcedure[comments]" id="comments">{{ $requestedProcedure->comments or old('comments') }}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
					
					