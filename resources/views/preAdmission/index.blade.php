@extends('layouts.app')

@section('title', ucfirst(trans('titles.pre-admission')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.pre-admission')),
									'elem_type' => '',
									'elem_name' => '',
									'form_id' => '',
									'route' => '',
									'fancybox' => '',
									'routeBack' => '',
									'clearFilters' => true
								])

		<!-- Modal -->
	<div class="modal fade" id="suspendModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content" id="cboxContent">
				<div class="modal-header">
					<button type="button" id="cboxClose" data-dismiss="modal"><span>x</span></button>
					<h4 class="modal-title">{{ ucfirst(trans('labels.suspension-reason')) }}</h4>
				</div>
				<form id="suspendForm" method="post" action="{{ route('technician.suspend') }}">
					<div class="modal-body">
						<div>
							@include('includes.select', [
	                                                    'idname' => 'suspensionReason',
	                                                    'data' => $suspensionReasons,
	                                                    'keys' => ['id', 'description'] 
	                                                ])
						</div>
						<input type="hidden" id="orderId" name="orderId" value="-1">
						<input type="hidden" id="typeID" name="typeID" value="preadmission">
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</div>
					<div class="modal-footer">
						<button type="submit" id="btn-add-edit-save" class="btn btn-form">{{ ucfirst(trans('labels.save')) }}</button>
						@include('includes.select', [
	                                                    'idname'	=> 'user-change-sel',
	                                                    'value' 	=> \Auth::user()->id,
	                                                    'data' 		=> \App\User::all(),
	                                                    'keys' 		=> ['id', 'username'],
	                                                    'class'		=> 'dropup in-footer'
	                                                ])
					</div>
				</form>

			</div>
		</div>
	</div>
	<!-- Modal login -->
	@include('includes.modal-login')

	<div class="container-fluid pre-admission index">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="title">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h1>{{ ucfirst(trans('titles.preadmission-orders-list')) }}</h1>
						</div>
					</div>
				</div>
				
				<?php

				// Grid
                $options = (object) array(
                	'url' => 'preadmission'
                );
                $kendo    = new \App\CustomKendoGrid($options);

                // Fields
                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
				$PatientTypeIconField->type('string');
				$PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
				$PatientTypeField->type('string');
				$OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
				$OrderNumberField->type('string');
				$RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
				$RequestNumberField->type('string');
				$PatientNameField = new \Kendo\Data\DataSourceSchemaModelField('patientName');
				$PatientNameField->type('string');
				$PatientIdentificationIDField = new \Kendo\Data\DataSourceSchemaModelField('patientIdentificationID');
				$PatientIdentificationIDField->type('string');
				$ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
				$ProcedureField->type('string');
				$PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
				$PatientIDField->type('string');
				$OrderStatusField = new \Kendo\Data\DataSourceSchemaModelField('orderStatus');
				$OrderStatusField->type('string');
				$OrderStatusIDField = new \Kendo\Data\DataSourceSchemaModelField('orderStatusID');
				$OrderStatusIDField->type('string');
				//				$OrderStatus = new \Kendo\Data\DataSourceSchemaModelField('orderStatus');
				//				$OrderStatus->type('string');

				$kendo->addFields(array(
					$PatientTypeIconField,
					$PatientTypeField,
					$OrderNumberField,
					$RequestNumberField,
					$PatientNameField,
					$PatientIdentificationIDField,
					$ProcedureField,
					$PatientIDField,
					$OrderStatusField,
					$OrderStatusIDField,
				));

 				// Create Schema
                $kendo->createSchema(true, true);

                // Create Data Source
                $kendo->createDataSource();
                
                // Create Grid
                $kendo->createGrid('preadmission');

                // Columns
                $PatientTypeIcon = new \Kendo\UI\GridColumn();
				$PatientTypeIcon->field('patientTypeIcon')
						->attributes(['class' => 'font-awesome-td'])
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.patient-type-icon')));
				$PatientType = new \Kendo\UI\GridColumn();
				$PatientType->field('patientType')
						->title(ucfirst(trans('labels.technician-patient-type')));
				$OrderNumber = new \Kendo\UI\GridColumn();
				$OrderNumber->field('orderID')
						->title(ucfirst(trans('labels.order-id')));
				$RequestNumber = new \Kendo\UI\GridColumn();
				$RequestNumber->field('serviceRequestID')
						->title(ucfirst(trans('labels.service-request-id')));
				$PatientIdentificationID = new \Kendo\UI\GridColumn();
				$PatientIdentificationID->field('patientIdentificationID')
						->title(ucfirst(trans('labels.patient-id')));
				$PatientName = new \Kendo\UI\GridColumn();
				$PatientName->field('patientName')
						->title(ucfirst(trans('titles.patient')));
				$PatientID = new \Kendo\UI\GridColumn();
				$PatientID->field('patientIdentificationID')
						->title(ucfirst(trans('titles.patient-id')));
				$Procedure = new \Kendo\UI\GridColumn();
				$Procedure->field('procedureDescription')
						->title(ucfirst(trans('labels.procedure')));
				$OrderStatus = new \Kendo\UI\GridColumn();
				$OrderStatus->field('orderStatus')
						->title(ucfirst(trans('labels.status')));
				$OrderStatus = new \Kendo\UI\GridColumn();
				$OrderStatus->field('orderStatusID')
						->title(ucfirst(trans('labels.status')));
				
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->title(trans('labels.actions'));
				
				$slash = "<span class='slash'>|</span>";
				
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'preadmission', 'edit');
				
				//::::::::::::::::::::: ACTION BUTTONS ::::::::::::::::::::::::://
				
				$approveOrder = ($canEdit ? "\"<div class='order-action approve-order'><a href='" . url('preadmission/approve') .
						"/\" + row.orderID + \"" . "' id='preadmission-approve-order' " .
						"data-toggle='tooltip' data-placement='bottom' title='" . ucfirst(trans('labels.approve-order')) .
						"'><i class='fa fa-check-square-o' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');
					
//				$orderDetail = ($canEdit ? "\"" . $slash . "\" + \"<div class='order-action view-order-detail'><a href='" . url('preadmission/edit') .
//						"/\" + row.orderID + \"" .
//						"' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') .
//						"'><i class='fa fa-eye' aria-hidden='true'></i></a></div>\"" : '');

				$suspendOrder = ($canEdit ? "\"" . $slash . "\" + \"<div class='order-action suspend-order'><a  id='suspend-btn-\" + row.orderID + \"' onclick='modalSuspendOrderReason(\" + row.orderID + \");'  data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.suspend-order') .
						"'><i class='fa fa-hand-paper-o' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');

				$orderDetail = ($canEdit ? "\"" . $slash . "\" + \"<div class='order-action view-order-detail'><a href='". url('preadmission/show/'). "/\" + row.orderID + \"" ."'" .
						" data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') .
						"'><i class='fa fa-eye' aria-hidden='true'></i></a></div>\"" : '');
				
				//				$ableToBeBlockedButtons	= "\"<div class='actions'>\" + " . $print . $rewriteOrder . $suspendOrder . $orderDetail . " + \"</div>\"";
				$actions = "\"<div class='actions'>\" + " . $approveOrder . $suspendOrder . $orderDetail . "+ \"</div>\"";
				
				$actionButtons = "\"<div class='action-buttons'>\" + " . $actions . " + \"</div>\"";
				
				$Actions->template(new Kendo\JavaScriptFunction("
						function (row) {
							return  " . $actionButtons . ";
						}"));

                // Excel
                $kendo->setExcel('titles.preadmission-orders-list', '
                    function(e) {
		                if (!exportFlag) {
				            e.sender.hideColumn(0);
				            e.preventDefault();
				            exportFlag = true;
				            setTimeout(function () {
				              e.sender.saveAsExcel();
				            });
				          } else {
				            e.sender.showColumn(0);
				            exportFlag = false;
				          }
				    }');

                // PDF
                $kendo->setPDF('titles.preadmission-orders-list', 'preadmission/1', '
                    function (e) {
						e.sender.hideColumn(0);
						e.sender.hideColumn(8);
						e.promise.done(function() {
							e.sender.showColumn(0);
							e.sender.showColumn(8);
						});
					}');

                // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$PatientTypeIcon,
					$PatientType,
					$OrderNumber,
					$RequestNumber,
					$PatientName,
					$PatientIdentificationID,
					$Procedure,
					$Actions,
                ));

                $kendo->generate(true, true);

				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function(){
			// Refresh Kendo Grid each 30s
			// Custom Way kendoGridAutoRefresh('#grid', 30);
			kendoGridAutoRefresh();
		});
	</script>
	
	<script type="x/kendo-template" id="page-template">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
				{{ trans('titles.preadmission-orders-list') }}
			</div>
			<div class="watermark">IDACA</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<style type="text/css">
		/* Page Template for the exported PDF */
		.page-template {
			font-family: "Open Sans", "Arial", sans-serif;
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
		}
		.page-template .header {
			position: absolute;
			top: 30px;
			left: 30px;
			right: 30px;
			border-bottom: 1px solid #888;
			color: #888;
		}
		.page-template .footer {
			position: absolute;
			bottom: 30px;
			left: 30px;
			right: 30px;
			border-top: 1px solid #888;
			text-align: center;
			color: #888;
		}
		.page-template .watermark {
			font-weight: bold;
			font-size: 400%;
			text-align: center;
			margin-top: 30%;
			color: #aaaaaa;
			opacity: 0.1;
			transform: rotate(-35deg) scale(1.7, 1.5);
		}
	</style>

<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\OrderSuspendRequest', '#suspendForm'); !!}

<script>
	$(document).ready(function () {
		//SUSPENDER ORDEN
		callSuspendOrder();
	});
</script>

@endsection
