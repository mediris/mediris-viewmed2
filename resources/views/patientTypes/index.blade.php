@extends('layouts.app')

@section('title', ucfirst(trans('titles.patient-types')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.patient-types')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('patientTypes.add'),
									'fancybox' => 'cboxElement',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.patient-types')) }}</h1>
				</div>
				
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('string');
				$IconField = new \Kendo\Data\DataSourceSchemaModelField('icon');
				$IconField->type('string');
				$DescriptionField = new \Kendo\Data\DataSourceSchemaModelField('description');
				$DescriptionField->type('string');
				$AdministrativeIdField = new \Kendo\Data\DataSourceSchemaModelField('administrative_ID');
				$AdministrativeIdField->type('string');
				$PriorityField = new \Kendo\Data\DataSourceSchemaModelField('priority');
				$PriorityField->type('string');

				$kendo->addFields(array(
					$IdField,
					$IconField,
					$DescriptionField,
					$AdministrativeIdField,
					$PriorityField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $patientTypes = $patientTypes->toArray();
				
				$canShow = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'patientTypes', 'show');
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'patientTypes', 'edit');
				$canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'patientTypes', 'active');
				
				foreach ($patientTypes as $key => $value) {
					$title = $patientTypes[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $patientTypes[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					$showAction = $canShow ? '<a href=' . route('patientTypes.show', [$patientTypes[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.show') . '><i class="fa fa-eye" aria-hidden="true"></i></a>' : '';
					$editAction = $canEdit ? '<a href=' . route('patientTypes.edit', [$patientTypes[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . '><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';
					$activeAction = $canEnable ? '<span class="slash">|</span> <a href= ' . route('patientTypes.active', [$patientTypes[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>' : '';
					
					$patientTypes[$key]['actions'] = $editAction . $activeAction;
				}

				// Set  Patient Types
				$kendo->setDataSource($patientTypes);

				// Create Grid
                $kendo->createGrid('patientTypes');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$Description = new \Kendo\UI\GridColumn();
				$Description->field('description')
						->title(ucfirst(trans('labels.description')));
				$Icon = new \Kendo\UI\GridColumn();
				$Icon->field('icon')
						->attributes(['class' => 'font-awesome-td'])
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.icon')));
				$AdministrativeId = new \Kendo\UI\GridColumn();
				$AdministrativeId->field('administrative_ID')
						->title(ucfirst(trans('labels.administrative-id')));
				$Priority = new \Kendo\UI\GridColumn();
				$Priority->field('priority')
						->title(ucfirst(trans('labels.priority')));
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$Icon,
					$Description,
					$AdministrativeId,
					$Priority,
					$Actions,
                ));
				
                $kendo->generate();

				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>


@endsection