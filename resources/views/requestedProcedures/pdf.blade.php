<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ trans('labels.order-info') }}</title>
    <meta http-equiv="Content-Type" content="titleext/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('/css/pdfs.css') }}">
</head>
<body id="app-layout" class="{{ $draft }}">
    <script type="text/php">
        $size = 8;
        $y = 10;
        $x = $pdf->get_width() - 100;
        $font = $fontMetrics->get_font("sans-serif");
        $pdf->page_text($x, $y, trans('labels.page') . " {PAGE_NUM} " . trans('labels.of') . " {PAGE_COUNT}", $font, $size);
    </script>
    
    <div class="header">    
        @if($header)
            <img src="{{ $header }}" class="header-img">
        @endif    
        <table>
            <tr>
                <td class="label">{{ strtoupper( trans( 'labels.date' ) ) }}:</td>
                <td>{{ $date }}</td>
                <td class="label">{{ strtoupper( trans( 'labels.type' ) ) }}:</td>
                <td>{{ $type }}</td>
                <td class="label">{{ strtoupper( trans( 'labels.report' ) ) }}:</td>
                <td>{{ $id }}</td>
            </tr>
            <tr>
                <td class="label">{{ strtoupper( trans( 'titles.patient' ) ) }}:</td>
                <td colspan="3">{{ $name }}</td>
                <td class="label">C.I.:</td>
                <td>{{ $ci }}</td>
            </tr>
            <tr>
                <td class="label">{{ strtoupper( trans( 'labels.birth-date-small' ) ) }}:</td>
                <td colspan="3">{{ $birthdate }}</td>
                <td class="label">{{ strtoupper( trans( 'labels.sex' ) ) }}:</td>
                <td>{{ $sex }}</td>
            </tr>
            <tr>
                @if(is_null($referring))
                    <td class="label">{{ strtoupper( trans( 'labels.referring' ) ) }}:</td>
                    <td colspan="5">{{ strtoupper( trans( 'labels.not-specified' ) ) }}</td>
                @else
                    <td class="label">{{ strtoupper( trans( 'labels.referring' ) ) }}:</td>
                    <td colspan="5">{{ $referring }}</td>
                @endif
            </tr>
            <tr>
                <td class="label">{{ strtoupper( trans( 'labels.study' ) ) }}:</td>
                <td colspan="5">{{ $study }}</td>
            </tr>
        </table>
    </div>

    <div class="footer">
        <div class="signature">
            <img src="{{ $signature }}">
            <div class="radiologist">{{ $signatureRadiologist }}</div>
            <div class="technician">{{ $signatureRadiologistCharge }}</div>
            <div class="date">{{ $signatureDate }}</div>
        </div>
        @if($footer)
            <img src="{{ $footer }}" class="footer-img">
        @endif
    </div>

    <div class="content">
        {!! $text !!}
    </div>
</body>
</html>

<style>
    @if($signature)
        .footer {
            bottom: 3.5cm;
        }
        body {
            margin-bottom: 3cm;
        }
    @else
        .footer {
            bottom: 2cm;
        }
        body {
            margin-bottom: 1.5cm;
        }
    @endif
</style>