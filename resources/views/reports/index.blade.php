@extends('layouts.app')

@section('title', ucfirst(trans('titles.reports')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.reports')),
									'elem_type' => '',
									'elem_name' => '',
									'form_id' => '',
									'route' => '',
									'fancybox' => '',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">

		<div class="row" id="err">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							<li>{{ Session::get('message') }}</li>
						</ul>
					</div>
				@endif

				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="title">
					<h1>{{ ucfirst(trans('messages.reports')) }}</h1>
				</div>
				
				<?php
				
				$model = new \Kendo\Data\DataSourceSchemaModel();
				
				$IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('integer');
				
				$nameField = new \Kendo\Data\DataSourceSchemaModelField('name');
				$nameField->type('string');

				$subreportField = new \Kendo\Data\DataSourceSchemaModelField('subreport');
				$subreportField->type('string');

				$reportField = new \Kendo\Data\DataSourceSchemaModelField('report');
				$reportField->type('string');
				
				$model->addField($IdField)
						->addField($nameField)
						->addField($subreportField)
						->addField($reportField);
				
				$schema = new \Kendo\Data\DataSourceSchema();
				$schema->model($model);
				
				$dataSource = new \Kendo\Data\DataSource();

				$canGenerate = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'reports', 'show');
				$data = null;

				// ciclo foreach que permite el llenado de la lista de reportes en la Vista
				foreach ($reports as $key => $value) {

					$data[$key]['name'] = trans('labels.'.$reports[$key]['name']);
					$data[$key]['subreport'] = $reports[$key]['subreport'] ? trans('labels.yes') : trans('labels.no');

					// si isset($reports[$key]['parent']
					if(isset($reports[$key]['parent'])){
						$subreports = $reports[$key]['parent']->toArray();
						$nameSubreports = str_replace('-',' ',$subreports['name']);
					}else
						$nameSubreports = 'N/A';

					$data[$key]['report'] = $nameSubreports;


					// se genera la accion del los botones utilizados para la generacion del Reporte
					$data[$key]['actions'] = '';
					// se obtiene el id del reporte de la BD [$reports[$key]['id']]
					$generateAction = $canGenerate ? '<a href=' . route('reports.show', [$reports[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.generate') . ' class="edit-row cboxElement"><i class="fa fa-eye" aria-hidden="true"></i></a>' : '';

					//dd($generateAction);

					$data[$key]['actions'] = $generateAction;

				}
				
				$dataSource->data($data)
						->pageSize(20)
						->schema($schema);
				
				$grid = new \Kendo\UI\Grid('grid');
				$grid->attr('view-attr', 'reports');
				
				$Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');

				$name = new \Kendo\UI\GridColumn();
				$name->field('name')
						->title(ucfirst(trans('labels.name')));

				$subreport = new \Kendo\UI\GridColumn();
				$subreport->field('subreport')
						->title(ucfirst(trans('labels.subreport')));
				
				$report = new \Kendo\UI\GridColumn();
				$report->field('report')
						->title(ucfirst(trans('labels.report')));

				$pageable = new \Kendo\UI\GridPageable();
				$pageable->input(true)
						->numeric(false);

				$actions = new \Kendo\UI\GridColumn();
				$actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));
				
				$grid->addColumn($name)
						->addColumn($subreport)
						->addColumn($report)
						->addColumn($actions)
						->height(750)
						->sortable(true)
						->filterable(true)
						->pageable($pageable)
						->dataSource($dataSource)
						->dataBound('onDataBound');
				?>
				
				{!! $grid->render() !!}
			
			
			</div>
		</div>
	</div>

@endsection