<div class="panel sombra">
    <div class="panel-heading">
        <h3 class="panel-title">{{ trans('labels.patient-history') }}</h3>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if ( $total>0 || count($oldServiceRequests) > 0 )
                    @foreach($serviceRequests as $key => $institution)
                        @foreach($institution['serviceRequests'] as $serviceRequest)
                        <div id="panel_history_{{$key}}_{{ $serviceRequest->id }}" class="panel panel-history">
                            <div class="panel-heading bg-blue">
                                <h4 class="panel-title">{{ trans('labels.request-num') }}: {{ $serviceRequest->id }}</h4>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">

                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p class="text-right"><strong>{{ ucfirst(trans('labels.institution')) }}:</strong></p>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p>{{ $institution['institution_name'] }}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p class="text-right"><strong>{{ ucfirst(trans('labels.patient-type')) }}:</strong></p>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p>{{ $serviceRequest->patient_type->description }}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p class="text-right"><strong>{{ trans('labels.admission-date') }}:</strong></p>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p>{{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($serviceRequest->issue_date)) : date('Y-m-d', strtotime($serviceRequest->issue_date)) }}</p>
                                            </div>
                                        </div>
                                        @if(isset($serviceRequest->referring))
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p class="text-right"><strong>{{ ucfirst(trans('labels.referring')) }}:</strong></p>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p>{{ $serviceRequest->referring->last_name }}</p>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p class="text-right"><strong>{{ ucfirst(trans('labels.source')) }}:</strong></p>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p>{{ $serviceRequest->source->description }}</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                                        @foreach($serviceRequest->requested_procedures as $requestedProcedure)
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title">{{ $requestedProcedure->procedure->description }}</h5>
                                                </div>

                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <p class="text-right"><strong>{{ trans('labels.order-num') }}:</strong></p>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <p>{{ $requestedProcedure->id }}</p>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <p class="text-right"><strong>{{ trans('labels.status') }}:</strong></p>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                                            <div class="progress">
                                                                @if($requestedProcedure->requested_procedure_status_id == 7)
                                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                                                                        {{ trans('labels.'.$requestedProcedure->requested_procedure_status->description) }}
                                                                    </div>
                                                                @else
                                                                    <div class="progress-bar {{ ($requestedProcedure->requested_procedure_status_id / 6) == 1 ? 'progress-bar-success' : 'progress-bar-info' }}" role="progressbar" aria-valuenow="{{ $requestedProcedure->requested_procedure_status_id / 6 * 100 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $requestedProcedure->requested_procedure_status_id / 6 * 100 }}%">
                                                                        {{ trans('labels.'.$requestedProcedure->requested_procedure_status->description) }}
                                                                    </div>
                                                                @endif

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row btn-icons">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">    
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            @if( $requestedProcedure->requested_procedure_status_id >= 3 )
                                                                <a href="{{ route('radiologist.pac', ['id'=>$requestedProcedure->id]) }}" class="btn btn-form btn-icon btn-pac" data-toggle="tooltip" title="{{ trans('labels.view-images') }}">
                                                                    <i class="fa fa-picture-o"></i>
                                                                </a>
                                                            @endif
                                                            @if($requestedProcedure->text != '' && $requestedProcedure->requested_procedure_status_id >= 6)
                                                                
                                                                <a id="download-file" class="btn btn-form btn-icon"
                                                                    data-toggle="tooltip"
                                                                   target="_blank"
                                                                title="{{ trans('labels.view-file-report') }}"
                                                                    href="{{ route('finalreport.pdf', [
                                                                                        'id'=> $requestedProcedure->id,
                                                                                        'version' => 'original' 
                                                                                        ])
                                                                    }}">
                                                                    <i class="fa fa-file-pdf-o"></i>
                                                                </a>
                                                            @endif
                                                           
                                                            @if ( isset($requestedProcedure->addendums) && !empty($requestedProcedure->addendums) && $requestedProcedure->id == $requestedProcedure->addendums[0]->requested_procedure_id)
                                                               
                                                                <a class="btn btn-form btn-icon" id="download-file"
                                                                    data-toggle="tooltip"
                                                                    title="{{ trans('labels.view-file-addendum') }} "
                                                                    target="_blank"
                                                                    href="{{ route('finalreport.pdf', [
                                                                                        'id'=> $requestedProcedure->id,
                                                                                        'version' => 'addendum' 
                                                                                        ])
                                                                    }}">
                                                                    <i class="fa fa-file-pdf-o"></i>
                                                                </a>
                                                                
                                                            @endif
                                                        </div>




                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endforeach
                    @endforeach















                    @foreach($oldServiceRequests as $oldServiceRequest)
                        <div id="panel_history_{{ $oldServiceRequest->id }}" class="panel panel-history">
                            <div class="panel-heading bg-blue">
                                <h4 class="panel-title">{{ trans('labels.request-num') }}: {{ $oldServiceRequest->id }}</h4>
                            </div>
                            <div class="panel-body">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">

                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p class="text-right"><strong>{{ ucfirst(trans('labels.institution')) }}:</strong></p>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p>{{ $oldServiceRequest->institucion->nombre }}</p>
                                            </div>
                                        </div>                        

                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p class="text-right"><strong>{{ trans('labels.admission-date') }}:</strong></p>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <p>{{ App::getLocale() == 'es' ?
                                                        date('d-m-Y', strtotime($oldServiceRequest->fecha_solicitud)) :
                                                        date('Y-m-d', strtotime($oldServiceRequest->fecha_solicitud)) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                                        @foreach($oldServiceRequest->ordenes as $orden)
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title">{{ $orden->procedimiento->descripcion }}</h5>
                                                </div>

                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <p class="text-right"><strong>{{ trans('labels.order-num') }}:</strong></p>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <p>{{ $orden->id }}</p>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <p class="text-right"><strong>{{ trans('labels.status') }}:</strong></p>
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                                            <div class="progress">
                                                                @if($orden->estatus->id == 7)
                                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                                                                        {{ $orden->estatus->descripcion }}
                                                                    </div>
                                                                @else
                                                                    <div class="progress-bar {{ ($orden->estatus->id / 6) == 1 ? 'progress-bar-success' : 'progress-bar-info' }}" role="progressbar" aria-valuenow="{{ $orden->estatus->id / 6 * 100 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $orden->estatus->id / 6 * 100 }}%">
                                                                        {{ $orden->estatus->descripcion }}
                                                                    </div>
                                                                @endif

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row btn-icons">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            
                                                        </div>

                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            @if( $orden->estatus->id != 7 )
                                                                <a href="{{ route('radiologist.pac', ['id'=>$orden->id]) }}" class="btn btn-form btn-icon btn-pac" data-toggle="tooltip" title="{{ trans('labels.view-images') }}">
                                                                    <i class="fa fa-picture-o"></i>
                                                                </a>
                                                            @endif
                                                            @if($orden->texto != '' && $orden->estatus->id == 6)
                                                                <a id="download-file" class="btn btn-form btn-icon"
                                                                    data-toggle="tooltip"
                                                                    target="_blank"
                                                                    title="{{ trans('labels.view-file-report') }}"
                                                                    href="{{ route('finalreport.legacy', [
                                                                                        'id'=> $orden->id 
                                                                                        ])
                                                                    }}">
                                                                    <i class="fa fa-file-pdf-o"></i>
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach


                @else
                    <div class="row">
                        <div class="col-md-12">
                            <p> {{trans('messages.no-history')}}</p>
                        </div>

                    </div>
                @endif
                
                <?php $index = 1; ?>
                <ul class="pagination pagination-sm">
                    @foreach($serviceRequests as $key => $institution)
                        @foreach($institution['serviceRequests'] as $serviceRequest)
                        <li><a href="javascript:;" class="pagination-link {{ $index == 1 ? 'active disabled' : '' }}" panel-id="{{ $key }}_{{ $serviceRequest->id }}">{{ $index }}</a></li>
                        <?php $index++; ?>
                        @endforeach
                    @endforeach

                    @foreach($oldServiceRequests as $oldServiceRequest )
                        <li><a href="javascript:;" class="pagination-link {{ $index == 1 ? 'active disabled' : '' }}" panel-id="{{ $oldServiceRequest->id }}">{{ $index }}</a></li>
                        <?php $index++; ?>
                    @endforeach                    
                </ul>

            </div>
        </div>
    </div>
</div>

<script>
paginationPatientHistory();
</script>
