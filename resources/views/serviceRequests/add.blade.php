@extends('layouts.app')

@section('title',ucfirst(trans('titles.new-request')))

@section('content')

    @include('partials.actionbar',[ 'title' => trans('titles.new-request'),
    'elem_type' => 'button',
    'elem_name' => ucfirst(trans('labels.save')),
    'form_id' => '#ReceptionAddForm',
    'route' => '',
    'fancybox' => '',
    'routeBack' => route('reception')
])

<form method="post" action="{{ route('reception.add') }}" id="ReceptionAddForm">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="appointment_id" value="{{ $appointment->id or '' }}">
    <input type="hidden" name="active" value="1">
    <div class="container-fluid reception add">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <!-- informacion paciente -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 reception-group">
                @if ( empty($patient->id) )
                    <!-- búsqueda -->
                    <div class="row">
                        <div class="col-xs-12 reception-group">
                            <div class="panel sombra x3">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-search"
                                        aria-hidden="true"></i> {{ trans('titles.patient-search') }}</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 reception-group">
                                                <div>
                                                    <label for='patient_ID_search'>{{ ucfirst(trans('labels.patient-id')) }}</label>
                                                </div>
                                                <input type="text" name="patient_ID_search" id="patient_ID_search"
                                                class="input-field form-control user btn-style"/>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 reception-group">
                                                <div>
                                                    <label for='email_search'>{{ ucfirst(trans('labels.email')) }}</label>
                                                </div>
                                                <input type="text" name="email_search" id="email_search"
                                                class="input-field form-control user btn-style"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 reception-group">
                                                <div>
                                                    <label for='first_name_search'>{{ ucfirst(trans('labels.name')) }}</label>
                                                </div>
                                                <input type="text" name="first_name_search" id="first_name_search"
                                                class="input-field form-control user btn-style"/>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 reception-group">
                                                <div>
                                                    <label for='last_name_search'>{{ ucfirst(trans('labels.last-name')) }}</label>
                                                </div>
                                                <input type="text" name="last_name_search" id="last_name_search"
                                                class="input-field form-control user btn-style"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 reception-group">
                                                <button id="search-patient" class="btn btn-form btn-left">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                    {{ ucfirst(trans('labels.action-search')) }}
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /búsqueda -->
                    @endif

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                            <div class="panel sombra">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="dashboard-icon icon-user"
                                        aria-hidden="true"></i> {{ trans('titles.patient-info') }}
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center reception-group">

                                            <input type="hidden" name="patient[id]" id="patient_table_id"
                                            value="{{ isset($patient->id) ? $patient->id : '' }}">

                                            <div class="patient-img ">
                                                @if( isset($patient->id) && !empty($patient->photo))
                                                    <img src="{{ asset( 'storage/'. $patient->photo ) }}"
                                                    alt="Patient-Image" class="img-responsive" id='avatar'>
                                                @else
                                                    <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                                    class="img-responsive" id='avatar'>
                                                @endif
                                            </div>
                                            <div>
                                                <a type="button" name="edit-photo" id="edit-photo" class="btn btn-form"
                                                title="Edit avatar">
                                                <i class="fa fa-upload" aria-hidden="true"></i>
                                                {{ ucfirst(trans('labels.upload-photo')) }}
                                            </a>
                                            <input type="hidden" name="patient[photo]" id="photo"
                                            title="Patient avatar"
                                            value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">                  
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                <label for='anonymous' style="display: none;">{{ ucfirst(trans('labels.anonymous')) }} </label>
                                                @if(!$patient->id)
                                                    @include('includes.general-checkbox', [
                                                        'id'=>'anonymous-chk',
                                                        'name'=>'anonymous',
                                                        'label'=>"labels.anonymous",
                                                        'condition' => 0,
                                                        'val' => 0
                                                    ])
                                                    @if( old('anonymous') == 1 )
                                                        @push('scripts')
                                                            <script>
                                                            $(document).ready( function() {
                                                                $('#anonymous-chk').prop('checked', true);
                                                                $('#anonymous-chk').change();
                                                            });
                                                            </script>
                                                        @endpush
                                                    @endif
                                                @else
                                                    <input type="hidden" name="anonymous" value="0">
                                                @endif
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                <div>
                                                    <label class="title-label" for='patient_ID'>{{ ucfirst(trans('labels.patient-id')) }} *</label>
                                                    </div>
                                                    <div>
                                                        <input type="text" class="form-control" name="patient_ID"
                                                        id="patient_ID" value=
                                                        @if (isset($patient->id))
                                                            "{{ $patient->patient_ID }}"
                                                        @elseif(isset($appointment))
                                                            "{{ $appointment->patient_identification_id }}"
                                                        @else
                                                            "{{ old('patient_ID') }}"
                                                        @endif
                                                        >
                                                        {{-- {{ isset($patient->id) ? $patient->patient_ID : old('patient_ID') }}"> --}}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                    <div>
                                                        <label class="title-label" for="sex_id">{{ ucfirst(trans('labels.gender')) }} *</label>
                                                        </div>
                                                        <div>
                                                            <?php
                                                                if ( old('patient.sex_id') != null ) {
                                                                    $sexValue = old('patient.sex_id');
                                                                } else {
                                                                    $sexValue = (isset($patient->id))?$patient->sex_id:'';
                                                                }
                                                            ?>
                                                            @include('includes.select',[
                                                                        'idname' => 'patient[sex_id]',
                                                                        'data' => $sexes,
                                                                        'keys' => ['id','name'],
                                                                        'value' => $sexValue
                                                                    ])
                                                            @if ($errors->has('patient.sex_id'))
                                                                <span class="text-danger">
                                                                    <strong>{{ $errors->first('patient.sex_id') }}</strong>
                                                                </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                <div>
                                                    <label class="title-label" for='first_name'>{{ ucfirst(trans('labels.name')) }} *</label>
                                                </div>
                                                <div>
                                                    <input type="text" name="patient[first_name]" id="first_name"
                                                    class="input-field form-control user btn-style" value=
                                                        @if ( old('patient.first_name') != null )
                                                            "{{ old('patient.first_name')  }}"
                                                        @elseif (isset($patient->id))
                                                            "{{ $patient->first_name }}"
                                                        @elseif(isset($appointment))
                                                            "{{ $appointment->patient_first_name }}"    
                                                        @endif
                                                    >
                                                    {{-- {{ isset($patient->id) ? $patient->first_name : old('patient[first_name]') }}" /> --}}
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                <div>
                                                    <label class="title-label" for='last_name'>{{ ucfirst(trans('labels.last-name')) }} *</label>
                                                    </div>
                                                    <div>
                                                        <input type="text" name="patient[last_name]" id="last_name"
                                                        class="input-field form-control user btn-style" value=

                                                        @if ( old('patient.last_name') != null )
                                                            "{{ old('patient.last_name') }}"
                                                        @elseif (isset($patient->id))
                                                            "{{ $patient->last_name }}"
                                                        @elseif(isset($appointment))
                                                            "{{ $appointment->patient_last_name }}"
                                                        @endif
                                                        >
                                                        {{-- {{ isset($patient->id) ? $patient->last_name : old('patient[last_name]') }}" /> --}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                    <div>
                                                        <label class="title-label" for='birth_date'>{{ ucfirst(trans('labels.birth-date')) }} *</label>
                                                    </div>
                                                    <div class="reception-group">
                                                        <?php
                                                            if ( old('patient.birth_date') != null ) {
                                                                $bdValue = old('patient.birth_date');
                                                            } else {
                                                                $bdValue = isset($patient->id) ? $patient->getBirthDate() : '';
                                                            }
                                                        ?>
                                                        <input type="text" name="patient[birth_date]" id="birth_date" class="form-control" value="{{ $bdValue }}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                    <div>
                                                        <label for='email'>{{ ucfirst(trans('labels.email')) }} *</label>
                                                    </div>
                                                    <div>
                                                        <input type="email" class="form-control" name="email" id="email" value=
                                                        @if ( old('email') != null)
                                                            "{{ old('email') }}"
                                                        @elseif (isset($patient->id))
                                                            "{{ $patient->email }}"
                                                        @elseif(isset($appointment))
                                                            "{{ $appointment->patient_email }}"    
                                                        @endif
                                                        >
                                                        {{-- {{ isset($patient->id) ? $patient->email : old('email') }}"> --}}
                                                    </div>
                                                </div>
                                            </div>

                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                        <div>
                                                            <label class="title-label" for='country_of_residence'>{{ ucfirst(trans('labels.country-residence')) }} *</label>
                                                        </div>
                                                        <div>
                                                            <?php
                                                                $countryValue = 862;
                                                                if ( old('patient.country_id') != null ) {
                                                                    $countryValue = old('patient.country_id');
                                                                } elseif ( isset($patient->id) && !empty($patient->country_id) ) {
                                                                    $countryValue = $patient->country_id;
                                                                }
                                                            ?>

                                                            @include('includes.select',[
                                                                'id' => 'country_id',
                                                                'name' => 'patient[country_id]',
                                                                'data' => $countries,
                                                                'keys' => [
                                                                            'id',
                                                                            'name'
                                                                            ],
                                                                'value' => $countryValue
                                                            ])
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                        <div>
                                                            <label class="title-label" for='citizenship'>{{ ucfirst(trans('labels.citizenship')) }} *</label>
                                                        </div>
                                                        <div>
                                                            <?php
                                                                $citizenshipValue = 862;
                                                                if ( old('patient.citizenship') != null ) {
                                                                    $citizenshipValue = old('patient.citizenship');
                                                                } elseif ( isset($patient->id) && !empty($patient->citizenship) ) {
                                                                    $citizenshipValue = $patient->citizenship;    
                                                                }
                                                            ?>
                                                            
                                                            @include('includes.select',[
                                                                'id' => 'citizenship',
                                                                'name' => 'patient[citizenship]',
                                                                'data' => $countries,
                                                                'keys' => [
                                                                            'id',
                                                                            'citizenship'
                                                                            ],
                                                                'value' => $citizenshipValue
                                                            ])
                                                        </div>
                                                    </div>
                                                </div>
                                                         
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                                <div>
                                                                    <label class="title-label" for='telephone_number'>{{ ucfirst(trans('labels.telephone-number')) }} </label>
                                                                    </div>
                                                                    <div>
                                                                        <input type="tel" class="form-control" name="telephone_number"
                                                                        id="telephone_number" value=

                                                                        @if ( old('telephone_number') != null )
                                                                            "{{ old('telephone_number') }}"
                                                                        @elseif (isset($patient->id))
                                                                            "{{ $patient->telephone_number }}"
                                                                        @elseif(isset($appointment))
                                                                            "{{ $appointment->patient_telephone_number }}"
                                                                        @endif

                                                                        >
                                                                        {{-- {{ isset($patient->id) ? $patient->telephone_number : old('telephone_number') }}"> --}}
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                                    <div>
                                                                        <label class="title-label" for='cellphone_number'>{{ ucfirst(trans('labels.cellphone-number')) }} </label>
                                                                        </div>
                                                                        <div>
                                                                            <input type="tel" class="form-control" name="cellphone_number"
                                                                            id="cellphone_number" value=
                                                                            @if ( old('cellphone_number') != null )
                                                                                "{{ old('cellphone_number')  }}"
                                                                            @elseif (isset($patient->id))
                                                                                "{{ $patient->cellphone_number }}"
                                                                            @elseif(isset($appointment))
                                                                                "{{ $appointment->patient_cellphone_number }}"
                                                                            @endif
                                                                            >
                                            {{-- {{ isset($patient->id) ? $patient->cellphone_number : old('cellphone_number') }}"> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                        <div>
                                            <label class="title-label" for='responsable_id'>{{ ucfirst(trans('labels.responsable-id')) }}</label>
                                        </div>
                                        <div>
                                            <?php
                                                if ( old('responsable.responsable_id') != null ) {
                                                    $responsableValue = old('responsable.responsable_id');
                                                } else {
                                                    $responsableValue = isset($patient->responsable->id) ? $patient->responsable->responsable_id : '';
                                                }
                                            ?>
                                            <input type="text" class="form-control"
                                                   name="responsable[responsable_id]"
                                                   id="responsable_id"
                                                   value="{{ $responsableValue }}">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                        <div>
                                            <label class="title-label" for='responsable_name'>{{ ucfirst(trans('labels.responsable-name')) }}</label>
                                        </div>
                                        <div>
                                            <?php
                                                if ( old('responsable.name') != null ) {
                                                    $responsableValue = old('responsable.name');
                                                } else {
                                                    $responsableValue = isset($patient->responsable->id) ? $patient->responsable->name : '';
                                                }
                                            ?>
                                            <input type="text" class="form-control" name="responsable[name]"
                                                   id="responsable_name"
                                                   value="{{ $responsableValue }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                        <div>
                                            <label class="title-label" for='occupation'>{{ ucfirst(trans('labels.occupation')) }}</label>
                                        </div>
                                        <div>
                                            <?php
                                                if ( old('patient.occupation') != null ) {
                                                    $ocupationValue = old('patient.occupation');
                                                } else {
                                                    $ocupationValue = isset($patient->id) ? $patient->occupation : '';
                                                }
                                            ?>
                                            <input type="text" class="form-control" name="patient[occupation]"
                                                   id="occupation"
                                                   value="{{ $ocupationValue }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                        <div>
                                            <label class="title-label" for='address'>{{ ucfirst(trans('labels.address')) }} *</label>
                                        </div>
                                        <div>
                                            <?php
                                                if ( old('patient.address') != null ) {
                                                    $addresssValue = old('patient.address');
                                                } else {
                                                    $addresssValue = isset($patient->id) ? $patient->address : '';
                                                }
                                            ?>
                                            <textarea class="form-control" name="patient[address]" id="address">{{ $addresssValue }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group" >
                                        <div>
                                            <label class="title-label" for='reason'>{{ ucfirst(trans('labels.reason')) }} *</label>
                                        </div>
                                        <div>
                                            <?php
                                                if ( old('patient.reason') != null ) {
                                                    $reasonValue = old('patient.reason');
                                                } else {
                                                    $reasonValue = isset($patient->id) ? $patient->reason : '';
                                                }
                                            ?>
                                            <textarea class="form-control" name="patient[reason]" id="reason">{{ $reasonValue }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                @if (isset($appointment)) 
                                    @if (is_null($appointment->allergies))
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                <p>
                                                    <strong>{{ ucfirst(trans('labels.allergies')) }}: </strong>
                                                    {{ isset($appointment->id) ? $appointment->patient_allergies : old('appointment[patient_allergies]') }}
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                @elseif($patient->allergies != null)
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                            <p>
                                                <strong>{{ ucfirst(trans('labels.allergies')) }}: </strong>
                                                {{ isset($patient->id) ? $patient->allergies : old('patient[allergies]') }}
                                            </p>
                                        </div>
                                    </div>
                                @endif

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                        <div>
                                            <label class="title-label" for='patientDocuments'>{{ ucfirst(trans('labels.documents')) }}</label>
                                        </div>
                                        <div>
                                            <input name="patientDocuments[]" id="patientDocuments" type="file" accept=".xlsx,.xls,.jpg,.jpeg,.png,.doc,.docx,.pdf"/>
                                            @if (isset($patient->id))
                                            <div class="k-widget k-upload k-header">
                                                <ul class="k-upload-files k-reset">
                                                @foreach($patient->patientDocuments as $key => $patientDocument)
                                                    <li class="k-file" id="k-file-{{ $key + 2000 }}" attr-id="{{ $patientDocument->id }}">
                                                        <input name="patientDocuments[{{ $key + 2000 }}][id]" value="{{ $patientDocument->id }}" type="hidden"/>
                                                        <input name="patientDocuments[{{ $key + 2000 }}][name]" value="{{ $patientDocument->name }}" type="hidden"/>
                                                        <input name="patientDocuments[{{ $key + 2000 }}][filename]" value="{{ $patientDocument->filename }}" type="hidden"/>
                                                        <input name="patientDocuments[{{ $key + 2000 }}][description]" value="{{ $patientDocument->description }}" type="hidden"/>
                                                        <input name="patientDocuments[{{ $key + 2000 }}][location]" value="{{ $patientDocument->location }}" type="hidden"/>
                                                        <input name="patientDocuments[{{ $key + 2000 }}][patient_id]" value="{{ $patientDocument->patient_id }}" type="hidden"/>
                                                        <input name="patientDocuments[{{ $key + 2000 }}][type]" value="{{ $patientDocument->type }}" type="hidden"/>
                                                        <span class="k-progress" style="width: 100%;"></span>
                                                        <span class="k-file-extension-wrapper">
                                                            <span class="k-file-extension">{{ $patientDocument->type }}</span>
                                                            <span class="k-file-state"></span>
                                                        </span>
                                                        <span class="k-file-name-size-wrapper">
                                                            <span class="k-file-name" filename="{{ $patientDocument->filename }}">{{ $patientDocument->name }}</span>
                                                            <span class="k-file-size">95.56 KB</span>
                                                        </span>
                                                        <strong class="k-upload-status">
                                                            <button deleteLocation="patientDocumentsToDelete"
                                                                type="button"
                                                                class="k-button k-button-bare k-upload-action k-delete-file"
                                                                id="k-delete-file-{{ $key + 2000 }}">
                                                                <span class="k-icon k-i-close k-i-delete" title="Quitar" aria-label="Quitar"></span>
                                                            </button>
                                                        </strong>
                                                    </li>
                                                @endforeach
                                                </ul>
                                            </div>
                                            <div id="patientDocumentsToDelete"></div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                            <div class="panel sombra">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><i
                                                        class="dashboard-icon icon-edit"></i> {{ trans('titles.request-info') }}
                                                    </h3>
                                                </div>
                                                <div class="panel-body">
                                                    @if (isset($appointment)) 
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                            <p>
                                                                <strong>{{ trans('labels.contrast-study') . ': ' }}</strong>
                                                                @if($appointment->procedure_contrast_study == 1)
                                                                    {{ trans('labels.yes')}}
                                                                @else
                                                                    {{ trans('labels.not')}}
                                                                @endif    
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                            @if ($appointment->patient_abdominal_circumference >0)
                                                            <p>
                                                                <strong>{{ trans('labels.patient-abdominal-circumference') . ': ' }}</strong> {{ $appointment->patient_abdominal_circumference or trans('labels.na') }}
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                            <div>
                                                                <label class="title-label" for='weight'>{{ ucfirst(trans('labels.weight')) }} kg </label>
                                                            </div>
                                                            <div>
                                                                <input type="number" class="form-control" step='5' name="weight"
                                                                id="weight" min="0"
                                                                value="{{ $serviceRequest->weight or old('weight') }}"
                                                                placeholder="50">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                                            <div>
                                                                <label class="title-label" for='height'>{{ ucfirst(trans('labels.height')) }} m </label>
                                                            </div>
                                                            <div>
                                                                <input type="number" class="form-control" step='0.1' name="height"
                                                                id="height" min="0"
                                                                value="{{ $serviceRequest->height or old('height') }}"
                                                                placeholder="1.5">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                   
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group space-bottom">
                                                            <div>
                                                                <label class="title-label" for="patient_type_id">{{ ucfirst(trans('labels.patient-type')) }}*</label>
                                                            </div>
                                                            <div>
                                                                <?php
                                                                //$patientTypeValue = '';
                                                                $patientTypeValue = 1;
                                                                if ( old('patient_type_id') != null ) {
                                                                    $patientTypeValue = old('patient_type_id');
                                                                } elseif ( isset($serviceRequest) && !empty($serviceRequest->patient_type->id) ) { 
                                                                    $patientTypeValue = $serviceRequest->patient_type->id;
                                                                }
                                                                ?>
                                                                @include('includes.select',[
                                                                           'idname' => 'patient_type_id',
                                                                           'data' => $patientTypes,
                                                                           'keys' => [ 'id', 'description' ],
                                                                           'value' => $patientTypeValue
                                                                           ])
                                                                @if ($errors->has('patient_type_id'))
                                                                    <span class="text-danger">
                                                                    <strong>{{ $errors->first('patient_type_id') }}</strong>
                                                                </span>
                                                                @endif


                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group space-bottom">
                                                            <div>
                                                                <label class="title-label" for="source_id">{{ ucfirst(trans('labels.source')) }} *</label>
                                                            </div>
                                                            <div>
                                                                <?php
                                                                //$sourceValue = '';
                                                                $sourceValue = 11;
                                                                if ( old('source_id') != null ) {
                                                                    $sourceValue = old('source_id');
                                                                } elseif ( isset($serviceRequest) && !empty( $serviceRequest->source_id ) ) { 
                                                                    $sourceValue =  $serviceRequest->source_id;
                                                                }
                                                                ?>

                                                                @include('includes.select',[
                                                                        'idname' => 'source_id',
                                                                        'data' => $sources,
                                                                        'keys' => [ 'id', 'description' ],
                                                                        'value' => $sourceValue
                                                                        ])
                                                                @if ($errors->has('source_id'))
                                                                    <span class="text-danger">
                                                                    <strong>{{ $errors->first('source_id') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 reception-group">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                                    <label class="title-label" for="referring_id">{{ ucfirst(trans('labels.referring')) }}</label>
                                                                </div>
                                                                <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 reception-group">
                                                                    <?php
                                                                        $referringValue=1;
                                                                    if ( old('referring_id') != null ) {
                                                                        $referringValue = old('referring_id');
                                                                    } elseif ( isset($serviceRequest) && !empty( $serviceRequest->referring->id ) ) { 
                                                                        $referringValue =  $serviceRequest->referring->id;
                                                                    }

                                                                    ?>
                                                                    
                                                                    @include('includes.select',[
                                                                            'idname' => 'referring_id',
                                                                            'data' => $referrings,
                                                                            'keys' => [ 'id', 'full_name'],
                                                                            'value' => $referringValue
                                                                            ])
                                                                    @if ($errors->has('referring_id'))
                                                                        <span class="text-danger">
                                                                            <strong>{{ $errors->first('referring_id') }}</strong>
                                                                        </span>
                                                                    @endif

                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 reception-group">
                                                                        <a class="referring-add-btn add-row cboxElement" id="submit-2" data-style="expand-left" href="{{ route('referrings.add') }}">
                                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 reception-group">
                                                            <div>
                                                                <label class="title-label" for="answer_id">{{ trans('labels.how-did') }} *</label>
                                                            </div>
                                                            <div>
                                                                <?php
                                                                    //$answerValue = '';
                                                                    $answerValue = 12;
                                                                    if ( old('answer_id') != null ) {
                                                                        $answerValue = old('answer_id');
                                                                    } elseif ( isset($serviceRequest) && !empty( $serviceRequest->answer->id ) ) { 
                                                                        $answerValue =  $serviceRequest->answer->id;
                                                                    }
                                                                ?>
                                                                @include('includes.select',[
                                                                    'idname' => 'answer_id',
                                                                    'data' => $answers,
                                                                    'keys' => [ 'id', 'description'],
                                                                    'value' => $answerValue
                                                                    ])
                                                                @if ($errors->has('answer_id'))
                                                                <span class="text-danger">
                                                                    <strong>{{ $errors->first('answer_id') }}</strong>
                                                                </span>
                                                                @endif

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                            <div>
                                                                <label class="title-label" for='comments'>{{ ucfirst(trans('labels.comments')) }}</label>
                                                            </div>
                                                            <div>
                                                                <textarea class="form-control" name="comments"
                                                                id="comments">{{ old('comments') }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                            <div class="panel sombra">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><i class="fa fa-file"
                                                        aria-hidden="true"></i> {{ trans('titles.related-documents') }}
                                                    </h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                            <div>
                                                                <a type="button" name="scan-file" id="scan-file" class="btn btn-form"
                                                                title="Scan File">
                                                                <i class="fa fa-print" aria-hidden="true"></i>
                                                                {{ ucfirst(trans('labels.scan-file')) }}
                                                            </a>
                                                        </div>
                                                        <div class="col-xs-12 content-scan-file-take"></div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                        <div>
                                                            <label class="title-label" for='requestDocuments'>{{ ucfirst(trans('labels.documents')) }}</label>
                                                        </div>
                                                        <div>
                                                            <input name="requestDocuments[]" id="requestDocuments"  type="file" accept=".xlsx,.xls,.jpg,.jpeg,.png,.doc,.docx,.pdf"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- informacion paciente -->

                            <!-- Procedimientos -->
                            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
                                <div class="panel sombra x3">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="fa fa-file-text"
                                            aria-hidden="true"></i> {{ ucfirst(trans('titles.procedures')) }}
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div>
                                                    <label class="title-label" for="modality_id_reception">{{ ucfirst(trans('labels.modality')) }}</label>
                                                </div>
                                                <div>
                                                    @include('includes.select', [
                                                        'idname' => 'modality_id_reception',
                                                        'data' => $modalities,
                                                        'keys' => ['id', 'name'] 
                                                    ])
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div>
                                                    <label class="title-label" for="procedure_id">{{ ucfirst(trans('labels.procedures')) }}</label>
                                                </div>
                                                <div>
                                                    @include('includes.procedures-select',[
                                                        'idname' => 'procedure_id',
                                                        'procedures' => $procedures
                                                    ])
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="inline-button">
                                                    <a type="button" href="javascript:;" name="edit-photo" id="addRequestedProcedure"
                                                    class="btn btn-form" title="Edit avatar">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                    {{ ucfirst(trans('labels.add')) }}
                                                </a>
                                                {{-- <a href="javascript:;" class="btn btn-danger" id="removeRequestedProcedure">
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                {{ ucfirst(trans('labels.delete-last')) }}
                                            </a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Procedimientos -->

                    <!-- RequestedProcedures -->
                    <div id="requestedProcedures">

                        @if (isset($appointment))

                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 requestedProcedure" id="0">
                                <div class="panel sombra">
                                    <div class="panel-heading bg-blue">
                                        <a type="button" id="close-procedure" class="close white requested-procedure-close" id-attr="0">×</a>
                                        <h3 class="panel-title">{{ $appointment->procedure->description }} - {{ $appointment->procedure->modality->name }}</h3>
                                    </div>
                                    <div class="panel-body">
                                        <input type="hidden" name="requestedProcedures[0][procedure_id]" value="{{ $appointment->procedure->id }}">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                <div id="check-awesome">
                                                    <div class="ckeckbox-left">    
                                                        <input type="checkbox" name="requestedProcedures[0][urgent]" id="urgent-0" class="form-control" value="1" {{ $appointment->urgent == 1 ? 'checked' : '' }}>  
                                                        <label for="urgent-0">
                                                            <div class="check-container">
                                                                <span class="check"></span>
                                                            </div>
                                                            <span class="box"></span>
                                                        </label> 
                                                    </div>
                                                    <div class="ckeckbox-right">
                                                        <span class="message-checkbox">
                                                        {{ ucfirst(trans('labels.urgent')) }}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                                <div>
                                                    <label class="title-label" for='comments'>{{ ucfirst(trans('labels.comments')) }}</label>
                                                </div>
                                                <div>
                                                    <textarea class="form-control" name="requestedProcedures[0][comments]"
                                                    id="comments">{{ $appointment->observations or old('observations') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif
                    </div>
                    <!-- /RequestedProcedures -->
                </div>
            </div>

        </form>
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\ServiceRequestAddRequest', '#ReceptionAddForm'); !!}



        <div class="modal fade" id="patient-search-modal" tabindex="-1" role="dialog"
        aria-labelledby="patient-search-Label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"
                        id="patient-search-Label">{{ ucfirst(trans('labels.search-results')) }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group" id="search-modal-content">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="close-search"
                        data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="img-modal-Label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="img-modal-Label">{{ ucwords(trans('labels.img-edit')) }}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div class="croppic-init" id="patient-img-container">
                                        <!-- <img src="{{ "/images/patients/avatars/empty.jpg" }}" alt="Avatar" id="editable-img"/> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="close-img-edit"
                            data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
                            <button type="button" class="btn btn-form"
                            id="set-photo">{{ ucfirst(trans('labels.save')) }}</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            @include('includes.modal-scan-file');
        @endsection