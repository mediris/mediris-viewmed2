@extends('layouts.app')

@section('title', ucfirst(trans('titles.reception')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.reception')),
                                    'elem_type' => 'a',
                                    'elem_name' => ucfirst(trans('labels.newreception')),
                                    'form_id' => '',
                                    'route' => route('reception.add'),
                                    'fancybox' => '',
                                    'routeBack' => '',
                                    'clearFilters' => TRUE
                                ])

    <div class="container-fluid reception index">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="title">
                    <h1>{{ trans('titles.appointments-list') }}</h1>
                </div>

                <?php

                // Grid
                $options = (object) array(
                    'url' => 'reception'
                );
                $kendo    = new \App\CustomKendoGrid($options);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
                $IdField->type('number');
                $DateField = new \Kendo\Data\DataSourceSchemaModelField('date');
                $DateField->type('date');
                $StartTimeField = new \Kendo\Data\DataSourceSchemaModelField('dateStartTime');
                $StartTimeField->type('time');
                $EndTimeField = new \Kendo\Data\DataSourceSchemaModelField('dateEndTime');
                $EndTimeField->type('time');
                $PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('lastName');
                $PatientLastNameField->type('string');
                $PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('firstName');
                $PatientFirstNameField->type('string');
                $PatientIdentificationIDField = new \Kendo\Data\DataSourceSchemaModelField('patientIdentificationID');
                $PatientIdentificationIDField->type('number');
                $PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                $PatientIDField->type('string');
                $ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureName');
                $ProcedureField->type('string');
                $RoomField = new \Kendo\Data\DataSourceSchemaModelField('room');
                $RoomField->type('string');
                $AppointmentStatusField = new \Kendo\Data\DataSourceSchemaModelField('statusDescription');
                $AppointmentStatusField->type('string');
                $AppointmentStatusColorField = new \Kendo\Data\DataSourceSchemaModelField('statusColor');
                $AppointmentStatusColorField->type('string');

                // Add Fields
                $kendo->addFields(array(
                    $IdField,
                    $DateField,
                    $StartTimeField,
                    $EndTimeField,
                    $PatientLastNameField,
                    $PatientFirstNameField,
                    $PatientIdentificationIDField,
                    $ProcedureField,
                    $RoomField,
                    $AppointmentStatusField,
                    $AppointmentStatusColorField,
                ));

                // Create Schema
                $kendo->createSchema(true, true);

                // Create Data Source
                $kendo->createDataSource();
                
                // Create Grid
                $kendo->createGrid('serviceRequests');

                $Id = new \Kendo\UI\GridColumn();
                $Id->field('id')
                        ->filterable(false)
                        ->title(ucfirst(trans('labels.appointment-id')));
                $Date = new \Kendo\UI\GridColumn();
                $Date->field('date')
                        ->title(ucfirst(trans('labels.date')))
                        ->format('{0:dd/MM/yyyy}');
                $StartTimeFilterable = new \Kendo\UI\GridColumnFilterable();
                $StartTimeFilterable->ui(new \Kendo\JavaScriptFunction('startTimeFilter'));
                $StartTime = new \Kendo\UI\GridColumn();
                $StartTime->field('dateStartTime')
                        ->filterable(false)
                        ->title(ucfirst(trans('labels.start-time')))
                        ->format('{0:h:mm tt}');
                $EndTimeFilterable = new \Kendo\UI\GridColumnFilterable();
                $EndTimeFilterable->ui(new \Kendo\JavaScriptFunction('endTimeFilter'));
                $EndTime = new \Kendo\UI\GridColumn();
                $EndTime->field('dateEndTime')
                        ->filterable(false)
                        ->title(ucfirst(trans('labels.end-time')))
                        ->format('{0:h:mm tt}');
                $PatientLastName = new \Kendo\UI\GridColumn();
                $PatientLastName->field('lastName')
                        ->title(ucfirst(trans('labels.patient.last-name')));
                $PatientFirstName = new \Kendo\UI\GridColumn();
                $PatientFirstName->field('firstName')
                        ->title(ucfirst(trans('labels.patient.first-name')));
                $PatientIdentificationIDFilterable = new \Kendo\UI\GridColumnFilterable();
                $PatientIdentificationIDFilterable->ui(new \Kendo\JavaScriptFunction('patientIDFilter'));
                $PatientIdentificationID = new \Kendo\UI\GridColumn();
                $PatientIdentificationID->field('patientIdentificationID')
                            ->filterable($PatientIdentificationIDFilterable)
                            ->title(ucfirst(trans('labels.patient-id')))
                            ->format('{0:n0}'); 
                $Procedure = new \Kendo\UI\GridColumn();
                $Procedure->field('procedureName')
                        ->title(ucfirst(trans('labels.procedure')));
                $Room = new \Kendo\UI\GridColumn();
                $Room->field('room')
                        ->title(ucfirst(trans('labels.room')));
                $AppointmentStatus = new \Kendo\UI\GridColumn();
                $AppointmentStatus->field('statusDescription')
                        ->filterable(false)
                        ->encoded(false)
                        ->title(ucfirst(trans('labels.status')))
                        ->template(new Kendo\JavaScriptFunction("function(row) {
                                return '<div class=\"' + row.statusColor + '\" id=\"status-' + row.patientIdentificationID + '\">'" .
                                " + row.statusDescription + '</div><div class=\"loader\"><img src=\"/images/loader.gif\"></div>';
                            }
                        "));
                $Actions = new \Kendo\UI\GridColumn();
                $Actions->title(ucfirst(trans('labels.actions')));

                $slash = "<span class='slash'>|</span>";

                $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'reception', 'edit');

                //::::::::::::::::::::: ACTION BUTTONS ::::::::::::::::::::::::://

                $admitOrder = ($canEdit ? "\"<div class='order-action admit-order'><a href='" . url('reception/add') .
                        "/\" + row.patientID + \"/\" + row.id + \"" .
                        "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.admit') .
                        "'><i class='fa fa-check-square-o' aria-hidden='true'></i></a></div>\" + " : '');

                $rejectOrder = ($canEdit ? "\"" . $slash . "\" + \"<div class='order-action reject-order'>" .
                        "<a href='" . url('appointments/reject') . "/\" + row.id + \"" .
                        "' data-toggle='tooltip' data-placement='bottom' class='reject-appointment' title='" . trans('labels.reject') .
                        "'><i class='fa fa-times' aria-hidden='true'></i></a></div>\"" : '');

                $ableToBeBlockedButtons = "<div class='actions'>\" + " . $admitOrder . $rejectOrder . " + \"</div>\"";

                $actionButtons = "\"<div class='action-buttons'>" . $ableToBeBlockedButtons . " + \"</div>\"";


                $Actions->template(new Kendo\JavaScriptFunction("
						function (row) {
                            //console.log(row);
                            if ( row.statusDescription == '" . ucfirst(trans('labels.created')) ."'  ) {
                                return  " . $actionButtons . ";
                            } else {
                                return '';
                            }
						}"));

                // Excel
                $kendo->setExcel('titles.appointments-list', '
                    function(e) {
                        if (!exportFlag) {
                            e.sender.hideColumn(6);
                            e.preventDefault();
                            exportFlag = true;
                            setTimeout(function () {
                              e.sender.saveAsExcel();
                            });
                          } else {
                            e.sender.showColumn(6);
                            exportFlag = false;
                          }
                    }');

                // PDF
                $kendo->setPDF('titles.appointments-list', 'reception/1', '
                    function (e) {
                        e.sender.hideColumn(6);
                        e.promise.done(function() {
                            e.sender.showColumn(6);
                        });
                    }');

                // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
                    $Id,
                    $Date,
                    $StartTime,
                    $EndTime,
                    $PatientLastName,
                    $PatientFirstName,
                    $PatientIdentificationID,
                    $Procedure,
                    $Room,
                    $AppointmentStatus,
                    $Actions
                ));

                $kendo->generate(true, true);
                        
                ?>
                
                {!! $kendo->render() !!}
            
            </div>
        </div>
    </div>

    <div class="modal fade" id="appointment-reject-modal" tabindex="-1" role="dialog"
         aria-labelledby="appointment-reject-Label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="appointment-reject-Label">{{ ucfirst(trans('labels.reject')) }} {{ trans('titles.appointment') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="appointment-reject-modal-content">
                            <form action="#" role="form">

                                <div class="form-group">
                                    <label for="rejection-reason">{{ trans('labels.rejection-reason') }}</label>
                                    <textarea type="text" class="form-control" name="rejection-reason"
                                              id="rejection-reason"
                                              value="{{ trans('labels.rejection-reason') }}"></textarea>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close-appointment-reject-modal"
                            data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
                    <button type="button" class="btn btn-form"
                            id="save-appointment-reject-status">{{ ucfirst(trans('labels.save')) }}</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        function patientIDFilter(element) {
            element.kendoComboBox({
                dataSource: {
                    transport: {
                        read: {
                            url: "reception",
                            type: "POST"
                        }
                    },
                    schema: {
                        data: "data"
                    }
                },
                dataTextField: "patientIdentificationID",
                dataValueField: "patientIdentificationID",
                optionLabel: "--Selecciona un Valor--"
            });
        }

        function startTimeFilter(element) {
            element.kendoTimePicker({
                dataSource: {
                    transport: {
                        read: {
                            url: "reception",
                            type: "POST"
                        }
                    },
                    schema: {
                        data: "data"
                    }
                },
                dataInput: true,
                field: "dateStartTime",
                valueField: "dateStartTime",
                optionLabel: "--Selecciona un Valor--",
                format: "{0:h:mm tt}",
                interval: 60,
            });
        }

        function endTimeFilter(element) {
            element.kendoTimePicker({
                dataSource: {
                    transport: {
                        read: {
                            url: "reception",
                            type: "POST"
                        }
                    },
                    schema: {
                        data: "data"
                    }
                },
                culture: 'es-VE',
                dataInput: true,
                field: "dateEndTime",
                valueField: "dateEndTime",
                optionLabel: "--Selecciona un Valor--",
                format: "{0:h:mm tt}",
                interval: 60,
            });
        }

    </script>

    <script type="x/kendo-template" id="page-template">
        <div class="page-template">
            <div class="header">
                <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
                
            </div>
            <div class="watermark">{{ Session::get('institution')->name }}</div>
            <div class="footer">
                {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
            </div>
        </div>
    </script>
@endsection
