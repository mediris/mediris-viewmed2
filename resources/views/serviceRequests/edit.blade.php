@extends('layouts.app')

@section('title',ucfirst(trans('titles.edit-request')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => trans('titles.new-request'),
	'elem_type' => 'button',
	'elem_name' => ucfirst(trans('labels.save')),
	'form_id' => '#ReceptionEditForm',
	'route' => '',
	'fancybox' => '',
	'routeBack' => route('reception')
])
	
	<div class="container-fluid reception edit">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="panel sombra x3">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-search"
						                           aria-hidden="true"></i> {{ trans('titles.patient-search') }}</h3>
					</div>
					<div class="panel-body">
						
						<form action="{{ route('patients.search') }}" method="post" id="search-patient-form">
							
							{!! csrf_field() !!}
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='patient_ID_search'>{{ ucfirst(trans('labels.patient-id')) }}</label>
									</div>
									<input type="text" name="patient_ID_search" id="patient_ID_search"
									       class="input-field form-control user btn-style"/>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='email_search'>{{ ucfirst(trans('labels.email')) }}</label>
									</div>
									<input type="text" name="email_search" id="email_search"
									       class="input-field form-control user btn-style"/>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='first_name'>{{ ucfirst(trans('labels.name')) }}</label>
									</div>
									<input type="text" name="first_name" id="first_name"
									       class="input-field form-control user btn-style"/>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='last_name'>{{ ucfirst(trans('labels.last-name')) }}</label>
									</div>
									<input type="text" name="last_name" id="last_name"
									       class="input-field form-control user btn-style"/>
								</div>
							</div>
						</form>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<button id="search-patient" class="btn btn-form btn-left">
									<i class="fa fa-search" aria-hidden="true"></i>
									{{ ucfirst(trans('labels.action-search')) }}
								</button>
							</div>
						</div>
					
					</div>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
				<div class="panel sombra x3">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-file-text"
						                           aria-hidden="true"></i> {{ ucfirst(trans('titles.procedures')) }}
						</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
								<div>
									<label for="modality_id_reception">{{ ucfirst(trans('labels.modality')) }}</label>
								</div>
								<div>
									<select class="form-control" name="modality_id" id="modality_id_reception">
										<option value="" disabled
										        selected>{{ ucfirst(trans('labels.select')) }}</option>
										@foreach($modalities as $modality)
											<option value="{{ $modality->name }}">{{ $modality->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
								<div>
									<label for="procedure_id">{{ ucfirst(trans('labels.procedures')) }}</label>
								</div>
								<div>
									<select class="form-control" name="procedure_id" id="procedure_id">
										<option value="" disabled
										        selected>{{ ucfirst(trans('labels.select')) }}</option>
										@foreach($procedures as $procedure)
											<option value="{{ $procedure->id }}"
											        class="{{ $procedure->getModalitiesString(Session::get('institution')->url, $procedure->id) }}">{{ $procedure->description }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
								<div class="inline-button">
									<a type="button" href="javascript:;" name="edit-photo" id="addRequestedProcedure"
									   class="btn btn-form" title="Edit avatar">
										<i class="fa fa-plus" aria-hidden="true"></i>
										{{ ucfirst(trans('labels.add')) }}
									</a>
									{{--<a href="javascript:;" class="btn btn-danger" id="removeRequestedProcedure">--}}
									{{--<i class="fa fa-minus" aria-hidden="true"></i>--}}
									{{--{{ ucfirst(trans('labels.delete-last')) }}--}}
									{{--</a>--}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<form method="post" action="{{ route('reception.edit', $serviceRequest->id) }}" id="ReceptionEditForm">
			
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="panel sombra">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="dashboard-icon icon-user"
							                           aria-hidden="true"></i> {{ trans('titles.patient-info') }}</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
									
									<input type="hidden" name="patient[id]" id="patient_table_id"
									       value="{{ $serviceRequest->patient->id }}">
									
									<div class="patient-img ">
										<img src="{{ $serviceRequest->patient->photo }}" alt="Patient-Image"
										     class="img-responsive" id='avatar'/>
									</div>
									<div>
										<a type="button" name="edit-photo" id="edit-photo" class="btn btn-form"
										   title="Edit avatar">
											<i class="fa fa-upload" aria-hidden="true"></i>
											{{ ucfirst(trans('labels.upload-photo')) }}
										</a>
										<input type="hidden" name="patient[photo]" id="photo" title="Patient avatar"
										       value="{{ $serviceRequest->patient->photo }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											@include('includes.general-checkbox', [
											    'id'        =>'active-chk',
											    'name'      =>'anonymous',
											    'label'     =>'labels.anonymous',
											    'condition' => $serviceRequest->anonymous
											])
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div>
												<label for='patient_ID'>{{ ucfirst(trans('labels.patient-id')) }}</label>
											</div>
											<div>
												<input type="text" class="form-control" name="patient_ID"
												       id="patient_ID"
												       value="{{ $serviceRequest->patient->patient_ID }}" readonly>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div>
												<label for="sex_id">{{ ucfirst(trans('labels.gender')) }}</label>
											</div>
											<div>
												<select class="form-control" name="patient[sex_id]" id="sex_id">
													<option value="" disabled
													        selected>{{ ucfirst(trans('labels.select')) }}</option>
													@foreach($sexes as $sex)
														@if(isset($patient->id))
															<option value="{{ $sex->id }}" {{ $patient->sex_id == $sex->id ? 'selected' : '' }}>{{ trans('labels.'.$sex->name) }}</option>
														@else
															<option value="{{ $sex->id }}" {{ $serviceRequest->patient->sex_id == $sex->id ? 'selected' : '' }}>{{ trans('labels.'.$sex->name) }}</option>
														@endif
													@endforeach
												</select>
											</div>
										</div>
									</div>
								
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='first_name'>{{ ucfirst(trans('labels.name')) }}</label>
									</div>
									<input type="text" name="patient[first_name]" id="first_name"
									       class="input-field form-control user btn-style"
									       value="{{ $serviceRequest->patient->first_name }}"/>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='last_name'>{{ ucfirst(trans('labels.last-name')) }}</label>
									</div>
									<input type="text" name="patient[last_name]" id="last_name"
									       class="input-field form-control user btn-style"
									       value="{{ $serviceRequest->patient->last_name }}"/>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='birth_date'>{{ ucfirst(trans('labels.birth-date')) }}</label>
									</div>
									<div>
										<input type="date" name="patient[birth_date]" id="birth_date"
										       class="form-control" value="{{ $serviceRequest->patient->birth_date }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for="name_prefix">{{ ucfirst(trans('labels.prefix')) }}</label>
									</div>
									<div>
										<select class="form-control" name="patient[name_prefix]" id="name_prefix">
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($prefixes as $prefix)
												@if(isset($patient->id))
													<option value="{{ $prefix->name }}" {{ $patient->name_prefix == $prefix->name ? 'selected' : '' }}>{{ $prefix->name }}</option>
												@else
													<option value="{{ $prefix->name }}" {{ $serviceRequest->patient->name_prefix == $prefix->name ? 'selected' : '' }}>{{ $prefix->name }}</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									
									<div>
										<label for='country_of_residence'>{{ ucfirst(trans('labels.country-residence')) }}</label>
									</div>
									<div>
										@include('includes.select',[
                                            'id' => 'country_id',
                                            'name' => 'patient[country_id]'
                                            'data' => $countries,
                                            'keys' => [
                                                        'id',
                                                        'name'
                                                        ],
                                            'value' => $patient->country_id
                                        ])
									</div>
								
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									
									<div>
										<label for='citizenship'>{{ ucfirst(trans('labels.citizenship')) }}</label>
									</div>
									<div>
										@include('includes.select',[
                                            'id' => 'citizenship',
                                            'name' => 'patient[citizenship]'
                                            'data' => $countries,
                                            'keys' => [
                                                        'id',
                                                        'citizenship'
                                                        ],
                                            'value' => $patient->citizenship
                                        ])
									</div>
								
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='email'>{{ ucfirst(trans('labels.email')) }}</label>
									</div>
									<div>
										<input type="email" class="form-control" name="email" id="email"
										       value="{{ $serviceRequest->patient->email }}" readonly>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='telephone_number'>{{ ucfirst(trans('labels.telephone-number')) }}</label>
									</div>
									<div>
										<input type="tel" class="form-control" name="telephone_number"
										       id="telephone_number"
										       value="{{ $serviceRequest->patient->telephone_number }}">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='cellphone_number'>{{ ucfirst(trans('labels.cellphone-number')) }}</label>
									</div>
									<div>
										<input type="tel" class="form-control" name="cellphone_number"
										       id="cellphone_number"
										       value="{{ $serviceRequest->patient->cellphone_number }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='occupation'>{{ ucfirst(trans('labels.occupation')) }}</label>
									</div>
									<div>
										<input type="text" class="form-control" name="patient[occupation]"
										       id="occupation" value="{{ $serviceRequest->patient->occupation }}">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='responsable_name'>{{ ucfirst(trans('labels.responsable-name')) }}</label>
									</div>
									<div>
										<input type="text" class="form-control" name="responsable[name]"
										       id="responsable_name"
										       value="{{ isset($serviceRequest->patient->responsable->name) ? $serviceRequest->patient->responsable->name : '' }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='responsable_id'>{{ ucfirst(trans('labels.responsable-id')) }}</label>
									</div>
									<div>
										<input type="text" class="form-control" name="responsable[responsable_id]"
										       id="responsable_id"
										       value="{{ isset( $serviceRequest->patient->responsable->responsable_id) ?  $serviceRequest->patient->responsable->responsable_id : '' }}">
									</div>
								</div>
							</div>
							
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='address'>{{ ucfirst(trans('labels.address')) }}</label>
									</div>
									<div>
										<textarea class="form-control" name="patient[address]"
										          id="address">{{ $serviceRequest->patient->address }}</textarea>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='patientDocuments'>{{ ucfirst(trans('labels.documents')) }}</label>
									</div>
									<div>
										<input name="patientDocuments[]" id="patientDocuments" type="file" accept=".xlsx,.xls,.jpg,.jpeg,.png,.doc,.docx,.pdf"/>
										
										<div class="k-widget k-upload k-header">
											<ul class="k-upload-files k-reset">
												
												@foreach($serviceRequest->patient->patientDocuments as $key => $patientDocument)
													<li class="k-file" id="k-file-{{ $key + 2000 }}"
													    attr-id="{{ $patientDocument->id }}">
														<input name="patientDocuments[{{ $key + 2000 }}][id]"
														       value="{{ $patientDocument->id }}" type="hidden"/>
														<input name="patientDocuments[{{ $key + 2000 }}][name]"
														       value="{{ $patientDocument->name }}" type="hidden"/>
														<input name="patientDocuments[{{ $key + 2000 }}][filename]"
														       value="{{ $patientDocument->filename }}" type="hidden"/>
														<input name="patientDocuments[{{ $key + 2000 }}][description]"
														       value="{{ $patientDocument->description }}"
														       type="hidden"/>
														<input name="patientDocuments[{{ $key + 2000 }}][location]"
														       value="{{ $patientDocument->location }}" type="hidden"/>
														<input name="patientDocuments[{{ $key + 2000 }}][patient_id]"
														       value="{{ $patientDocument->patient_id }}"
														       type="hidden"/>
														<input name="patientDocuments[{{ $key + 2000 }}][type]"
														       value="{{ $patientDocument->type }}" type="hidden"/>
														<span class="k-progress" style="width: 100%;"></span>
														<span class="k-file-extension-wrapper">
                                                        <span class="k-file-extension">{{ $patientDocument->type }}</span>
                                                        <span class="k-file-state"></span>
                                                    </span>
														<span class="k-file-name-size-wrapper">
                                                        <span class="k-file-name"
                                                              filename="{{ $patientDocument->filename }}">{{ $patientDocument->name }}</span>
                                                        <span class="k-file-size">95.56 KB</span>
                                                    </span>
														<strong class="k-upload-status">
															<button deleteLocation="patientDocumentsToDelete"
															        type="button"
															        class="k-button k-button-bare k-upload-action k-delete-file"
															        id="k-delete-file-{{ $key + 2000 }}">
                                                            <span class="k-icon k-i-close k-i-delete" title="Quitar"
                                                                  aria-label="Quitar">
                                                            </span>
															</button>
														</strong>
													</li>
												@endforeach
											
											</ul>
										</div>
										
										<div id="patientDocumentsToDelete">
										
										</div>
									
									</div>
								</div>
							</div>
						
						</div>
					</div>
				</div>
				
				<div id="requestedProcedures">
				
				</div>
			
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="panel sombra">
						<div class="panel-heading">
							<h3 class="panel-title"><i
										class="dashboard-icon icon-edit"></i> {{ trans('titles.request-info') }}</h3>
						</div>
						
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='weight'>{{ ucfirst(trans('labels.weight')) }} kg </label>
									</div>
									<div>
										<input type="number" min="1" class="form-control" name="weight" id="weight"
										       value="{{ $serviceRequest->weight }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='height'>{{ ucfirst(trans('labels.height')) }} m </label>
									</div>
									<div>
										<input type="number" min="1" class="form-control" name="height" id="height"
										       value="{{ $serviceRequest->height }}">
									</div>
								</div>
							</div>
							
							<div class="row">
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="patient_type_id">{{ ucfirst(trans('labels.patient-type')) }}</label>
									</div>
									<div>
										<select class="form-control" name="patient_type_id" id="patient_type_id">
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($patientTypes as $patientType)
												<option value="{{ $patientType->id }}" {{ $patientType->id == $serviceRequest->patient_type_id ? 'selected' : '' }}>{{ $patientType->description }}</option>
											@endforeach
										</select>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="source_id">{{ ucfirst(trans('labels.source')) }}</label>
									</div>
									<div>
										<select class="form-control" name="source_id" id="source_id">
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($sources as $source)
												<option value="{{ $source->id }}" {{ $source->id == $serviceRequest->source_id ? 'selected' : '' }}>{{ $source->description }}</option>
											@endforeach
										</select>
									</div>
								</div>
							
							</div>
							
							<div class="row">

								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="referring_id">{{ ucfirst(trans('labels.referring')) }}</label>
										</div>
										<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
												<select class="form-control" name="referring_id" id="referring_id">
													<option value="" disabled
															selected>{{ ucfirst(trans('labels.select')) }}</option>

													@foreach($referrings as $referring)
														<option value="{{ $referring->id }}" {{ $referring->id == $serviceRequest->referring_id ? 'selected' : '' }}>{{ $referring->last_name }} {{ $referring->first_name }}</option>
													@endforeach

												</select>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
												<a class="referring-add-btn add-row cboxElement" id="submit-2" data-style="expand-left" href="http://mediris.dev/referrings/add">
													<i class="fa fa-plus" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="answer_id">{{ trans('labels.how-did') }}</label>
									</div>
									<div>
										<select class="form-control" name="answer_id" id="answer_id">
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($answers as $answer)
												<option value="{{ $answer->id }}" {{ $answer->id == $serviceRequest->answer_id ? 'selected' : '' }}>{{ trans('statuses.'.$answer->description) }}</option>
											@endforeach
										</select>
									</div>
								</div>
							
							</div>
							
							<div class="row">
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='parent'>{{ ucfirst(trans('labels.parent')) }}</label>
									</div>
									<input type="text" name="parent" id="parent"
									       class="input-field form-control user btn-style"/>
								</div>
							</div>
							
							<div class="row">
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="smoking_status_id">{{ ucfirst(trans('labels.smoking-status')) }}</label>
									</div>
									<div>
										<select class="form-control" name="smoking_status_id" id="smoking_status_id">
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($smokingStatuses as $smokingStatus)
												<option value="{{ $smokingStatus->id }}" {{ $smokingStatus->id == $serviceRequest->smoking_status_id ? 'selected' : '' }}>{{ trans('statuses.'.$smokingStatus->name) }}</option>
											@endforeach
										</select>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='patient_state'>{{ ucfirst(trans('labels.patient-state')) }}</label>
									</div>
									<select class="form-control" name="patient_state_id" id="patient_state_id">
										<option value="" disabled
										        selected>{{ ucfirst(trans('labels.select')) }}</option>
										@foreach($patientStates as $patientState)
											<option value="{{ $patientState->id }}" {{ $patientState->id == $serviceRequest->patient_state_id ? 'selected' : '' }}>{{  $patientState->description }}</option>
										@endforeach
									</select>
									{{--<div>--}}
									{{--<input type="text" name="patient_state" id="patient_state" class="input-field form-control user btn-style" value="{{ $serviceRequest->patient_state }}"/>--}}
									{{--</div>--}}
								</div>
							
							</div>
							
							<div class="row">
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='issue_date'>{{ ucfirst(trans('labels.issue-date')) }}</label>
									</div>
									<div>
										<input type="date" name="issue_date" id="issue_date" class="form-control"
										       value="{{ $serviceRequest->issue_date }}">
									</div>
								</div>
							
							</div>
							
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='comments'>{{ ucfirst(trans('labels.comments')) }}</label>
									</div>
									<div>
										<textarea class="form-control" name="comments"
										          id="comments">{{ $serviceRequest->comments }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="panel sombra">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-file"
							                           aria-hidden="true"></i> {{ trans('titles.related-documents') }}
							</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<a type="button" name="scan-file" id="scan-file" class="btn btn-form"
										   title="Scan File">
											<i class="fa fa-print" aria-hidden="true"></i>
											{{ ucfirst(trans('labels.scan-file')) }}
										</a>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='requestDocuments'>{{ ucfirst(trans('labels.documents')) }}</label>
									</div>
									
									<input name="requestDocuments[]" id="requestDocuments" type="file" accept=".xlsx,.xls,.jpg,.jpeg,.png,.doc,.docx,.pdf"/>
									
									<ul class="k-upload-files k-reset">
										
										@foreach($serviceRequest->request_documents as $key => $requestDocument)
											<li class="k-file" id="k-file-{{ $key + 100 }}">
												<input name="requestDocuments[{{ $key + 100 }}][id]"
												       value="{{ $requestDocument->id }}" type="hidden"/>
												<input name="requestDocuments[{{ $key + 100 }}][name]"
												       value="{{ $requestDocument->name }}" type="hidden"/>
												<input name="requestDocuments[{{ $key + 100 }}][description]"
												       value="{{ $requestDocument->description }}" type="hidden"/>
												<input name="requestDocuments[{{ $key + 100 }}][location]"
												       value="{{ $requestDocument->location }}" type="hidden"/>
												<input name="requestDocuments[{{ $key + 100 }}][service_request_id]"
												       value="{{ $requestDocument->service_request_id }}"
												       type="hidden"/>
												<input name="requestDocuments[{{ $key + 100 }}][type]"
												       value="{{ $requestDocument->type }}" type="hidden"/>
												<span class="k-progress" style="width: 100%;"></span>
												<span class="k-file-extension-wrapper">
                                            <span class="k-file-extension">{{ $requestDocument->type }}</span>
                                            <span class="k-file-state"></span>
                                        </span>
												<span class="k-file-name-size-wrapper">
                                            <span class="k-file-name"
                                                  title="chivas-12.png">{{ $requestDocument->name }}</span>
                                            <span class="k-file-size">95.56 KB</span>
                                        </span>
												<strong class="k-upload-status">
													<button deleteLocation="requestDocumentsToDelete" type="button"
													        class="k-button k-button-bare k-upload-action k-delete-file"
													        id="k-delete-file-{{ $key + 100 }}">
                                                <span class="k-icon k-i-close k-i-delete" title="Quitar"
                                                      aria-label="Quitar">
                                                </span>
													</button>
												</strong>
											</li>
										@endforeach
									</ul>
									
									<div id="requestDocumentsToDelete">
									
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
		{!! JsValidator::formRequest('App\Http\Requests\ServiceRequestEditRequest', '#ReceptionEditForm'); !!}
	
	</div>
	
	<div class="modal fade" id="patient-search-modal" tabindex="-1" role="dialog"
	     aria-labelledby="patient-search-Label">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="patient-search-Label">{{ ucwords(trans('labels.search-results')) }}</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="search-modal-content">
						
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="close-search"
					        data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
					<button type="button" class="btn btn-form"
					        id="set-patient">{{ ucfirst(trans('labels.select')) }}</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="img-modal-Label">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="img-modal-Label">{{ ucwords(trans('labels.img-edit')) }}</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="croppic-init" id="patient-img-container">
							<!-- <img src="{{ "/images/patients/avatars/empty.jpg" }}" alt="Avatar" id="editable-img"/> -->
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="close-img-edit"
					        data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
					<button type="button" class="btn btn-form"
					        id="set-photo">{{ ucfirst(trans('labels.save')) }}</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	@foreach($serviceRequest->requested_procedures as $key => $requestedProcedure)
		
		<script>
			
			var value = '<?php echo $requestedProcedure->id;  ?>';
			var procedure_id = '<?php echo $requestedProcedure->procedure_id;  ?>';
			var size = '<?php echo $key++; ?>';
			
			$.get('/requestedprocedures/form/' + size + '/' + procedure_id + '/' + value, function (data) {
				$('#requestedProcedures').hide().append(data).fadeIn(400);
			});
			
			$('select#procedure_id option').each(function () {
				if ($(this).attr('value') == value) {
					$(this).hide();
				}
			});
			
			$('#removeRequestedProcedure').fadeIn(function () {
				
				$(this).show().css('display', 'inline-block');
				
			});
		
		</script>
	
	@endforeach

@endsection
