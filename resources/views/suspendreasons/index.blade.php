@extends('layouts.app')
@section('title', ucfirst(trans('titles.suspendreasons')))
@section('content')

	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.suspendreasons')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('suspend_reasons.add'),
									'fancybox' => 'add-row cboxElement',
									'routeBack' => '',
									'clearFilters' => true
								 ])

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="title">
					<h1>{{ ucfirst(trans('titles.suspendreasons')) }}</h1>
				</div>
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('string');
				$DescriptionField = new \Kendo\Data\DataSourceSchemaModelField('description');
				$DescriptionField->type('string');

				$kendo->addFields(array(
					$IdField,
					$DescriptionField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $suspendreasons = $suspendreasons->toArray();
				
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'suspend_reasons', 'edit');
				$canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'suspend_reasons', 'active');

				foreach ($suspendreasons as $key => $value) {
					$title = $suspendreasons[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $suspendreasons[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					$editAction = $canEdit ? '<a href=' . route('suspend_reasons.edit', [$suspendreasons[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement"><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';

					$suspendreasons[$key]['actions'] = $editAction;
				}

				// Set Suspend Reasons
				$kendo->setDataSource($suspendreasons);

				// Create Grid
                $kendo->createGrid('actions');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$Description = new \Kendo\UI\GridColumn();
				$Description->field('description')
						->title(ucfirst(trans('labels.description')));
				$typesSuspent = new \Kendo\UI\GridColumn();
				$typesSuspent->field('admin_reason')
						->title(ucfirst(trans('labels.types-suspension')));
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$Description,
					$typesSuspent,
					$Actions,
                ));
				
                // Defines & Generate
                $kendo->generate();

                // Redefines Data Bound
                $kendo->dataBound('onDataBound');

				?>

				{!! $kendo->render() !!}

			</div>
		</div>
	</div>
	<script>
		$( document ).ready(function() {
		var grid = jQuery("#grid").data("kendoGrid"),
		tec = '{{ trans('labels.technical')  }}',
		admin = '{{ trans('labels.admin')  }}';
		$.each( grid.dataSource.options.data, function( key, value ) {
			if(value.admin_reason==0){ 
				value.admin_reason=tec;
				}
			else if(value.admin_reason==1){
				value.admin_reason=admin;
				}
			});
			grid.dataSource.read();
			grid.refresh();
		});
	</script>
@endsection