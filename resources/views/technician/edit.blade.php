@extends('layouts.app')

@section('title',ucfirst(trans('labels.technician')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('labels.technician')),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('labels.finish-order')),
                                    'form_id' => '#TechnicianEditForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('technician')
                                ])

    <span id="notification" style="display:none;"></span>

    <script id="equipmentTemplate" type="text/x-kendo-template">
        <div class="approve-orders">
            <i class="fa fa-exclamation-circle fa-4x" aria-hidden="true"></i>
            <h3>#= title #</h3>
            <p>#= message #</p>
        </div>
    </script>

    <div class="container-fluid technician edit">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <form method="post" action="{{ route('technician.edit', $requestedProcedure->id) }}" id="TechnicianEditForm">

            <input type="hidden" name="patient[patient_ID]" id="patient_ID" class="form-control" value="{{ $requestedProcedure->patient->patient_ID }}" readonly>
            <input type="hidden" name="patient[id]" id="patient_id" class="form-control" value="{{ $requestedProcedure->patient->id }}" readonly>
            <input type="hidden" name="requestedProcedure[id]" id="requested_procedure_id" class="form-control" value="{{ $requestedProcedure->id }}" readonly>
            <input type="hidden" name="requestedProcedure[service_request_id]" id="service_request_id" class="form-control" value="{{ $requestedProcedure->service_request_id }}" readonly>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-file" aria-hidden="true"></i> {{ trans('titles.patient-info') }}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="patient-img ">
                                        @if( isset($requestedProcedure->patient->id) && !empty($requestedProcedure->patient->photo))
                                          <img src="{{ asset( 'storage/'. $requestedProcedure->patient->photo ) }}"
                                          alt="Patient-Image" class="img-responsive" id='avatar'>
                                        @else
                                          <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                          class="img-responsive" id='avatar'>
                                        @endif
                                     </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="patient-info">
                                                <p><strong>{{ ucfirst(trans('labels.name')) }}:</strong> {{ $requestedProcedure->patient->first_name
                                              . ' ' . $requestedProcedure->patient->last_name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.age')) }}:</strong> {{ $requestedProcedure->patient->age['years']
                                            }} {{ trans('labels.years') }}, {{ $requestedProcedure->patient->age['months'] }}
                                            {{ $requestedProcedure->patient->age['months'] == 1 ? trans('labels.month') : trans('labels.months')
                                            }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.gender')) }}:</strong> {{ $requestedProcedure->patient->sex_id
                                          == 1 ? trans('labels.male') : trans('labels.female') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.height')) }}:</strong> {{ $requestedProcedure->service_request->height
                                            . ' ' . trans('labels.meters') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.weight')) }}:</strong> {{ $requestedProcedure->service_request->weight
                                            . ' ' . trans('labels.kilograms') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ trans('labels.id') }}:</strong> {{ $requestedProcedure->patient->patient_ID }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.birth-date')) }}:</strong> {{ App::getLocale() == 'es' ?
                                          date('d-m-Y', strtotime($requestedProcedure->patient->birth_date)) : date('Y-m-d', strtotime($requestedProcedure->patient->birth_date))
                                        }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.history-id')) }}:</strong> {{ $requestedProcedure->patient->id }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.address')) }}:</strong> {{ $requestedProcedure->patient->address
                                        }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.responsable')) }}:</strong> {{ $requestedProcedure->service_request->parent
                                        or trans('labels.na') }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.admission-id')) }}:</strong> {{ $requestedProcedure->service_request->id
                                        }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="check-awesome">
                                            <div class="ckeckbox-left">
                                                <input type="checkbox" name="patient[contrast_allergy]" id="contrast_allergy" class="form-control" value="1" {{ $requestedProcedure->patient->contrast_allergy == 1 ? 'checked' : '' }}>
                                                <label for="contrast_allergy">
                                                    <div class="check-container">
                                                        <span class="check"></span>
                                                    </div>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                                            <div class="ckeckbox-right">
                                                <span class="message-checkbox">
                                                {{ ucfirst(trans('labels.contrast-allergy')) }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div>
                                            <label for="medical_alerts">{{ trans('labels.medical-alerts') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div>
                                            <textarea class="form-control" name="patient[medical_alerts]" id="medical_alerts"
                                                      aria-invalid="false" maxlength="250"
                                                      rows="5">{{ $requestedProcedure->patient->medical_alerts }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div>
                                            <label for="allergies">{{ trans('labels.allergies') }}</label>
                                        </div>
                                        <div>
                                            <textarea class="form-control" name="patient[allergies]" id="allergies"
                                                      aria-invalid="false" maxlength="250"
                                                      rows="5" maxle>{{ $requestedProcedure->patient->allergies }}</textarea>
                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-file" aria-hidden="true"></i> {{ ucfirst(trans('titles.procedure')) }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="patient-info">
                                        @if($requestedProcedure->urgent == 1)
                                            <p class="alert alert-danger text-center">{{ strtoupper(trans('labels.urgent')) }}</p>
                                        @endif
                                        <p><strong>{{ $requestedProcedure->procedure->description }}</strong></p>
                                        <p>
                                            <strong>{{ trans('labels.service-request-id') . ': ' }}</strong>{{ $requestedProcedure->id }}
                                        </p>
                                        <p>
                                            <strong>{{ trans('labels.source') . ': ' }}</strong>{{ ucfirst($requestedProcedure->source->description) }}
                                        </p>
                                        <p>
                                            <strong>{{ ucfirst(trans('labels.patient-type')) . ': ' }}</strong>{{ ucfirst($requestedProcedure->service_request->patient_type->description ) }}
                                        </p>
                                        <p>
                                            <strong>{{ trans('labels.referring') . ': ' }}</strong>{{ $requestedProcedure->service_request->referring ? ucwords($requestedProcedure->service_request->referring->first_name . ' ' . $requestedProcedure->service_request->referring->last_name) : ucfirst(trans('labels.not-specified'))}}
                                        </p>
                                        <p>
                                            <strong>{{ trans('labels.admitted-by') . ': ' }}</strong> {{  ucwords($requestedProcedure->user->first_name . ' ' . $requestedProcedure->user->last_name) }}
                                        </p>
                                        <p>
                                            <strong>{{ trans('labels.admission-date') . ': ' }}</strong> {{  date('d-m-Y h:i:s A',strtotime($requestedProcedure->service_request->issue_date)) }}
                                        </p>
                                        <p>
                                             <strong>{{ trans('labels.contrast-study') . ': ' }}</strong>
                                                                @if($requestedProcedure->procedure_contrast_study == 1)
                                                                    {{ trans('labels.yes')}}
                                                                @else
                                                                    {{ trans('labels.not')}}
                                                                @endif
                                        </p>
                                        <p>
                                            <strong>{{ trans('labels.patient-abdominal-circumference') . ': ' }}</strong> {{ $requestedProcedure->abdominal_circumference or trans('labels.na') }}
                                        </p>
                                        <div>
                                            <label for="medical_alerts">{{ trans('labels.observations') }}</label>
                                        </div>
                                        <div>
                                            <textarea readonly class="form-control" name="requestedProcedure[comments]" id="comments"
                                                      aria-invalid="false"
                                                      rows="5">{{ $requestedProcedure->comments }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('labels.documents')) }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="k-widget k-upload k-header">
                                <ul class="k-upload-files k-reset">
                                    @foreach($requestedProcedure->patient->patientDocuments as $key => $patientDocument)
                                        <a href="{{ route('patients.document', ['document' => $patientDocument->id ]) }}" target="_blank">
                                            <li class="k-file" id="k-file-{{ $key + 100 }}" attr-id="{{ $patientDocument->id }}">
                                                <span class="k-progress" style="width: 100%;"></span>
                                                <span class="k-file-extension-wrapper">
                                                    <span class="k-file-extension">{{ $patientDocument->type }}</span>
                                                    <span class="k-file-state"></span>
                                                </span>
                                                <span class="k-file-name-size-wrapper">
                                                    <span class="k-file-name" filename="{{ $patientDocument->filename }}">{{ $patientDocument->name }}</span>
                                                    <span class="k-file-size">95.56 KB</span>
                                                </span>
                                            </li>
                                        </a> 
                                    @endforeach 
                                    @foreach($requestedProcedure->service_request->request_documents as $key => $requestDocument)
                                        <a href="{{ route('reception.document', ['document' => $requestDocument->id ]) }}" target="_blank">
                                            <li class="k-file" id="k-file-{{ $key + 100 }}">
                                                <span class="k-progress" style="width: 100%;"></span>
                                                <span class="k-file-extension-wrapper">
                                                    <span class="k-file-extension">{{ $requestDocument->type }}</span>
                                                    <span class="k-file-state"></span>
                                                </span>
                                                <span class="k-file-name-size-wrapper">
                                                    <span class="k-file-name" title="{{ $requestDocument->name }}">{{ $requestDocument->name }}</span>
                                                    <span class="k-file-size">95.56 KB</span>
                                                </span>
                                            </li>
                                        </a>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <div class="panel sombra x6 x6-less">
                    <div class="panel-heading">

                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="active"><a href="#order-info" data-toggle="tab"><h1 class="tab-title">{{ trans('labels.order-info') }}</h1></a></li>
                                    <li><a data-toggle="tab" href="#patient-history"><h1 class="tab-title">{{ trans('labels.patient-history') }}</h1></a></li>
                                    {{--<li role="presentation">--}}
                                    {{--<a href="#patient-historic" aria-controls="profile" role="tab"--}}
                                    {{--data-toggle="tab">--}}
                                    {{--{{ trans('labels.patient-historic') }}--}}
                                    {{--</a>--}}
                                    {{--</li>--}}
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active fade in" id="order-info">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                                <div id="check-awesome">
                                                    <div class="ckeckbox-left">
                                                        <input type="checkbox" name="requestedProcedure[urgent]" id="urgent" class="form-control" value="1" {{ $requestedProcedure->urgent ? 'checked' : '' }}>
                                                        <label for="urgent">
                                                            <div class="check-container">
                                                                <span class="check"></span>
                                                            </div>
                                                            <span class="box"></span>
                                                        </label>
                                                    </div>
                                                    <div class="ckeckbox-right">
                                                        <span class="message-checkbox">
                                                        {{ ucfirst(trans('labels.urgent')) }}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                                <div id="check-awesome">
                                                    <div class="ckeckbox-left">
                                                        <input type="checkbox" name="requestedProcedure[print_label]" id="print_label" class="form-control" value="1" {{ $requestedProcedure->print_label ? 'checked' : '' }}>
                                                        <label for="print_label">
                                                            <div class="check-container">
                                                                <span class="check"></span>
                                                            </div>
                                                            <span class="box"></span>
                                                        </label>
                                                    </div>
                                                    <div class="ckeckbox-right">
                                                        <span class="message-checkbox">
                                                        {{ ucfirst(trans('labels.print-label')) }}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div id="check-awesome">
                                                    <div class="ckeckbox-left">
                                                        <input type="checkbox" name="requestedProcedure[force_images]" id="force_images" class="form-control" value="1" {{ $requestedProcedure->force_images ? 'checked' : '' }}>
                                                        <label for="force_images">
                                                            <div class="check-container">
                                                                <span class="check"></span>
                                                            </div>
                                                            <span class="box"></span>
                                                        </label>
                                                    </div>
                                                    <div class="ckeckbox-right">
                                                        <span class="message-checkbox">
                                                        {{ ucfirst(trans('labels.force-images')) }}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div>
                                                    <label for="culmination_date">{{ trans('labels.order-culmination') }}</label>
                                                </div>
                                                <div>
                                                    <input type="dateTime" data-datetype='dateTime' name="requestedProcedure[culmination_date]" id="culmination_date"

                                                           @if($requestedProcedure->culmination_date==null or $requestedProcedure->culmination_date=='0000-00-00 00:00:00' )

                                                           @else
                                                           value="{{ $requestedProcedure->culmination_date }}"
                                                           @endif


                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div>
                                                    <label for="study_number">{{ trans('labels.study-number') }}</label>
                                                </div>
                                                <div>
                                                    <input type="text" name="requestedProcedure[study_number]" id="study_number"
                                                           class="form-control" value="{{ $requestedProcedure->study_number }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div>
                                                    <label for="number_of_plates">{{ trans('labels.number-of-plates') }}</label>
                                                </div>
                                                <div>
                                                    <input type="number" name="requestedProcedure[number_of_plates]"
                                                           id="number_of_plates" class="form-control" value="{{ $requestedProcedure->number_of_plates }}">
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div>
                                                    <label for="plates_size_id">{{ trans('labels.plates-sizes') }}</label>
                                                </div>
                                                <select class="form-control" name="requestedProcedure[plates_size_id]" id="plates_size_id">
                                                    <option value="" disabled
                                                            selected>{{ ucfirst(trans('labels.select')) }}</option>
                                                    @foreach($requestedProcedure->plates_sizes as $plate_size)
                                                        <option value="{{ $plate_size->id }}" {{ $requestedProcedure->plates_size_id == $plate_size->id ? 'selected' : '' }}>{{ $plate_size->size }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div>
                                                    <label for="nurse">{{ trans('labels.nurse') }}</label>
                                                </div>
                                                <select class="form-control" name="requestedProcedure[nurse]" id="nurse">
                                                    <option value="" disabled
                                                            selected>{{ ucfirst(trans('labels.select')) }}</option>
                                                    {{--@foreach($requestedProcedure->plates_sizes as $plate_size)--}}
                                                    {{--<option value="{{ $plate_size->id }}">{{ $plate_size->size }}</option>--}}
                                                    {{--@endforeach--}}
                                                </select>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div>
                                                    <label for="equipment">{{ ucfirst(trans('labels.equipment')) }}</label>
                                                </div>

                                                @include('includes.select', [
                                                    'idname' => 'requestedProcedure[equipment_id]',
                                                    'value'  => $requestedProcedure->equipment_id,
                                                    'data'   => $equipments,
                                                    'keys'   => ['id', 'name']
                                                ])
                                            </div>
                                        </div>
                                    </div>
                                    <div id="patient-history" class="tab-pane fade">

                                    </div>
                                </div>
                               </div>
                            {{--<div role="tabpanel" class="tab-pane fade" id="patient-historic">--}}
                            {{----}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>

        <script>
            $(document).ready(function datePicker () {
                loadDateTimePicker();
                transformDate();

                var dateNow = new Date();
            });
            $.get('/servicerequests/history/' + {{ $requestedProcedure->patient->id }} , function (response) {
             $('#patient-history').append(response);
            });


        </script>

        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\TechnicianRequest', '#TechnicianEditForm'); !!}

    </div>

@endsection
