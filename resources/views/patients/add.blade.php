@extends('layouts.app')

@section('title',ucfirst(trans('titles.add')).' '.ucfirst(trans('titles.patient')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.patient')),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('labels.save')),
                                    'form_id' => '#PatientAddForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('patients')
                                ])

    <div class="container-fluid patients add">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 reception-group">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
            </div>
        </div>

        <form id="PatientAddForm" method="post" action="{{ route('patients.add') }}" files="true"
              enctype="multipart/form-data">

            {!! csrf_field() !!}

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="row patients add">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 reception-group">
                    <div class="panel sombra x8 elastic" id="main-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('labels.main-info') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center reception-group">
                                    <div class="patient-img">
                                        <img src="{{ "/images/patients/default_avatar.jpeg" }}" alt="Patient-Image"
                                             class="img-responsive" id='avatar'/>
                                    </div>
                                    <div>
                                        <a type="button" name="edit-photo" id="edit-photo" class="btn btn-form"
                                           title="Edit avatar">
                                            <i class="fa fa-upload" aria-hidden="true"></i>
                                            {{ trans('labels.upload-photo') }}
                                        </a>
                                        <input type="hidden" name="photo" id="photo"
                                               title="Patient avatar" value="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 reception-group">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                            @include('includes.general-checkbox', [
                                                'id'        =>'active-chk',
                                                'name'      =>'active',
                                                'label'     =>'labels.is-active',
                                                'condition' => 0
                                            ])
                                            @if ($errors->has('active'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('active') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                            <div>
                                                <label for='patient_ID'>{{ ucfirst(trans('labels.patient-id')) }} *</label>
                                            </div>
                                            <div>
                                                <input type="text" class="form-control" name="patient_ID"
                                                       id="patient_ID"
                                                       value="{{ old('patient_ID') }}">
                                                @if ($errors->has('patient_ID'))
                                                    <span class="text-danger">
                                                        <strong>{{ $errors->first('patient_ID') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                            <div>
                                                <label for="sex_id">{{ ucfirst(trans('labels.gender')) }} *</label>
                                            </div>
                                            <div>
                                                <select class="form-control" name="sex_id" id="sex_id">
                                                    <option value="" disabled
                                                            selected>{{ ucfirst(trans('labels.select')) }}</option>
                                                    @foreach($sexes as $sex)
                                                        <option value="{{ $sex->id }}">{{ trans('labels.'.$sex->name) }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('sex_id'))
                                                    <span class="text-danger">
                                                        <strong>{{ $errors->first('sex_id') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='first_name'>{{ ucfirst(trans('labels.name')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="text" class="form-control" name="first_name"
                                               id="first_name"
                                               value="{{ old('first_name') }}">
                                        @if ($errors->has('first_name'))
                                            <span class="text-danger">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='last_name'>{{ ucfirst(trans('labels.last-name')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="text" class="form-control" name="last_name" id="last_name"
                                               value="{{ old('last_name') }}">
                                        @if ($errors->has('last_name'))
                                            <span class="text-danger">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='birth_date'>{{ ucfirst(trans('labels.birth-date')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="date" name="birth_date" id="birth_date" class="form-control"
                                               value="{{ old('birth_date') }}">
                                        @if ($errors->has('birth_date'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('birth_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for="responsable_id">{{ ucfirst(trans('labels.responsable')) }}</label>
                                    </div>
                                    <div>
                                        <select class="form-control" name="responsable_id" id="responsable_id">
                                            <option value="" disabled
                                                    selected>{{ ucfirst(trans('labels.select')) }}</option>
                                            @foreach($responsables as $responsable)
                                                <option value="{{ $responsable->id }}">{{ $responsable->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('responsable_id'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('responsable_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='patientDocuments'>{{ ucfirst(trans('labels.documents')) }}</label>
                                    </div>
                                    <div>
                                        <input name="patientDocuments[]" id="patientDocuments" type="file" accept=".xlsx,.xls,.jpg,.jpeg,.png,.doc,.docx,.pdf"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                <div>
                                    <a type="button" name="scan-file" id="scan-file" class="btn btn-form"
                                       title="Scan File">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                        {{ ucfirst(trans('labels.scan-file')) }}
                                    </a>
                                </div>

                                <div class="col-xs-12 content-scan-file-take">

                                </div>


                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 reception-group">
                    <div class="panel sombra x8 elastic" id="contact-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('labels.contact-info') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='country_of_residence'>{{ ucfirst(trans('labels.country-residence')) }} *</label>
                                    </div>
                                    <div>
                                        @include('includes.select',[
                                            'idname' => 'country_id',
                                            'data' => $countries,
                                            'keys' => [
                                                        'id',
                                                        'name'
                                                        ],
                                            'value' => 862
                                        ])  
                                        @if ($errors->has('country_id'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('country_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='citizenship'>{{ ucfirst(trans('labels.citizenship')) }} *</label>
                                    </div>
                                    <div>
                                        @include('includes.select',[
                                            'idname' => 'citizenship',
                                            'data' => $countries,
                                            'keys' => [
                                                        'id',
                                                        'citizenship'
                                                        ],
                                            'value' => 862
                                        ])
                                        @if ($errors->has('citizenship'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('citizenship') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='email'>{{ ucfirst(trans('labels.email')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="email" class="form-control" name="email" id="email"
                                               value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='telephone_number'>{{ ucfirst(trans('labels.telephone-number')) }} </label>
                                    </div>
                                    <div>
                                        <input type="tel" class="form-control" name="telephone_number"
                                               id="telephone_number"
                                               value="{{ old('telephone_number') }}">
                                        @if ($errors->has('telephone_number'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('telephone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='telephone_number_2'>{{ ucfirst(trans('labels.telephone-number-2')) }}
                                        </label>
                                    </div>
                                    <div>
                                        <input type="tel" class="form-control" id="telephone_number_2"
                                               name="telephone_number_2"
                                               value="{{ old('telephone_number_2') }}">
                                        @if ($errors->has('telephone_number_2'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('telephone_number_2') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='cellphone_number'>{{ ucfirst(trans('labels.cellphone-number')) }} </label>
                                    </div>
                                    <div>
                                        <input type="tel" class="form-control" name="cellphone_number"
                                               id="cellphone_number"
                                               value="{{ old('cellphone_number') }}">
                                        @if ($errors->has('cellphone_number'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('cellphone_number') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='cellphone_number_2'>{{ ucfirst(trans('labels.cellphone-number-2')) }}
                                        </label>
                                    </div>
                                    <div>
                                        <input type="tel" class="form-control" name="cellphone_number_2"
                                               id="cellphone_number_2"
                                               value="{{ old('cellphone_number_2') }}">
                                        @if ($errors->has('cellphone_number_2'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('cellphone_number_2') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='address'>{{ ucfirst(trans('labels.address')) }} </label>
                                    </div>
                                    <div>
                                        <textarea class="form-control" name="address"
                                                  id="address">{{ old('address') }}</textarea>
                                        @if ($errors->has('address'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='occupation'>{{ ucfirst(trans('labels.occupation')) }}</label>
                                    </div>
                                    <div>
                                        <input type="text" class="form-control" name="occupation" id="occupation"
                                               value="{{ old('occupation') }}">
                                        @if ($errors->has('occupation'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('occupation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 reception-group">
                    <div class="panel sombra x8 elastic" id="dates-notification-panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('labels.medical-info') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='medical_alerts'>{{ ucfirst(trans('labels.medical-alerts')) }}</label>
                                    </div>
                                    <div>
                                        <textarea class="form-control" name="medical_alerts" rows="5"
                                                  id="medical_alerts">{{ old('medical_alerts') }}</textarea>
                                        @if ($errors->has('medical_alerts'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('medical_alerts') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='allergies'>{{ ucfirst(trans('labels.allergies')) }}</label>
                                    </div>
                                    <div>
                                        <textarea class="form-control" name="allergies" rows="5"
                                                  id="allergies">{{ old('allergies') }}</textarea>
                                        @if ($errors->has('allergies'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('allergies') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='special_needs'>{{ ucfirst(trans('labels.special-needs')) }}</label>
                                    </div>
                                    <div>
                                        <textarea class="form-control" name="special_needs" rows="5"
                                                  id="special_needs">{{ old('special_needs') }}</textarea>
                                        @if ($errors->has('text-danger'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('special_needs') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                                    <div>
                                        <label for='comments'>{{ ucfirst(trans('labels.comments')) }}</label>
                                    </div>
                                    <div>
                                        <textarea class="form-control" name="comments" rows="5"
                                                  id="comments">{{ old('comments') }}</textarea>
                                        @if ($errors->has('comments'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('comments') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\PatientAddRequest', '#PatientAddForm') !!}
    </div>

    <div class="modal fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="img-modal-Label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="img-modal-Label">{{ ucwords(trans('labels.img-edit')) }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 reception-group">
                            <div class="croppic-init" id="patient-img-container">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close-img-edit"
                            data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
                    <button type="button" class="btn btn-form" id="set-photo">{{ ucfirst(trans('labels.save')) }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    @include('includes.modal-scan-file');

@endsection
{{-- 
@push('styles')
<link rel="stylesheet" href="/scanfile/bower/cropper/dist/cropper.css">

<style media="screen">
/** VIDEO CAPTURE CSS */


</style>
@endpush

@push('scripts')
<script src="/scanfile/js/webcam.js" ></script>
<script src="/scanfile/lib/modernizr/modernizr-custom.js"></script>
<script src="/scanfile/bower/cropper/dist/cropper.js"></script>

@endpush --}}
