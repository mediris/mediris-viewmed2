<!DOCTYPE html>
<html>

    <head>
        <title>{{ trans('titles.patient-record') }}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        @include('partials.links')
    </head>

    <body id="app-layout" data-page="panels">

        <div class="container-fluid patients record">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="row flex">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h3><img src="{{ url('/images/logo_idaca.svg') }}" alt=""></h3>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 flex-end">
                                    <p class="institution_id text-right">Rif. {{ Session::get('institution')->institution_id }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <h3 class="record-title">FICHA GENERADA {{ date("d/m/Y h:i") }}</h3>
                                </div>
                            </div>

                            <div class="row flex">
                                <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                                    <div class="patient-img">
                                        <img src="{{ $patient->photo != '' ? $patient->photo : '/images/patients/default_avatar.jpeg' }}" alt="Patient-Image" class="img-responsive" id='avatar'/>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9">
                                    <div class="record-heading">
                                        <h3>{{ strtoupper($patient->first_name) }} {{ strtoupper($patient->last_name) }}</h3>
                                    </div>
                                    <div id="record-content">
                                        <div class="row">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                                <label for="patient_id">{{ ucfirst(trans('labels.patient-id')) }}</label>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                                                <p>{{ $patient->patient_ID }}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                                <label for="birth_date">{{ ucfirst(trans('labels.birth-date')) }}</label>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                                                <p>{{ $patient->birth_date }}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                                <label for="address">{{ ucfirst(trans('labels.address')) }}</label>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                                                <p>{{ $patient->address }}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                                <label for="gender">{{ ucfirst(trans('labels.gender')) }}</label>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                                                <p>{{ trans('labels.'.$patient->sex->name) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        @include('partials.jscripts')
    </body>

</html>