@extends('layouts.app')

@section('title',ucfirst(trans('titles.patient-record')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.patient-record')),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('labels.print')),
                                    'elem_id' => 'print-label',
                                    'form_id' => '#PatientAddForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('patients')
                                ])

<div class="container-fluid patients show">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
            <div class="panel sombra">
                <div class="panel-heading">
                    <div class="row flex">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 logo-institution">
                            <h3><img src="{{ Session::get('institution')->logo }}" alt=""></h3>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 flex-end info-institution">
                            <p class="institution_id text-left rif">Rif. {{ Session::get('institution')->institution_id }}</p>
                            <p class="institution_id text-left name">{{ Session::get('institution')->name }}</p>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h3 class="record-title">FICHA GENERADA {{ App::getLocale() == 'es' ? date("d/m/Y h:i") : date("m/d/Y h:i") }}</h3>
                        </div>
                    </div>

                    <div class="row flex">
                        <div class="col-xs-4 col-lg-3">
                            <div class="patient-img record-img">
                                @if( isset($patient->id) && !empty($patient->photo))
                                    <img src="{{ asset( 'storage/'. $patient->photo ) }}"
                                    alt="Patient-Image" class="img-responsive" id='avatar'>
                                @else
                                    <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                    class="img-responsive" id='avatar'>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-8 col-lg-9">
                            <div class="record-heading">
                                <h3>{{ strtoupper($patient->first_name) }} {{ strtoupper($patient->last_name) }}</h3>
                            </div>

                            <div id="record-content">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                        <label >{{ ucfirst(trans('labels.patient-id')) }}</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-8 text-left">
                                        <h5>{{ $patient->patient_ID }}</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                        <label for="birth_date">{{ ucfirst(trans('labels.birth-date')) }}</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-8 text-left">
                                        <h5>{{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($patient->birth_date)) : date('Y-m-d', strtotime($patient->birth_date)) }}</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                        <label for="address">{{ ucfirst(trans('labels.address')) }}</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-8 text-left">
                                        <h5>{{ $patient->address }}</h5>
                                    </div>
                                </div>
                                @if ( isset($patient->sex) )
                                <div class="row">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                        <label for="gender">{{ ucfirst(trans('labels.gender')) }}</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-8 text-left">
                                        <h5>{{ trans('labels.'.$patient->sex->name) }}</h5>
                                    </div>
                                </div>
                                @endif
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection
