@extends('layouts.app')

@section('title',ucfirst(trans('titles.show')).' '.trans('titles.order').' - '.ucfirst(trans('titles.radiologist')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.radiologist')).' - '.ucfirst(trans('titles.orders-to-dictate')).' - '.ucfirst(trans('titles.detail')),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('labels.finish-order')),
                                    'form_id' => '#RadiologistEditForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('radiologist'),
                                    'second_elem_name' => Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'approve') ? ucfirst(trans('labels.finish-approve-order')) : '',
                                    'second_elem_id' => 'submit-approve'
                                ])

    <div class="container-fluid radiologist edit">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
          	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-file" aria-hidden="true"></i> {{ trans('titles.patient-info') }}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="patient-img ">
                                        @if( isset($requestedProcedure->patient->id) && !empty($requestedProcedure->patient->photo))
                                          <img src="{{ asset( 'storage/'. $requestedProcedure->patient->photo ) }}"
                                          alt="Patient-Image" class="img-responsive" id='avatar'>
                                        @else
                                          <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                          class="img-responsive" id='avatar'>
                                        @endif
                                     </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="patient-info">
                                                <p><strong>{{ ucfirst(trans('labels.name')) }}:</strong> {{ $requestedProcedure->patient->first_name
                                              . ' ' . $requestedProcedure->patient->last_name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="patient-info">
                                                <p><strong>{{ ucfirst(trans('labels.patient-id')) }}:</strong> {{ $requestedProcedure->patient->patient_ID }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.age')) }}:</strong> {{ $requestedProcedure->patient->age['years']
                                            }} {{ trans('labels.years') }}, {{ $requestedProcedure->patient->age['months'] }}
                                            {{ $requestedProcedure->patient->age['months'] == 1 ? trans('labels.month') : trans('labels.months')
                                            }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.gender')) }}:</strong> {{ $requestedProcedure->patient->sex_id
                                          == 1 ? trans('labels.male') : trans('labels.female') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.height')) }}:</strong> {{ $requestedProcedure->service_request->height
                                            . ' ' . trans('labels.meters') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.weight')) }}:</strong> {{ $requestedProcedure->service_request->weight
                                            . ' ' . trans('labels.kilograms') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.birth-date')) }}:</strong> {{ App::getLocale() == 'es' ?
                                      date('d-m-Y', strtotime($requestedProcedure->patient->birth_date)) : date('Y-m-d', strtotime($requestedProcedure->patient->birth_date))
                                    }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.history-id')) }}:</strong> {{ $requestedProcedure->patient->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.address')) }}:</strong> {{ $requestedProcedure->patient->address
                                    }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.responsable')) }}:</strong> {{ $requestedProcedure->service_request->parent
                                    or trans('labels.na') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.admission-id')) }}:</strong> {{ $requestedProcedure->service_request->id
                                    }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.contrast-allergy')) }}:</strong> {{ $requestedProcedure->patient->contrast_allergy
                                    ? ucfirst(trans('labels.yes')) : 'No' }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.medical-alerts')) }}:</strong> {{ $requestedProcedure->patient->medical_alerts
                                    }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ ucfirst(trans('labels.allergies')) }}: </strong>
                                        {{ $requestedProcedure->patient->allergies }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-file" aria-hidden="true"></i> {{ ucfirst(trans('titles.procedure')) }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ $requestedProcedure->procedure->description }}</strong> </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.request-num') }}:</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.order-num') }}:</strong> {{ $requestedProcedure->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.patient-type')) }}:</strong> {{ $requestedProcedure->service_request->patient_type->description }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.source')) }}:</strong> {{ $requestedProcedure->service_request->source->description }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.referring') . ': ' }}</strong>{{ $requestedProcedure->service_request->referring ? ucwords($requestedProcedure->service_request->referring->first_name . ' ' . $requestedProcedure->service_request->referring->last_name) : ucfirst(trans('labels.not-specified'))}}
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.admitted-by') }}:</strong> {{ $requestedProcedure->service_request->user->first_name }} {{ $requestedProcedure->service_request->user->last_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.attended-by') }}:</strong> {{ $requestedProcedure->technician_user_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                    <strong>{{ trans('labels.contrast-study') . ': ' }}</strong>
                                    @if($requestedProcedure->procedure_contrast_study == 1)
                                        {{ trans('labels.yes')}}
                                    @else
                                        {{ trans('labels.not')}}
                                    @endif    
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.patient-abdominal-circumference') . ': ' }}</strong> {{ $requestedProcedure->abdominal_circumference or trans('labels.na') }}
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.observations') }}:</strong> {{ $requestedProcedure->comments }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('labels.documents')) }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="k-widget k-upload k-header">
                                <ul class="k-upload-files k-reset">
                                    @foreach($requestedProcedure->patient->patientDocuments as $key => $patientDocument)
                                        <a href="{{ route('patients.document', ['document' => $patientDocument->id ]) }}" target="_blank">
                                            <li class="k-file" id="k-file-{{ $key + 100 }}" attr-id="{{ $patientDocument->id }}">
                                                <span class="k-progress" style="width: 100%;"></span>
                                                <span class="k-file-extension-wrapper">
                                                    <span class="k-file-extension">{{ $patientDocument->type }}</span>
                                                    <span class="k-file-state"></span>
                                                </span>
                                                <span class="k-file-name-size-wrapper">
                                                    <span class="k-file-name" filename="{{ $patientDocument->filename }}">{{ $patientDocument->name }}</span>
                                                    <span class="k-file-size">95.56 KB</span>
                                                </span>
                                            </li>
                                        </a> 
                                    @endforeach 
                                    @foreach($requestedProcedure->service_request->request_documents as $key => $requestDocument)
                                        <a href="{{ route('reception.document', ['document' => $requestDocument->id ]) }}" target="_blank">
                                            <li class="k-file" id="k-file-{{ $key + 100 }}">
                                                <span class="k-progress" style="width: 100%;"></span>
                                                <span class="k-file-extension-wrapper">
                                                    <span class="k-file-extension">{{ $requestDocument->type }}</span>
                                                    <span class="k-file-state"></span>
                                                </span>
                                                <span class="k-file-name-size-wrapper">
                                                    <span class="k-file-name" title="{{ $requestDocument->name }}">{{ $requestDocument->name }}</span>
                                                    <span class="k-file-size">95.56 KB</span>
                                                </span>
                                            </li>
                                        </a>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <div class="panel sombra">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('labels.dictation') }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <p><strong>Dictado:</strong></p>
                                <audio id="audio" controls src="{{ old('audio_data') }}">
                                    Your browser does not support the audio element.
                                </audio>
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-form" id="record" disabled>{{trans('labels.record')}}</button>
                            <button class="btn btn-form" id="stop" disabled>{{trans('labels.stop')}}</button>
                            <button class="btn btn-form" id="pause" disabled>Pause</button>
                            <button class="btn btn-form" id="resumen" disabled>Resumen</button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <div class="panel sombra">
                    <div class="panel-heading">
                        
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a data-toggle="tab" href="#order-info">
                                            <h1 class="tab-title">{{ trans('labels.order-info') }}</h1>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#patient-history">
                                            <h1 class="tab-title">{{ trans('labels.patient-history') }}</h1>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="order-info" class="tab-pane fade in active">
                                        <form action="{{ route('radiologist.edit', [$requestedProcedure->id]) }}" method='post' id="RadiologistEditForm">
                                            <input id="approve" type="hidden" name="approve" value="0">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <!-- <a href="{{ route('radiologist.pac', ['id'=>$requestedProcedure->id]) }}" class="btn btn-form btn-pac"> -->
                                                            <a href="https://192.168.26.109/viewer?m_i=0&l=1&def_session=demo_session#viewer2/accession/{{$requestedProcedure->id}}" class="btn btn-form">
                                                                <span><i class="fa fa-picture-o" aria-hidden="true"></i> {{ trans('labels.open-images') }}</span>
                                                            </a>
                                                        </div>
                                                        <!--BI-Bards-->
                                                        @if($requestedProcedure->procedure->mammography)
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div>
                                                                    <label for="bi_rad_id">{{ trans('labels.category-bi-rad') }} *</label>
                                                                </div>
                                                                <div>
                                                                    <select name="bi_rad_id" id="bi_rad_id">
                                                                        <option value="" selected disabled>{{ trans('labels.none') }}</option>
                                                                            @foreach($biRads as $biRad)
                                                                                <option value="{{ $biRad->id }}">{{ $biRad->description }}</option>
                                                                            @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <!---->
                                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div>
                                                                <label for="category-panel"></label>
                                                            </div>
                                                            <div class="panel panel-input-like" id="category-panel">
                                                                <div class="panel-heading" role="tab">
                                                                    <a role="button" data-toggle="collapse" href="#search-teaching-file" aria-expanded="false" aria-controls="#search-teaching-file"
                                                                    class="collapsed">
                                                                        <h4 class="panel-title">
                                                                            {{ ucfirst(trans('labels.teaching-file')) }}
                                                                        </h4>
                                                                    </a>
                                                                </div>
                                                                <div id="search-teaching-file" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                                <div>
                                                                                    <label for="category">{{ ucfirst(trans('labels.categories')) }}</label>
                                                                                </div>
                                                                                <select name="category_id" id="category_id">
                                                                                    @foreach($categories as $category)
                                                                                        <option value="{{ $category->id }}" {{ $category->id == $requestedProcedure->category_id ? 'selected' : '' }}>{{ $category->description }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                                <div>
                                                                                    <label for="subcategory">{{ ucfirst(trans('labels.subcategories')) }}</label>
                                                                                </div>
                                                                                <select name="sub_category_id" id="sub_category_id" class="form-control">
                                                                                    @foreach($subcategories as $subcategory)
                                                                                        <option value="{{ $subcategory->id }}" {{ $subcategory->id == $requestedProcedure->sub_category_id ? 'selected' : '' }}>{{ $subcategory->description }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                                <div>
                                                                                    <label for="teaching_file_text">{{ ucfirst(trans('labels.observations')) }}</label>
                                                                                </div>
                                                                                <div>
                                                                                    <textarea class="form-control" name="teaching_file_text" id="teaching_file_text">{{ $requestedProcedure->teaching_file_text }}</textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module-textarea-plantilla">
                                                    <div class="row m-t-20">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="btn-icons">
                                                                <label for="template_id">{{ ucfirst(trans('labels.template')) }}:</label>
                                                                <select name="template_id" id="template_id">
                                                                    <option value="" selected disabled>{{ ucfirst(trans('labels.select')) }}</option>
                                                                    @foreach($requestedProcedure->procedure->templates as $template)
                                                                        <option value="{{ $template->id }}">{{ $template->description }}</option>
                                                                    @endforeach
                                                                    <option value="create-temprate"> Crear Plantilla</option>
                                                                </select>
                                                            @foreach($requestedProcedure->procedure->templates as $template)
                                                                <input type="hidden" id="template_{{ $template->id }}" value="{{ $template->template }}">
                                                            @endforeach
                                                            <a href="{{ route('radiologist.edit.draft', [$requestedProcedure->id]) }}" id="btn-save-draft" class="btn btn-form btn-icon" data-toggle="tooltip" title="{{ trans('labels.save-draft') }}">
                                                                <i class="fa fa-floppy-o"></i>
                                                            </a>
                                                            <a href="{{ route('radiologist.edit.draft', [$requestedProcedure->id]) }}" target="_blank" class="btn btn-form btn-icon" data-toggle="tooltip" title="{{ trans('labels.preview-draft') }}">
                                                                <i class="fa fa-search"></i>
                                                            </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row m-t-20">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 big-textarea-plantilla">
                                                            <textarea class="small-editor" name="text" id="template">{{ $requestedProcedure->text }}</textarea>
                                                            @if ($errors->has('text'))
                                                                <span class="help-block error-help-block">{{ $errors->first('text') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="patient-history" class="tab-pane fade">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $.get('/servicerequests/history/' + {{ $requestedProcedure->patient->id }} , function (response) {
            $('#patient-history').append(response);
        });

        if($('#teaching_file').is(':checked'))
        {
            $('#docent-file-content').show();
        }
    </script>

    @endsection
