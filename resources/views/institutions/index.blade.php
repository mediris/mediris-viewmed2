@extends('layouts.app')

@section('title', ucfirst(trans('titles.institutions')))


@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.institutions')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('institutions.add'),
									'fancybox' => '',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
					<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.institutions')) }}</h1>
				</div>
				
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('integer');
				$NameField = new \Kendo\Data\DataSourceSchemaModelField('name');
				$NameField->type('string');
				$InstitutionIdField = new \Kendo\Data\DataSourceSchemaModelField('institution_id');
				$InstitutionIdField->type('string');
				$TelField = new \Kendo\Data\DataSourceSchemaModelField('telephone_number');
				$TelField->type('string');
				$EmailField = new \Kendo\Data\DataSourceSchemaModelField('email');
				$EmailField->type('string');
				$UrlField = new \Kendo\Data\DataSourceSchemaModelField('url');
				$UrlField->type('string');
				$AdministrativeIdField = new \Kendo\Data\DataSourceSchemaModelField('administrative_id');
				$AdministrativeIdField->type('string');
				$GroupField = new \Kendo\Data\DataSourceSchemaModelField('group_name');
				$GroupField->type('string');

				$kendo->addFields(array(
					$IdField,
					$InstitutionIdField,
					$TelField,
					$EmailField,
					$UrlField,
					$AdministrativeIdField,
					$NameField,
					$GroupField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $institutions = $institutions->toArray();
				
				foreach ($institutions as $key => $value) {
					$institutions[$key]['telephone_number'] = '<a href="tel:' . $institutions[$key]['telephone_number'] . '">' . $institutions[$key]['telephone_number'] . '</a>';
					$institutions[$key]['email'] = '<a href="mailto:' . $institutions[$key]['email'] . '">' . $institutions[$key]['email'] . '</a>';
					$institutions[$key]['url'] = '<a href="' . $institutions[$key]['url'] . '">' . $institutions[$key]['url'] . '</a>';
					$title = $institutions[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $institutions[$key]['active'] == 1 ? "" : "active";
					//$institutions[$key]['actions'] = '<a href=' . route('institutions.edit', [$institutions[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement" ><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>';
					$institutions[$key]['actions'] = '<a href=' . route('institutions.edit', [$institutions[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . '><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>';
				}

				// Set Institutions
				$kendo->setDataSource($institutions);

				// Create Grid
                $kendo->createGrid('institutions');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$Name = new \Kendo\UI\GridColumn();
				$Name->field('name')
						->title(ucfirst(trans('labels.name')));
				$InstitutionId = new \Kendo\UI\GridColumn();
				$InstitutionId->field('institution_id')
						->title(ucfirst(trans('labels.institution-id')));
				$AdministrativeId = new \Kendo\UI\GridColumn();
				$AdministrativeId->field('administrative_id')
						->title(ucfirst(trans('labels.administrative-id')));
				$Url = new \Kendo\UI\GridColumn();
				$Url->field('url')
						->encoded(false)
						->title(ucfirst(trans('labels.url')));
				$Email = new \Kendo\UI\GridColumn();
				$Email->field('email')
						->encoded(false)
						->title(ucfirst(trans('labels.email')));
				$Tel = new \Kendo\UI\GridColumn();
				$Tel->field('telephone_number')
						->encoded(false)
						->title(ucfirst(trans('labels.telephone-number')));
				$Group = new \Kendo\UI\GridColumn();
				$Group->field('group_name')
						->title(ucfirst(trans('labels.group')));
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
	                $Id,
					$Name,
					$InstitutionId,
					$AdministrativeId,
					$Url,
					$Email,
					$Tel,
					$Group,
					$Actions,
                ));
				
                // Defines & Generate
                $kendo->generate();

                // Redefines Data Bound
                $kendo->dataBound('onDataBound');

				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>
@endsection