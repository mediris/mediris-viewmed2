@extends('layouts.app')

@section('title',ucfirst(trans('titles.edit')).' '.trans('titles.institution'))

@section('content')


	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.institutions')),
									'elem_type' => 'button',
									'elem_name' => ucfirst(trans('labels.save')),
									'form_id' => '#form-1',
									'route' => '',
									'fancybox' => '',
									'routeBack' => route('institutions')
								])


	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			@if(Session::has('message'))
				<div class="{{ Session::get('class') }}">
				<p>{{ Session::get('message') }}</p>
				</div>
			@endif
			</div>
		</div>
  
  		<form id="form-1" method="post" action="{{ route("institutions.edit", [$institution]) }}" id="InstitutionEditForm">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    
			<div class="row">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
					</ul>
				</div>
				@endif
			</div>

        	<div class="row">
        		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          			<div class="panel sombra x6 elastic" id="dates-notification-panel">
						<div class="panel-heading">
							<h3 class="panel-title">{{ trans('labels.main-info') }}</h3>
						</div>
            			<div class="panel-body">
    						
							<div class="row">
      							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        							<div>
          								<label for='name'>{{ ucfirst(trans('labels.name')) }} *</label>
        							</div>
        							<div>
										<input type="text" name="name" id="name" class="input-field form-control user btn-style" value="{{ $institution->name }}"/>
										@if ($errors->has('name'))
											<span class="text-danger">
												<strong>{{ $errors->first('name') }}</strong>
											</span>
										@endif
        							</div>
      							</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='indstitution_id'>{{ ucfirst(trans('labels.institution-id')) }} *</label>
									</div>
									<div>
										<input type="text" class="form-control" name="institution_id" id="institution_id"
												value="{{ $institution->institution_id }}">
										@if ($errors->has('institution_id'))
											<span class="text-danger">
												<strong>{{ $errors->first('institution_id') }}</strong>
											</span>
										@endif
									</div>
								</div>
    						</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }} *</label>
									</div>
									<div>
										<input type="text" class="form-control" name="administrative_ID" id="administrative_ID"
												value="{{ $institution->administrative_id }}">
										@if ($errors->has('administrative_ID'))
											<span class="text-danger">
												<strong>{{ $errors->first('administrative_ID') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='base_structure'>{{ ucfirst(trans('labels.base_structure')) }} *</label>
									</div>
									<div>
										<input type="text" name="base_structure" id="base_structure" class="input-field form-control user btn-style"
										value="{{ $institution->base_structure }}" maxlength="30"/>
									@if ($errors->has('base_structure'))
										<span class="text-danger">
											<strong>{{ $errors->first('base_structure') }}</strong>
										</span>
									@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div>
										<label for='url'>{{ ucfirst(trans('labels.url')) }} *</label>
									</div>
									<div>
										<input type="text" name="url" id="url" class="input-field form-control user btn-style"
												value="{{ $institution->url }}"/>
										@if ($errors->has('url'))
											<span class="text-danger">
												<strong>{{ $errors->first('url') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>		
							<div class="row">
								<div class="col-md-12">
									<div>
										<label for='url_pacs'>{{ ucfirst(trans('labels.url_pacs')) }} *</label>
									</div>
									<div>
										<input type="text" name="url_pacs" id="url_pacs" class="input-field form-control user btn-style"
											value="{{ $institution->url_pacs }}"/>
										@if ($errors->has('url_pacs'))
											<span class="text-danger">
												<strong>{{ $errors->first('url_pacs') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div>
										<label for='url_external_api'>{{ ucfirst(trans('labels.url_external_api')) }}</label>
									</div>
									<div>
										<input type="text" name="url_external_api" id="url_external_api" class="input-field form-control user btn-style"
											value="{{ $institution->url_external_api }}"/>
										@if ($errors->has('url_external_api'))
											<span class="text-danger">
												<strong>{{ $errors->first('url_external_api') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='telephone_number'>{{ ucfirst(trans('labels.telephone-number')) }} *</label>
									</div>
									<div>
										<input type="tel" class="form-control" name="telephone_number" id="telephone_number"
												value="{{ $institution->telephone_number }}">
										@if ($errors->has('telephone_number'))
											<span class="text-danger">
												<strong>{{ $errors->first('telephone_number') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='telephone_number_2'>{{ ucfirst(trans('labels.telephone-number-2')) }}</label>
									</div>
									<div>
										<input type="tel" class="form-control" name="telephone_number_2" id="telephone_number_2"
												value="{{ $institution->telephone_number_2 }}">
										@if ($errors->has('telephone_number_2'))
											<span class="text-danger">
												<strong>{{ $errors->first('telephone_number_2') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='email'>{{ ucfirst(trans('labels.email')) }} *</label>
									</div>
									<div>
										<input type="email" class="form-control" name="email" id="email" value="{{ $institution->email }}">
										@if ($errors->has('email'))
											<span class="text-danger">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='group'>{{ ucfirst(trans('labels.group')) }} *</label>
									</div>
									<div>
										<select class="form-control" name="group_id" id="group_id">
											<option value="">{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($groups as $group)
											<option value="{{ $group->id }}" {{ $group->id == $institution->group_id ? 'selected' : '' }}>{{ $group->name }}</option>
											@endforeach
										</select>
										@if ($errors->has('group_id'))
											<span class="text-danger">
												<strong>{{ $errors->first('group_id') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div>
										<label for='address'>{{ ucfirst(trans('labels.address')) }} *</label>
									</div>
									<div>
									<textarea class="form-control" name="address" id="address" rows="5">{{ $institution->address }}</textarea>
										@if ($errors->has('address'))
											<span class="text-danger">
												<strong>{{ $errors->first('address') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
            			</div>
          			</div>
        		</div>
        		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          			<div class="panel sombra x6 elastic" id="dates-notification-panel">
            			<div class="panel-heading">
							<h3 class="panel-title">{{ trans('labels.institution-images') }}</h3>
						</div>
            			<div class="panel-body">

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='message'>{{ ucfirst(trans('labels.logo')) }}</label>
									</div>
									<div class="logo-img">
										@if( !empty($institution->logo))
										<img src="{{ $institution->logo }}" alt="{{ trans('labels.logo') }}" class="img-responsive" id='logo-image'>
										@else
										<img src="/images/institutions/default_logo.jpeg" alt="{{ trans('labels.logo') }}" class="img-responsive" id='logo-image'>
										@endif
										<a type="button" id="edit-logo" class="btn btn-form do-crop" target="logo" title="{{ trans('labels.edit-logo') }}">
											<i class="fa fa-upload" aria-hidden="true"></i>
											{{ trans('labels.edit-logo') }}
										</a>
									</div>
									<input type="hidden" name="logo" id="logo" title="Institution logo" value="">
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='message'>{{ ucfirst(trans('labels.report-header')) }}</label>
									</div>
									<div class="logo-img">
										@if( !empty($institution->report_header))
										<img src="{{ $institution->report_header }}" alt="{{ trans('labels.logo') }}" class="img-responsive" id='report-header-image'>
										@else
										<img src="/images/institutions/default_logo.jpeg" alt="{{ trans('labels.logo') }}" class="img-responsive" id='report-header-image'>
										@endif
										<a type="button" id="edit-report-header" class="btn btn-form do-crop" target="report-header" title="{{ trans('labels.edit-logo') }}">
											<i class="fa fa-upload" aria-hidden="true"></i>
											{{ trans('labels.edit-logo') }}
										</a>
									</div>
									<input type="hidden" name="report-header" id="report-header" value="">
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='message'>{{ ucfirst(trans('labels.report-footer')) }}</label>
									</div>
									<div class="logo-img">
										@if( !empty($institution->report_footer))
										<img src="{{ $institution->report_footer }}" alt="{{ trans('labels.logo') }}" class="img-responsive" id='report-footer-image'>
										@else
										<img src="/images/institutions/default_logo.jpeg" alt="{{ trans('labels.logo') }}" class="img-responsive" id='report-footer-image'>
										@endif
										<a type="button" id="edit-report-footer" class="btn btn-form do-crop" target="report-footer" title="{{ trans('labels.edit-logo') }}">
											<i class="fa fa-upload" aria-hidden="true"></i>
											{{ trans('labels.edit-logo') }}
										</a>
									</div>
									<input type="hidden" name="report-footer" id="report-footer" value="">
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='logo-email'>{{ ucfirst(trans('labels.email-img')) }}</label>
									</div>

									<div class="logo-img">
										@if( !empty($institution->logo_email))
										<img src="{{ $institution->logo_email }}" alt="{{ trans('labels.logo-email') }}" class="img-responsive" id='email-img-image'>
										@else
										<img src="/images/institutions/default_logo.jpeg" alt="{{ trans('labels.logo-email') }}" class="img-responsive" id='email-img-image'>
										@endif
										<a type="button" id="edit-email-img" class="btn btn-form do-crop" target="email-img" title="{{ trans('labels.edit-logo') }}">
											<i class="fa fa-upload" aria-hidden="true"></i>
											{{ trans('labels.edit-logo') }}
										</a>
									</div>
									<!-- <div class="active-logo">
										@include('includes.general-checkbox', [
											'id'        =>'logo_report',
											'name'      =>'logo_actives_report',
											'label'     =>'labels.actives-report',
											'condition' => $institution->logo_actives_report
										])
									</div> -->
									<input type="hidden" name="email-img" id="email-img" value="">
								</div>
							</div>
    
  
        				</div>
          			</div>
        		</div>
      		</div>
		</form>
		<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
		{!! JsValidator::formRequest('App\Http\Requests\InstitutionEditRequest', '#InstitutionEditForm'); !!}
	</div>

<div class="modal modal-fancy fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="img-modal-Label">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="img-modal-Label">{{ ucwords(trans('labels.img-edit')) }}</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="croppic-init" id="logo-img-container">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="close-img-edit"
                data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
        <button type="button" class="btn btn-form"
                id="set-logo">{{ ucfirst(trans('labels.save')) }}</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  //ColorBoxSelectsInit();
  ColorBoxmultiSelectsInit();
  initMaskedInputs();
  editImageModal();
</script>

@endsection