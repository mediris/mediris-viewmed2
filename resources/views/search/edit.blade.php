@extends('layouts.app')

@section('title',ucfirst(trans('titles.search')))

@section('content')

	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.search')),
	'elem_type' => 'button',
	'elem_name' => ucfirst(trans('labels.save')),
	'form_id' => '#SearchEditForm',
	'elem_id' => 'searchSubmit',
	'route' => '',
	'fancybox' => '',
	'routeBack' => route('search')
])

	<div class="container-fluid search edit">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif

				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		<form method="post" action="{{ url('search/edit/' . $requestedProcedure->id ) }}"
		      id="SearchEditForm">

			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="serviceRequest[request_status_id]" value="{{ $serviceRequest->request_status_id}}">
			<input type="hidden" name="serviceRequest[patient_state_id]" value="{{ $serviceRequest->patient_state_id}}">

			<div class="row">
				{{-- PATIENT INFORMATION --}}
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="panel sombra x7 x7-less elastic">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-file"
							                           aria-hidden="true"></i> {{ trans('titles.patient-info') }}</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
									<div class="patient-img ">
                                        @if( isset($patient->id) && !empty($patient->photo))
                                            <img src="{{ asset( 'storage/'. $patient->photo ) }}"
                                            alt="Patient-Image" class="img-responsive" id='avatar'>
                                        @else
                                            <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                            class="img-responsive" id='avatar'>
                                        @endif
                                    </div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">

                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="patient-info">
                                                <p><strong>{{ ucfirst(trans('labels.name')) }}:</strong> {{ $requestedProcedure->patient->first_name . ' ' . $requestedProcedure->patient->last_name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.id')) }}:</strong> {{ $patient->patient_ID }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.age')) }}:</strong> {{ $patient->getAgeAndMonth()['years'] . ' ' . ($patient->getAgeAndMonth()['years'] > 1 ? trans('labels.years') : trans('labels.year')) .
											', ' . $patient->getAgeAndMonth()['months'] . ' ' . ($patient->getAgeAndMonth()['months'] > 1 ? trans('labels.months') : trans('labels.month')) }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.gender')) }}:</strong> {{ $requestedProcedure->patient->sex_id == 1 ? trans('labels.male') : trans('labels.female') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.height')) }}:</strong> {{ $requestedProcedure->service_request->height . ' ' . trans('labels.meters') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.weight')) }}:</strong> {{ $requestedProcedure->service_request->weight . ' Kg' }}</p>
                                        </div>
                                    </div>
                                    
                                </div>
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<p>
										<strong>{{ trans('labels.birth-date') . ': ' }}</strong>{{ date('d/m/Y', strtotime($patient->birth_date)) }}
									</p>
									<p>
										<strong>{{ trans('labels.history-id') . ': ' }}</strong>{{ $patient->id }}
									</p>
									<p>
										<strong>{{ ucfirst(trans('labels.cellphone-number')) . ': ' }}<i
													class="fa fa-mobile fa-lg fa-fw"></i></strong>{{ $patient->cellphone_number }}
									</p>
									<p>
										<strong>{{ ucfirst(trans('labels.telephone-number')) . ': ' }}<i
													class="fa fa-phone fa-lg fa-fw"></i></strong>{{ $patient->telephone_number }}
									</p>
									<p>
										<strong>{{ trans('labels.address') . ': ' }}</strong> {{ !empty($patient->address) ? $patient->address : trans('labels.na') }}
									</p>
									@if(isset($patient->parent) && !empty($patient->parent))
										<p>
											<strong>{{ trans('labels.responsable') . ': ' }}</strong> {{ !empty($serviceRequest->parent) ? $patient->parent : trans('labels.na') }}
										</p>
									@endif
									{{--<p><strong>{{ trans('labels.admission-id') . ': ' }}</strong></p>--}}
									<p>
										<strong>{{ ucfirst(trans('labels.email')) . ': ' }}</strong> {{ $patient->email }}
									</p>
									<p>
										<strong>{{ trans('labels.contrast-allergy') . ': ' }}</strong>{{ $patient->contrast_allergy == 1 ? ucfirst(trans('labels.no')) : ucfirst(trans('labels.yes')) }}
									</p>
									<p>
										<strong>{{ trans('labels.medical-alerts') . ': ' }}</strong>{{ !empty($patient->medical_alerts) ? $patient->medical_alerts : trans('labels.na') }}
									</p>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

									<div>
										<label>{{ ucfirst(trans('labels.documents')) }}</label>
									</div>
									<div>
										<div class="k-widget k-upload k-header">
											<ul class="k-upload-files k-reset">
												@foreach($requestedProcedure->patient->patientDocuments as $key => $patientDocument)
													<li class="k-file" id="k-file-{{ $key + 2000 }}" attr-id="{{ $patientDocument->id }}">
														<span class="k-progress" style="width: 100%;"></span>
														<a href="{{ route('patients.document', ['document' => $patientDocument->id ]) }}" target="_blank">
															<span class="k-file-extension-wrapper">
																<span class="k-file-extension">{{ $patientDocument->type }}</span>
																<span class="k-file-state"></span>
															</span>
															<span class="k-file-name-size-wrapper">
																<span class="k-file-name" filename="{{ $patientDocument->filename }}">{{ $patientDocument->name }}</span>
																<span class="k-file-size">95.56 KB</span>
															</span>
														</a>
														<strong class="k-upload-status">
                                                            <button deleteLocation="patientDocumentsToDelete"
                                                                type="button"
                                                                class="k-button k-button-bare k-upload-action k-delete-file"
                                                                id="k-delete-file-{{ $key + 2000 }}">
                                                                <span class="k-icon k-i-close k-i-delete" title="Quitar" aria-label="Quitar"></span>
                                                            </button>
                                                        </strong>
													</li>
												@endforeach 
											</ul>
										</div>
										<div id="patientDocumentsToDelete"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				{{-- RECEPTION EDITION --}}
				<input type="text" hidden="hidden" value="{{ $serviceRequest->patient_id  }}" name="serviceRequest[patient_id]">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="panel sombra x7 x7-less">
						<div class="panel-heading">
							<h3 class="panel-title"><i
										class="dashboard-icon icon-edit"></i> {{ trans('titles.request-info') }}
							</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='weight'>{{ ucfirst(trans('labels.weight')) }} kg *</label>
									</div>
									<div>
										<input type="number" class="form-control" step='5' name="serviceRequest[weight]"
										       id="weight"
										       value="{{ $serviceRequest->weight or old('weight') }}"
										       placeholder="50">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for='height'>{{ ucfirst(trans('labels.height')) }} m *</label>
									</div>
									<div>
										<input type="number" class="form-control" step='0.1'
										       name="serviceRequest[height]"
										       id="height"
										       value="{{ $serviceRequest->height or old('height') }}"
										       placeholder="1.5">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for="patient_type_id">{{ ucfirst(trans('labels.patient-type')) }}
											*</label>
									</div>
									<div>
										<select class="form-control" name="serviceRequest[patient_type_id]"
										        id="patient_type_id">
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>

											@if(isset($serviceRequest))
												@foreach($patientTypes as $patientType)
													<option value="{{ $patientType->id }}" {{ $patientType->id == $serviceRequest->patient_type_id ? 'selected' : '' }}> {{ $patientType->description }}</option>
												@endforeach
											@endif

										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for="source_id">{{ ucfirst(trans('labels.source')) }} *</label>
									</div>
									<div>
										<select class="form-control" name="serviceRequest[source_id]" id="source_id">
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>

											@if(isset($serviceRequest))
												@foreach($sources as $source)
													<option value="{{ $source->id }}" {{ $serviceRequest->source_id == $source->id ? 'selected' : '' }}>{{ $source->description }}</option>
												@endforeach
											@endif

										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="referring_id">{{ ucfirst(trans('labels.referring')) }}
											</label>
									</div>
									<div>
										<select class="form-control" name="serviceRequest[referring_id]"
										        id="referring_id">

											@if(isset($serviceRequest))
												@foreach($referrings as $referring)
													<option value="{{ $referring->id }}" {{ $serviceRequest->referring_id == $referring->id ? 'selected' : '' }}>{{ $referring->id == 1 ? trans('labels.none') : $referring->last_name." ".$referring->first_name }}</option>
												@endforeach
											@endif

										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="answer_id">{{ trans('labels.how-did') }} *</label>
									</div>
									<div>
										<select class="form-control" name="serviceRequest[answer_id]" id="answer_id">
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>

											@if(isset($serviceRequest))
												@foreach($answers as $answer)
													<option value="{{ $answer['id'] }}" {{ $serviceRequest->answer_id == $answer['id'] ? 'selected' : '' }}>{{ $answer['description'] }}</option>
												@endforeach
											@endif

										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='comments'>{{ ucfirst(trans('labels.comments')) }}</label>
									</div>
									<div>
										<textarea class="form-control" name="serviceRequest[comments]"
										          id="service_request_comments">{{ $serviceRequest->comments }}</textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label>{{ ucfirst(trans('titles.related-documents')) }}</label>
									</div>
									<div>
										<input name="requestDocuments[]" id="requestDocuments" type="file" accept=".xlsx,.xls,.jpg,.jpeg,.png,.doc,.docx,.pdf"/>
										<div class="k-widget k-upload k-header">
											<ul class="k-upload-files k-reset">
												@foreach($requestedProcedure->service_request->request_documents as $key => $requestDocument)
													<li class="k-file" id="k-file-{{ $key + 100 }}" attr-id="{{ $requestDocument->id }}">
														<input name="requestDocuments[{{ $key + 100 }}][id]" value="{{ $requestDocument->id }}" type="hidden" />
														<input name="requestDocuments[{{ $key + 100 }}][name]" value="{{ $requestDocument->name }}" type="hidden" />
														<input name="requestDocuments[{{ $key + 100 }}][description]" value="{{ $requestDocument->description }}" type="hidden" />
														<input name="requestDocuments[{{ $key + 100 }}][location]" value="{{ $requestDocument->location }}" type="hidden" />
														<input name="requestDocuments[{{ $key + 100 }}][type]" value="{{ $requestDocument->type }}" type="hidden" />
														<span class="k-progress" style="width: 100%;"></span>
														<a href="{{ route('reception.document', ['document' => $requestDocument->id ]) }}" target="_blank">
															<span class="k-file-extension-wrapper">
																<span class="k-file-extension">{{ $requestDocument->type }}</span>
																<span class="k-file-state"></span>
															</span>
															<span class="k-file-name-size-wrapper">
																<span class="k-file-name" filename="{{ $requestDocument->name }}">{{ $requestDocument->name }}</span>
																<span class="k-file-size">95.56 KB</span>
															</span>
														</a>
													</li>
												@endforeach
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="panel sombra x7 x7-less elastic">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-file-text"
							                           aria-hidden="true"></i> {{ ucfirst(trans('titles.procedures')) }}
							</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<p>
										<strong>{{ ucfirst(trans('labels.accession-number')) }}: </strong>{{ $requestedProcedure->id }}
									</p>
								</div>
								
                            </div>
							<div class="row">
								
									
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="requested_procedure_status_id">{{ ucfirst(trans('labels.order-status')) }}</label>
									</div>
									<div>
										<select class="form-control" name="requestedProcedure[requested_procedure_status_id]"
										        id="requested_procedure_status_id" {{Auth::user()->hasRole(1) || Auth::user()->hasRole(6) ? '' : 'disabled'}}>
											<option value="" disabled
											        selected>{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($requestedProcedureStatuses as $requestedProcedureStatus)
												@if($requestedProcedureStatus->id <= $requestedProcedure->requested_procedure_status_id)
												<option value="{{ $requestedProcedureStatus->id }}"
														{{ $requestedProcedure->requested_procedure_status_id == $requestedProcedureStatus->id ? 'selected' : '' }}>
													{{ ucfirst(trans('labels.' . $requestedProcedureStatus->description)) }}
												</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
									<div>
										<label for=""></label>
									</div>
									<div id="check-awesome">
                                        <div class="ckeckbox-left">    
                                            <input type="checkbox" name="requestedProcedure[urgent]" id="urgent" class="form-control" value="{{ $requestedProcedure->urgent }}" {{ $requestedProcedure->urgent == 1 ? 'checked' : '' }}>  
                                            <label for="urgent">
                                                <div class="check-container">
                                                    <span class="check"></span>
                                                </div>
                                                <span class="box"></span>
                                            </label> 
                                        </div>
                                        <div class="ckeckbox-right">
                                            <span class="message-checkbox">
                                            {{ ucfirst(trans('labels.urgent')) }}
                                            </span>
                                        </div>
                                    </div>  
                                </div>
								
								

									
							</div>
							<div class="row">
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="modality_id_reception">{{ ucfirst(trans('labels.modality')) }}</label>
									</div>
									<div>
										@include('includes.select', [
													'attr' => Auth::user()->hasRole(1) || Auth::user()->hasRole(4) || Auth::user()->hasRole(6) ? '' : 'disabled',
                                                    'id' => 'modality_id_reception',
                                                    'name' => 'requestedProcedure[procedure][modality_id]',
                                                    'data' => $modalities,
                                                    'keys' => ['id', 'name'],
                                                    'value' => $requestedProcedure->procedure->modality_id
                                                ])
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for="procedure_id">{{ ucfirst(trans('labels.procedures')) }}</label>
									</div>
									<div>
										@include('includes.procedures-select',[
                                                        'id' => 'procedure_id',
                                                        'name' => 'requestedProcedure[procedure_id]',
                                                        'procedures' => $procedures,
                                                        'coordinator' => Auth::user()->hasRole(1) || Auth::user()->hasRole(4) || Auth::user()->hasRole(6) == 1 ? false : true,
                                                        'match' => $requestedProcedure->procedure->id
                                                    ])
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for="comments">{{ ucfirst(trans('labels.comments')) }}</label>
									</div>
									<div>
										<textarea readonly class="form-control" name="requestedProcedure[comments]"
										          id="comments">{{ $requestedProcedure->comments or old('comments') }}</textarea>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for="category-panel"></label>
									</div>
									<div class="panel panel-input-like" id="category-panel">
										<div class="panel-heading" role="tab">
											<a role="button" data-toggle="collapse" href="#search-teaching-file"
											   aria-expanded="false" aria-controls="#search-teaching-file"
											   class="collapsed">
												<h4 class="panel-title">
													{{ ucfirst(trans('labels.teaching-file')) }}
												</h4>
											</a>
										</div>
										<div id="search-teaching-file" class="panel-collapse collapse" role="tabpanel"
										     aria-labelledby="headingOne">
											<div class="panel-body">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
														<div>
															<label for="category">{{ ucfirst(trans('labels.categories')) }}</label>
														</div>
														<select name="requestedProcedure[category_id]" id="category" class="form-control">
															<option value="999"
															        selected>{{ ucfirst(trans('labels.select')) }}</option>
															@if(isset($categories))
																@foreach($categories as $category)
																	<option value="{{ $category->id }}" {{ $requestedProcedure->category_id == $category->id ? 'selected': '' }}>{{ $category->description }}</option>
																@endforeach
															@endif
														</select>
													</div>

													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
														<div>
															<label for="subcategory">{{ ucfirst(trans('labels.subcategories')) }}</label>
														</div>
														<select name="requestedProcedure[sub_category_id]" id="sub-category"
														        class="form-control">
															<option value="999"
															        selected>{{ ucfirst(trans('labels.select')) }}</option>
															@if(isset($subcategories))
																@foreach($subcategories as $subcategory)
																	<option value="{{ $subcategory->id }}" {{ $requestedProcedure->sub_category_id == $subcategory->id ? 'selected': '' }}>{{ $subcategory->description }}</option>
																@endforeach
															@endif
														</select>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div>
															<label for="teaching-file-text">{{ ucfirst(trans('labels.teaching-file-text')) }}</label>
														</div>
														<div>
															<textarea type="text" name="requestedProcedure[teaching_file_text]"
															          id="teaching-file-text" class="form-control"
															          title="{{ trans('labels.text') }}">{{ 	$requestedProcedure->teaching_file_text }}</textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							@if($requestedProcedure->requested_procedure_status_id != 7)
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
											<label for="category-panel"></label>
										</div>
									<div class="container-out">
										
										<div class="btn-icons text-center">
											<a class="btn btn-form btn-icon" id="download-file"
		                                        data-toggle="tooltip"
		                                        title="{{ucfirst(trans('labels.patient-label'))}}"
		                                        target="_blank"
		                                        href="{{url('patients/show').'/'.$patient->id}}">
		                                        <i class="fa fa-id-card-o"></i>
		                                    </a>
		                                    @if($requestedProcedure->requested_procedure_status_id == 6)
		                                    <a class="btn btn-form btn-icon" id="download-file"
		                                        data-toggle="tooltip"
		                                        title="{{ucfirst(trans('labels.deliver-results'))}}"
		                                        target="_blank"
		                                        href="{{url('results/edit').'/'.$requestedProcedure->id}}">
		                                        <i class="fa fa-files-o"></i>
		                                    </a>
		                                    @endif
		                                    @if($requestedProcedure->requested_procedure_status_id > 2)
		                                    <a class="btn btn-form btn-icon" id="download-file"
		                                        data-toggle="tooltip"
		                                        title="{{ucfirst(trans('labels.print-label'))}}"
		                                        target="_blank"
		                                        href="{{url('technician/printer').'/'.$requestedProcedure->id.'/'.$patient->id}}">
		                                        <i class="fa fa-print"></i>
		                                    </a>
		                                    <a class="btn btn-form btn-icon btn-pac"
		                                    	data-toggle="tooltip" 
		                                    	title="{{ trans('labels.view-images') }}"
		                                    	href="{{ route('radiologist.pac', ['id'=>$requestedProcedure->id]) }}">
		                                    	<i class="fa fa-picture-o"></i>
	                                        </a>
	                                        @endif
	                                        <a class="btn btn-form btn-icon" id="download-file"
		                                        data-toggle="tooltip"
		                                        title="{{ucfirst(trans('labels.patient-history'))}}"
		                                        target="_blank"
		                                        href="{{url('patients/history').'/'.$patient->id}}">
		                                        <i class="fa fa-history"></i>
		                                    </a>
	                                    </div>
                                    </div>
								</div>
							</div>
							@endif
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p><strong>{{ ucfirst(trans('labels.issue-date')) .': ' }}</strong>{{ date('d/m/Y h:i A',strtotime($serviceRequest->issue_date)) }}</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right"><strong>{{ trans('labels.admin-aprobation-date') .': ' }}</strong>{{ $requestedProcedure->admin_approval_date ? date('d/m/Y h:i A',strtotime($requestedProcedure->admin_approval_date)) : trans('labels.na') }}</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												<strong>
													<i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.create')) . ': ' }}
												</strong>
													{{ $serviceRequest->user->first_name }} {{ $serviceRequest->user->last_name }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
											</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="separator"></div>
										</div>
									</div>

									@if(!empty($requestedProcedure->technician_user_id && $requestedProcedure->requested_procedure_status->id != 7))

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												{{ ucfirst(trans('labels.to-do')) }}
												<strong>
													<i class="fa fa-arrow-right fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.to-dictate')) }}
												</strong>
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
												<strong><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i></strong>
												{{ date('d/m/Y h:i A',strtotime($requestedProcedure->technician_end_date)) }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												<strong>
													<i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.technician')) . ': ' }}
												</strong>
													{{ $requestedProcedure->technician_user_name }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
											</p>
										</div>
									</div>
									@elseif(!empty($requestedProcedure->suspension_user_id && $requestedProcedure->requested_procedure_status->id == 7))
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												{{ ucfirst(trans('labels.to-admit')) }}
												<strong>
													<i class="fa fa-arrow-right fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.suspended')) }}
												</strong>
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
												<p class="text-right">
													<strong>
															{{ $requestedProcedure->suspension_reason->description }}
													</strong>
												</p>
											</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												<strong>
													<i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.technician')) . ': ' }}
												</strong>
													{{ $requestedProcedure->suspension_user_name }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
												<strong><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i></strong>
												{{ date('d/m/Y h:i A',strtotime($requestedProcedure->suspension_date)) }}
											</p>
										</div>

										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
											</p>
										</div>
									</div>	
									@endif

									@if(!empty($requestedProcedure->radiologist_user_id))
									<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="separator"></div>
											</div>
										</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												{{ ucfirst(trans('labels.to-dictate')) }}
												<strong>
													<i class="fa fa-arrow-right fa-fw" aria-hidden="true"></i>
													{{ $requestedProcedure->transcriptor_user_id == 0 ? ucfirst(trans('labels.to-approve')) : ucfirst(trans('labels.to-transcribe')) }}
												</strong>
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
												<strong><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i></strong>
												{{ date('d/m/Y h:i A',strtotime($requestedProcedure->dictation_date)) }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												<strong>
													<i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.radiologist')) . ': ' }}
												</strong>
												{{ $requestedProcedure->radiologist_user_name }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
											</p>
										</div>
									</div>
									@endif

									@if(!empty($requestedProcedure->transcriptor_user_id))
									<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="separator"></div>
											</div>
										</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												{{ ucfirst(trans('labels.to-transcribe')) }}
												<strong>
													<i class="fa fa-arrow-right fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.to-approve')) }}
												</strong>
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
												<strong><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i></strong>
												{{ date('d/m/Y h:i A',strtotime($requestedProcedure->transcription_date)) }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												<strong>
													<i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.transcriber')) . ': ' }}
												</strong>
												{{ $requestedProcedure->transcriptor_user_name }}
											</p>
										</div>
									</div>
									@endif

									@if($requestedProcedure->requested_procedure_status->id == 6)
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="separator"></div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												{{ ucfirst(trans('labels.to-approve')) }}
												<strong>
													<i class="fa fa-arrow-right fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.finished')) }}
												</strong>
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
												<strong><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i></strong>
												{{ date('d/m/Y h:i A',strtotime($requestedProcedure->culmination_date)) }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p>
												<strong>
													<i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i>
													{{ ucfirst(trans('labels.radiologist')) . ': ' }}
												</strong>
													{{ $requestedProcedure->approve_user_name }}
											</p>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
											<p class="text-right">
											</p>
										</div>
									</div>	
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		
		<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
		{!! JsValidator::formRequest('App\Http\Requests\PreAdmissionEditForm', '#PreAdmissionEditForm'); !!}
	</div>

	<div class="modal fade" id="img-modal" tabindex="-1" role="dialog" aria-labelledby="img-modal-Label">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="img-modal-Label">{{ ucwords(trans('labels.img-edit')) }}</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="croppic-init" id="patient-img-container">
							<!-- <img src="{{ "/images/patients/avatars/empty.jpg" }}" alt="Avatar" id="editable-img"/> -->
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="close-img-edit"
					        data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
					<button type="button" class="btn btn-form"
					        id="set-photo">{{ ucfirst(trans('labels.save')) }}</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	@push('scripts')
	    <script type="text/javascript">
			$( document ).ready(function() {
			   	$('#modality_id_reception').change();
			});
		</script>
	@endpush
	
@endsection

