<div class="container-fluid">
    <form method="post" action="{{ route('alertMessages.add') }}" id="AlertMessageAddForm">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

      
        
        <div class="row">
            <div class="modal-header">
                <h4 class="modal-title">{{ ucfirst(trans('titles.add')).' '.trans('titles.alertmessages') }}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                
                @include('includes.general-checkbox', [
                    'id'        =>'active-chk',
                    'name'      =>'active',
                    'label'     =>'labels.is-active',
                    'condition' => 1
                ])
                
                
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <label for='section_id'>{{ ucfirst(trans('labels.institutions')) }} *</label>
                </div>
                <div>
                    <select class="form-control" name="institution_id" id="institution_id">
                        <option value="0" selected>{{ ucfirst(trans('labels.all-institutions')) }}</option>
                        @foreach($institutions as $institution)
                            <option value="{{ $institution->id }}">{{ ucfirst($institution->name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="col-md-6">
                <div>
                    <label for='section_id'>{{ ucfirst(trans('labels.type')) }} *</label>
                </div>
                <div>
                    <select class="form-control" name="alert_message_type_id" id="alert_message_type_id">
                        <option value="" disabled selected>{{ ucfirst(trans('labels.select')) }}</option>
                        @foreach($alertmessagetypes as $alertmessagetype)
                            <option value="{{ $alertmessagetype->id }}">{{ ucfirst($alertmessagetype->name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <label for='start_date'>{{ ucfirst(trans('labels.start')) }}</label>
                </div>
                <div>
                    <input type="dateTime" data-datetype='dateTime' name="start_date" id="start_date" class="form-control">
                </div>
            </div>

            <div class="col-md-6">
                <div>
                    <label for='end_date'>{{ ucfirst(trans('labels.end')) }}</label>
                </div>
                <div>
                    <input type="dateTime" data-datetype='dateTime' name="end_date" id="end_date" class="form-control">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='message'>{{ ucfirst(trans('labels.message')) }}</label>
                </div>
                <div>
                    <textarea class="form-control" rows="5" name="message" id="message"></textarea>
                </div>
            </div>
        </div>

        <div class="row m-t-20"></div>

        <div class="row">
            <div class="modal-footer">
                <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                    <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                </button>
            </div>
        </div>            
    </form>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\AlertMessageAddRequest', '#AlertMessageAddForm'); !!}
</div>
<script>
    loadDateTimePicker();

</script>



