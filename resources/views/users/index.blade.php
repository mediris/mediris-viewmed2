@extends('layouts.app')

@section('title', ucfirst(trans('titles.users')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.users')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('users.add'),
									'fancybox' => '',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.users')) }}</h1>
				</div>
				
				
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('integer');
				$FirstNameField = new \Kendo\Data\DataSourceSchemaModelField('first_name');
				$FirstNameField->type('string');
				$LastNameField = new \Kendo\Data\DataSourceSchemaModelField('last_name');
				$LastNameField->type('string');
				$EmailField = new \Kendo\Data\DataSourceSchemaModelField('email');
				$EmailField->type('string');
				$TelField = new \Kendo\Data\DataSourceSchemaModelField('telephone_number');
				$TelField->type('string');
				$CellField = new \Kendo\Data\DataSourceSchemaModelField('cellphone_number');
				$CellField->type('string');

				$kendo->addFields(array(
					$IdField,
					$FirstNameField,
					$LastNameField,
					$EmailField,
					$TelField,
					$CellField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $users = $users->toArray();
				
				foreach ($users as $key => $value) {
					$users[$key]['email'] = '<a href=mailto:' . $users[$key]['email'] . '>' . $users[$key]['email'] . '</a>';
					$users[$key]['telephone_number'] = html_entity_decode('<a href=tel:' . $users[$key]['telephone_number'] . '>' . $users[$key]['telephone_number'] . '</a>');
					$users[$key]['cellphone_number'] = html_entity_decode('<a href=tel:' . $users[$key]['cellphone_number'] . '>' . $users[$key]['cellphone_number'] . '</a>');
					$title = $users[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $users[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					$users[$key]['actions'] =
							'<a href=' . route('users.edit', [$users[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . '><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>'
							. '<span class="slash">|</span> <a href= ' . route('users.active', [$users[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>';
				}

				// Set Institutions
				$kendo->setDataSource($users);

				// Create Grid
                $kendo->createGrid('users');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$FirstName = new \Kendo\UI\GridColumn();
				$FirstName->field('first_name')
						->title(ucfirst(trans('labels.name')));
				$LastName = new \Kendo\UI\GridColumn();
				$LastName->field('last_name')
						->title(ucfirst(trans('labels.last-name')));
				$Email = new \Kendo\UI\GridColumn();
				$Email->field('email')
						->encoded(false)
						->title(ucfirst(trans('labels.email')));
				$Tel = new \Kendo\UI\GridColumn();
				$Tel->field('telephone_number')
						->encoded(false)
						->title(ucfirst(trans('labels.telephone-number')));
				$Cell = new \Kendo\UI\GridColumn();
				$Cell->field('cellphone_number')
						->encoded(false)
						->title(ucfirst(trans('labels.cellphone-number')));
				$Actions = new \Kendo\UI\GridColumn();
				/*$Actions->template(new \Kendo\JavaScriptFunction('function(row){
					if(!(row.id == null))
					{
						return "<a id=show-link-"+row.id+" href="+route("users", "show", row.id)+" data-toggle=\'tooltip\' data-placement=\'bottom\' title=\'show\'><img src=/images/icon_ver_off.png width=26 /></a>"

							+ "<span>|</span> <a id=edit-link-"+row.id+" href= "+route("users", "edit",row.id)+" data-toggle=\'tooltip\' data-placement=\'bottom\' title=\'edit\'><i class =\'fa fa-pencil-square-o\' aria-hidden=true></i></a>"

							+ "<span class=\'slash\'>|</span> <a id=active-link-"+row.id+" href= "+route("users", "active",row.id)+" data-toggle=\'tooltip\' data-placement=\'bottom\' title=\'"+(row.active == 1 ? "disable" : "enable")+"\'><i class =\'fa fa-power-off "+(row.active == 1 ? "" : "active")+"\' aria-hidden=true></i></a>";
					}
					else
					{
						return "";
					}
				}'))*/
				$Actions->field('actions')
						->filterable(false)
						->sortable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$FirstName,
					$LastName,
					$Email,
					$Tel,
					$Cell,
					$Actions,
                ));
				
                $kendo->generate();

				?>
				
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>

@endsection