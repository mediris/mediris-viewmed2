
@extends('layouts.app')

@section('title',ucfirst(trans('titles.edit')).' '.trans('titles.role'))

@section('content')

@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.edit')).' '.trans('titles.role'),
                                'elem_type' => 'button',
                                'elem_name' => ucfirst(trans('labels.save')),
                                'form_id' => '#RoleEditForm',
                                'route' => '',
                                'fancybox' => '',
                                'routeBack' => route('roles')
                            ])

    <div class="container-fluid">
        <!--<div id="result"></div>-->
        <form method="post" action="{{ route('roles.edit', [$role->id]) }}" id="RoleEditForm">

            {!! csrf_field() !!}

            <div class="row">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <!-- CITAS -->
                    <div class="panel sombra" id="dates-notification-panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('titles.edit')).' '.trans('titles.role') }}</h3>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div>
                                        <label for='name'>{{ ucfirst(trans('labels.name')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="text" class="form-control" name="name" value="{{ $role->name }}">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                {{ $errors->first('name') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div>
                                        <label>{{ ucfirst(trans('labels.access-to')) }}</label>
                                    </div>

                                    <div id="actions">

                                    </div>

                                    <div>
                                        <?php
                                        $treeview = new \Kendo\UI\TreeView('treeview');

                                        $checkboxes = new \Kendo\UI\TreeViewCheckboxes();
                                        $checkboxes->checkChildren(true);
                                        $treeview->checkboxes($checkboxes);

                                        $treeview->check("onCheck");

                                // helper function that creates TreeViewItem with id and spriteCssClass
                                        function TreeViewItem($id, $text, $spriteCssClass)
                                        {
                                            $item = new \Kendo\UI\TreeViewItem($text);
                                            $item->spriteCssClass($spriteCssClass);
                                            $item->id = $id;
                                            return $item;
                                        }

                                        $root = TreeViewItem('all', ucfirst(trans('labels.all')), '');
                                        $root->expanded(true);

                                        foreach($sections as $section) {

                                            $sectionsAll = TreeViewItem('section_'.$section->id, ucfirst(trans('labels.'.$section->name)), '');

                                            foreach($section->actions as $action) {

                                                $actionsAll = TreeViewItem('action_'.$action->id.'_'.$section->id, ucfirst(trans('labels.'.$action->name)), '');

                                                if($role->hasAction($action->id, $section->id) != null) {
                                                    $actionsAll->checked($role->hasAction($action->id, $section->id));
                                                    $sectionsAll->expanded(true);
                                                }
                                                $sectionsAll->addItem($actionsAll);
                                            }
                                            $root->addItem($sectionsAll);
                                        }
                                        $dataSource = new \Kendo\Data\HierarchicalDataSource();

                                        $dataSource->data(array($root));

                                        $treeview->dataSource($dataSource);

                                        echo $treeview->render();
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\RoleEditRequest', '#RoleEditForm'); !!}
    </div>

    <script>


        // function that gathers IDs of checked nodes
        function checkedNodeIds(nodes, checkedNodes)
        {
            for (var i = 0; i < nodes.length; i++)
            {

                if (nodes[i].checked)
                {
                    $('#actions').append('<input type="hidden" class="actions" name="actions[]">');

                    $('input.actions:last-child').val(nodes[i].id);
                    checkedNodes.push(nodes[i].id);
                }

                if (nodes[i].hasChildren)
                {
                    checkedNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        }

        // show checked node IDs on datasource change
        function onCheck()
        {
            $('#actions .actions').each(function(){
                $(this).remove();
            });

            var checkedNodes = [],
            treeView = $("#treeview").data("kendoTreeView"),
            message;

            checkedNodeIds(treeView.dataSource.view(), checkedNodes);

            if (checkedNodes.length > 0)
            {
                message = "IDs of checked nodes: " + checkedNodes.join(",");
            }
            else
            {
                message = "No nodes checked.";
            }

            $("#result").html(message);
        }
        $(document).ready(function(){
           onCheck();
        });
    </script>

    @endsection

