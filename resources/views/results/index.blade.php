@extends('layouts.app')

@section('title', ucfirst(trans('titles.results')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.results')),
                                    'elem_type' => '',
                                    'elem_name' => '',
                                    'form_id' => '',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => '',
                                    'clearFilters' => true
                                ])
    <div class="container-fluid results index">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="title">
                    <h1>{{ trans('labels.orders-finished') }}</h1>
                </div>

                <?php

                // Grid
                $options = (object) array(
                    'url' => 'results'
                );
                $kendo    = new \App\CustomKendoGrid($options);

                // Fields
                $IsUrgentField = new \Kendo\Data\DataSourceSchemaModelField('isUrgent');
                $IsUrgentField->type('string');
                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
                $PatientTypeIconField->type('string');
                $PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
                $PatientTypeField->type('string');
                $ApprovalDateField = new \Kendo\Data\DataSourceSchemaModelField('approvalDate');
                $ApprovalDateField->type('date');
                $ServiceRequestIdField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
                $ServiceRequestIdField->type('string');
                $OrderIdField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
                $OrderIdField->type('string');
                $ProcedureDescriptionField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
                $ProcedureDescriptionField->type('string');
                $PatientNameField = new \Kendo\Data\DataSourceSchemaModelField('patientName');
                $PatientNameField->type('string');
                $PatientIdField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                $PatientIdField->type('string');
                
                // Add Fields
                $kendo->addFields(array(
                    $IsUrgentField,
                    $PatientTypeIconField,
                    $PatientTypeField,
                    $ApprovalDateField,
                    $ServiceRequestIdField,
                    $OrderIdField,
                    $ProcedureDescriptionField,
                    $PatientNameField,
                    $PatientIdField,
                ));

                // Create Schema
                $kendo->createSchema(true, true);

                // Create Data Source
                $kendo->createDataSource();
                
                // Create Grid
                $kendo->createGrid(null, 'grid');

                // Columns
                $IsUrgent = new \Kendo\UI\GridColumn();
                $IsUrgent->title(ucfirst(trans('labels.urgent')))
                        ->template(new Kendo\JavaScriptFunction("function(row){
                            if(row.isUrgent == 1)
                            {
                                return '<i class=\"fa fa-exclamation-triangle red-danger\" aria-hidden=\"true\"></i>';
                            }
                            else
                            {
                               return '<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>'
                            }
                        }"));
                $PatientTypeIcon = new \Kendo\UI\GridColumn();
                $PatientTypeIcon->field('patientTypeIcon')
                        ->attributes(['class' => 'font-awesome-td'])
                        ->filterable(false)
                        ->encoded(false)
                        ->title(ucfirst(trans('labels.patient-type-icon')));
                $PatientType = new \Kendo\UI\GridColumn();
                $PatientType->field('patientType')
                        ->title(ucfirst(trans('labels.patient-type')));
                $ApprovalDate = new \Kendo\UI\GridColumn();
                $ApprovalDate->field('approvalDate')
                        ->format('{0: dd/MM/yyyy}')
                        ->title(ucfirst(trans('labels.dictation-date')));
                $RequestNumber = new \Kendo\UI\GridColumn();
                $RequestNumber->field('serviceRequestID')
                        ->title(ucfirst(trans('labels.service-request-id')));
                $OrderNumber = new \Kendo\UI\GridColumn();
                $OrderNumber->field('orderID')
                        ->title(ucfirst(trans('labels.order-id')));
                $Procedure = new \Kendo\UI\GridColumn();
                $Procedure->field('procedureDescription')
                        ->title(ucfirst(trans('labels.procedure')));
                $PatientName = new \Kendo\UI\GridColumn();
                $PatientName->field('patientName')
                        ->title(ucfirst(trans('titles.patient')));
                $PatientID = new \Kendo\UI\GridColumn();
                $PatientID->field('patientID')
                        ->title(ucfirst(trans('labels.patient-id')));
                $actions = new \Kendo\UI\GridColumn();
                $actions->title(trans('labels.actions'));

                $slash = "<span class='slash'>|</span>";

                $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'results', 'edit');

                $editAction = $canEdit ? "\"<a href='" . url('results/edit') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "' target='_blank'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '';

                $actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $editAction . ";

                            }"
                ));

                // Excel
                $kendo->setExcel('titles.finished-orders-list', '
                    function(e) {
                        if (!exportFlag) {
                            e.sender.hideColumn(0);
                            e.sender.hideColumn(9);
                            e.preventDefault();
                            exportFlag = true;
                            setTimeout(function () {
                              e.sender.saveAsExcel();
                            });
                          } else {
                            e.sender.showColumn(0);
                            e.sender.showColumn(9);
                            exportFlag = false;
                          }
                    }', '.xlsx', ' ' . date('d-M-Y'));
                
                // PDF
                $kendo->setPDF('titles.finished-orders-list', 'transcriber/1', '
                    function (e) {
                    e.sender.hideColumn(0);
                    e.sender.hideColumn(9);
                    e.promise.done(function() {
                        e.sender.showColumn(0);
                        e.sender.showColumn(9);
                    });
                }', ' ' . date('d-M-Y'), 'page-template');

                // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
                    $PatientTypeIcon,
                    $IsUrgent,
                    $PatientType,
                    $ApprovalDate,
                    $RequestNumber,
                    $OrderNumber,
                    $Procedure,
                    $PatientName,
                    $PatientID,
                    $actions,
                ));

                $kendo->generate(true, true, null, 550);

                ?>

                {!! $kendo->render() !!}

            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            // Refresh Kendo Grid each 30s
            // Custom Way kendoGridAutoRefresh('#grid', 30);
            kendoGridAutoRefresh();
        });
    </script>

    <script type="x/kendo-template" id="page-template">
	    <div class="page-template">
		    <div class="header">
			    <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
			    {{ trans('titles.orders-to-transcribe') }}
		    </div>
		    <div class="watermark">{{ Session::get('institution')->name }}</div>
		    <div class="footer">
			    {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
		    </div>
	    </div>
    </script>

    <style type="text/css">
	    /* Page Template for the exported PDF */
	    .page-template {
		    font-family: "Open Sans", "Arial", sans-serif;
		    position: absolute;
		    width: 100%;
		    height: 100%;
		    top: 0;
		    left: 0;
	    }
	    .page-template .header {
		    position: absolute;
		    top: 30px;
		    left: 30px;
		    right: 30px;
		    border-bottom: 1px solid #888;
		    color: #888;
	    }
	    .page-template .footer {
		    position: absolute;
		    bottom: 30px;
		    left: 30px;
		    right: 30px;
		    border-top: 1px solid #888;
		    text-align: center;
		    color: #888;
	    }
	    .page-template .watermark {
		    font-weight: bold;
		    font-size: 400%;
		    text-align: center;
		    margin-top: 30%;
		    color: #aaaaaa;
		    opacity: 0.1;
		    transform: rotate(-35deg) scale(1.7, 1.5);
	    }
    </style>
	
@endsection