@extends('layouts.app')

@section('title',ucfirst(trans('titles.deliver')).' '.trans('titles.results'))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.deliver')).' '.trans('titles.results'),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('titles.deliver')),
                                    'elem_id' => 'deliver_results',
                                    'form_id' => '#ResultsEditForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('results'),
                                ])

    <div class="container-fluid transcriber edit">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('titles.patient-info') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="patient-img ">
                                        @if( isset($requestedProcedure->patient->id) && !empty($requestedProcedure->patient->photo))
                                            <img src="{{ asset( 'storage/'. $requestedProcedure->patient->photo ) }}"
                                            alt="Patient-Image" class="img-responsive" id='avatar'>
                                        @else
                                            <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                            class="img-responsive" id='avatar'>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="patient-info">
                                                <p><strong>{{ ucfirst(trans('labels.name')) }}:</strong> {{ $requestedProcedure->patient->first_name . ' ' . $requestedProcedure->patient->last_name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.age')) }}:</strong> {{ $requestedProcedure->patient->getStrAgeAndMonth() }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.gender')) }}:</strong> {{ $requestedProcedure->patient->sex_id == 1 ? trans('labels.male') : trans('labels.female') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.height')) }}:</strong> {{ $requestedProcedure->service_request->height . ' ' . trans('labels.meters') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.birth-date')) }}:</strong> {{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($requestedProcedure->patient->birth_date)) : date('Y-m-d', strtotime($requestedProcedure->patient->birth_date)) }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.email')) }}:</strong> {{ $requestedProcedure->patient->email }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.history-id')) }}:</strong> {{ $requestedProcedure->patient->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.address')) }}:</strong> {{ $requestedProcedure->patient->address }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.responsable')) }}:</strong> {{ $requestedProcedure->service_request->parent or trans('labels.na') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.admission-id')) }}:</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.contrast-allergy')) }}:</strong> {{ $requestedProcedure->patient->contrast_allergy ? ucfirst(trans('labels.yes')) : 'No' }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.medical-alerts')) }}:</strong> {{ $requestedProcedure->patient->medical_alerts }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ $requestedProcedure->procedure->description }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.request-num') }}:</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.order-num') }}:</strong> {{ $requestedProcedure->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.patient-type')) }}:</strong> {{ $requestedProcedure->service_request->patient_type->description }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.source')) }}:</strong> {{ $requestedProcedure->service_request->source->description }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.referring') . ': ' }}</strong>{{ $requestedProcedure->service_request->referring ? ucwords($requestedProcedure->service_request->referring->first_name . ' ' . $requestedProcedure->service_request->referring->last_name) : ucfirst(trans('labels.not-specified'))}}
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.admitted-by') }}:</strong> {{ $requestedProcedure->service_request->user->first_name }} {{ $requestedProcedure->service_request->user->last_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.attended-by') }}:</strong> {{ $requestedProcedure->technician_user_name }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.dictated-by') }}:</strong> {{ $requestedProcedure->radiologist_user_name }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.approved-by') }}:</strong> {{ $requestedProcedure->approve_user_name }}</p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.observations') }}:</strong> {{ $requestedProcedure->comments }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel sombra">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ ucfirst(trans('labels.previousdeliveries')) }}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>{{ ucfirst(trans('labels.date')) }}</th>
                                                <th>{{ ucfirst(trans('labels.cireceiver')) }}</th>
                                                <th>{{ ucfirst(trans('labels.receiver')) }}</th>
                                                <th>{{ ucfirst(trans('labels.report')) }}</th>
                                                <th>{{ ucfirst(trans('labels.cd')) }}</th>
                                                <th>{{ ucfirst(trans('labels.plates')) }}</th>
                                                <th>{{ ucfirst(trans('labels.responsable')) }}</th>
                                                <th>{{ ucfirst(trans('labels.observations')) }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($requestedProcedure->delivers as $deliver)
                                                <tr>
                                                    <td>{{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($deliver->date)) : date('m-d-Y', strtotime($deliver->date)) }}</td>
                                                    <td>{{ $deliver->receptor_id }}</td>
                                                    <td>{{ $deliver->receptor_name }}</td>
                                                    <td>{{ $deliver->file ? ucfirst(trans('labels.yes')) : ucfirst(trans('labels.not')) }}</td>
                                                    <td>{{ $deliver->cd ?  ucfirst(trans('labels.yes')) . '('. $deliver->num_cd . ')' : ucfirst(trans('labels.not')) }}</td>
                                                    <td>{{ $deliver->plates ? ucfirst(trans('labels.yes')) .'('. $deliver->num_plates . ')' : ucfirst(trans('labels.not')) }}</td>
                                                    <td>{{ $deliver->responsable_name }}</td>
                                                    <td>{{ $deliver->observations }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel sombra">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ ucfirst(trans('titles.deliver')) }} {{ trans('titles.results') }}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                                        <div class="panel sombra">
                                            <div class="panel-heading bg-blue">
                                                <h3 class="panel-title">{{ ucfirst(trans('titles.delivery-information')) }}</h3>
                                            </div>
                                            <div class="panel-body">
                                                <form action="{{ route('results.edit', [$requestedProcedure->id]) }}" method='post' id="ResultsEditForm">
                                                    <div class="row">
                                                        <div class="col-xs-9 col-sm-4">
                                                            @include('includes.general-checkbox', [
                                                                'id'        =>'deliver_cd',
                                                                'name'      =>'cd',
                                                                'label'     =>'labels.deliver-cd',
                                                                'condition' => 0
                                                            ])
                                                        </div>
                                                        <div class="col-xs-9 col-sm-4">
                                                            @include('includes.general-checkbox', [
                                                                'id'        =>'deliver_plates',
                                                                'name'      =>'plates',
                                                                'label'     =>'labels.deliver-plates',
                                                                'condition' => 0
                                                            ])
                                                        </div>
                                                        <div class="col-xs-9 col-sm-4">
                                                            @include('includes.general-checkbox', [
                                                                'id'        =>'deliver_file',
                                                                'name'      =>'file',
                                                                'label'     =>'labels.deliver-file',
                                                                'condition' => 0
                                                            ])
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-9 col-sm-4">
                                                            <div>
                                                                <label for="number_of_cd">{{ trans('labels.number-of-cd') }}</label>
                                                            </div>
                                                            <div>
                                                                <input type="number" name="num_cd" id="num_cd" class="form-control" value="0">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-9 col-sm-4">
                                                            <div>
                                                                <label for="number_of_plates">{{ trans('labels.number-of-plates') }}</label>
                                                            </div>
                                                            <div>
                                                                <input type="number" name="num_plates" id="number_of_plates" class="form-control" value="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div>
                                                                <label for='receptor_name'>Receptor:</label>
                                                            </div>
                                                            <div>
                                                                <input type="text" name="receptor_name" id="receptor_name" class="input-field form-control user btn-style" value="{{ $requestedProcedure->patient->first_name }} {{ $requestedProcedure->patient->last_name }}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div>
                                                                <label for='receptor_id'>{{ trans('labels.id') }}:</label>
                                                            </div>
                                                            <div>
                                                                <input type="text" name="receptor_id" id="receptor_id" class="input-field form-control user btn-style" value="{{ $requestedProcedure->patient->patient_ID }}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div>
                                                                <label for='observations'>{{ ucfirst(trans('labels.observations')) }}:</label>
                                                            </div>

                                                            <div>
                                                                <textarea class="form-control" name="observations" id="observations"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                                        <div class="panel sombra">
                                            <div class="panel-heading bg-blue">
                                                <h3 class="panel-title">{{ ucfirst(trans('titles.deliver')) }} {{ trans('titles.results') }}</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                                                        <div class="btn-icons text-center">
                                                                @php($version = "original")
                                                                @if ( isset($requestedProcedure->addendums) && !empty($requestedProcedure->addendums) )
                                                                    @php($version = "original-draf")
                                                                @endif
                                                            <a id="download-file" class="btn btn-form btn-icon" data-toggle="tooltip"
                                                            target="_blank"
                                                                title="{{ trans('labels.view-file-report') }}"
                                                                href="{{ route('finalreport.pdf', [
                                                                                    'id'=> $requestedProcedure->id,
                                                                                    'version' => $version 
                                                                                ])
                                                                }}">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                            </a>

    
                                                            @if ( isset($requestedProcedure->addendums) && !empty($requestedProcedure->addendums) )
                                                                @foreach ( $requestedProcedure->addendums as $addendum )
                                                                    <a class="btn btn-form btn-icon" id="download-file"
                                                                        data-toggle="tooltip"
                                                                        title="{{ trans('labels.view-file-addendum') }} "
                                                                        target="_blank"
                                                                        href="{{ route('finalreport.pdf', [
                                                                                            'id'=> $requestedProcedure->id,
                                                                                            'version' => 'addendum' 
                                                                                            ])
                                                                        }}">
                                                                        <i class="fa fa-file-pdf-o"></i>
                                                                    </a>
                                                                @endforeach
                                                            @endif
                                                                    
                                                            <?php $notificationTemplate = App\NotificationTemplate::remoteFind(8) ?>
                                                            
                                                            @if ( isset($notificationTemplate) && $notificationTemplate->active )
                                                                <a id="send-email" class="btn btn-form btn-icon" data-toggle="tooltip"
                                                                    title="{{ trans('labels.send-email') }}"
                                                                    href="{{ route('finalreport.email', [
                                                                                        'id'        => $requestedProcedure->id,
                                                                                        'patient'   => $requestedProcedure->patient->id
                                                                                    ])
                                                                    }}">
                                                                    <i class="fa fa-envelope"></i>
                                                                </a>
                                                            @endif
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection