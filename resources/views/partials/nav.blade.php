@if(!\Session::has('permissions') && !Auth::guest())
	@if(Session::has('institution'))
		<?php  Auth::user()->getPermissionByInstitution(Session::get('institution')->id)?>
	@endif
@endif
@if (!Auth::guest() && Session::has('institution'))
	<nav id="sidebar">
		<div id="main-menu">
			<ul class="sidebar-nav align-center">

				@include('partials.submenus.home')

				@if(Session::has('institution') && Session::has('permissions'))

					<?php
						$subSection = false;
						$subCategories = false;					
					?>

					@foreach(Session::get('menu') as $menu)
						<!-- Basic -->
						<?php			
							$current 	= false;
							$subcurrent = false;
							$catcurrent = false;
							if(Route::has($menu->section)){ 
								if(Route::has(Route::getCurrentRoute()->getPath())){
									$path = route(Route::getCurrentRoute()->getPath());
								} else {
									$path = '';
								}
								if(route($menu->section) == $path){
									$current = true;
								}
							} else {
								if(isset($menu->sub)){
									if(count($menu->sub) > 0){
										if(Route::has(Route::getCurrentRoute()->getPath())){
											$path    = route(Route::getCurrentRoute()->getPath());
											foreach ($menu->sub as $submenu) {
												if(Route::has($submenu->section)){
													if(route($submenu->section) == $path){
														$current 	= true;
														$subcurrent = true;
													}
												} else {
													if(isset($submenu->sub)){
														if(count($submenu->sub) > 0){
															foreach ($submenu->sub as $subcategory) {
																if(Route::has($subcategory->section)){
																	if(route($subcategory->section) == $path){
																		$current 	= true;
																		$subcurrent = true;
																		$catcurrent = true;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						?>
						@if($current)
							<li class="current active">
						@else
							<li>
						@endif
								@include('partials.submenus.section')
								<!-- Sub Menu -->
								@if(isset($menu->sub))
									@if(count($menu->sub) > 0)
										@if($current)
											<ul class="submenu collapse in" aria-expanded="true" style="height: auto;">
										@else 
											<ul class="submenu collapse" aria-expanded="false" style="height: 0px;">
										@endif
												@foreach($menu->sub as $submenu)
													@include('partials.submenus.category')
												@endforeach
											</ul>
									@endif
								@endif
							</li>
					@endforeach				
				@endif

				<li><!-- FILL FOR BETTER VISUALIZATION --></li>
			</ul>
		</div>
	</nav>
@elseif(!Auth::guest())
	<nav id="sidebar">
		<div id="main-menu">
			<ul class="sidebar-nav align-center">
				@include('partials.submenus.home')
				@include('partials.submenus.system')
				<li><!-- FILL FOR BETTER VISUALIZATION --></li>
			</ul>
		</div>
	</nav>
@endif
