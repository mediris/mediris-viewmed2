<script id="editor" type="text/x-kendo-template">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-11">
                <div>
                    <label>Fecha: </label>
                    <span>#: kendo.toString(start, "dd - MMMM - yyyy") # </span>
                    <label> Hora: </label>
                    <span>#: kendo.toString(start, "hh:mm tt") # - #: kendo.toString(end, "hh:mm tt") #</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div>
                    <label for="applicant">{{ ucfirst(trans('labels.applicant')) }}</label>
                </div>
                <div>
                    <input type="text" class="input-field form-control user btn-style k-textbox" name="applicant" data-bind="value:applicant">
                </div>
            </div>
            <div class="col-md-5">
                <div>
                    <label for="procedure_contrast_study">{{ ucfirst(trans('labels.contrast-study')) }} *</label>
                </div>
                <div>
                    <label>{{ ucfirst(trans('labels.yes')) }}
                        <input class="input-field form-control user btn-style" type="radio" name="procedure_contrast_study" value="1"/>
                    </label>
                    <label>{{ ucfirst(trans('labels.not')) }}
                        <input class="input-field form-control user btn-style" type="radio" name="procedure_contrast_study" value="0"/>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div>
                    <label for="patient_identification_id">{{ ucfirst(trans('labels.patient-id')) }} *</label>
                </div>
                <div>
                    <input id="patient_identification_id" list="recents_patient_identification_id" class="input-field form-control user btn-style k-textbox" name="patient_identification_id" data-bind="value:patient_identification_id">
                    <datalist id="recents_patient_identification_id">
                    </datalist>
                </div>
            </div>
            <div class="col-md-5">
                <div>
                    <label for="patient_email">{{ ucfirst(trans('labels.email')) }} *</label>
                </div>
                <div>
                    <input id="patient_email" type="email" class="input-field form-control user btn-style k-textbox" name="patient_email" data-bind="value:patient_email">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div>
                    <label for="patient_first_name">{{ ucfirst(trans('labels.name')) }} *</label>
                </div>
                <div>
                    <input id="patient_first_name" type="text" class="input-field form-control user btn-style k-textbox" name="patient_first_name" data-bind="value:patient_first_name">
                </div>
            </div>
            <div class="col-md-5">
                <div>
                    <label for="patient_last_name">{{ ucfirst(trans('labels.last-name')) }} *</label>
                </div>
                <div>
                    <input id="patient_last_name" type="text" class="input-field form-control user btn-style k-textbox" name="patient_last_name" data-bind="value:patient_last_name">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div>
                    <label for="patient_telephone_number">{{ ucfirst(trans('labels.telephone-number')) }} </label>
                </div>
                <div >
                    <input id="patient_telephone_number" type="text" class="input-field form-control user btn-style k-textbox" name="patient_telephone_number" id="patient_telephone_number" data-bind="value:patient_telephone_number">
                </div>
            </div>
            <div class="col-md-5">
                <div>
                    <label for="patient_cellphone_number">{{ ucfirst(trans('labels.cellphone-number')) }} </label>
                </div>
                <div >
                    <input id="patient_cellphone_number" type="text" class="input-field form-control user btn-style k-textbox" name="patient_cellphone_number" id="patient_cellphone_number" data-bind="value:patient_cellphone_number">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11">
                <div>
                    <label for="patient_allergies">{{ ucfirst(trans('labels.allergies')) }} *</label>
                    <label>{{ ucfirst(trans('labels.yes')) }}
                        <input class="form-control" type="radio" name="patient_allergies" value="1" />
                    </label>
                    <label>{{ ucfirst(trans('labels.not')) }}
                        <input class="form-control" type="radio" name="patient_allergies" value="0"/>
                    </label>
                </div>
            </div>
        </div>
        <div class="row" id="div_allergie">
            <div class="col-md-11">
                <div>
                    <input id="patient_allergies_txt" type="text" class="input-field form-control user btn-style k-textbox" name="patient_allergies_txt" data-bind="value:patient_allergies_txt" placeholder="{{ ucfirst(trans('labels.patient_allergies_txt')) }}">
                </div>
            </div>
        </div>
        <!--Validación de tipo de estudio. Se incluyo a solicitud del cliente Junio 2017. Si el estudio es Tomografia o resonancia magnetica-->
        <div class="row" id="div-study-validate">
            <div class="col-md-2">
                <div>
                    <label for="patient_weight">{{ ucfirst(trans('labels.weight')) }} *</label>
                </div>
                <div>
                    <input id="patient_weight"
                           name="patient_weight"
                           data-role="numerictextbox"
                           role="spinbutton"
                           step='5'
                           style="width: 85%"
                           min="0"
                           placeholder="30,00">

                </div>
            </div>
            <div class="col-md-3">
                <div>
                    <label for="patient_height">{{ ucfirst(trans('labels.height')) }} *</label>
                </div>
                <div>
                    <input id="patient_height"
                           data-role="numerictextbox"
                           role="spinbutton"
                           step='0,1'
                           style="width: 80%"
                           name="patient_height"
                           min="0"
                           placeholder="50,00">
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <label for="patient_abdominal_circumference">{{ ucfirst(trans('labels.patient-abdominal-circumference')) }} *</label>
                </div>
                <div>
                    <input id="patient_abdominal_circumference"
                           name="patient_abdominal_circumference"
                           data-role="numerictextbox"
                           role="spinbutton"
                           step="5"
                           min="0"
                           placeholder="40.00">
                </div>
            </div>
        </div>
        <!---->
        <div class="row">
            <div class="col-md-11">
                <div>
                    <label for="observations">{{ ucfirst(trans('labels.observations')) }}</label>
                </div>
                <div >
                    <textarea class="form-control k-textbox" name="observations" id="observations" data-bind="value:observations"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11">
                <div>
                    <p><strong>{{ ucfirst(trans('labels.indications')) }}</strong></p>
                </div>
                <div id="div_procedure_indication">
                    <p>{{ ucfirst(trans('labels.indications_not')) }}</p>
                </div>
            </div>
        </div>
    </div>
</script>