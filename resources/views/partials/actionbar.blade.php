<div class="container-fluid hidden-print">
    <div id="scroll-action-bar-hidden" class="row action-bar-hidden sombra">
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            <legend class="nav-legend">{{ $title }}</legend>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <div>

                @if(!empty($routeBack))
                    <a class="btn btn-default btn-right btn-cerrar" id="goback-btn" href="{{ $routeBack }}">
                        <span>
                            {{ trans('labels.goback') }}
                        </span>
                    </a>
                @endif

                @if(isset($clearFilters))
                    <button class="btn btn-default btn-right {{ is_array($clearFilters) ? $clearFilters['class'] : '' }}"
                            id="{{ is_array($clearFilters) ? $clearFilters['id-1'] : 'clear-filters-1' }}">
                        {{ trans('labels.clear-filters') }}
                    </button>
                @endif
                
                @if(!empty($elem_type))

                    @if(isset($second_elem_name) && !empty($second_elem_name))
                        <{{ $elem_type }} class="btn btn-form btn-right {{ $second_elem_id }} {{ $fancybox }}" id="{{ $second_elem_id }}{{ $num[0] or '' }}" data-style="expand-left" {{ !empty($form_id) ? ' formtarget='. $form_id : '' }} {{ !empty($route) ? ' href='. $route : '' }}">
                            <span>
                                {{ $second_elem_name }}
                            </span>
                        </{{ $elem_type  }}>
                    @endif
	        
		            @if(isset($elem_name) && !empty($elem_name))
	                    <{{ $elem_type }} class="btn btn-form btn-right {{ $fancybox }} {{ !empty($elem_id) ? $elem_id : "" }}" id="{{ !empty($elem_id) ? $elem_id : "submit-1" }}" data-style="expand-left" {{ !empty($form_id) ? ' formtarget='. $form_id : '' }} {{ !empty($route) ? ' href='. $route : '' }}>
	                        <span>
	                            {{ $elem_name }}
	                        </span>
	                    </{{ $elem_type  }}>
	                 @endif
	    
                @endif

                @if ( isset( $allowImpersonate ) )
                    @include('includes.select', [
                                                    'idname' => 'user-change-sel',
                                                    'value' => \Auth::user()->id,
                                                    'data' => \App\User::all(),
                                                    'keys' => ['id', 'username'],
                                                    'class'     => 'in-footer'
                                                ])
                    <label class="in-footer">{{ trans('labels.responsable') }}:</label>
                @endif

                @if ( isset( $allowKillSessions ) )
                    <input type = "hidden" id= "kill-session" name= "kill-session" value="0">

                @endif

            </div>
        </div>
    </div>

    <div id="scroll-action-bar" class="row action-bar sombra">
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            <legend class="nav-legend">{{ $title }}</legend>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <div>

                @if(isset($search_bar) && !empty($search_bar))
                    <div class="row">
                        @if(isset($search_bar_title) && !empty($search_bar_title))
                            <div class="col-xs-3">
                                <h3 class="panel-title special_top"><i class="fa fa-search"></i> {{ $search_bar_title }}</h3>
                            </div>
                        @endif
                        <div class="col-xs-6 col-sm-7 col-md-7 col-lg-7">
                            <input type="text" name="search_criteria" id="search_criteria" class="input-field form-control user btn-style search_bar" placeholder="{{ $search_bar }}"/>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                            @if(isset($search_action) && !empty($search_action) && isset($search_action_id) && !empty($search_action_id))
                                <button id="{{ $search_action_id }}" class="btn btn-form btn-left">
                                    <i class="fa fa-search" aria-hidden="true"></i> {{ $search_action }}
                                </button>
                            @endif
                        </div>
                    </div>
                @endif

                @if(!empty($routeBack))
                    <a class="btn btn-default btn-right btn-cerrar" id="goback-btn" href="{{ $routeBack }}">
                        <span>
                            {{ trans('labels.goback') }}
                        </span>
                    </a>
                @endif

                @if(isset($clearFilters))
		                <button class="btn btn-default btn-right {{ is_array($clearFilters) ? $clearFilters['class'] : '' }}"
		                        id="{{ is_array($clearFilters) ? $clearFilters['id-2'] : 'clear-filters-2' }}">
			                {{ trans('labels.clear-filters') }}
		                </button>
                @endif
                    
                @if(!empty($elem_type))

                    @if(isset($second_elem_name) && !empty($second_elem_name))
                        <{{ $elem_type }} class="btn btn-form btn-right {{ $second_elem_id }} {{ $fancybox }}" id="{{ $second_elem_id }}{{ $num[1] or '' }}" data-style="expand-left" {{ !empty($form_id) ? ' formtarget='. $form_id : '' }} {{ !empty($route) ? ' href='. $route : '' }}">
                            <span>
                                {{ $second_elem_name }}
                            </span>
                        </{{ $elem_type  }}>
                    @endif
					
	                @if(isset($elem_name) && !empty($elem_name))
	                    <{{ $elem_type }} class="btn btn-form btn-right {{ $fancybox }} {{ !empty($elem_id) ? $elem_id : "" }}" id="{{ !empty($elem_id) ? $elem_id : "submit-2" }}" data-style="expand-left" {{ !empty($form_id) ? ' formtarget='. $form_id : '' }} {{ !empty($route) ? ' href='. $route : '' }}>
	                                <span>
	                                    {{ $elem_name }}
	                                </span>
	                    </{{ $elem_type != '' ? $elem_type : '' }}>
		            @endif
	    
                @endif

                @if ( isset( $allowImpersonate ) )
                    @include('includes.select', [
                                                    'idname' => 'user-change-sel2',
                                                    'value' => \Auth::user()->id,
                                                    'data' => \App\User::all(),
                                                    'keys' => ['id', 'username'],
                                                    'class'     => 'in-footer'
                                                ])
                    <label class="in-footer">{{ trans('labels.responsable') }}:</label>
                @endif
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $( ".btn-cerrar" ).click(function() {
            window.close();
        });
    });
</script>

@if ( isset($allowImpersonate)  )
    @include('includes.modal-login')
@endif