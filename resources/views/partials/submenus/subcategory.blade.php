
<?php 
	if(isset($subcategory->section)) {
		if(Route::has($subcategory->section)){ 
	        $route = route($subcategory->section);
			if(route($subcategory->section) == $path){
				$catcurrent = true;
			} 
	    } else {
	        $route = '#';
	    }
	} else {
		$route = '#';
	}
?>
@if(isset($subcategory->section))
	<li>
	    <a class="docent-file-link" href="{{ $route }}">
	        <span class="sidebar-text">{{ ucfirst(trans('labels.'.$subcategory->section)) }}</span>
	    </a>
	</li>
@endif