<?php 
	$parent = $menu->section;
	if(Route::has($menu->section)){ 
		$route = route($menu->section);
	} else {
		$route = '#';
	}
?>
<a href="{{ $route }}" target="_self">
    <i class="{{ 'ico icon-'.trans('icons.'.$menu->section) }}"></i>
    <span class="sidebar-text">{{ ucfirst(trans('labels.'.$menu->section)) }}</span>
    @if(isset($menu->sub))
		@if(count($menu->sub) > 0)
    		<span class="arrow glyphicon glyphicon-triangle-right"></span>
		@endif
	@endif
</a>