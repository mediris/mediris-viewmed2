<li>
    <a href="{{ route('configurations.edit', Session::get('institution')->id) }}">
        <span class="sidebar-text">{{ ucfirst(trans('labels.configuration')) }}</span>
    </a>
</li>