@extends('layouts.app')

@section('title', ucfirst(trans('titles.login')))

@section('content')
    <div class="container" id="login-block">
        <div class="row">
            <div class="logo">
                <img src="/images/viewmed-logo.png" alt="">
            </div>
            <div class="col-sm-6 col-md-6 col-sm-offset-3">
                <div class="login-box clearfix">
                    <!-- BEGIN ERROR BOX -->
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <ul>
                                @foreach($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <!-- END ERROR BOX -->
                        <!-- <div class="login-logo">
                            <img src="/images/mediris_icon.png" alt="Mediris Logo">
                        </div> -->
                        <div class="login-form ">
                            <form method="post" id="loginForm" action="{{ url('/login') }}">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label for='email' class="m-t-20">{{ ucfirst(trans('labels.email')) }}</label>
                                    <div>
                                        <input type="email" name="email" id="email" class="input-field form-control user btn-style" value="{{ old('email') }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">{{ ucfirst(trans('labels.password')) }}</label>
                                    <div>
                                        <input type="password" id="password" name="password" class="input-field form-control password" />
                                    </div>
                                </div>

                                <div class="row remember-links">

                                    <div class="col-md-6 login-check">
                                        <div id="check-awesome" class="check-login">
                                            <div class="ckeckbox-left">    
                                                <input type="checkbox" name="remember" id="active-chk" class="form-control" value="1">  
                                                <label for="active-chk">
                                                    <div class="check-container">
                                                        <span class="check"></span>
                                                    </div>
                                                    <span class="box"></span>
                                                </label> 
                                            </div>
                                            <div class="ckeckbox-right">
                                                <span class="message-checkbox remember-login">
                                                {{ ucfirst(trans('labels.remember')) }} {{ ucfirst(trans('labels.password')) }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <a href="{{ url('/password/reset') }}" >{{ trans('messages.forgot-password') }}</a>
                                    </div>
                                </div>
                                <div class="btn-caja"> <button id="submit-form" class="btn btn-login ladda-button" data-style="expand-left" href="home.html"><span class="ladda-label">{{ ucfirst(trans('labels.login')) }}</span></button></div>
                            </form>
                            <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
                            {!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#loginForm'); !!}
                        </div>
                </div>

            </div>
        </div>
    </div>
@endsection
