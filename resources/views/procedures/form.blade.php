<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="{{ $position }}">
    <div class="panel sombra procedure procedure_{{ $procedure->id }}">
        <div class="panel-heading bg-blue">
            <a type="button" id="close-procedure" class="close white requested-procedure-close" id-attr="{{ $position }}">×</a>
            <h3 class="panel-title">{{ $procedure->description }}</h3>
        </div>
        <div class="panel-body">

            <input type="hidden" name="procedures[{{ $position }}][id]" value="{{ $procedure->id }}">
            <div>
                <label for='duration_{{ $position }}'>Duraci&oacute;n en cupos *</label>
            </div>
            <div>
                <input type="number" min="1" class="form-control" name="procedures[{{ $position }}][duration]" id="duration_{{ $position }}" value="{{ isset($duration) ? $duration : 1 }}">
            </div>
        </div>
    </div>
    <?php /*
    <script>
        deleteRequestedProcedure();
    </script>
    */ ?>
</div>