@extends('layouts.app')

@section('title', ucfirst(trans('titles.templates')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.templates')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('templates.add'),
									'fancybox' => '',
									'routeBack' => '',
									'clearFilters' => true
								 ])
	
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.templates')) }}</h1>
				</div>
				
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('string');
				$DescriptionField = new \Kendo\Data\DataSourceSchemaModelField('description');
				$DescriptionField->type('string');

				$kendo->addFields(array(
					$IdField,
					$DescriptionField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $templates = $templates->toArray();
				
				$canShow = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'templates', 'show');
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'templates', 'edit');
				$canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'templates', 'active');
				
				foreach ($templates as $key => $value) {
					$title = $templates[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $templates[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					$showAction = $canShow ? '<a href=' . route('templates.show', [$templates[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.show') . '><i class="fa fa-eye" aria-hidden="true"></i></a>' : '';
					$editAction = $canEdit ? '<a href=' . route('templates.edit', [$templates[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . '><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';
					$activeAction = $canEnable ? '<span class="slash">|</span> <a href= ' . route('templates.active', [$templates[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>' : '';
					
					$templates[$key]['actions'] = $editAction . $activeAction;
				}

				// Set  Templates
				$kendo->setDataSource($templates);

				// Create Grid
                $kendo->createGrid('actions');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$Description = new \Kendo\UI\GridColumn();
				$Description->field('description')
						->title(ucfirst(trans('labels.description')));
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$Description,
					$Actions,
                ));
				
                $kendo->generate();

				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>


@endsection