<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Routes Auth
 */
Auth::routes();

/**
 * Routes Patients
 */
Route::group([ 'prefix' => 'patients' ], function () {

    Route::get('/uploadImage', [ 'as' => 'patients.uploadImage', 'uses' => 'PatientsController@uploadImage' ]);
    Route::get('/cropImage', [ 'as' => 'patients.cropImage', 'uses' => 'PatientsController@cropImage' ]);
    Route::post('/uploadImage', [ 'as' => 'patients.uploadImage', 'uses' => 'PatientsController@uploadImage' ]);
    Route::post('/cropImage', [ 'as' => 'patients.cropImage', 'uses' => 'PatientsController@cropImage' ]);
    Route::post('/search', [ 'as' => 'patients.search', 'uses' => 'PatientsController@search' ]);
    Route::get('/all', [ 'as' => 'patients.all', 'uses' => 'PatientsController@all' ]);
    Route::post('/info', [ 'as' => 'patients.info', 'uses' => 'PatientsController@info' ]);

});

Route::get('/servicerequests/history/{patient}', [ 'as' => 'servicerequests.patients.history', 'uses' => 'ServiceRequestsController@history' ]);
Route::get('/servicerequests/legacyhistory/{patient}', [ 'as' => 'servicerequests.patients.legacyhistory', 'uses' => 'ServiceRequestsController@legacyhistory' ]);

/**
 * Routes Appointments
 */
Route::group([ 'prefix' => 'appointments' ], function () {

    Route::get('/refresh', [ 'as' => 'appointments.refresh', 'uses' => 'AppointmentsController@refresh' ]);
    Route::post('/refresh', [ 'as' => 'appointments.refresh', 'uses' => 'AppointmentsController@refresh' ]);

});

/**
 * Middleware Auth
 */
Route::group([ 'prefix' => '/', 'middleware' => 'auth' ], function () {

    /**
     * Routes of Errors
     */
    Route::group([ 'prefix' => 'errors' ], function () {

        Route::get('/{id}', [ 'as' => 'errors', 'uses' => 'ErrorsController@index' ]);
        Route::get('403', [ 'as' => 'errors.403', 'uses' => 'ErrorsController@forbidden' ]);
        Route::get('401', [ 'as' => 'errors.401', 'uses' => 'ErrorsController@unauthorized' ]);
        Route::get('503', [  'as' => 'errors.503', 'uses' => 'ErrorsController@unavailable' ]);

    });

    /**
     * Routes of Institutions
     */
    Route::group([ 'prefix' => 'institutions' ], function () {

        /**
         * Middleware Root
         */
        Route::group([ 'middleware' => 'root' ], function () {

            Route::get('/', [ 'as' => 'institutions', 'uses' => 'InstitutionsController@index', 'groupBy' => 'system' ]);
            Route::get('/add', [ 'as' => 'institutions.add', 'uses' => 'InstitutionsController@add' ]);
            Route::post('/add', [ 'as' => 'institutions.add', 'uses' => 'InstitutionsController@add' ]);
            Route::get('/show/{institution}', [ 'as' => 'institutions.show', 'uses' => 'InstitutionsController@show' ]);
            Route::get('/edit/{institution}', [ 'as' => 'institutions.edit', 'uses' => 'InstitutionsController@edit' ]);
            Route::post('/edit/{institution}', [ 'as' => 'institutions.edit', 'uses' => 'InstitutionsController@edit' ]);
            //Route::get('/delete/{institution}', ['as' => 'institutions.delete', 'uses' => 'InstitutionsController@delete']);
            Route::get('/active/{institution}', [ 'as' => 'institutions.active', 'uses' => 'InstitutionsController@active' ]);

        });

        Route::get('/select', [ 'as' => 'institutions.select', 'uses' => 'InstitutionsController@select' ]);
        Route::get('/select/{institution}', [ 'as' => 'institutions.selected', 'uses' => 'InstitutionsController@select' ]);

    });

    /**
     * Routes of Modalities
     */
    Route::group([ 'prefix' => 'modalities'], function () {

        /**
         * Middleware Root
         */
        Route::group([ 'middleware' => 'root' ], function () {

            Route::get('/', [ 'as' => 'modalities', 'uses' => 'ModalitiesController@index', 'groupBy' => 'system' ]);
            Route::get('/add', [ 'as' => 'modalities.add', 'uses' => 'ModalitiesController@add' ]);
            Route::post('/add', [ 'as' => 'modalities.add', 'uses' => 'ModalitiesController@add' ]);
            Route::get('/edit/{modality}', [ 'as' => 'modalities.edit', 'uses' => 'ModalitiesController@edit' ]);
            Route::post('/edit/{modality}', [ 'as' => 'modalities.edit', 'uses' => 'ModalitiesController@edit' ]);
            Route::get('/active/{modality}', [ 'as' => 'modalities.active', 'uses' => 'ModalitiesController@active' ]);

        });

    });

    /**
     * Routes of Users
     */
    Route::group([ 'prefix' => 'users' ], function () {

        /**
         * Middleware Root
         */
        Route::group([ 'middleware' => 'root' ], function () {

            Route::get('/', [ 'as' => 'users', 'uses' => 'UsersController@index', 'groupBy' => 'system' ]);
            Route::get('/add', [ 'as' => 'users.add', 'uses' => 'UsersController@add' ]);
            Route::post('/add', [ 'as' => 'users.add', 'uses' => 'UsersController@add' ]);
            Route::get('/show/{user}', [ 'as' => 'users.show', 'uses' => 'UsersController@show' ]);
            Route::get('/edit/{user}', [ 'as' => 'users.edit', 'uses' => 'UsersController@edit' ]);
            Route::post('/edit/{user}', [ 'as' => 'users.edit', 'uses' => 'UsersController@edit' ]);
            //Route::get('/delete/{user}', ['as' => 'users.delete', 'uses' => 'UsersController@delete']);
            Route::get('/active/{user}', [ 'as' => 'users.active', 'uses' => 'UsersController@active' ]);
        });

        Route::post('/change_password/{user}', [ 'as' => 'users.password', 'uses' => 'UsersController@change_password' ]);
        //Route::get('/out', [ 'as' => 'users.out', 'uses' => 'UsersController@out' ]);
        
        //shows a user signature image from storage
        Route::get('/signature/{user}', [ 'as' => 'users.signature', 'uses' => 'UsersController@signature' ]);
        //shows a legacy user signature image from storage
        Route::get('/signature/legacy/{user}', [ 'as' => 'users.legacy.signature', 'uses' => 'UsersController@legacySignature' ]);
    });

    /**
    * Middleware sel-inst 
    * Routes only when an institution was selected
    */
    Route::group([ 'middleware' => 'sel-inst' ], function () {

        Route::get('/', [ 'as' => '/', 'uses' => 'HomeController@index' ]);
        Route::get('/home', [ 'as' => 'home', 'uses' => 'HomeController@index' ]);

        /**
         * Routes of Roles
         */
        Route::group([ 'prefix' => 'roles'], function () {

            /**
             * Middleware Root
             */
            Route::group([ 'middleware' => 'root' ], function () {

                Route::get('/', [ 'as' => 'roles', 'uses' => 'RolesController@index', 'groupBy' => 'system' ]);
                Route::get('/add', [ 'as' => 'roles.add', 'uses' => 'RolesController@add' ]);
                Route::post('/add', [ 'as' => 'roles.add', 'uses' => 'RolesController@add' ]);
                Route::get('/show/{role}', [ 'as' => 'roles.show', 'uses' => 'RolesController@show' ]);
                Route::get('/edit/{role}', [ 'as' => 'roles.edit', 'uses' => 'RolesController@edit' ]);
                Route::post('/edit/{role}', [ 'as' => 'roles.edit', 'uses' => 'RolesController@edit' ]);
                //Route::get('/delete/{role}', ['as' => 'roles.delete', 'uses' => 'RolesController@delete']);
                Route::get('/active/{role}', [ 'as' => 'roles.active', 'uses' => 'RolesController@active' ]);

            });

        });


        /**
         * Route Appointments
         */
        Route::group([ 'prefix' => 'appointments'], function () {

            Route::get('/datasource', [ 'as' => 'appointments.datasource', 'uses' => 'AppointmentsController@datasource' ]);
            Route::post('/search', [ 'as' => 'appointments.search', 'uses' => 'AppointmentsController@search' ]);

        });

        /**
         * Route Procedures
         */
        Route::group([ 'prefix' => 'procedures'], function () {
           
            Route::get('/form/{position}/{procedure}', [ 'as' => 'procedures.form', 'uses' => 'ProceduresController@form' ]);
            Route::get('/form/{position}/{procedure}/{duration}', [ 'as' => 'procedures.form', 'uses' => 'ProceduresController@form' ]);
            Route::get('/modalities/{id}', [ 'as' => 'procedures.modalities', 'uses' => 'ProceduresController@modalities' ]);
        
        });

        /**
         * Route Steps
         */
        Route::group([ 'prefix' => 'steps'], function () {
          
            Route::get('/form/{position}/{random}', [ 'as' => 'steps.form', 'uses' => 'StepsController@form' ]);
            Route::get('/form/{position}/{random}/{step}', [ 'as' => 'steps.form', 'uses' => 'StepsController@form' ]);
        
        });

        Route::get('/users/info/{user}', [ 'as' => 'users.info', 'uses' => 'UsersController@info' ]);

        /**
         * Route Requested Procedures
         */
        Route::group([ 'prefix' => 'requestedprocedures'], function () {

            Route::get('/form/{position}/{procedureId}', [ 'as' => 'requestedProcedures.form', 'uses' => 'RequestedProceduresController@form' ]);
            Route::get('/form/{position}/{procedureId}/{requestedProcedureId}', [ 'as' => 'requestedProcedures.form', 'uses' => 'RequestedProceduresController@form' ]);
       
        });

        /**
         * Route Alert Messages
         */
        Route::group([ 'prefix' => 'alertMessages'], function () {

            Route::post('/show', [ 'as' => 'alertMessages.show', 'uses' => 'AlertMessagesController@show' ]);
       
        });

        /**
         * Middleware Permission
         */
        Route::group([ 'middleware' => 'permission' ], function () {

            /**
             * Routes of Interview
             */
            //Route::group([ 'prefix' => 'interview' ], function () {
            //  Route::get('/', [ 'as' => 'interview', 'uses' => 'InterviewsController@index' ]);
            //});

            /**
             * Routes of Divisions
             */
            Route::group([ 'prefix' => 'divisions' ], function () {

                Route::get('/', [ 'as' => 'divisions', 'uses' => 'DivisionsController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add/', [ 'as' => 'divisions.add', 'uses' => 'DivisionsController@add' ]);
                Route::post('/add', [ 'as' => 'divisions.add', 'uses' => 'DivisionsController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'divisions.show', 'uses' => 'DivisionsController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'divisions.edit', 'uses' => 'DivisionsController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'divisions.edit', 'uses' => 'DivisionsController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'divisions.delete', 'uses' => 'DivisionsController@delete']);
                Route::get('/active/{id}', [ 'as' => 'divisions.active', 'uses' => 'DivisionsController@active' ]);

            });

            /**
             * Routes of Printers
             */
            Route::group([ 'prefix' => 'printers' ], function () {

                Route::get('/', [ 'as' => 'printers', 'uses' => 'PrintersController@index', 'priority' => -1 ]);
                Route::get('/add/', [ 'as' => 'printers.add', 'uses' => 'PrintersController@add' ]);
                Route::post('/add', [ 'as' => 'printers.add', 'uses' => 'PrintersController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'printers.show', 'uses' => 'PrintersController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'printers.edit', 'uses' => 'PrintersController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'printers.edit', 'uses' => 'PrintersController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'printers.delete', 'uses' => 'PrintersController@delete']);
                Route::get('/active/{id}', [ 'as' => 'printers.active', 'uses' => 'PrintersController@active' ]);

            });

            /**
             * Routes of Rooms
             */
            Route::group([ 'prefix' => 'rooms' ], function () {

                Route::get('/', [ 'as' => 'rooms', 'uses' => 'RoomsController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add/', [ 'as' => 'rooms.add', 'uses' => 'RoomsController@add' ]);
                Route::post('/add', [ 'as' => 'rooms.add', 'uses' => 'RoomsController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'rooms.show', 'uses' => 'RoomsController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'rooms.edit', 'uses' => 'RoomsController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'rooms.edit', 'uses' => 'RoomsController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'rooms.delete', 'uses' => 'RoomsController@delete']);
                Route::get('/active/{id}', [ 'as' => 'rooms.active', 'uses' => 'RoomsController@active' ]);

            });

            /**
             * Routes of Consumables
             */
            Route::group([ 'prefix' => 'consumables' ], function () {

                Route::get('/', [ 'as' => 'consumables', 'uses' => 'ConsumablesController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add/', [ 'as' => 'consumables.add', 'uses' => 'ConsumablesController@add' ]);
                Route::post('/add', [ 'as' => 'consumables.add', 'uses' => 'ConsumablesController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'consumables.show', 'uses' => 'ConsumablesController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'consumables.edit', 'uses' => 'ConsumablesController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'consumables.edit', 'uses' => 'ConsumablesController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'consumables.delete', 'uses' => 'ConsumablesController@delete']);
                Route::get('/active/{id}', [ 'as' => 'consumables.active', 'uses' => 'ConsumablesController@active' ]);

            });

            /**
             * Routes of Units
             */
            Route::group([ 'prefix' => 'units' ], function () {

                Route::get('/', [ 'as' => 'units', 'uses' => 'UnitsController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add/', [ 'as' => 'units.add', 'uses' => 'UnitsController@add' ]);
                Route::post('/add', [ 'as' => 'units.add', 'uses' => 'UnitsController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'units.show', 'uses' => 'UnitsController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'units.edit', 'uses' => 'UnitsController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'units.edit', 'uses' => 'UnitsController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'consumables.delete', 'uses' => 'ConsumablesController@delete']);
                Route::get('/active/{id}', [ 'as' => 'units.active', 'uses' => 'UnitsController@active' ]);

            });

            /**
             * Routes of PatientTypes
             */
            Route::group([ 'prefix' => 'patientTypes' ], function () {

                Route::get('/', [ 'as' => 'patientTypes', 'uses' => 'PatientTypesController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add', [ 'as' => 'patientTypes.add', 'uses' => 'PatientTypesController@add' ]);
                Route::post('/add', [ 'as' => 'patientTypes.add', 'uses' => 'PatientTypesController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'patientTypes.show', 'uses' => 'PatientTypesController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'patientTypes.edit', 'uses' => 'PatientTypesController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'patientTypes.edit', 'uses' => 'PatientTypesController@edit' ]);
                //Route::get('/delete/{id}',['as' => 'patientTypes.delete', 'uses' => 'PatientTypesController@delete']);
                Route::get('/active/{id}', [ 'as' => 'patientTypes.active', 'uses' => 'PatientTypesController@active' ]);

            });

            /**
             * Route of Old Data Migrations
             */
            Route::group([ 'prefix' => 'migrations' ], function () {
                    
                Route::get('/', [ 'as' => 'migrations', 'uses' => 'OldRecordsController@import', 'groupBy' => 'system' ]);
                Route::post('/', [ 'as' => 'migrations.consulta', 'uses' => 'OldRecordsController@consulta' ]);
                Route::post('/inst', [ 'as' => 'migrations.inst', 'uses' => 'OldRecordsController@importInstitucion' ]);
                Route::post('/data', [ 'as' => 'migrations.data', 'uses' => 'OldRecordsController@importData' ]);
                Route::post('/status/{centro}', [ 'as' => 'migrations.status', 'uses' => 'OldRecordsController@importEstatus' ]);
                Route::post('/test', [ 'as' => 'migrations.test', 'uses' => 'OldRecordsController@test_connection' ]);
                Route::post('/modal/{centro}', [ 'as' => 'migrations.modal', 'uses' => 'OldRecordsController@importModalidad' ]);
                Route::post('/process/{centro}', [ 'as' => 'migrations.process', 'uses' => 'OldRecordsController@importProcedimiento' ]);
                Route::post('/users/{centro}', [ 'as' => 'migrations.users', 'uses' => 'OldRecordsController@importUsuario' ]);
                Route::post('/patient/{centro}', [ 'as' => 'migrations.patient', 'uses' => 'OldRecordsController@importPatients' ]);
                Route::post('/request/{centro}', [ 'as' => 'migrations.request', 'uses' => 'OldRecordsController@importSolicitud' ]);
                Route::post('/orders/{centro}', [ 'as' => 'migrations.orders', 'uses' => 'OldRecordsController@importOrden' ]);
                Route::post('/docs/{centro}', [ 'as' => 'migrations.docs', 'uses' => 'OldRecordsController@import_documents_scan' ]);
                Route::get('/log/{log}', [ 'as' => 'migrations.showlog', 'uses' => 'OldRecordsController@showLog' ]);
                Route::post('/file', [ 'as' => 'migrations.fileOrders', 'uses' => 'OldRecordsController@fileOrders' ]);

            });

            /**
             * Routes of Sources
             */
            Route::group([ 'prefix' => 'sources' ], function () {

                Route::get('/', [ 'as' => 'sources', 'uses' => 'SourcesController@index', 'groupBy' => 'administration' ]);
                Route::get('/add', [ 'as' => 'sources.add', 'uses' => 'SourcesController@add' ]);
                Route::post('/add', [ 'as' => 'sources.add', 'uses' => 'SourcesController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'sources.show', 'uses' => 'SourcesController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'sources.edit', 'uses' => 'SourcesController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'sources.edit', 'uses' => 'SourcesController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'sources.delete', 'uses' => 'SourcesController@delete']);
                Route::get('/active/{id}', [ 'as' => 'sources.active', 'uses' => 'SourcesController@active' ]);

            });

            /**
             * Routes of Templates
             */
            Route::group([ 'prefix' => 'templates' ], function () {

                Route::get('/', [ 'as' => 'templates', 'uses' => 'TemplatesController@index', 'groupBy' => 'administration' ]);
                Route::get('/add', [ 'as' => 'templates.add', 'uses' => 'TemplatesController@add' ]);
                Route::post('/add', [ 'as' => 'templates.add', 'uses' => 'TemplatesController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'templates.show', 'uses' => 'TemplatesController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'templates.edit', 'uses' => 'TemplatesController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'templates.edit', 'uses' => 'TemplatesController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'templates.delete', 'uses' => 'TemplatesController@delete']);
                Route::get('/active/{id}', [ 'as' => 'templates.active', 'uses' => 'TemplatesController@active' ]);

            });

            /**
             * Routes of Notification templates
             */
            Route::group([ 'prefix' => 'notification_templates' ], function () {

                Route::get('/', [ 'as' => 'notification_templates', 'uses' => 'NotificationTemplateController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add', [ 'as' => 'notification_templates.add', 'uses' => 'NotificationTemplateController@add' ]);
                Route::post('/add', [ 'as' => 'notification_templates.add', 'uses' => 'NotificationTemplateController@add' ]);
                Route::get('/edit/{id}', [ 'as' => 'notification_templates.edit', 'uses' => 'NotificationTemplateController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'notification_templates.edit', 'uses' => 'NotificationTemplateController@edit' ]);
                Route::get('/active/{id}', [ 'as' => 'notification_templates.active', 'uses' => 'NotificationTemplateController@active' ]);

            });

            /**
             * Routes of Suspend Reasons
             */
            Route::group([ 'prefix' => 'suspend_reasons' ], function () {
                    
                Route::get('/', [ 'as' => 'suspend_reasons', 'uses' => 'SuspendReasonsController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add', [ 'as' => 'suspend_reasons.add', 'uses' => 'SuspendReasonsController@add' ]);
                Route::post('/add', [ 'as' => 'suspend_reasons.add', 'uses' => 'SuspendReasonsController@add' ]);
                Route::get('/edit/{id}', [ 'as' => 'suspend_reasons.edit', 'uses' => 'SuspendReasonsController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'suspend_reasons.edit', 'uses' => 'SuspendReasonsController@edit' ]);
                Route::get('/active/{id}', [ 'as' => 'suspend_reasons.active', 'uses' => 'SuspendReasonsController@active' ]);

            });

            /**
             * Routes of Equipment
             */
            Route::group([ 'prefix' => 'equipment' ], function () {

                Route::get('/', [ 'as' => 'equipment', 'uses' => 'EquipmentController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add', [ 'as' => 'equipment.add', 'uses' => 'EquipmentController@add' ]);
                Route::post('/add', [ 'as' => 'equipmen.add', 'uses' => 'EquipmentController@add' ]);
                Route::get('/show/{equipment}', [ 'as' => 'equipment.show', 'uses' => 'EquipmentController@show' ]);
                Route::get('/edit/{equipment}', [ 'as' => 'equipment.edit', 'uses' => 'EquipmentController@edit' ]);
                Route::post('/edit/{equipment}', [ 'as' => 'equipment.edit', 'uses' => 'EquipmentController@edit' ]);
                //Route::get('/delete/{equipment}', ['as' => 'equipment.delete', 'uses' => 'EquipmentController@delete']);
                Route::get('/active/{id}', [ 'as' => 'equipment.active', 'uses' => 'EquipmentController@active' ]);

            });

            /**
             * Routes of Alert Messages
             */
            Route::group([ 'prefix' => 'alertMessages' ], function () {

                Route::get('/', [ 'as' => 'alertMessages', 'uses' => 'AlertMessagesController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add', [ 'as' => 'alertMessages.add', 'uses' => 'AlertMessagesController@add' ]);
                Route::post('/add', [ 'as' => 'alertMessages.add', 'uses' => 'AlertMessagesController@add' ]);
                Route::get('/edit/{alertmessage}', [ 'as' => 'alertMessages.edit', 'uses' => 'AlertMessagesController@edit' ]);
                Route::post('/edit/{alertmessage}', [ 'as' => 'alertMessages.edit', 'uses' => 'AlertMessagesController@edit' ]);
                Route::post('/rewrite', [ 'as' => 'alertMessages.rewrite', 'uses' => 'AlertMessagesController@rewrite' ]);
                Route::post('/lock', [ 'as' => 'alertMessages.lock', 'uses' => 'AlertMessagesController@lock' ]);

            });

            /**
             * Routes of Configurations
             */
            Route::group([ 'prefix' => 'configurations' ], function () {

                Route::get('/config', ['as' => 'configurations.configurations', 'uses' => 'ConfigurationsController@configurations', 'groupBy' => 'configurations', 'menu' => true ]);
                Route::get('/edit/{configuration}', [ 'as' => 'configurations.edit', 'uses' => 'ConfigurationsController@edit', 'groupBy' => 'configurations']);
                Route::post('/edit/{configuration}', [ 'as' => 'configurations.edit', 'uses' => 'ConfigurationsController@edit' ]);

            });

            /**
             * Routes of Steps
             */
            Route::group([ 'prefix' => 'steps' ], function () {

                Route::get('/', [ 'as' => 'steps', 'uses' => 'StepsController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add', [ 'as' => 'steps.add', 'uses' => 'StepsController@add' ]);
                Route::post('/add', [ 'as' => 'steps.add', 'uses' => 'StepsController@add' ]);
                Route::get('/show/{step}', [ 'as' => 'steps.show', 'uses' => 'StepsController@show' ]);
                Route::get('/edit/{step}', [ 'as' => 'steps.edit', 'uses' => 'StepsController@edit' ]);
                Route::post('/edit/{step}', [ 'as' => 'steps.edit', 'uses' => 'StepsController@edit' ]);
                //Route::get('/delete/{step}', ['as' => 'steps.delete', 'uses' => 'StepsController@delete']);
                Route::get('/active/{id}', [ 'as' => 'steps.active', 'uses' => 'StepsController@active' ]);

            });

            /**
             * Routes of Procedures
             */
            Route::group([ 'prefix' => 'procedures' ], function () {

                Route::get('/', [ 'as' => 'procedures', 'uses' => 'ProceduresController@index', 'groupBy' => 'configurations' ]);
                Route::get('/add', [ 'as' => 'procedures.add', 'uses' => 'ProceduresController@add' ]);
                Route::post('/add', [ 'as' => 'procedures.add', 'uses' => 'ProceduresController@add' ]);
                Route::get('/show/{procedure}', [ 'as' => 'procedures.show', 'uses' => 'ProceduresController@show' ]);
                Route::get('/edit/{procedure}', [ 'as' => 'procedures.edit', 'uses' => 'ProceduresController@edit' ]);
                Route::post('/edit/{procedure}', [ 'as' => 'procedures.edit', 'uses' => 'ProceduresController@edit' ]);
                //Route::get('/delete/{procedure}', ['as' => 'procedures.delete', 'uses' => 'ProceduresController@delete']);
                Route::get('/active/{id}', [ 'as' => 'procedures.active', 'uses' => 'ProceduresController@active' ]);

            });

            /**
             * Routes of Orders
             */
            Route::group([ 'prefix' => 'orders' ], function () {

                Route::get('/', [ 'as' => 'orders', 'uses' => 'OrdersController@index', 'groupBy' => 'administration']);
                Route::get('/add', [ 'as' => 'orders.add', 'uses' => 'OrdersController@add' ]);
                Route::post('/add', [ 'as' => 'orders.add', 'uses' => 'OrdersController@add' ]);
                Route::get('/show/{order}', [ 'as' => 'orders.show', 'uses' => 'OrdersController@show' ]);
                Route::get('/edit/{order}', [ 'as' => 'orders.edit', 'uses' => 'OrdersController@edit' ]);
                Route::post('/edit/{order}', [ 'as' => 'orders.edit', 'uses' => 'OrdersController@edit' ]);
                //Route::get('/delete/{order}', ['as' => 'orders.delete', 'uses' => 'OrdersController@delete']);
                Route::get('/active/{order}', [ 'as' => 'orders.active', 'uses' => 'OrdersController@active' ]);

            });

            /**
             * Routes of Referrings
             */
            Route::group([ 'prefix' => 'referrings' ], function () {

                Route::get('/', [ 'as' => 'referrings', 'uses' => 'ReferringsController@index', 'groupBy' => 'administration' ]);
                Route::get('/add', [ 'as' => 'referrings.add', 'uses' => 'ReferringsController@add' ]);
                Route::post('/add', [ 'as' => 'referrings.add', 'uses' => 'ReferringsController@add' ]);
                Route::get('/show/{referring}', [ 'as' => 'referrings.show', 'uses' => 'ReferringsController@show' ]);
                Route::get('/edit/{referring}', [ 'as' => 'referrings.edit', 'uses' => 'ReferringsController@edit' ]);
                Route::post('/edit/{referring}', [ 'as' => 'referrings.edit', 'uses' => 'ReferringsController@edit' ]);
                //Route::get('/delete/{referring}', ['as' => 'referrings.delete', 'uses' => 'ReferringsController@delete']);
                Route::get('/active/{referring}', [ 'as' => 'referrings.active', 'uses' => 'ReferringsController@active' ]);

            });

            /**
             * Routes of PatientStates
             */
            Route::group([ 'prefix' => 'patientStates' ], function () {

                Route::get('/', [ 'as' => 'patientStates', 'uses' => 'PatientStatesController@index', 'priority' => -1 ]);
                Route::get('/add', [ 'as' => 'patientStates.add', 'uses' => 'PatientStatesController@add' ]);
                Route::post('/add', [ 'as' => 'patientStates.add', 'uses' => 'PatientStatesController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'patientStates.show', 'uses' => 'PatientStatesController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'patientStates.edit', 'uses' => 'PatientStatesController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'patientStates.edit', 'uses' => 'PatientStatesController@edit' ]);
                //Route::get('/delete/{id}',['as' => 'patientStates.delete', 'uses' => 'PatientStatesController@delete']);
                Route::get('/active/{id}', [ 'as' => 'patientStates.active', 'uses' => 'PatientStatesController@active' ]);

            });

            /**
             * Routes of Categories
             */
            Route::group([ 'prefix' => 'categories' ], function () {

                Route::get('/', [ 'as' => 'categories', 'uses' => 'CategoriesController@index', 'groupBy' => 'administration', 'parent' => 'docent-file', 'submenu' => true ]);
                Route::get('/add/', [ 'as' => 'categories.add', 'uses' => 'CategoriesController@add' ]);
                Route::post('/add', [ 'as' => 'categories.add', 'uses' => 'CategoriesController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'categories.show', 'uses' => 'CategoriesController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'categories.edit', 'uses' => 'CategoriesController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'categories.edit', 'uses' => 'CategoriesController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'categories.delete', 'uses' => 'CategoriesController@delete']);
                Route::get('/active/{id}', [ 'as' => 'categories.active', 'uses' => 'CategoriesController@active' ]);

            });

            /**
             * Routes of SubCategories
             */
            Route::group([ 'prefix' => 'subcategories' ], function () {

                Route::get('/', [ 'as' => 'subcategories', 'uses' => 'SubCategoriesController@index', 'groupBy' => 'administration', 'parent' => 'docent-file', 'submenu' => true ]);
                Route::get('/add/', [ 'as' => 'subcategories.add', 'uses' => 'SubCategoriesController@add' ]);
                Route::post('/add', [ 'as' => 'subcategories.add', 'uses' => 'SubCategoriesController@add' ]);
                Route::get('/show/{id}', [ 'as' => 'subcategories.show', 'uses' => 'SubCategoriesController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'subcategories.edit', 'uses' => 'SubCategoriesController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'subcategories.edit', 'uses' => 'SubCategoriesController@edit' ]);
                //Route::get('/delete/{id}', ['as' => 'subcategories.delete', 'uses' => 'SubCategoriesController@delete']);
                Route::get('/active/{id}', [ 'as' => 'subcategories.active', 'uses' => 'SubCategoriesController@active' ]);

            });

            /**
             * Routes of Patients
             */
            Route::group([ 'prefix' => 'patients' ], function () {

                Route::get('/', [ 'as' => 'patients', 'uses' => 'PatientsController@index', 'priority' => 2 ]);
                Route::post('/', [ 'as' => 'patients', 'uses' => 'PatientsController@index' ]);
                Route::get('/add', [ 'as' => 'patients.add', 'uses' => 'PatientsController@add' ]);
                Route::post('/add', [ 'as' => 'patients.add', 'uses' => 'PatientsController@add' ]);
                Route::get('/show/{patient}', [ 'as' => 'patients.show', 'uses' => 'PatientsController@show' ]);
                Route::get('/edit/{patient}', [ 'as' => 'patients.edit', 'uses' => 'PatientsController@edit' ]);
                Route::post('/edit/{patient}', [ 'as' => 'patients.edit', 'uses' => 'PatientsController@edit' ]);        
                //Route::get('/delete/{patient}', ['as' => 'patients.delete', 'uses' => 'PatientsController@delete']);
                Route::get('/active/{patient}', [ 'as' => 'patients.active', 'uses' => 'PatientsController@active' ]);
                //Route::post('/search', ['as' => 'patients.search', 'uses' => 'PatientsController@search']);
                Route::get('/history/{patient}', [ 'as' => 'patients.history', 'uses' => 'PatientsController@history' ]);
                //match the patient with a patient of the legacy system
                Route::get('/match/{patient}', [ 'as' => 'patients.match', 'uses' => 'PatientsController@match' ]);
                Route::post('/match/{patient}', [ 'as' => 'patients.match', 'uses' => 'PatientsController@match' ]);
                Route::get('/{print}', [ 'as' => 'patients.print', 'uses' => 'PatientsController@index' ]);
                Route::post('/{print}', [ 'as' => 'patients.print', 'uses' => 'PatientsController@index' ]);
                //show a patient document from storage
                Route::get('/document/{document}', [ 'as' => 'patients.document', 'uses' => 'PatientsController@document' ]);
                //Delete image scan file
                Route::get('deletescanfile/{file_id}', [ 'as' => 'deletescanfile', 'uses' => 'PatientsController@deleteFileScan' ]);

            });

            /**
             * Routes of Legacy Patients
             */
            Route::group([ 'prefix' => 'legacypatients' ], function () {
                Route::get('/',                     [ 'as' => 'legacypatients',
                                                      'uses' => 'LegacyPatientsController@index',
                                                      'priority' => 1 ]);
                Route::post('/',                    [ 'as' => 'legacypatients',
                                                      'uses' => 'LegacyPatientsController@index' ]);
                Route::get('/history/{patient}',    [ 'as' => 'legacypatients.history',
                                                      'uses' => 'LegacyPatientsController@history' ]);
            });

            /**
             * Routes of Reception
             */
            Route::group([ 'prefix' => 'reception' ], function () {

                Route::get('/', [ 'as' => 'reception', 'uses' => 'ServiceRequestsController@index', 'priority' => 9 ]);
                Route::post('/', [ 'as' => 'reception', 'uses' => 'ServiceRequestsController@index' ]);
                Route::get('/add', [ 'as' => 'reception.add', 'uses' => 'ServiceRequestsController@add' ]);
                Route::post('/add', [ 'as' => 'reception.add', 'uses' => 'ServiceRequestsController@add' ]);
                Route::get('/add/{patient}', [ 'as' => 'reception.add.patient', 'uses' => 'ServiceRequestsController@add' ]);
                Route::get('/show/{serviceRequest}', [ 'as' => 'reception.show', 'uses' => 'ServiceRequestsController@show' ]);
                Route::get('/edit/{serviceRequest}', [ 'as' => 'reception.edit', 'uses' => 'ServiceRequestsController@edit' ]);
                Route::post('/edit/{serviceRequest}', [ 'as' => 'reception.edit', 'uses' => 'ServiceRequestsController@edit' ]);
                Route::get('/active/{serviceRequest}', [ 'as' => 'reception.active', 'uses' => 'ServiceRequestsController@active' ]);
                Route::get('/add/{patient}/{appointment}', [ 'as' => 'reception.add.patient.appointment', 'uses' => 'ServiceRequestsController@add' ]);
                Route::post('/{print}', [ 'as' => 'reception.print', 'uses' => 'ServiceRequestsController@index' ]);
                Route::get('/{print}', [ 'as' => 'reception.print', 'uses' => 'ServiceRequestsController@index' ]);
                //show a serviceRequest document
                Route::get('/document/{document}', [ 'as' => 'reception.document', 'uses' => 'ServiceRequestsController@document' ]);

            });

            /**
             * Routes of Appointments
             */
            Route::group([ 'prefix' => 'appointments' ], function () {

                Route::get('/', [ 'as' => 'appointments', 'uses' => 'AppointmentsController@index', 'priority' => 10 ]);
                Route::get('/add', [ 'as' => 'appointments.add', 'uses' => 'AppointmentsController@add' ]);
                Route::post('/add', [ 'as' => 'appointments.add', 'uses' => 'AppointmentsController@add' ]);
                Route::get('/add/{appointment}', [ 'as' => 'appointments.add', 'uses' => 'AppointmentsController@add' ]);
                Route::get('/show/{appointment}', [ 'as' => 'appointments.show', 'uses' => 'AppointmentsController@show' ]);
                Route::get('/edit/{appointment}', [ 'as' => 'appointments.edit', 'uses' => 'AppointmentsController@edit' ]);
                Route::post('/edit/{appointment}', [ 'as' => 'appointments.edit', 'uses' => 'AppointmentsController@edit' ]);
                Route::post('/delete/{id}', [ 'as' => 'appointments.delete', 'uses' => 'AppointmentsController@delete' ]);
                Route::post('/reject/{id}', [ 'as' => 'appointments.reject', 'uses' => 'AppointmentsController@reject' ]);

            });

            /**
             * Routes of Technicians
             */
            Route::group([ 'prefix' => 'technician' ], function () {

                Route::get('/', [ 'as' => 'technician', 'uses' => 'TechnicianController@index', 'priority' => 7 ]);
                Route::post('/', [ 'as' => 'technician', 'uses' => 'TechnicianController@index' ]);
                Route::get('/add', [ 'as' => 'technician.add', 'uses' => 'TechnicianController@add' ]);
                Route::post('/add', [ 'as' => 'technician.add', 'uses' => 'TechnicianController@add' ]);
                Route::get('/add/{technician}', [ 'as' => 'technician.add', 'uses' => 'TechnicianController@add' ]);
                Route::get('/show/{technician}', [ 'as' => 'technician.show', 'uses' => 'TechnicianController@show' ]);
                Route::get('/edit/{id}', [ 'as' => 'technician.edit', 'uses' => 'TechnicianController@edit' ])->middleware('orders');
                Route::post('/edit/{id}', [ 'as' => 'technician.edit', 'uses' => 'TechnicianController@edit' ])->middleware('orders');
                Route::get('/active/{technician}', [ 'as' => 'technician.active', 'uses' => 'TechnicianController@active' ]);
                Route::post('/rewrite/{requestedProcedure}/{equipment}', [ 'as' => 'technician.rewrite', 'uses' => 'TechnicianController@rewrite' ]);
                Route::post('/rewrite/{requestedProcedure}', [ 'as' => 'technician.rewrite', 'uses' => 'TechnicianController@rewrite' ]);
                //Route::get('/lock/{patient}', ['as' => 'technician.lock.patient', 'uses' => 'TechnicianController@lock'])->middleware('orders');
                Route::post('/lock/{patient}/{id}', [ 'as' => 'technician.lock.patient.id', 'uses' => 'TechnicianController@lock' ])->middleware('orders');
                Route::post('/active/{technician}/{status}', [ 'as' => 'technician.active.technician.status', 'uses' => 'TechnicianController@active' ]);
                Route::post('/suspend', [ 'as' => 'technician.suspend', 'uses' => 'TechnicianController@suspend' ]);
                Route::get('/printer/{requestedProcedure}/{patient}', [ 'as' => 'technician.printer.requestedProcedure.patient', 'uses' => 'TechnicianController@printer' ]);
                Route::post('/printer/{requestedProcedure}/{patient}', [ 'as' => 'technician.printer.requestedProcedure.patient', 'uses' => 'TechnicianController@printer' ]);
                Route::get('/{print}', [ 'as' => 'technician.print', 'uses' => 'TechnicianController@index' ]);

            });

            /**
             * Routes of Radiologists
             */
            Route::group([ 'prefix' => 'radiologist' ], function () {

                Route::get('/', ['as' => 'radiologist', 'uses' => 'RadiologistController@index', 'groupBy' => 'radiologist', 'priority' => 6 ]);
                // Custom
                Route::get('/OrdenesDictar', ['as' => 'radiologist.OrdenesDictar', 'uses' => 'RadiologistController@OrdenesDictar', 'groupBy' => 'radiologist', 'menu' => true ]);
                Route::get('/OrdenesAprobar', ['as' => 'radiologist.OrdenesAprobar', 'uses' => 'RadiologistController@OrdenesAprobar', 'groupBy' => 'radiologist', 'menu' => true ]);
                Route::get('/ToAddendum', ['as' => 'radiologist.ToAddendum', 'uses' => 'RadiologistController@ToAddendum', 'groupBy' => 'radiologist', 'menu' => true ]);
                Route::get('/OrdenesDictadas', ['as' => 'radiologist.OrdenesDictadas', 'uses' => 'RadiologistController@OrdenesDictadas', 'groupBy' => 'radiologist', 'menu' => true ]);
                // Custom End
                Route::get('/{status}', ['as' => 'radiologist.status', 'uses' => 'RadiologistController@index', 'groupBy' => 'radiologist']);
                Route::post('/{status}', ['as' => 'radiologist.status', 'uses' => 'RadiologistController@index']);
                Route::get('/add', ['as' => 'radiologist.add', 'uses' => 'RadiologistController@add']);
                Route::get('/add/{id}', ['as' => 'radiologist.add', 'uses' => 'RadiologistController@add']);
                Route::post('/add', ['as' => 'radiologist.add', 'uses' => 'RadiologistController@add']);
                Route::get('/show/{id}', ['as' => 'radiologist.show', 'uses' => 'RadiologistController@show']);
                Route::get('/edit/{id}', ['as' => 'radiologist.edit', 'uses' => 'RadiologistController@edit'])->middleware('orders');
                Route::post('/edit/{id}', ['as' => 'radiologist.edit', 'uses' => 'RadiologistController@edit'])->middleware('orders');
                Route::get('/edit/{id}/draft', ['as' => 'radiologist.edit.draft', 'uses' => 'RadiologistController@edit'])->middleware('orders');
                Route::post('/edit/{id}/draft', ['as' => 'radiologist.edit.draft', 'uses' => 'RadiologistController@edit'])->middleware('orders');
                //Route::get('/delete/{id}', ['as' => 'radiologist.delete', 'uses' => 'RadiologistController@delete']);
                Route::get('/active/{id}', [ 'as' => 'radiologist.active', 'uses' => 'RadiologistController@active' ]);
                Route::get('/lock/{id}', [ 'as' => 'radiologist.lock', 'uses' => 'RadiologistController@lock' ]);
                Route::post('/lock/{id}', [ 'as' => 'radiologist.lock', 'uses' => 'RadiologistController@lock' ]);
                Route::get('/printer/{status}', [ 'as' => 'radiologist.printer', 'uses' => 'RadiologistController@printer' ]);
                Route::post('/printer/{status}', [ 'as' => 'radiologist.printer', 'uses' => 'RadiologistController@printer' ]);
                Route::get('/approve/{id}', [ 'as' => 'radiologist.approve', 'uses' => 'RadiologistController@approve' ]);
                Route::post('/approve/{id}', [ 'as' => 'radiologist.approve', 'uses' => 'RadiologistController@approve' ]);
                Route::get('/revert/{id}', [ 'as' => 'radiologist.revert', 'uses' => 'RadiologistController@revert' ]);
                Route::post('/revert/{id}', [ 'as' => 'radiologist.revert', 'uses' => 'RadiologistController@revert' ]);
                Route::get('/addendum/{id}', ['as' => 'radiologist.addendum', 'uses' => 'RadiologistController@addendum'])->middleware('orders');
                Route::post('/addendum/{id}', ['as' => 'radiologist.addendum', 'uses' => 'RadiologistController@addendum']);
                Route::get('/pac/{id}', [ 'as' => 'radiologist.pac', 'uses' => 'RadiologistController@pac']);
                Route::get('/{status}/{print}', [ 'as' => 'radiologist.status.print', 'uses' => 'RadiologistController@index' ]);
                Route::post('/{status}/{print}', [ 'as' => 'radiologist.status.print', 'uses' => 'RadiologistController@index' ]);

            });

      Route::group([ 'prefix' => 'finalreport'], function() {

        Route::get('/pdf/{id}/{version}', ['as' => 'finalreport.pdf', 'uses' => 'FinalReportController@pdf', 'priority' => -1 ]);
        Route::get('/legacy/{orden}/', ['as' => 'finalreport.legacy', 'uses' => 'FinalReportController@legacy']);
        Route::get('/email/pdf/{id}/patient/{patient}', ['as' => 'finalreport.email', 'uses' => 'FinalReportController@email', 'priority' => -1]);
      });

      /**
      * Routes of Pre-Admission
      */
      Route::group([ 'prefix' => 'preadmission' ], function () {

                Route::get('/', [ 'as' => 'preadmission', 'uses' => 'PreAdmissionController@index', 'priority' => 8 ]);
                Route::post('/', [ 'as' => 'preadmission', 'uses' => 'PreAdmissionController@index' ]);
                Route::get('/edit/{requestedProcedure}', [ 'as' => 'preadmission.edit', 'uses' => 'PreAdmissionController@edit' ]);
                Route::post('/edit/{requestedProcedure}/{serviceRequest}', [ 'as' => 'preadmission.edit', 'uses' => 'PreAdmissionController@edit' ]);
                Route::get('/show/{requestedProcedure}', [ 'as' => 'preadmission.show', 'uses' => 'PreAdmissionController@show' ]);
                Route::post('/show/{requestedProcedure}', [ 'as' => 'preadmission.show', 'uses' => 'PreAdmissionController@show' ]);
                Route::post('/suspend', [ 'as' => 'preadmission.suspend', 'uses' => 'PreAdmissionController@suspend' ]);
                Route::get('/approve/{requestedProcedure}', [ 'as' => 'preadmission.approve', 'uses' => 'PreAdmissionController@approve' ]);
                Route::post('/approve/{requestedProcedure}', [ 'as' => 'preadmission.approve', 'uses' => 'PreAdmissionController@approve' ]);
                Route::post('/active/{requestedProcedure}', [ 'as' => 'preadmission.active', 'uses' => 'PreAdmissionController@active' ]);
                Route::get('/{print}', [ 'as' => 'preadmission.print', 'uses' => 'PreAdmissionController@index' ]);
                Route::post('/{print}', [ 'as' => 'preadmission.print', 'uses' => 'PreAdmissionController@index' ]);

            });

            /**
             * Routes of Search
             */
            Route::group([ 'prefix' => 'search' ], function () {

                Route::get('/', [ 'as' => 'search', 'uses' => 'SearchController@index', 'priority' => 3 ]);
                Route::post('/', [ 'as' => 'search', 'uses' => 'SearchController@index' ]);
                Route::get('/show/{requestedProcedure}', [ 'as' => 'search.show', 'uses' => 'SearchController@show' ]);
                Route::post('/show/{requestedProcedure}', [ 'as' => 'search.show', 'uses' => 'SearchController@show' ]);
                Route::get('/edit/{requestedProcedure}', [ 'as' => 'search.edit', 'uses' => 'SearchController@edit' ]);
                Route::post('/edit/{requestedProcedure}', [ 'as' => 'search.edit', 'uses' => 'SearchController@edit' ]);
                Route::get('/approve/{requestedProcedure}', [ 'as' => 'search.approve', 'uses' => 'SearchController@approve' ]);
                Route::post('/approve/{requestedProcedure}', [ 'as' => 'search.approve', 'uses' => 'SearchController@approve' ]);
                Route::post('/active/{requestedProcedure}', [ 'as' => 'search.active', 'uses' => 'SearchController@active' ]);
                Route::post('/printer', [ 'as' => 'search.printer', 'uses' => 'SearchController@printer' ]);

            });

            /**
             * Routes of Reports
             */
            Route::group([ 'prefix' => 'reports' ], function () {

                Route::get('/', [ 'as' => 'reports', 'uses' => 'ReportsController@index', 'priority' => 1 ]);
                Route::post('/', [ 'as' => 'reports', 'uses' => 'ReportsController@index' ]);
                Route::get('/add', [ 'as' => 'reports.add', 'uses' => 'ReportsController@add' ]);
                Route::post('/add', [ 'as' => 'reports.add', 'uses' => 'ReportsController@add' ]);
                Route::get('/edit/{id}', [ 'as' => 'reports.edit', 'uses' => 'ReportsController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'reports.edit', 'uses' => 'ReportsController@edit' ]);
                Route::get('/show/{report}', [ 'as' => 'reports.show', 'uses' => 'ReportsController@show' ]);
                Route::post('/show/{report}', [ 'as' => 'reports.show', 'uses' => 'ReportsController@show' ]);

            });

                        /**
             * Routes of Polls
             */

            Route::group([ 'prefix' => 'polls' ], function () {

                Route::get('/', [ 'as' => 'polls', 'uses' => 'PollsController@sendView', 'groupBy' => 'administration']);
                Route::post('/send', [ 'as' => 'polls.send', 'uses' => 'PollsController@sendPolls']);
            });

            /**
             * Routes of Transcriber
             */
            Route::group([ 'prefix' => 'transcriber' ], function () {

                Route::get('/', [ 'as' => 'transcriber', 'uses' => 'TranscriberController@index', 'priority' => 5 ]);
                Route::post('/', [ 'as' => 'transcriber', 'uses' => 'TranscriberController@index' ]);
                Route::get('/{print}', [ 'as' => 'transcriber.print', 'uses' => 'TranscriberController@index' ]);
                Route::get('/edit/{id}', [ 'as' => 'transcriber.edit', 'uses' => 'TranscriberController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'transcriber.edit', 'uses' => 'TranscriberController@edit' ]);

            });

            /**
             * Routes of Results
             */
            Route::group([ 'prefix' => 'results' ], function () {

                Route::get('/', [ 'as' => 'results', 'uses' => 'ResultsController@index', 'priority' => 4 ]);
                Route::post('/', [ 'as' => 'results', 'uses' => 'ResultsController@index' ]);
                Route::get('/{print}', [ 'as' => 'results.print', 'uses' => 'ResultsController@index' ]);
                Route::post('/{print}', [ 'as' => 'results.print', 'uses' => 'ResultsController@index' ]);
                Route::get('/edit/{id}', [ 'as' => 'results.edit', 'uses' => 'ResultsController@edit' ]);
                Route::post('/edit/{id}', [ 'as' => 'results.edit', 'uses' => 'ResultsController@edit' ]);

            });

        });

    });

});
